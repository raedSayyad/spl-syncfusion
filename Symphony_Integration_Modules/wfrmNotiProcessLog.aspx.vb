﻿Imports ClassLibraries
Imports Syncfusion.EJ.Export
Imports Syncfusion.XlsIO
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections

Public Class wfrmNotiProcessLog
    Inherits System.Web.UI.Page

    Dim _sqlObj As New SQLFunction

    Public Class Logs
        Public Sub New()
        End Sub

        Public Sub New(Type As String, Timestamp As String, Text As String)
            Me.Type = Type
            Me.Timestamp = Timestamp
            Me.Text = Text
        End Sub

        Public Property Type() As String
            Get
                Return m_ErrorType
            End Get
            Set(value As String)
                m_ErrorType = value
            End Set
        End Property
        Private m_ErrorType As String

        Public Property Timestamp() As String
            Get
                Return m_dt
            End Get
            Set(value As String)
                m_dt = value
            End Set
        End Property
        Private m_dt As String

        Public Property Text() As String
            Get
                Return m_text
            End Get
            Set(value As String)
                m_text = value
            End Set
        End Property
        Private m_text As String
    End Class

    Dim log As New List(Of Logs)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
                Dim dt As New DataTable
                Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
                Dim name As String = info.ToString
                Dim sqlScript As String = ""
                If dbType = "SQL" Then
                    sqlScript = "SELECT Top 0 *  FROM [Symphony_Integration].[dbo].[tbl_Notification_Process_Log]" 'File.ReadAllText(name + "SQL_Script\Notification\SQLConfig.sql")
                ElseIf dbType = "ORA" Then
                    sqlScript = "" 'File.ReadAllText(name + "SQL_Script\Notification\ORAConfig.sql")
                End If
                If sqlScript <> "" Then
                    dt = _sqlObj.fillDT(sqlScript, CommandType.Text, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                    For i As Integer = 0 To dt.Rows.Count - 1
                        log.Add(New Logs(dt.Rows(i).Item(0), dt.Rows(i).Item(1), dt.Rows(i).Item(2)))
                    Next
                    grdProcessLog.DataSource = log
                    grdProcessLog.DataBind()
                End If
            Else
                log = New List(Of Logs)
                Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
                Dim dt As New DataTable
                Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
                Dim name As String = info.ToString
                Dim sqlScript As String = ""
                If dbType = "SQL" Then
                    sqlScript = "SELECT *  FROM [Symphony_Integration].[dbo].[tbl_Notification_Process_Log] WHERE [DataTime] >='" & Session("dtFrom") & "' AND [DataTime]<='" & Session("dtTo") & "'" 'File.ReadAllText(name + "SQL_Script\Notification\SQLConfig.sql")
                ElseIf dbType = "ORA" Then
                    sqlScript = "" 'File.ReadAllText(name + "SQL_Script\Notification\ORAConfig.sql")
                End If
                If sqlScript <> "" Then
                    dt = _sqlObj.fillDT(sqlScript, CommandType.Text, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                    For i As Integer = 0 To dt.Rows.Count - 1
                        log.Add(New Logs(dt.Rows(i).Item(0), dt.Rows(i).Item(1), dt.Rows(i).Item(2)))
                    Next
                    grdProcessLog.DataSource = log
                    grdProcessLog.DataBind()
                End If
            End If
        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

    Private Sub btnEFilter_Click(sender As Object, e As EventArgs) Handles btnPFilter.Click
        Try
            log = New List(Of Logs) 
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim dt As New DataTable
            Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
            Dim name As String = info.ToString
            Dim sqlScript As String = ""
            If dbType = "SQL" Then
                sqlScript = "SELECT *  FROM [Symphony_Integration].[dbo].[tbl_Notification_Process_Log] WHERE [DataTime] >='" & hdfDtFm.Value & "' AND [DataTime]<='" & hdfDtTo.Value & "'" 'File.ReadAllText(name + "SQL_Script\Notification\SQLConfig.sql")
            ElseIf dbType = "ORA" Then
                sqlScript = "" 'File.ReadAllText(name + "SQL_Script\Notification\ORAConfig.sql")
            End If
            If sqlScript <> "" Then
                dt = _sqlObj.fillDT(sqlScript, CommandType.Text, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                For i As Integer = 0 To dt.Rows.Count - 1
                    log.Add(New Logs(dt.Rows(i).Item(0), dt.Rows(i).Item(1), dt.Rows(i).Item(2)))
                Next
                grdProcessLog.DataSource = log
                grdProcessLog.DataBind()
            End If
            Session("dtFrom") = hdfDtFm.Value
            Session("dtTo") = hdfDtTo.Value
        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

    Protected Sub FlatGrid_ServerExcelExporting(sender As Object, e As Syncfusion.JavaScript.Web.GridEventArgs)
        Try
            Dim exp As ExcelExport = New ExcelExport()
            exp.Export(grdProcessLog.Model, DirectCast(grdProcessLog.DataSource, IEnumerable), "ProcessLog.xlsx", ExcelVersion.Excel2010, True, True, "flat-lime")
            Label1.Text = "Excel"
        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

    Protected Sub FlatGrid_ServerWordExporting(sender As Object, e As Syncfusion.JavaScript.Web.GridEventArgs)
        Try
            Dim exp As WordExport = New WordExport()
            exp.Export(grdProcessLog.Model, DirectCast(grdProcessLog.DataSource, IEnumerable), "ProcessLog.docx", True, True, "flat-lime")
            Label1.Text = "Word"
        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

    Protected Sub FlatGrid_ServerPdfExporting(sender As Object, e As Syncfusion.JavaScript.Web.GridEventArgs)
        Try
            Dim exp As PdfExport = New PdfExport()
            exp.Export(grdProcessLog.Model, DirectCast(grdProcessLog.DataSource, IEnumerable), "ProcessLog.pdf", True, True, "flat-lime")
            Label1.Text = "PDF"
        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

End Class