﻿Imports ClassLibraries

Public Class wfrmContent
    Inherits System.Web.UI.Page

    Dim gotoURL As String = ""
    Public objUserInfo As New Users.UserInfo
    Public PageTitle As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim User_ID As String
            Dim Lang As String = Request.QueryString("Lang")
            If Not Session("UserInfo") Is Nothing Then
                objUserInfo = Session("UserInfo")
                User_ID = objUserInfo.User_ID
            Else
                If Lang = "Ara" Then
                    Response.Redirect("~/wfrmLogin.aspx?Lang=Ara&goto=/uhtbin/cgisirsi.exe/x/x/0/1/32/X")
                Else
                    Response.Redirect("~/wfrmLogin.aspx?goto=/uhtbin/cgisirsi.exe/x/x/0/1/32/X")
                End If
            End If
            Dim objOtherRes As New ewsOthers.IewsOthersClient
            Dim Terms As String = Request.QueryString("TermsID")
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
           
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            btnSubmit.Text = rc.GetString("_Continue", Culture_Used)
            Dim TermsID As String = ""
            If Terms = "Visit_Reservation" Or Terms = "Hall_Reservation" Then
                If Terms = "Visit_Reservation" Then
                    TermsID = "1"
                ElseIf Terms = "Hall_Reservation" Then
                    TermsID = "2"
                End If
                If Lang = "Ara" Then
                    gotoURL = "~/wfrmReserveUserDetails.aspx?TermsID=" + Terms + "&Mode=Add&Lang=Ara"
                Else
                    gotoURL = "~/wfrmReserveUserDetails.aspx?TermsID=" + Terms + "&Mode=Add"
                End If
            ElseIf Terms = "Usrugd" Then
                TermsID = "21"
                If Lang = "Ara" Then
                    gotoURL = "~/wfrmUserUpgrade.aspx?Lang=Ara"
                Else
                    gotoURL = "~/wfrmUserUpgrade.aspx"
                End If
                PageTitle = rc.GetString("UpgradeUser", Culture_Used)
                Dim ReqSent As String = rc.GetString("UpgradeReqSent", Culture_Used)
                Dim ReqApp As String = rc.GetString("UpgradeReqApp", Culture_Used)
                'User_ID = objUserInfo.User_ID
                Dim RegType As String = objUserInfo.RegType
                If RegType.Trim = "2" Then
                    Response.Redirect("~/wfrmResulte.aspx?TermsID=Usrugd&Source=" & PageTitle & "&Msg=" & ReqSent)
                ElseIf RegType.Trim = "3" Then
                    Response.Redirect("~/wfrmResulte.aspx?TermsID=Usrugd&Source=" & PageTitle & "&Msg=" & ReqApp)
                End If
            End If
            If Lang = "Ara" Then
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                lblTerms.Text = objOtherRes.GetContent(TermsID).ADESCRIPTION
            Else
                lblTerms.Text = objOtherRes.GetContent(TermsID).EDESCRIPTION
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Response.Redirect(gotoURL)
    End Sub
End Class