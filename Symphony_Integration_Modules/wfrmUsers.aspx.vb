﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmUsers
    Inherits System.Web.UI.Page

    Public Detail As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim objAllLibraryStaff As New List(Of Others.LibraryStaff)
            Dim objLibraryStaff As New Others.LibraryStaff

            For i As Integer = 0 To objEwsOtherClient.GetLibraryStaff().Count - 1
                objLibraryStaff = New Others.LibraryStaff
                objLibraryStaff = objEwsOtherClient.GetLibraryStaff()(i)
                objAllLibraryStaff.Add(objLibraryStaff)
            Next
            UserRenReqGrid.DataSource = objAllLibraryStaff
            UserRenReqGrid.DataBind()
            lblError.Text += " Page 1 "
            Detail = "Detail"
        Catch ex As Exception
            lblError.Text += ex.Message + " Page Error"
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim objLibraryStaff As New Others.LibraryStaff
            For i As Integer = 0 To objEwsOtherClient.GetLibraryStaff().Count - 1
                If i = CInt(selectedIndex) Then
                    objLibraryStaff = New Others.LibraryStaff
                    objLibraryStaff = objEwsOtherClient.GetLibraryStaff()(i)
                End If
            Next
            Session("LibraryStaffDetail") = objLibraryStaff
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmLibraryStaffDetail.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmLibraryStaffDetail.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class