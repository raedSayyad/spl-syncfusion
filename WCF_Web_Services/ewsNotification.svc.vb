﻿Imports ClassLibraries
' NOTE: You can use the "Rename" command on the context menu to change the class name "ewsNotification" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ewsNotification.svc or ewsNotification.svc.vb at the Solution Explorer and start debugging.
Public Class ewsNotification
    Implements IewsNotification

    Public Function GetConfig(ByVal Query As String) As DataSet Implements IewsNotification.GetConfig
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet(Query, ConfigurationManager.AppSettings("ESConnStr"))
        Return ds
    End Function

    Public Function exeNonQuery(ByVal Query As String) As String Implements IewsNotification.exeNonQuery
        Dim objSql As New SQLFunction
        Return objSql.exeNonQuery(Query, ConfigurationManager.AppSettings("ESConnStr"))
    End Function

End Class
