﻿Public Class Template

    Dim _sqlObj As New SQLFunction

    Public Function Create_Template(ProcessType As String, ProcessName As String, Ext As String, Template_Ar As String, Template_En As String, ByVal key As String, ByVal Lang As String, ByVal name As String, ByVal id As String, date_time_charged As String, ByVal date_time_discharged As String, ByVal date_time_due As String, ByVal title As String, _
   ByVal Email As String, ByVal Mobile As String, ByVal Library As String, ByVal itemid As String, alternative_id As String, date_privilege_granted As String, date_privilege_expires As String, Profile As String, Status As String, date_placed As String, date_expires As String, date_responded As String, req_status As String, req_type As String, _
   request_id As String, bill_reason As String, amount_billed As String, date_billed As String, payment_amount As String, payment_date As String, payment_type As String, ByVal Array_Tags() As String, _
   ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()

        Try
            Dim _ObjWindowsOperation As New WindowsOperation
            Dim Return_Var() As String = Nothing
            Dim FileName As String = ProcessName & "_" & key
            Dim FullPath As String = ""
            If System.IO.File.Exists(System.Windows.Forms.Application.StartupPath & "\Items_Processing\" & FileName & "." & Ext) = False Then
                FullPath = System.Windows.Forms.Application.StartupPath & "\Items_Processing\" & FileName & "." & Ext
            Else
                FileName = ProcessName & "_" & key & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second
                FullPath = System.Windows.Forms.Application.StartupPath & "\Items_Processing\" & FileName & "." & Ext
            End If

            Dim COunter As Integer = 0

            Dim COunterEng As Integer = 0

            Dim strLine As String = ""

            Dim strLineEng As String = ""

            Dim Array_NewLines() As String = Nothing

            Dim Array_NewLinesEng() As String = Nothing

            Dim Template As String = ""
            Dim Template_Org As String = ""

            Dim TemplateEng As String = ""
            Dim Template_OrgEng As String = ""

            If Lang.ToUpper = "ARABIC" Then
                Template = Template_Ar
                Template_Org = Template_Ar
            Else
                Template = Template_En
                Template_Org = Template_En
            End If

            TemplateEng = Template_En
            Template_OrgEng = Template_En

Second_Mobile_call:

            'Template = "Templates\" & ProcessType & "\" & Template
            'Dim objStreamReader As IO.StreamReader = New IO.StreamReader(System.Windows.Forms.Application.StartupPath & "\" & Template & "." & Ext)
            'Do While Not strLine Is Nothing

            '    strLine = objStreamReader.ReadLine
            '    If strLine <> "" Then
            '        ReDim Preserve Array_NewLines(COunter)
            '        Array_NewLines(COunter) = strLine
            '        COunter += 1
            '    End If

            'Loop
            'objStreamReader.Close()

            Dim dbType As String = "ORA"
            Dim dt As New DataTable
            dt = New DataTable

            Dim sqlScript As String = ""
            If dbType = "SQL" Then
                sqlScript = "SELECT [TempValue]  FROM [Symphony_Integration].[dbo].[tbl_Notification_Email_Temp] where [TempName]='" & Template_En & "'"
            ElseIf dbType = "ORA" Then
                sqlScript = "SELECT TempValue  FROM tbl_Notification_Email_Temp where TempName='" & Template_En & "'"
            End If
            If sqlScript <> "" Then
                dt = _sqlObj.fillDT(sqlScript, CommandType.Text, "Data Source=unic;User Id=ESERVICES;Password=SPL1234;")
                If dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        ReDim Preserve Array_NewLines(i)
                        Array_NewLines(i) = dt.Rows(i).Item(0)
                    Next
                End If
            End If

            If Array_NewLines IsNot Nothing Then
                For i As Integer = 0 To Array_NewLines.Length - 1
                    Dim tag As String = ""
                    Dim Tag_New As String = ""
                    For y As Integer = 0 To Array_Tags.Length - 1
                        tag = Array_Tags(y)
                        Tag_New = tag.Replace("||", "")

                        If Tag_New = "id" Then
                            Tag_New = id
                        End If
                        If Tag_New = "date_time_due" Then
                            Tag_New = date_time_due
                        End If
                        If Tag_New = "Email" Then
                            Tag_New = Email
                        End If
                        If Tag_New = "Mobile" Then
                            Tag_New = Mobile
                        End If
                        If Tag_New = "Library" Then
                            Tag_New = Library
                        End If
                        If Tag_New = "itemid" Then
                            Tag_New = itemid
                        End If
                        If Tag_New = "alternative_id" Then
                            Tag_New = alternative_id
                        End If
                        If Tag_New = "date_privilege_granted" Then
                            Tag_New = date_privilege_granted
                        End If
                        If Tag_New = "date_privilege_expires" Then
                            Tag_New = date_privilege_expires
                        End If
                        If Tag_New = "Profile" Then
                            Tag_New = Profile
                        End If
                        If Tag_New = "Status" Then
                            Tag_New = Status
                        End If
                        If Tag_New = "name" Then
                            Tag_New = name
                        End If
                        If Tag_New = "date_placed" Then
                            Tag_New = date_placed
                        End If
                        If Tag_New = "date_expires" Then
                            Tag_New = date_expires
                        End If
                        If Tag_New = "date_responded" Then
                            Tag_New = date_responded
                        End If
                        'If Tag_New = "REQ_Type" Then
                        '    Dim req_type_new As String = req_type
                        '    For u As Integer = 0 To Array_Requests.Length - 1
                        '        If Array_Requests(u).ToString.Split("|")(0) = req_type Then
                        '            If Lang.ToUpper = "ARABIC" Then
                        '                req_type_new = Array_Requests(u).ToString.Split("|")(2)
                        '            ElseIf Lang.ToUpper = "ENGLISH" Then
                        '                req_type_new = Array_Requests(u).ToString.Split("|")(1)
                        '            End If
                        '            If Return_Var IsNot Nothing Then
                        '                If Return_Var(1) IsNot Nothing And Return_Var(2) Is Nothing Then
                        '                    req_type_new = Array_Requests(u).ToString.Split("|")(1)
                        '                End If
                        '                If Return_Var(2) IsNot Nothing And Return_Var(1) Is Nothing Then
                        '                    req_type_new = Array_Requests(u).ToString.Split("|")(2)
                        '                End If
                        '            End If
                        '        End If
                        '    Next
                        '    Tag_New = req_type_new
                        'End If
                        If Tag_New = "REQ_Status" Then
                            Tag_New = req_status
                        End If
                        If Tag_New = "request_id" Then
                            Tag_New = request_id
                        End If
                        If Tag_New = "date_billed" Then
                            Tag_New = date_billed
                        End If
                        If Tag_New = "amount_billed" Then
                            Tag_New = amount_billed
                        End If
                        If Tag_New = "bill_reason" Then
                            Tag_New = bill_reason
                        End If
                        If Tag_New = "payment_amount" Then
                            Tag_New = payment_amount
                        End If
                        If Tag_New = "payment_date" Then
                            Tag_New = payment_date
                        End If
                        If Tag_New = "payment_type" Then
                            Tag_New = payment_type
                        End If
                        'If Tag_New = "approval" Then
                        '    Dim Approvals_new As String = ProcessName
                        '    Approvals_new = Approvals_new.Replace("_R", "")
                        '    For u As Integer = 0 To Array_Approvals.Length - 1
                        '        If Array_Approvals(u).ToString.Split("|")(0) = Approvals_new Then
                        '            If Lang.ToUpper = "ARABIC" Then
                        '                Approvals_new = Array_Approvals(u).ToString.Split("|")(3)
                        '            ElseIf Lang.ToUpper = "ENGLISH" Then
                        '                Approvals_new = Array_Approvals(u).ToString.Split("|")(2)
                        '            End If
                        '            If Return_Var IsNot Nothing Then
                        '                If Return_Var(1) IsNot Nothing And Return_Var(2) Is Nothing Then
                        '                    Approvals_new = Array_Approvals(u).ToString.Split("|")(2)
                        '                End If
                        '                If Return_Var(2) IsNot Nothing And Return_Var(1) Is Nothing Then
                        '                    Approvals_new = Array_Approvals(u).ToString.Split("|")(3)
                        '                End If
                        '            End If
                        '        End If
                        '    Next
                        '    Tag_New = Approvals_new
                        'End If
                        If Tag_New = "approval_r" Then
                            Tag_New = bill_reason
                        End If
                        If Tag_New = "name" Then
                            Tag_New = name
                        End If
                        If Tag_New = "name" Then
                            Tag_New = name
                        End If
                        If Tag_New = "title" Then
                            Tag_New = title
                            Tag_New = Tag_New.Split("|")(0)
                            Tag_New = Replace(Tag_New, "&", "")
                        End If
                        If Tag_New = "date_time_discharged" Then
                            Tag_New = date_time_discharged
                        End If
                        If Tag_New = "date_time_charged" Then
                            Tag_New = date_time_charged
                        End If
                        If Array_NewLines(i).Contains(tag) = True Then
                            Array_NewLines(i) = Array_NewLines(i).Replace(tag, Tag_New)
                        End If
                    Next
                    If ProcessType = "Email" Then
                        _ObjWindowsOperation.SaveTextToFile(FullPath, Array_NewLines(i))
                    End If
                Next
            End If

            '////////////////////////////////
            'TemplateEng = "Templates\" & ProcessType & "\" & TemplateEng
            'Dim objStreamReaderEng As IO.StreamReader = New IO.StreamReader(System.Windows.Forms.Application.StartupPath & "\" & TemplateEng & "." & Ext)
            'Do While Not strLineEng Is Nothing

            '    strLineEng = objStreamReaderEng.ReadLine
            '    If strLineEng <> "" Then
            '        ReDim Preserve Array_NewLinesEng(COunterEng)
            '        Array_NewLinesEng(COunterEng) = strLineEng
            '        COunterEng += 1
            '    End If

            'Loop
            'objStreamReaderEng.Close()

            If Array_NewLinesEng IsNot Nothing Then
                For i As Integer = 0 To Array_NewLinesEng.Length - 1
                    Dim tag As String = ""
                    Dim Tag_New As String = ""
                    For y As Integer = 0 To Array_Tags.Length - 1
                        tag = Array_Tags(y)
                        Tag_New = tag.Replace("||", "")

                        If Tag_New = "id" Then
                            Tag_New = id
                        End If
                        If Tag_New = "date_time_due" Then
                            Tag_New = date_time_due
                        End If
                        If Tag_New = "Email" Then
                            Tag_New = Email
                        End If
                        If Tag_New = "Mobile" Then
                            Tag_New = Mobile
                        End If
                        If Tag_New = "Library" Then
                            Tag_New = Library
                        End If
                        If Tag_New = "itemid" Then
                            Tag_New = itemid
                        End If
                        If Tag_New = "alternative_id" Then
                            Tag_New = alternative_id
                        End If
                        If Tag_New = "date_privilege_granted" Then
                            Tag_New = date_privilege_granted
                        End If
                        If Tag_New = "date_privilege_expires" Then
                            Tag_New = date_privilege_expires
                        End If
                        If Tag_New = "Profile" Then
                            Tag_New = Profile
                        End If
                        If Tag_New = "Status" Then
                            Tag_New = Status
                        End If
                        If Tag_New = "name" Then
                            Tag_New = name
                        End If
                        If Tag_New = "date_placed" Then
                            Tag_New = date_placed
                        End If
                        If Tag_New = "date_expires" Then
                            Tag_New = date_expires
                        End If
                        If Tag_New = "date_responded" Then
                            Tag_New = date_responded
                        End If
                        If Tag_New = "REQ_Type" Then
                            Dim req_type_new As String = req_type
                            For u As Integer = 0 To Array_Requests.Length - 1
                                If Array_Requests(u).ToString.Split("|")(0) = req_type Then
                                    'If Lang.ToUpper = "ARABIC" Then
                                    'req_type_new = Unicorn_Notification.Array_Requests(u).ToString.Split("|")(2)
                                    'ElseIf Lang.ToUpper = "ENGLISH" Then
                                    req_type_new = Array_Requests(u).ToString.Split("|")(1)
                                    'End If
                                    If Return_Var IsNot Nothing Then
                                        If Return_Var(1) IsNot Nothing And Return_Var(2) Is Nothing Then
                                            req_type_new = Array_Requests(u).ToString.Split("|")(1)
                                        End If
                                        If Return_Var(2) IsNot Nothing And Return_Var(1) Is Nothing Then
                                            req_type_new = Array_Requests(u).ToString.Split("|")(2)
                                        End If
                                    End If
                                End If
                            Next
                            Tag_New = req_type_new
                        End If
                        If Tag_New = "REQ_Status" Then
                            Tag_New = req_status
                        End If
                        If Tag_New = "request_id" Then
                            Tag_New = request_id
                        End If
                        If Tag_New = "date_billed" Then
                            Tag_New = date_billed
                        End If
                        If Tag_New = "amount_billed" Then
                            Tag_New = amount_billed
                        End If
                        If Tag_New = "bill_reason" Then
                            Tag_New = bill_reason
                        End If
                        If Tag_New = "payment_amount" Then
                            Tag_New = payment_amount
                        End If
                        If Tag_New = "payment_date" Then
                            Tag_New = payment_date
                        End If
                        If Tag_New = "payment_type" Then
                            Tag_New = payment_type
                        End If
                        If Tag_New = "approval" Then
                            Dim Approvals_new As String = ProcessName
                            Approvals_new = Approvals_new.Replace("_R", "")
                            For u As Integer = 0 To Array_Approvals.Length - 1
                                If Array_Approvals(u).ToString.Split("|")(0) = Approvals_new Then
                                    'If Lang.ToUpper = "ARABIC" Then
                                    '   Approvals_new = Unicorn_Notification.Array_Approvals(u).ToString.Split("|")(3)
                                    'ElseIf Lang.ToUpper = "ENGLISH" Then
                                    Approvals_new = Array_Approvals(u).ToString.Split("|")(2)
                                    'End If
                                    If Return_Var IsNot Nothing Then
                                        If Return_Var(1) IsNot Nothing And Return_Var(2) Is Nothing Then
                                            Approvals_new = Array_Approvals(u).ToString.Split("|")(2)
                                        End If
                                        If Return_Var(2) IsNot Nothing And Return_Var(1) Is Nothing Then
                                            Approvals_new = Array_Approvals(u).ToString.Split("|")(3)
                                        End If
                                    End If
                                End If
                            Next
                            Tag_New = Approvals_new
                        End If
                        If Tag_New = "approval_r" Then
                            Tag_New = bill_reason
                        End If
                        If Tag_New = "name" Then
                            Tag_New = name
                        End If
                        If Tag_New = "name" Then
                            Tag_New = name
                        End If
                        If Tag_New = "title" Then
                            Tag_New = title
                            Tag_New = Tag_New.Split("|")(0)
                            Tag_New = Replace(Tag_New, "&", "")
                        End If
                        If Tag_New = "date_time_discharged" Then
                            Tag_New = date_time_discharged
                        End If
                        If Tag_New = "date_time_charged" Then
                            Tag_New = date_time_charged
                        End If
                        If Array_NewLinesEng(i).Contains(tag) = True Then
                            Array_NewLinesEng(i) = Array_NewLinesEng(i).Replace(tag, Tag_New)
                        End If
                    Next
                    If ProcessType = "Email" Then
                        _ObjWindowsOperation.SaveTextToFile(FullPath, Array_NewLinesEng(i))
                    End If
                Next
            End If
            '///////////////////////////////
            If ProcessType = "Email" Then
                ReDim Preserve Return_Var(0)
                Return_Var(0) = FullPath
                Return Return_Var
            ElseIf ProcessType = "Mobile" Then
                Dim Mobile_Str As String = ""
                Dim Mobile_StrEng As String = ""
                For i As Integer = 0 To Array_NewLines.Length - 1
                    Mobile_Str &= Array_NewLines(i)
                Next

                For i As Integer = 0 To Array_NewLinesEng.Length - 1
                    Mobile_StrEng &= Array_NewLinesEng(i)
                Next

                If Return_Var Is Nothing Then
                    ReDim Preserve Return_Var(2)
                    Return_Var(0) = Mobile_Str
                End If
                If Return_Var(1) Is Nothing Then
                    If Return_Var(1) = "" Then
                        If Template_Org = Template_Ar Then
                            Return_Var(1) = Mobile_Str
                            Template = Template_En
                            Template_Org = Template_En
                        End If
                    End If
                End If
                If Return_Var(2) Is Nothing Then
                    If Return_Var(2) = "" Then
                        If Template_Org = Template_En Then
                            Return_Var(2) = Mobile_StrEng
                            Template = Template_Ar
                            Template_Org = Template_Ar
                        End If
                    End If
                End If
                If Return_Var(0) <> "" And Return_Var(1) <> "" And Return_Var(2) <> "" Then
                    Return Return_Var
                End If
                COunter = 0
                strLine = ""
                COunterEng = 0
                strLineEng = ""
                GoTo Second_Mobile_call
                Return Return_Var

                'Return Mobile_Str
            End If
        Catch Ex As Exception
            Return Nothing
        End Try
        Return Nothing
    End Function

End Class
