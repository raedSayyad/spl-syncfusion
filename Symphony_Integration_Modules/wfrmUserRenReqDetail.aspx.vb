﻿Imports ClassLibraries

Public Class wfrmUserRenReqDetail
    Inherits System.Web.UI.Page

    Public ddlCityInd As String = "0"
    Public ddlNatInd As String = "0"
    Public ddlTitleInd As String = "0"
    Public ddlLibraryInd As String = "0"
    Public ddlGenderInd As String = "0"
    Public ddlMemberTypeInd As String = "0"

    Public objUserRenRequest As New Users.UserRenRequest

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objUserRenRequest = Session("UserRenRequest")
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim UserID As String = objUserRenRequest.User_ID
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                ddlCity.EnableRTL = True
                ddlNat.EnableRTL = True
                ddlTitle.EnableRTL = True
            Else
                objSymAdminSDH.locale = "en"
            End If

            Dim objMemType As New List(Of MemType)()
            objMemType.Add(New MemType(0, "public_user"))
            objMemType.Add(New MemType(1, "family"))
            objMemType.Add(New MemType(2, "academic"))
            objMemType.Add(New MemType(3, "postgraduate_student"))
            objMemType.Add(New MemType(4, "undergraduate_student"))
            objMemType.Add(New MemType(5, "special_user"))
            objMemType.Add(New MemType(6, "Special_Need"))
            ddlMemType.DataSource = objMemType
            ddlMemType.DataValueField = "id"
            ddlMemType.DataTextField = "text"

            ddlMemType.DataBind()
            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            objlookupPolicyListReq.policyType = "CAT3"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLIST = New Others.DDLList
                objDDLIST.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLIST.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlist.Add(objDDLIST)
            Next
            ddlTitle.DataSource = objAllDDlist
            ddlTitle.DataValueField = "Value"
            ddlTitle.DataTextField = "Text"
            ddlTitle.DataBind()

            objlookupPolicyListReq.policyType = "LIBR"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTLib As New Others.DDLList
            Dim objAllDDlistLib As New List(Of Others.DDLList)

            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTLib = New Others.DDLList
                objDDLISTLib.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTLib.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistLib.Add(objDDLISTLib)
            Next
            ddlLibrary.DataSource = objAllDDlistLib
            ddlLibrary.DataValueField = "Value"
            ddlLibrary.DataTextField = "Text"
            ddlLibrary.DataBind()

            objlookupPolicyListReq.policyType = "CAT2"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTG As New Others.DDLList
            Dim objAllDDlistG As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTG = New Others.DDLList
                objDDLISTG.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTG.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistG.Add(objDDLISTG)
            Next
            ddlGender.DataSource = objAllDDlistG
            ddlGender.DataValueField = "Value"
            ddlGender.DataTextField = "Text"
            ddlGender.DataBind()

            objlookupPolicyListReq.policyType = "CAT4"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTN As New Others.DDLList
            Dim objAllDDlistN As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTN = New Others.DDLList
                objDDLISTN.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTN.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistN.Add(objDDLISTN)
            Next
            ddlNat.DataSource = objAllDDlistN
            ddlNat.DataValueField = "Value"
            ddlNat.DataTextField = "Text"
            ddlNat.DataBind()

            objlookupPolicyListReq.policyType = "CAT5"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTC As New Others.DDLList
            Dim objAllDDlistC As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTC = New Others.DDLList
                objDDLISTC.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTC.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistC.Add(objDDLISTC)
            Next
            ddlCity.DataSource = objAllDDlistC
            ddlCity.DataValueField = "Value"
            ddlCity.DataTextField = "Text"
            ddlCity.DataBind()

            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            If Not IsPostBack Then
                txtFullName.Text = objUserRenRequest.FullName
                Dim DOB As String = objUserRenRequest.DOB
                Dim DOBArr() As String = DOB.Split(" ")
                txtDOB.Text = DOBArr(0)
                txtCompany.Text = objUserRenRequest.Employer
                txtNatID.Text = objUserRenRequest.NationalID
                txtMobile.Text = objUserRenRequest.Mobile
                txtTele.Text = objUserRenRequest.HomePhone
                txtWorkPhone.Text = objUserRenRequest.WorkPhone
                txtExt.Text = objUserRenRequest.Ext
                txtFax.Text = objUserRenRequest.Fax
                txtPostBox.Text = objUserRenRequest.POBox
                txtWebSite.Text = objUserRenRequest.WebSite
                hprlnkPersonalImg.NavigateUrl = objUserRenRequest.PersonalPhoto
                hprlnkNatID1.NavigateUrl = objUserRenRequest.NationalIDPhoto1
                hprlnkNatID2.NavigateUrl = objUserRenRequest.NationalIDPhoto2
                hprlnkSNeedImg.NavigateUrl = objUserRenRequest.SNeeds
                ddlMemberTypeInd = objUserRenRequest.MemberType
                For c As Integer = 0 To objAllDDlistLib.Count - 1
                    If objAllDDlistLib(c).Value = objUserRenRequest.Library Then
                        ddlLibraryInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlistG.Count - 1
                    If objAllDDlistG(c).Value = objUserRenRequest.Gender Then
                        ddlGenderInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlist.Count - 1
                    If objAllDDlist(c).Value = objUserRenRequest.JobTitle Then
                        ddlTitleInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlistN.Count - 1
                    If objAllDDlistN(c).Value = objUserRenRequest.Nationality Then
                        ddlNatInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlistC.Count - 1
                    If objAllDDlistC(c).Value = objUserRenRequest.City Then
                        ddlCityInd = CStr(c)
                    End If
                Next
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            Dim objResponse_Msg As New Responce_Msg
            Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim DOB As String = txtDOB.Text.Trim
            Dim DOBArr() As String = DOB.Split("/")
            DOB = ""
            If DOBArr(0).Length = 1 Then
                DOB += "0" + DOBArr(0) + "/"
            Else
                DOB += DOBArr(0) + "/"
            End If
            If DOBArr(1).Length = 1 Then
                DOB += "0" + DOBArr(1) + "/"
            Else
                DOB += DOBArr(1) + "/"
            End If
            DOB += DOBArr(2)
            objResponse_Msg = objewsUserRegClient.LoadUserRenewal("1", "Ara", objUserRenRequest.User_ID, ddlLibrary.Value, txtFullName.Text, DOB, txtMobile.Text, _
                                                                  ddlCity.Value, ddlMemType.Value, ddlNat.Value, ddlTitle.Value, txtNatID.Text, _
                                                                  hprlnkSNeedImg.NavigateUrl, txtCompany.Text, ddlGender.Value, hprlnkNatID1.NavigateUrl, _
                                                                  hprlnkNatID2.NavigateUrl, hprlnkPersonalImg.NavigateUrl, txtTele.Text, txtWorkPhone.Text, _
                                                                  txtExt.Text, txtPostBox.Text, txtFax.Text, txtWebSite.Text)
            If objResponse_Msg.UserId <> "" Then
                Dim result As String = objewsUserRegClient.UpdateUserRenewal(objUserRenRequest.ID, "2", " ", "ADMIN", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                If result <> "1" Then
                    lblError.Text += result + " || "
                Else
                    lblError.Text = "Approved Successfuly"
                End If
            Else
                lblError.Text += objResponse_Msg.Msg
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmUserRenReq.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmUserRenReq.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Ren&Lang=Ara")
            Else
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Ren")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Class MemType
        Public Property text() As String
            Get
                Return m_text
            End Get
            Set(value As String)
                m_text = value
            End Set
        End Property
        Private m_text As String
        Public Property id() As Integer
            Get
                Return m_id
            End Get
            Set(value As Integer)
                m_id = value
            End Set
        End Property
        Private m_id As Integer
        Public Sub New(id As Integer, txt As String)
            Me.id = id
            Me.text = txt
        End Sub
    End Class


End Class