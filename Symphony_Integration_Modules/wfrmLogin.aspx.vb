﻿Imports ClassLibraries
Imports Symphony_Integration_Modules.wsSymAdmin

Public Class wfrmLogin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Write("Cookie Value " + Request.Cookies("LogCookies").Value)
        'Response.Write("Cookie Date " + CStr(Request.Cookies("LogCookies").Expires))
    End Sub

    Dim Obj_EncDec As New EncDec

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim objwsSymSecurityClient As New wsSymSecurity.SecurityEndpointClient
            Dim objAuthenticateUserRequest As New wsSymSecurity.AuthenticateUserRequest
            Dim objSymAdminSDH As New wsSymSecurity.SdHeader
            objAuthenticateUserRequest.login = TextBox1.Text
            objAuthenticateUserRequest.password = TextBox2.Text
            objSymAdminSDH.clientID = "DS_CLIENT"
            Dim AuthBool As Boolean = objwsSymSecurityClient.authenticateUser(objSymAdminSDH, objAuthenticateUserRequest)
            If TextBox1.Text.ToUpper = "ADMIN" Then
                If TextBox2.Text = ConfigurationManager.AppSettings("AdminPassword").ToString Then
                    Session("Login") = "ADMIN"
                    Response.Redirect("~/wfrmHome.aspx")
                Else
                    Label1.Text = "Wrong Username And/Or Password"
                End If
            ElseIf TextBox1.Text.ToUpper = "REGISTER" Then
                If TextBox2.Text = ConfigurationManager.AppSettings("RegisterPassword").ToString Then
                    Session("Login") = "REGISTER"
                    Response.Redirect("~/wfrmApproval.aspx")
                Else
                    Label1.Text = "Wrong Username And/Or Password"
                End If
            Else
                If AuthBool = True Then
                    Session("Login") = TextBox1.Text
                    Response.Redirect("~/wfrmApproval.aspx")
                Else
                    Label1.Text = "Wrong Username And/Or Password"
                End If
            End If
        Catch ex As Exception
            Label1.Text = ex.Message
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'Try
        '    Dim _srvLoginReq As New srvLogin.IwsLoginClient
        '    Dim _srvLoginRes As String = ""
        '    _srvLoginRes = _srvLoginReq.GetAuthKey(TextBox3.Text)
        '    If _srvLoginRes = hdfUserID.Value Then
        '        Session("Login") = _srvLoginRes
        '        Response.Redirect("~/wfrmHome.aspx")
        '        Label2.Text = "Success"
        '    Else
        '        Session("Login") = "0"
        '        Label2.Text = "False"
        '    End If
        'Catch ex As Exception
        '    Label2.Text = ex.Message
        'End Try
    End Sub
End Class