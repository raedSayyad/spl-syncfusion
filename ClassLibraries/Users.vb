﻿Public Class Users
    Public Class ReseveUserInfo
        Public User_ID As String
        Public FullName As String
        Public Title As String
        Public Company As String
        Public Nationality As String
        Public Mobile As String
        Public Telephone As String
        Public Fax As String
        Public PostBox As String
        Public Email As String
        Public City As String
        Public MemberShipType As String
    End Class

    Public Class UserExist
        Public ext As Boolean
        Public objException As CustomException
    End Class

    Public Class UserRegType
        Public Execute As Boolean
        Public objException As New CustomException
    End Class

    Public Class UserPin
        Public User_ID As String
        Public Pin As String
        Public objException As New CustomException
    End Class

    Public Class UserInfo
        Public User_ID As String
        Public Library As String
        Public Email As String
        Public FullName As String
        Public Profile As String
        Public DOB As String
        Public MemberType As String
        Public Nationality As String
        Public JobTitle As String
        Public NationalID As String
        Public SNeeds As String
        Public Employer As String
        Public City As String
        Public Gender As String
        Public NationalIDPhoto1 As String
        Public NationalIDPhoto2 As String
        Public PersonalPhoto As String
        Public HomePhone As String
        Public WorkPhone As String
        Public Ext As String
        Public Mobile As String
        Public POBox As String
        Public Fax As String
        Public Zip As String
        Public WebSite As String
        Public RegType As String
        Public ReqType As String
        Public ExpireDate As String
        Public objException As New CustomException
    End Class

    Public Class UserUpgRequest
        Public ID As String
        Public User_ID As String
        Public FullName As String
        Public DOB As String
        Public City As String
        Public Library As String
        Public MemberType As String
        Public Nationality As String
        Public JobTitle As String
        Public NationalID As String
        Public SNeeds As String
        Public Employer As String
        Public Gender As String
        Public NationalIDPhoto1 As String
        Public NationalIDPhoto2 As String
        Public PersonalPhoto As String
        Public HomePhone As String
        Public WorkPhone As String
        Public Ext As String
        Public Mobile As String
        Public POBox As String
        Public Fax As String
        Public WebSite As String
        Public Status As String
        Public RejectMsg As String
        Public AdminName As String
        Public DateCreate As String
        Public DateUpdate As String
        Public objException As CustomException
    End Class

    Public Class UserRenRequest
        Public ID As String
        Public User_ID As String
        Public FullName As String
        Public DOB As String
        Public City As String
        Public Library As String
        Public MemberType As String
        Public Nationality As String
        Public JobTitle As String
        Public NationalID As String
        Public SNeeds As String
        Public Employer As String
        Public Gender As String
        Public NationalIDPhoto1 As String
        Public NationalIDPhoto2 As String
        Public PersonalPhoto As String
        Public HomePhone As String
        Public WorkPhone As String
        Public Ext As String
        Public Mobile As String
        Public POBox As String
        Public Fax As String
        Public WebSite As String
        Public Status As String
        Public RejectMsg As String
        Public AdminName As String
        Public DateCreate As String
        Public DateUpdate As String
        Public objException As CustomException
    End Class

    Public Class UserCancelRequest
        Public ID As String
        Public User_ID As String
        Public Reason As String
        Public Status As String
        Public RejectMsg As String
        Public AdminName As String
        Public DateCreate As String
        Public DateUpdate As String
        Public objException As CustomException
    End Class
End Class
