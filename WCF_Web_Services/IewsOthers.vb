﻿Imports System.ServiceModel
Imports ClassLibraries

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IewsOthers" in both code and config file together.
<ServiceContract()>
Public Interface IewsOthers

    <OperationContract()>
    Sub InstLibraryBranch(ByVal LibararyBranchID As String, ByVal ATitle As String, ByVal ETitle As String, ByVal ADescription As String, ByVal EDescription As String, ByVal AAddress As String, ByVal EAddress As String, ByVal Telephone As String, ByVal URL As String, ByVal Email As String, ByVal Latitude As String, ByVal Longitude As String, ByVal WorkingHours As String, Optional Likes As Integer = 0, Optional Memebers As Integer = 0, Optional NumberOfWatchings As String = "")
    <OperationContract()>
    Sub UptLibraryBranch(ByVal LibararyBranchID As String, ByVal ATitle As String, ByVal ETitle As String, ByVal ADescription As String, ByVal EDescription As String, ByVal AAddress As String, ByVal EAddress As String, ByVal Telephone As String, ByVal URL As String, ByVal Email As String, ByVal Latitude As String, ByVal Longitude As String, ByVal WorkingHours As String, Optional Likes As Integer = 0, Optional Memebers As Integer = 0, Optional NumberOfWatchings As String = "")
    <OperationContract()>
    Function GetLibraryBranch(ByVal LibararyBranchID As String) As Others.LibraryBranch
    <OperationContract()>
    Function GetAllLibraryBranch() As List(Of Others.LibraryBranch)


    <OperationContract()>
    Sub InstNews(ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer)
    <OperationContract()>
    Sub UptNews(ByVal ID As String, ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer)
    <OperationContract()>
    Function GetNews(ByVal ID As String) As Others.News
    <OperationContract()>
    Function GetAllNews() As List(Of Others.News)

    <OperationContract()>
    Sub InstEvents(ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer)
    <OperationContract()>
    Sub UptEvents(ByVal ID As String, ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer)
    <OperationContract()>
    Function GetEvents(ByVal ID As String) As Others.Events
    <OperationContract()>
    Function GetAllEvents() As List(Of Others.Events)

    <OperationContract()>
    Sub InsContent(ByVal Content As String, ByVal ADescription As String, ByVal EDescription As String)
    <OperationContract()>
    Sub UptContent(ByVal ID As String, ByVal Content As String, ByVal ADescription As String, ByVal EDescription As String)
    <OperationContract()>
    Function GetContent(ByVal CONTENT As String) As Others.Content
    <OperationContract()>
    Function GetAllContent() As List(Of Others.Content)

    <OperationContract()>
    Function InsReserveVisit(ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, ByVal DateTo As String, ByVal Status As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String
    <OperationContract()>
    Function UptReserveVisit(ByVal ID As String, ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, ByVal DateTo As String, ByVal Status As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String
    <OperationContract()>
    Function GetReserveVisit(ByVal ID As String) As Others.ReserveVisit
    <OperationContract()>
    Function GetAllReserveVisitByUserID(ByVal User_ID As String) As List(Of Others.ReserveVisit)
    <OperationContract()>
    Function GetAllReserveVisit() As List(Of Others.ReserveVisit)
    <OperationContract()>
    Function ReserveVisitAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String) As Boolean
    <OperationContract()>
    Function UpdateVisitAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String, ByVal ID As String) As Boolean

    <OperationContract()>
    Function InsReserveHall(ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, ByVal DateTo As String, ByVal LectureType As String, ByVal AttendanceNo As String, ByVal Devices As String, ByVal InstructorName As String, ByVal InstructorPassport As String, ByVal InstructorPpNo As String, ByVal Status As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String
    <OperationContract()>
    Function UptReserveHall(ByVal ID As String, ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, ByVal DateTo As String, ByVal LectureType As String, ByVal AttendanceNo As String, ByVal Devices As String, ByVal InstructorName As String, ByVal InstructorPassport As String, ByVal InstructorPpNo As String, ByVal Status As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String
    <OperationContract()>
    Function GetReserveHall(ByVal ID As String) As Others.ReserveHall
    <OperationContract()>
    Function GetAllReserveHallByUserID(ByVal User_ID As String) As List(Of Others.ReserveHall)
    <OperationContract()>
    Function GetAllReserveHall() As List(Of Others.ReserveHall)
    <OperationContract()>
    Function ReserveHallAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String) As Boolean
    <OperationContract()>
    Function UpdateHallAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String, ByVal ID As String) As Boolean

    <OperationContract()>
    Function InsHallDevices(ByVal AraDesc As String, ByVal EngDesc As String) As String
    <OperationContract()>
    Function UptHallDevices(ByVal ID As String, ByVal AraDesc As String, ByVal EngDesc As String) As String
    <OperationContract()>
    Function GetAllHallDevices() As DataTable

    <OperationContract()>
    Function LookUp(ByVal CATNUM As String, ByVal Lang As String) As List(Of Others.DDLList)
    <OperationContract()>
    Function ImageUpload(ByVal User_ID As String, ByVal Type As String, ByVal Ext As String, ByVal Image_Byte As Byte()) As Responce_Msg
    <OperationContract()>
    Function imageUrlToBase64(ByVal URL As String) As String

    <OperationContract()>
    Function InstUserPayment(ByVal User_ID As String, ByVal TP_BranchId As String, ByVal TP_Merchant As String, ByVal TP_RefNo As String, _
                                  ByVal TP_Language As String, ByVal TP_ServiceInfo As String, ByVal TP_PayerName As String, _
                                  ByVal TP_ReturnURL As String, ByVal BillsNumber As String, ByVal BillsAmount As String, ByVal wpStatus As String, ByVal wsStatus As String, ByVal DateCreate As String, _
                                  ByVal DateUpdate As String) As String
    <OperationContract()>
    Function UptUserPaymentWP(ByVal ID As String, ByVal Status As String, ByVal DateUpdate As String) As String

    <OperationContract()>
    Function UptUserPaymentWS(ByVal ID As String, ByVal Status As String, ByVal DateUpdate As String) As String

    <OperationContract()>
    Function GetUserPayments(ByVal TP_RefNo As String) As DataSet

    <OperationContract()>
    Function GetUserBillReason(ByVal User_ID As String, ByVal Bill_Number As String) As String

    <OperationContract()>
    Function GetUserPaymentsRequests(ByVal User_ID As String) As DataSet

    <OperationContract()>
    Function InstUserPaymentRes(ByVal User_ID As String, ByVal DepAmount As String, ByVal TP_Amount As String, _
                                  ByVal TP_ExtraFees As String, ByVal TP_PaymentDate As String, ByVal TP_PayMethod As String, _
                                  ByVal TP_ReceiptNo As String, ByVal TP_RefNo As String, ByVal TP_ResultCode As String, ByVal TP_SecHash As String, ByVal DateCreate As String, _
                                  ByVal DateUpdate As String) As String

    <OperationContract()>
    Function InstUserTahseelResponse(ByVal User_ID As String, ByVal MemberType As String, ByVal ResultCode As String, ByVal ResultDescription As String, ByVal TahseelCardNumber As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String

    <OperationContract()>
    Function GetLibraryStaff() As List(Of Others.LibraryStaff)
   
End Interface
