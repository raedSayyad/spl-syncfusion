﻿
Public Class wfrmForgetPassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
            End If

            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            btnSubmit.Text = rc.GetString("Submit", Culture_Used)
            lblEmail.Text = rc.GetString("Email", Culture_Used)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try

            Dim objEwsUserReg As New ewsUserReg.IewsUserRegClient
            lblError.Text = objEwsUserReg.ForgetPassword(txtEmail.Text.Trim)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class