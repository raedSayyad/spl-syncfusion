﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmHallReservation.aspx.vb" Inherits="eServices.wfrmHallReservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }

        CBLcss {
            border: 0 !important;
        }

        input[type="checkbox"] + label:before,
        input[type="radio"] + label:before {
            right: -20px !important;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div>
                        <section>
                            <div class="<%=Row%> uniform 50%">
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblLibrary" runat="server" Text="Library"></asp:Label>
                                    <ej:DropDownList ID="ddlLibrary" runat="server" Height="35px" Width="100%" CssClass="CustomDDL"></ej:DropDownList>
                                    <asp:Label ID="lblValdDdl" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblPurpose" runat="server" Text="Purpose"></asp:Label>
                                    <asp:TextBox ID="txtPurpose" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:Label ID="lblValdTxtP" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblLecrType" runat="server" Text="Lecture Type"></asp:Label>
                                    <asp:TextBox ID="txtLecrType" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:Label ID="lblValdTxtL" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblDFrom" runat="server" Text="Date From"></asp:Label>&nbsp;&nbsp;
                                    <ej:DatePicker ID="dpFrom" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" ClientSideOnBeforeDateCreate="disableDate"></ej:DatePicker>
                                    <asp:Label ID="lblValdDpF" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblDTo" runat="server" Text="Date To"></asp:Label>&nbsp;&nbsp;
                                    <ej:DatePicker ID="dpTo" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" ClientSideOnBeforeDateCreate="disableDate"></ej:DatePicker>
                                    <asp:Label ID="lblValdDpT" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblAttnce" runat="server" Text="Attendance"></asp:Label>
                                    <asp:TextBox ID="txtAttnce" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:Label ID="lblValdAttnce" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblTFrom" runat="server" Text="Time From"></asp:Label>&nbsp;&nbsp;
                                    <ej:TimePicker ID="tpFrom" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" Interval="60" Value="08:00 AM">
                                        <DisableTimeRanges>
                                            <ej:DisableTimeRange StartTime="12:00 AM" EndTime="07:00 AM" />
                                            <ej:DisableTimeRange StartTime="05:00 PM" EndTime="11:00 PM" />
                                        </DisableTimeRanges>
                                    </ej:TimePicker>
                                    <asp:Label ID="lblValdTpF" runat="server" ForeColor="Red" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblTTo" runat="server" Text="Time To"></asp:Label>&nbsp;&nbsp;
                                    <ej:TimePicker ID="tpTo" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" Interval="60" Value="05:00 PM">
                                        <DisableTimeRanges>
                                            <ej:DisableTimeRange StartTime="12:00 AM" EndTime="08:00 AM" />
                                            <ej:DisableTimeRange StartTime="06:00 PM" EndTime="11:00 PM" />
                                        </DisableTimeRanges>
                                    </ej:TimePicker>
                                    <asp:Label ID="lblValdTpT" runat="server" ForeColor="Red" Text=""></asp:Label>
                                    <asp:Label ID="lblAva" runat="server" ForeColor="Green" Text=""></asp:Label>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblDevice" runat="server" Text="Devices"></asp:Label>
                                    <asp:CheckBoxList ID="cblDevices" runat="server" CssClass="e-listbox e-js e-ul e-box e-popup e-widget e-wrap e-scroller e-js CBLcss" Style="border: 0!important;">
                                        <asp:ListItem Text="Test1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Test2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Test3" Value="3"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </div>
                                <div class="12<%=u%>$">
                                    <asp:Label ID="lblInstructor" runat="server" Text="Instructors"></asp:Label>
                                    <asp:Label ID="lblValdInstr" runat="server" ForeColor="Red" Text=""></asp:Label>
                                    <br />
                                    <ej:Grid ID="InstructorsGrid" runat="server" AllowPaging="True" OnServerEditRow="EditEvents_ServerEditRow"
                                        OnServerAddRow="EditEvents_ServerAddRow" OnServerDeleteRow="EditEvents_ServerDeleteRow">
                                        <ClientSideEvents ActionComplete="complete" EndAdd="endAdd" EndDelete="endDelete" EndEdit="endEdit" />
                                        <Columns>
                                            <ej:Column Field="PassportNo" HeaderText="Passport Number" IsPrimaryKey="true" TextAlign="Left" Width="90"></ej:Column>
                                            <ej:Column Field="InstructorN" HeaderText="Instructor Name" TextAlign="Left" Width="90"></ej:Column>
                                            <ej:Column Field="PassportImg" HeaderText="Passport Image" Width="110" Template="#templateTest"></ej:Column>
                                        </Columns>
                                        <EditSettings AllowEditing="True" AllowAdding="True" AllowDeleting="True" EditMode="externalformtemplate" ExternalFormTemplateID="#template"></EditSettings>
                                        <ToolbarSettings ShowToolbar="True" ToolbarItems="add,edit,delete,update,cancel"></ToolbarSettings>
                                    </ej:Grid>
                                    <script id="templateTest" type="text/x-jsrender">
                                        <a href="{{: PassportImg }}" target="_blank">
                                            <img style="width: 75px; height: 70px" src="{{: PassportImg }}" alt="{{ : PassportNo }}" />
                                        </a>
                                    </script>
                                    <script type="text/template" id="template">
                                        <%--<b>Order Details</b>--%>
                                        <table cellspacing="10">
                                            <tr>
                                                <td style="text-align: right;"><%:Passport_Number%></td>
                                                <td style="text-align: center">
                                                    <input id="PassportNo" name="PassportNo" value="{{: PassportNo}}" class="e-field e-ejinputtext valid" style="text-align: right; width: 116px; height: 28px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;"><%:Instructor_Name%></td>
                                                <td style="text-align: center">
                                                    <input id="InstructorN" name="InstructorN" value="{{: InstructorN}}" class="e-field e-ejinputtext valid" style="width: 116px; height: 28px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;"><%:Passport_Image%></td>
                                                <td style="text-align: center">
                                                    <input id="PassportImg" name="PassportImg" style="visibility: hidden;" value="{{: PassportImg}}" />
                                                    <input id="PassportImgU" name="PassportImgU" type="file" class="e-field e-ejinputfile valid" onchange="readURL(this);" value="{{: PassportImg}}" />
                                                </td>
                                            </tr>
                                        </table>
                                    </script>
                                    <script type="text/javascript">
                                        function readURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();

                                                reader.onload = function (e) {
                                                    document.getElementById("PassportImg").value = e.target.result
                                                };

                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }
                                    </script>
                                    <script type="text/javascript">
                                        $(function () {
                                            //$("#sampleProperties").ejPropertiesPanel();
                                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                                        });
                                        function EndRequestHandler(e) {
                                            var gridObj = $('#<%= InstructorsGrid.ClientID%>').data("ejGrid");
                                            $('#<%= InstructorsGrid.ClientID%>').ejGrid("option", { "editSettings": { editMode: "externalformtemplate", externalFormTemplateID: "#template" } });
                                        }
                                        function endAdd(args) {
                                            $('#<%= InstructorsGrid.ClientID%>').ejWaitingPopup("show");
                                        }
                                        function endDelete(args) {
                                            $('#<%= InstructorsGrid.ClientID%>').ejWaitingPopup("show");
                                        }
                                        function endEdit(args) {
                                            $('#<%= InstructorsGrid.ClientID%>').ejWaitingPopup("show");
                                        }
                                        function complete(args) {
                                            if (args.requestType == "refresh" || args.requestType == "save") {
                                                $('#<%= InstructorsGrid.ClientID%>').ejWaitingPopup("hide");
                                            }
                                        }
                                    </script>
                                </div>
                                <div class="12<%=u%>$">
                                    <asp:Button ID="btnBack" runat="server" Text="Back" />
                                    <asp:Button ID="btnReserve" runat="server" Text="Reseve" />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible="false" />
                                </div>
                                <div class="12<%=u%>$">
                                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </section>
                        <script>
                            function disableDate(args) {
                                if (args.date.getDay() == 5 || args.date.getDay() == 6) {
                                    //Removes the current month/other month class and adds the e-disable class to disable the dates.
                                    $(args.element).addClass('e-hidedate e-disable').removeClass('current-month other-month');
                                }
                            }
                        </script>
                        <script type="text/javascript">
                            var target;
                            $(function () {
                                target = $('#<%=ddlLibrary.ClientID%>').data("ejDropDownList");
                                target.option({ selectedIndex: <%=ddlLibraryV%> });
                            });
                            $("#tpFrom").ejTimePicker({  value : "<%=tpFromV%>" });
                            $("#tpTo").ejTimePicker({  value : "<%=tpToV%>" });
                        </script>
                        <asp:HiddenField ID="hfHallID" runat="server" />
                        <asp:HiddenField ID="hdDateCreate" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
