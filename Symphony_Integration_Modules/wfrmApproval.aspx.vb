﻿Imports ClassLibraries

Public Class wfrmApproval
    Inherits System.Web.UI.Page

    Dim ObjSql As New SQLFunction
    Dim SqlConn As String = System.Configuration.ConfigurationManager.AppSettings("SymConnStr")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Accordion1.Visible = False
            Accordion2.Visible = False
            Accordion3.Visible = False
            Dim user As String = Session("Login")
            If user = "ADMIN" Then
                Panel1.Visible = True
                Panel2.Visible = True
                Panel3.Visible = True
                Accordion1.Visible = True
                Accordion2.Visible = True
                Accordion3.Visible = True
            ElseIf user = "REGISTER" Then
                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = True
                Accordion1.Visible = False
                Accordion2.Visible = False
                Accordion3.Visible = True
            Else
                Panel1.Visible = True
                Panel2.Visible = False
                Panel3.Visible = False
                Accordion1.Visible = True
                Accordion2.Visible = False
                Accordion3.Visible = False
            End If
            '    Dim order As New List(Of Orders)
            '    order = BindDataSource()
            '    EmployeesGrid.DataSource = order
            '    EmployeesGrid.DataBind()
            '    Accordion1.Items(0).HeaderSection.InnerText = "Upgrade Requests : 9"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Public Function BindDataSource() As List(Of Orders)
    '    Dim SqlScript As String = "SELECT *  FROM [Integration_Admin].[dbo].[UsersUpgrade] WHERE [Status] = '1' AND [IsDeleted] = '0'"
    '    Dim dt As New DataTable
    '    dt = ObjSql.fillDT(SqlScript, CommandType.Text, SqlConn)
    '    Dim order As New List(Of Orders)
    '    For i As Integer = 0 To dt.Rows.Count - 1
    '        order.Add(New Orders(dt.Rows(i).Item(0), dt.Rows(i).Item(1), dt.Rows(i).Item(2), dt.Rows(i).Item(3), dt.Rows(i).Item(4), dt.Rows(i).Item(5), dt.Rows(i).Item(6), dt.Rows(i).Item(7), _
    '                             dt.Rows(i).Item(8), dt.Rows(i).Item(9), dt.Rows(i).Item(10), dt.Rows(i).Item(11), dt.Rows(i).Item(12), dt.Rows(i).Item(13), dt.Rows(i).Item(14), dt.Rows(i).Item(15), _
    '                             dt.Rows(i).Item(16), dt.Rows(i).Item(17), dt.Rows(i).Item(18), dt.Rows(i).Item(19), dt.Rows(i).Item(20), dt.Rows(i).Item(21), dt.Rows(i).Item(22), dt.Rows(i).Item(23), _
    '                             dt.Rows(i).Item(24), dt.Rows(i).Item(25)))
    '    Next
    '    Return order
    'End Function

    'Public Class Orders

    '    Public Sub New()
    '    End Sub
    '    Public Sub New(ID As String, RequestType As String, UserID As String, Library As String, Profile As String, FullName As String, Nationality As String, Profession As String, NationalID As String, DOB As String, Employer As String, Gender As String, TelHome As String, TelOffice As String, Mobile As String, Emirates As String, Fax As String, PPhotoPath As String, NatID1 As String, NatID2 As String, SpecNeed As String, File_Path As String, Status As String, IsDeleted As String, CreateDate As String, UpdateDate As String)
    '        Me.ID = ID
    '        Me.RequestType = RequestType
    '        Me.UserID = UserID
    '        Me.Library = Library
    '        Me.Profile = Profile
    '        Me.FullName = FullName
    '        Me.Nationality = Nationality
    '        Me.Profession = Profession
    '        Me.NationalID = NationalID
    '        Me.DOB = DOB
    '        Me.Employer = Employer
    '        Me.Gender = Gender
    '        Me.TelHome = TelHome
    '        Me.TelOffice = TelOffice
    '        Me.Mobile = Mobile
    '        Me.Emirates = Emirates
    '        Me.Fax = Fax
    '        Me.PPhotoPath = PPhotoPath
    '        Me.NatID1 = NatID1
    '        Me.NatID2 = NatID2
    '        Me.SpecNeed = SpecNeed
    '        Me.File_Path = File_Path
    '        Me.Status = Status
    '        Me.IsDeleted = IsDeleted
    '        Me.CreateDate = CreateDate
    '        Me.UpdateDate = UpdateDate

    '    End Sub

    '    Private m_ID As String
    '    Private m_RequestType As String
    '    Private m_UserID As String
    '    Private m_Library As String
    '    Private m_Profile As String
    '    Private m_FullName As String
    '    Private m_Nationality As String
    '    Private m_Profession As String
    '    Private m_NationalID As String
    '    Private m_DOB As String
    '    Private m_Employer As String
    '    Private m_Gender As String
    '    Private m_TelHome As String
    '    Private m_TelOffice As String
    '    Private m_Mobile As String
    '    Private m_Emirates As String
    '    Private m_Fax As String
    '    Private m_PPhotoPath As String
    '    Private m_NatID1 As String
    '    Private m_NatID2 As String
    '    Private m_SpecNeed As String
    '    Private m_File_Path As String
    '    Private m_Status As String
    '    Private m_IsDeleted As String
    '    Private m_CreateDate As String
    '    Private m_UpdateDate As String


    '    Public Property ID() As String 
    '        Get
    '            Return m_ID
    '        End Get
    '        Set(value As String)
    '            m_ID = value
    '        End Set
    '    End Property

    '    Public Property RequestType() As String
    '        Get
    '            Return m_RequestType
    '        End Get
    '        Set(value As String)
    '            m_RequestType = value
    '        End Set
    '    End Property

    '    Public Property UserID() As String
    '        Get
    '            Return m_UserID
    '        End Get
    '        Set(value As String)
    '            m_UserID = value
    '        End Set
    '    End Property

    '    Public Property Library() As String
    '        Get
    '            Return m_Library
    '        End Get
    '        Set(value As String)
    '            m_Library = value
    '        End Set
    '    End Property

    '    Public Property Profile() As String
    '        Get
    '            Return m_Profile
    '        End Get
    '        Set(value As String)
    '            m_Profile = value
    '        End Set
    '    End Property

    '    Public Property FullName() As String
    '        Get
    '            Return m_FullName
    '        End Get
    '        Set(value As String)
    '            m_FullName = value
    '        End Set
    '    End Property

    '    Public Property Nationality() As String
    '        Get
    '            Return m_Nationality
    '        End Get
    '        Set(value As String)
    '            m_Nationality = value
    '        End Set
    '    End Property

    '    Public Property Profession() As String
    '        Get
    '            Return m_Profession
    '        End Get
    '        Set(value As String)
    '            m_Profession = value
    '        End Set
    '    End Property

    '    Public Property NationalID() As String
    '        Get
    '            Return m_NationalID
    '        End Get
    '        Set(value As String)
    '            m_NationalID = value
    '        End Set
    '    End Property


    '    Public Property DOB() As String
    '        Get
    '            Return m_DOB
    '        End Get
    '        Set(value As String)
    '            m_DOB = value
    '        End Set
    '    End Property

    '    Public Property Employer() As String
    '        Get
    '            Return m_Employer
    '        End Get
    '        Set(value As String)
    '            m_Employer = value
    '        End Set
    '    End Property

    '    Public Property Gender() As String
    '        Get
    '            Return m_Gender
    '        End Get
    '        Set(value As String)
    '            m_Gender = value
    '        End Set
    '    End Property

    '    Public Property TelHome() As String
    '        Get
    '            Return m_TelHome
    '        End Get
    '        Set(value As String)
    '            m_TelHome = value
    '        End Set
    '    End Property
    '    Public Property TelOffice() As String
    '        Get
    '            Return m_TelOffice
    '        End Get
    '        Set(value As String)
    '            m_TelOffice = value
    '        End Set
    '    End Property
    '    Public Property Mobile() As String
    '        Get
    '            Return m_Mobile
    '        End Get
    '        Set(value As String)
    '            m_Mobile = value
    '        End Set
    '    End Property
    '    Public Property Emirates() As String
    '        Get
    '            Return m_Emirates
    '        End Get
    '        Set(value As String)
    '            m_Emirates = value
    '        End Set
    '    End Property
    '    Public Property Fax() As String
    '        Get
    '            Return m_Fax
    '        End Get
    '        Set(value As String)
    '            m_Fax = value
    '        End Set
    '    End Property
    '    Public Property PPhotoPath() As String
    '        Get
    '            Return m_PPhotoPath
    '        End Get
    '        Set(value As String)
    '            m_PPhotoPath = value
    '        End Set
    '    End Property
    '    Public Property NatID1() As String
    '        Get
    '            Return m_NatID1
    '        End Get
    '        Set(value As String)
    '            m_NatID1 = value
    '        End Set
    '    End Property
    '    Public Property NatID2() As String
    '        Get
    '            Return m_NatID2
    '        End Get
    '        Set(value As String)
    '            m_NatID2 = value
    '        End Set
    '    End Property
    '    Public Property SpecNeed() As String
    '        Get
    '            Return m_SpecNeed
    '        End Get
    '        Set(value As String)
    '            m_SpecNeed = value
    '        End Set
    '    End Property
    '    Public Property File_Path() As String
    '        Get
    '            Return m_File_Path
    '        End Get
    '        Set(value As String)
    '            m_File_Path = value
    '        End Set
    '    End Property
    '    Public Property Status() As String
    '        Get
    '            Return m_Status
    '        End Get
    '        Set(value As String)
    '            m_Status = value
    '        End Set
    '    End Property
    '    Public Property IsDeleted() As String
    '        Get
    '            Return m_IsDeleted
    '        End Get
    '        Set(value As String)
    '            m_IsDeleted = value
    '        End Set
    '    End Property
    '    Public Property CreateDate() As String
    '        Get
    '            Return m_CreateDate
    '        End Get
    '        Set(value As String)
    '            m_CreateDate = value
    '        End Set
    '    End Property
    '    Public Property UpdateDate() As String
    '        Get
    '            Return m_UpdateDate
    '        End Get
    '        Set(value As String)
    '            m_UpdateDate = value
    '        End Set
    '    End Property
    'End Class

End Class