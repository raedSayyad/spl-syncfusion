﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmHallReq
    Inherits System.Web.UI.Page

    Public Detail As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim objAllHallRequest As New List(Of Others.ReserveHall)
            Dim objHAllRequest As New Others.ReserveHall
            lblError.Text += CStr(objEwsOtherClient.GetAllReserveHall().Count)
            For i As Integer = 0 To objEwsOtherClient.GetAllReserveHall().Count - 1
                objHAllRequest = New Others.ReserveHall
                objHAllRequest = objEwsOtherClient.GetAllReserveHall()(i)
                objAllHallRequest.Add(objHAllRequest)
            Next
            HallReqGrid.DataSource = objAllHallRequest
            HallReqGrid.DataBind()
            lblError.Text += " Page 1 "
            Detail = "Detail"
        Catch ex As Exception
            lblError.Text += ex.Message + " Page Error"
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim objHAllRequest As New Others.ReserveHall
            For i As Integer = 0 To objEwsOtherClient.GetAllReserveHall().Count - 1
                If i = CInt(selectedIndex) Then
                    objHAllRequest = New Others.ReserveHall
                    objHAllRequest = objEwsOtherClient.GetAllReserveHall()(i)
                End If
            Next
            Dim objUserInfo As New Users.UserInfo
            Dim objewUserReg As New ewsUserReg.IewsUserRegClient
            objUserInfo = objewUserReg.UserInfo(objHAllRequest.User_ID)
            Session("HallUserInfo") = objUserInfo
            Session("HallRequest") = objHAllRequest
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmHallReqDetail.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmHallReqDetail.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class