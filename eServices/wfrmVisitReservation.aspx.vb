﻿Imports System.Web.UI.WebControls
Imports ClassLibraries
Imports System.Configuration
'Imports System.Resources

Public Class wfrmVisitReservation

    Inherits System.Web.UI.Page
    Dim UserID As String '= "11523"
    Dim gotoURL As String = ""
    Dim Mode As String = ""
    Dim objVisitReservation As New Others.ReserveVisit
    Public objUserInfo As New Users.UserInfo
    Public ddlLibraryV As String
    Public tpFromV As String
    Public tpToV As String
    Public Row As String
    Public u As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Session("UserInfo") Is Nothing Then
                objUserInfo = Session("UserInfo")
                Dim User_ID As String = objUserInfo.User_ID
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                objUserInfo = objEwsUserClient.UserInfo(User_ID)
            Else
                Response.Redirect("~/wfrmLogin.aspx")
            End If
            UserID = objUserInfo.User_ID
            Mode = Request.QueryString("Mode")
            Dim Lang As String = Request.QueryString("Lang")
            Dim objOthersClient As New ewsOthers.IewsOthersClient
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                ddlLibrary.EnableRTL = True
                dpFrom.EnableRTL = True
                dpTo.EnableRTL = True
                tpFrom.EnableRTL = True
                tpTo.EnableRTL = True
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
                objSymAdminSDH.locale = "en"
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))

            lblLibrary.Text = rc.GetString("Library", Culture_Used)
            lblDFrom.Text = rc.GetString("DateFrom", Culture_Used)
            lblDTo.Text = rc.GetString("DateTo", Culture_Used)
            lblPurpose.Text = rc.GetString("Purpose", Culture_Used)
            lblTFrom.Text = rc.GetString("TimeFrom", Culture_Used)
            lblTTo.Text = rc.GetString("TimeTo", Culture_Used)
            btnBack.Text = rc.GetString("Back", Culture_Used)
            btnDelete.Text = rc.GetString("Delete", Culture_Used)
            btnReserve.Text = rc.GetString("Reserve", Culture_Used)
            dpFrom.WatermarkText = rc.GetString("DatePickerWaterMark", Culture_Used)
            dpTo.WatermarkText = rc.GetString("DatePickerWaterMark", Culture_Used)
            objlookupPolicyListReq.policyType = "LIBR"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)

            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLIST = New Others.DDLList
                objDDLIST.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLIST.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlist.Add(objDDLIST)
            Next
            ddlLibrary.DataSource = objAllDDlist ' objOthersClient.LookUp("LIBR", Lang)
            ddlLibrary.DataValueField = "Value"
            ddlLibrary.DataTextField = "Text"
            ddlLibrary.DataBind()

            If Mode = "Add" Then
                Dim stDate As DateTime = Now
                stDate = stDate.AddDays(1)
                Dim fnDate As DateTime = Now
                fnDate = fnDate.AddMonths(6)
                dpFrom.MinDate = stDate
                dpFrom.MaxDate = fnDate
                dpFrom.StartDay = 0

                fnDate = fnDate.AddDays(14)
                dpTo.MinDate = stDate
                dpTo.MaxDate = fnDate
                dpTo.StartDay = 0
            End If
            Dim asc As New AsyncPostBackTrigger
            asc.ControlID = dpFrom.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = dpTo.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = tpFrom.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = tpTo.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = ddlLibrary.UniqueID
            asc.EventName = "ValueSelect"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = txtPurpose.UniqueID
            asc.EventName = "TextChanged"
            UpdatePanel1.Triggers.Add(asc)

            If Lang = "Ara" Then
                gotoURL = "~/wfrmResulte.aspx?Source=" & rc.GetString("ReserveVisit", Culture_Used) & "&Msg=&" & rc.GetString("ReserVisitSucess", Culture_Used) & "TermsID=1&Lang=Ara"
            Else
                gotoURL = "~/wfrmResulte.aspx?Source=" & rc.GetString("ReserveVisit", Culture_Used) & "&Msg=" & rc.GetString("ReserVisitSucess", Culture_Used) & "&TermsID=1"
            End If
            If Not IsPostBack Then
                'Dim LibraryList() As Others.DDLList
                'LibraryList = objAllDDlist 'objOthersClient.LookUp("LIBR", Lang)
                If Mode = "Edit" Then
                    objVisitReservation = Session("UserVisitReservation")
                    For c As Integer = 0 To objAllDDlist.Count - 1
                        If objAllDDlist(c).Value = objVisitReservation.Library Then
                            ddlLibraryV = CStr(c)
                        End If
                    Next
                    hfVisitID.Value = CStr(objVisitReservation.ID)
                    dpFrom.Value = objVisitReservation.DateFrom
                    dpTo.Value = objVisitReservation.DateTo
                    tpFromV = objVisitReservation.DateFrom.Date.ToString("hh:mm tt")
                    tpToV = objVisitReservation.DateTo.Date.ToString("hh:mm tt")
                    txtPurpose.Text = objVisitReservation.ReservPurpose
                    hdDateCreate.Value = CStr(objVisitReservation.DateCreate)
                    btnReserve.Text = rc.GetString("Update", Culture_Used)
                    btnDelete.Visible = True
                    objVisitReservation = Session("UserVisitReservation")
                    If objVisitReservation.Status = "2" Or objVisitReservation.Status = "3" Or objVisitReservation.Status = "4" Or DateTime.Compare(Now, objVisitReservation.DateFrom) > 0 Then
                        btnDelete.Visible = False
                        btnReserve.Visible = False
                        dpFrom.Enabled = False
                        dpTo.Enabled = False
                        tpFrom.Enabled = False
                        tpTo.Enabled = False
                        ddlLibrary.Enabled = False
                        txtPurpose.ReadOnly = True
                    End If
                Else
                    btnBack.Text = rc.GetString("Cancel", Culture_Used)
                    tpFromV = "08:00 AM"
                    tpToV = "05:00 PM"
                End If
            End If

        Catch ex As Exception
            lblError.Text = ex.Message + " || " + ex.StackTrace 'Trace.GetFrame(0).GetFileLineNumber.ToString
        End Try

    End Sub

    Private Sub dpFrom_Select(sender As Object, e As Syncfusion.JavaScript.Web.DatePickerSelectEventArgs) Handles dpFrom.Select
        Try
            lblValdDpF.Text = ""
            Dim dt As DateTime = dpFrom.Value
            dpTo.MinDate = New DateTime(dt.Year, dt.Month, dt.Day)
            dpTo.Value = New DateTime(dt.Year, dt.Month, dt.Day)
            If Not ddlLibrary.Value Is Nothing Then
                If Not dpFrom.Value Is Nothing Then
                    If Not dpTo.Value Is Nothing Then
                        If Not tpFrom.Value Is Nothing Then
                            If Not tpTo.Value Is Nothing Then
                                Dim Ava As Boolean = CheckAva()
                                Dim Lang As String = Request.QueryString("Lang")
                                Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                                If Lang = "Ara" Then
                                    Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                                End If
                                Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                                If Ava = True Then
                                    lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                    lblAva.ForeColor = Drawing.Color.Red
                                Else
                                    lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                    lblAva.ForeColor = Drawing.Color.Green
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub tpFrom_Select(sender As Object, e As Syncfusion.JavaScript.Web.TimePickerEventArgs) Handles tpFrom.Select
        Try
            lblValdTpF.Text = ""
            Dim dstr As New List(Of Syncfusion.JavaScript.Models.DisableTimeRange)
            Dim item As New Syncfusion.JavaScript.Models.DisableTimeRange
            item.StartTime = "12:00 AM"
            item.EndTime = tpFrom.Value
            dstr.Add(item)
            item = New Syncfusion.JavaScript.Models.DisableTimeRange
            item.StartTime = "06:00 PM"
            item.EndTime = "11:00 PM"
            dstr.Add(item)
            tpTo.DisableTimeRanges = dstr

            If Not ddlLibrary.Value Is Nothing Then
                If Not dpFrom.Value Is Nothing Then
                    If Not dpTo.Value Is Nothing Then
                        If Not tpFrom.Value Is Nothing Then
                            If Not tpTo.Value Is Nothing Then
                                Dim Ava As Boolean = CheckAva()
                                Dim Lang As String = Request.QueryString("Lang")
                                Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                                If Lang = "Ara" Then
                                    Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                                End If
                                Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                                If Ava = True Then
                                    lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                    lblAva.ForeColor = Drawing.Color.Red
                                Else
                                    lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                    lblAva.ForeColor = Drawing.Color.Green
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub dpTo_Select(sender As Object, e As Syncfusion.JavaScript.Web.DatePickerSelectEventArgs) Handles dpTo.Select
        If Not ddlLibrary.Value Is Nothing Then
            If Not dpFrom.Value Is Nothing Then
                If Not dpTo.Value Is Nothing Then
                    lblValdDpT.Text = ""
                    If Not tpFrom.Value Is Nothing Then
                        If Not tpTo.Value Is Nothing Then
                            Dim Ava As Boolean = CheckAva()
                            Dim Lang As String = Request.QueryString("Lang")
                            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                            If Lang = "Ara" Then
                                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                            End If
                            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                            If Ava = True Then
                                lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                lblAva.ForeColor = Drawing.Color.Red
                            Else
                                lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                lblAva.ForeColor = Drawing.Color.Green
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub tpTo_Select(sender As Object, e As Syncfusion.JavaScript.Web.TimePickerEventArgs) Handles tpTo.Select
        If Not ddlLibrary.Value Is Nothing Then
            If Not dpFrom.Value Is Nothing Then
                If Not dpTo.Value Is Nothing Then
                    If Not tpFrom.Value Is Nothing Then
                        If Not tpTo.Value Is Nothing Then
                            lblValdTpT.Text = ""
                            Dim Ava As Boolean = CheckAva()
                            Dim Lang As String = Request.QueryString("Lang")
                            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                            If Lang = "Ara" Then
                                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                            End If
                            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                            If Ava = True Then
                                lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                lblAva.ForeColor = Drawing.Color.Red
                            Else
                                lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                lblAva.ForeColor = Drawing.Color.Green
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnReserve_Click(sender As Object, e As EventArgs) Handles btnReserve.Click
        Try
            Dim ValdBool As Boolean = False
            If Not ddlLibrary.Value Is Nothing Then
                lblValdDdl.Text = ""
            Else
                lblValdDdl.Text = "*"
                ValdBool = True
            End If
            If Not dpFrom.Value Is Nothing Then
                lblValdDpF.Text = ""
            Else
                lblValdDpF.Text = "*"
                ValdBool = True
            End If
            If Not dpTo.Value Is Nothing Then
                lblValdDpT.Text = ""
            Else
                lblValdDpT.Text = "*"
                ValdBool = True
            End If
            If Not tpFrom.Value Is Nothing Then
                lblValdTpF.Text = ""
            Else
                lblValdTpF.Text = "*"
                ValdBool = True
            End If
            If Not tpTo.Value Is Nothing Then
                lblValdTpT.Text = ""
            Else
                lblValdTpT.Text = "*"
                ValdBool = True
            End If

            If txtPurpose.Text <> "" Then
                lblValdTxt.Text = ""
            Else
                lblValdTxt.Text = "*"
                ValdBool = True
            End If

            If ValdBool = True Then
                Exit Sub
            End If



            Dim tsFrom As String
            Dim tsFromArr1() As String = dpFrom.Value.ToString.Split(" ")
            Dim tsFromArr2() As String = tsFromArr1(0).Split("/")
            If tsFromArr2(1).Length = 1 Then
                tsFromArr2(1) = "0" + tsFromArr2(1)
            End If
            If tsFromArr2(0).Length = 1 Then
                tsFromArr2(0) = "0" + tsFromArr2(0)
            End If
            Dim tsFrom3Arr() As String = tpFrom.Value.ToString.Split(":")
            If tsFrom3Arr(0).Length = 1 Then
                tsFrom3Arr(0) = "0" + tsFrom3Arr(0)
            End If

            tsFrom = tsFromArr2(0) + "/" + tsFromArr2(1) + "/" + tsFromArr2(2) + " " + tsFrom3Arr(0) + ":" + tsFrom3Arr(1) '+ ":" + tsFrom3Arr(2)
            lblError.Text += tsFrom + "|"
            Dim dtFrom As DateTime = DateTime.ParseExact(tsFrom, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InstalledUICulture) 'New DateTime(dpFrom.Value.Value.Year, dpFrom.Value.Value.Month, dpFrom.Value.Value.Day, CInt(tsFrom3Arr(0)), CInt(tsFrom3Arr(1)), CInt(tsFrom3Arr(2)), 0)

            Dim tsTo As String
            Dim tsToArr1() As String = dpTo.Value.ToString.Split(" ")
            Dim tsToArr2() As String = tsToArr1(0).Split("/")
            If tsToArr2(1).Length = 1 Then
                tsToArr2(1) = "0" + tsToArr2(1)
            End If
            If tsToArr2(0).Length = 1 Then
                tsToArr2(0) = "0" + tsToArr2(0)
            End If
            Dim tsToArr3() As String = tpTo.Value.ToString.Split(":")
            If tsToArr3(0).Length = 1 Then
                tsToArr3(0) = "0" + tsToArr3(0)
            End If

            tsTo = tsToArr2(0) + "/" + tsToArr2(1) + "/" + tsToArr2(2) + " " + tsToArr3(0) + ":" + tsToArr3(1) '+ ":" + tsToArr3(2)
            lblError.Text += tsTo + "|"
            Dim dtTo As DateTime = DateTime.ParseExact(tsTo, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InstalledUICulture) ' New DateTime(dpTo.Value.Value.Year, dpTo.Value.Value.Month, dpTo.Value.Value.Day, CInt(tsToArr3(0)), CInt(tsToArr3(1)), CInt(tsToArr3(2)), 0)

            If Not ddlLibrary.Value Is Nothing Then
                If Not dpFrom.Value Is Nothing Then
                    If Not dpTo.Value Is Nothing Then
                        If Not tpFrom.Value Is Nothing Then
                            If Not tpTo.Value Is Nothing Then
                                Dim Ava As Boolean = CheckAva()
                                If Ava = True Then
                                    Dim Lang As String = Request.QueryString("Lang")
                                    Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                                    If Lang = "Ara" Then
                                        Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                                    End If
                                    Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                                    lblError.Text += rc.GetString("NotAvailableMsg", Culture_Used) '"The selected Date or Time Not Available, Please select another Date or Time."
                                Else
                                    Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
                                    Dim result As String
                                    If Mode = "Add" Then
                                        Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
                                        objewsUserRegClient.InsUsersOPT(objUserInfo.User_ID, "0", "0", "0", "2", " ", "10", objUserInfo.User_ID, Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                        result = objEwsOtherClient.InsReserveVisit(UserID, ddlLibrary.Value, txtPurpose.Text, dtFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), dtTo.ToString("dd/MM/yyyy:hh:mm:sstt"), "0", "", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                    Else
                                        result = objEwsOtherClient.UptReserveVisit(hfVisitID.Value, UserID, ddlLibrary.Value, txtPurpose.Text, dtFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), dtTo.ToString("dd/MM/yyyy:hh:mm:sstt"), "1", "", CDate(hdDateCreate.Value).ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                    End If
                                    If result = "1" Then
                                        Response.Redirect(gotoURL)
                                    Else
                                        lblError.Text += result
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            lblError.Text += ex.Message + " || " + ex.StackTrace
        End Try

    End Sub

    Private Sub ddlLibrary_ValueSelect(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs) Handles ddlLibrary.ValueSelect
        If Not ddlLibrary.Value Is Nothing Then
            lblValdDdl.Text = ""
            If Not dpFrom.Value Is Nothing Then
                If Not dpTo.Value Is Nothing Then
                    If Not tpFrom.Value Is Nothing Then
                        If Not tpTo.Value Is Nothing Then
                            Dim Ava As Boolean = CheckAva()
                            Dim Lang As String = Request.QueryString("Lang")
                            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                            If Lang = "Ara" Then
                                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                            End If
                            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                            If Ava = True Then
                                lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                lblAva.ForeColor = Drawing.Color.Red
                            Else
                                lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                lblAva.ForeColor = Drawing.Color.Green
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub txtPurpose_TextChanged(sender As Object, e As EventArgs) Handles txtPurpose.TextChanged
        lblValdTxt.Text = ""
    End Sub

    Function CheckAva() As Boolean
        Dim tsFrom As String
        Dim tsFromArr1() As String = dpFrom.Value.ToString.Split(" ")
        Dim tsFromArr2() As String = tsFromArr1(0).Split("/")
        If tsFromArr2(1).Length = 1 Then
            tsFromArr2(1) = "0" + tsFromArr2(1)
        End If
        If tsFromArr2(0).Length = 1 Then
            tsFromArr2(0) = "0" + tsFromArr2(0)
        End If
        Dim tsFrom3Arr() As String = tpFrom.Value.ToString.Replace(" ", ":00").Split(":")
        If tsFrom3Arr(0).Length = 1 Then
            tsFrom3Arr(0) = "0" + tsFrom3Arr(0)
        End If
        tsFrom = tsFromArr2(1) + "/" + tsFromArr2(0) + "/" + tsFromArr2(2) + ":" + tsFrom3Arr(0) + ":" + tsFrom3Arr(1) + ":" + tsFrom3Arr(2)
        Dim tsTo As String
        Dim tsToArr1() As String = dpTo.Value.ToString.Split(" ")
        Dim tsToArr2() As String = tsToArr1(0).Split("/")
        If tsToArr2(1).Length = 1 Then
            tsToArr2(1) = "0" + tsFromArr2(1)
        End If
        If tsToArr2(0).Length = 1 Then
            tsToArr2(0) = "0" + tsToArr2(0)
        End If
        Dim tsToArr3() As String = tpTo.Value.ToString.Replace(" ", ":00").Split(":")
        If tsToArr3(0).Length = 1 Then
            tsToArr3(0) = "0" + tsToArr3(0)
        End If
        tsTo = tsToArr2(1) + "/" + tsToArr2(0) + "/" + tsToArr2(2) + ":" + tsToArr3(0) + ":" + tsToArr3(1) + ":" + tsToArr3(2)
        Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
        Dim Ava As Boolean
        If Mode = "Add" Then
            Ava = objEwsOtherClient.ReserveVisitAva(tsFrom, tsTo, ddlLibrary.Value)
        ElseIf Mode = "Edit" Then
            Ava = objEwsOtherClient.UpdateVisitAva(tsFrom, tsTo, ddlLibrary.Value, hfVisitID.Value)
        End If
        Return Ava
    End Function

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))

            If btnBack.Text = rc.GetString("Back", Culture_Used) Then
                If Lang = "Ara" Then
                    Response.Redirect("~/wfrmUserVisitReservation.aspx?Lang=Ara")
                Else
                    Response.Redirect("~/wfrmUserVisitReservation.aspx")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class