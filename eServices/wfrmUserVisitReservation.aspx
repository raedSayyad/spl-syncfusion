﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmUserVisitReservation.aspx.vb" Inherits="eServices.wfrmUserVisitReservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <ej:Grid ID="VisitsGrid" runat="server" ClientIDMode="Static" AllowPaging="True" OnServerRecordClick="onClick">
                        <ClientSideEvents RecordClick="RecordClick" />
                        <Columns>
                            <ej:Column Field="ID" HeaderText="ID" IsPrimaryKey="True" Visible="false" TextAlign="Right" Width="20"></ej:Column>
                            <ej:Column Field="User_ID" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Library" HeaderText="Library" Width="60"></ej:Column>
                            <ej:Column Field="ReservPurpose" HeaderText="Reserve Purpose" Width="100"></ej:Column>
                            <ej:Column Field="DateFrom" HeaderText="DateFrom" Width="90" Format="{0:MM/dd/yyyy hh:mm tt}"></ej:Column>
                            <ej:Column Field="DateTo" HeaderText="DateTo" Width="90" Format="{0:MM/dd/yyyy hh:mm tt}"></ej:Column>
                            <ej:Column Field="Status" HeaderText="Status" Width="60"></ej:Column>
                            <ej:Column Field="AdminName" HeaderText="AdminName" Width="10" Visible="false"></ej:Column>
                            <ej:Column Field="DateCreate" HeaderText="DateCreate" Width="80" Visible="false"></ej:Column>
                            <ej:Column Field="DateUpdate" HeaderText="DateUpdate" Width="80" Visible="false"></ej:Column>
                            <ej:Column HeaderText="Details" Template="true" TemplateID="#buttonTemplate" TextAlign="Center" Width="75"></ej:Column>
                        </Columns>
                    </ej:Grid>
                    <script type="text/x-jsrender" id="buttonTemplate">
                        <button class="Details" name="Details"><%:Detail%></button>
                    </script>
                    <script type="text/javascript">
                        $(function () {
                            $(".Details").ejButton();
                        });
                    </script>
                    <script type="text/javascript">
                        $(function () {
                            $(".Details").click(function (e) {
                                triggerEvent(e);
                            });
                        });

                        function triggerEvent(e) {
                            var obj = $("#VisitsGrid").data("ejGrid");
                            var args = { currentTarget: e.currentTarget.name, selectedRecord: obj.getSelectedRecords(), selectedIndex: obj.model.selectedRowIndex };
                            obj._trigger("recordClick", args);
                        }

                        function RecordClick(e) {
                            if (e.currentTarget != "Details")
                                return false
                            else {
                                triggerEvent(e);
                            }
                        }
                    </script>
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server" >
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
