﻿Public Class Others
    Public Class LibraryBranch
        Public LibararyBranchID As String
        Public ATitle As String
        Public ETitle As String
        Public ADescription As String
        Public EDescription As String
        Public AAddress As String
        Public EAddress As String
        Public Telephone As String
        Public URL As String
        Public Email As String
        Public Latitude As String
        Public Longitude As String
        Public WorkingHours As String
        Public Likes As Integer = 0
        Public Memebers As Integer = 0
        Public NumberOfWatchings As String = ""
    End Class

    Public Class News
        Public ID As Integer
        Public ATitle As String
        Public ADESCRIPTION As String
        Public ETITLE As String
        Public EDESCRIPTION As String
        Public ACTIVE As Integer
    End Class

    Public Class Events
        Public ID As Integer
        Public ATitle As String
        Public ADESCRIPTION As String
        Public ETITLE As String
        Public EDESCRIPTION As String
        Public ACTIVE As Integer
    End Class

    Public Class Content
        Public ID As Integer
        Public Content As String
        Public ADESCRIPTION As String
        Public EDESCRIPTION As String
    End Class

    Public Class ReserveVisit
        Public ID As Integer
        Public User_ID As String
        Public Library As String
        Public ReservPurpose As String
        Public DateFrom As DateTime
        Public DateTo As DateTime
        Public Status As String
        Public AdminName As String
        Public DateCreate As DateTime
        Public DateUpdate As DateTime
    End Class

    Public Class ReserveHall
        Public ID As Integer
        Public User_ID As String
        Public Library As String
        Public ReservPurpose As String
        Public DateFrom As DateTime
        Public DateTo As DateTime
        Public LectureType As String
        Public AttendanceNo As String
        Public Devices As String
        Public InstructorName As String
        Public InstructorPassport As String
        Public InstructorPpNo As String
        Public Status As String
        Public AdminName As String
        Public DateCreate As DateTime
        Public DateUpdate As DateTime
    End Class

    Public Class DDLList
        Public Value As String
        Public Text As String
    End Class

    Public Class LibraryStaff
        Public User_ID As String
        Public Name As String
        Public Library As String
    End Class

End Class
