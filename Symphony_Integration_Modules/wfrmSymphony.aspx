﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmSymphony.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmSymphony" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <header class="major">
        <h2>Unicorn Windows Services</h2>
        <p></p>
        <link href="Content/bootstrap.min.css" rel="stylesheet" />
        <link href="Content/bootstrap-switch.css" rel="stylesheet" />
        <script src="Scripts/bootstrap-switch.js"></script>
        <script>
            function pageLoad() {
                $("#<%=CheckBox0.ClientID%>").bootstrapSwitch();
                $("#<%=CheckBox1.ClientID%>").bootstrapSwitch();
                $("#<%=CheckBox2.ClientID%>").bootstrapSwitch();
                $("#<%=CheckBox3.ClientID%>").bootstrapSwitch();
                $("#<%=CheckBox4.ClientID%>").bootstrapSwitch();
                $("#<%=CheckBox5.ClientID%>").bootstrapSwitch();
                $("#<%=CheckBox6.ClientID%>").bootstrapSwitch();
                $("#<%=CheckBox7.ClientID%>").bootstrapSwitch();
            }
        </script>
    </header>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <div>
        <asp:Panel ID="PanelRunning" runat="server">
            <h3>Manage Unicorn Windows Services</h3>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblWinSrv" runat="server" Text="Unicorn Ipcm"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox0" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="lblWinSrvStatus" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Unicorn Loadpay"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="Unicorn Netstarter"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox2" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="Unicorn Reportcron"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox3" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" Text="Unicorn Sockserver"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox4" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label9" runat="server" Text="Unicorn Sockwrapper"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox5" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="Label10" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label11" runat="server" Text="Unicorn Starter"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox6" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="Label12" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label13" runat="server" Text="Unicorn Webstarter"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox7" runat="server" CssClass="ToggleButton" />
                            </td>
                            <td>
                                <asp:Label ID="Label14" runat="server" Text="Status"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: center;">
                                <asp:Button ID="Button1" runat="server" Text="Submit" OnClientClick="document.getElementById('navButton').className += ' hide';" />
                                <asp:Button ID="Button2" runat="server" Text="Restart All" OnClientClick="document.getElementById('navButton').className += ' hide';" />
                            </td>
                        </tr>
                    </table>
                    <hr>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel2" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV1" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                        Processing 
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </asp:Panel>
    </div>
</asp:Content>
