﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmEmailTemp.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmEmailTemp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table>
                        <tr>
                            <td colspan="2">Select Email Template:
                                <ej:DropDownList ID="ddlEmailTemp" runat="server" Width="250px">
                                    <Items>
                                        <ej:DropDownListItem Text="User New Holds" Value="Holds_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Expired Holds" Value="Holds_Expired"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Canceled Holds" Value="Holds_Canceled"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Holds New Date" Value="Holds_New_Date"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Responded Requests" Value="Requests_Responded"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Canceled Requests" Value="Requests_Canceled"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="New Charges" Value="Charges_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Renew Charges" Value="Charges_Renew"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Upgrade Approvel" Value="Approval_U"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Upgrade Reject" Value="Approval_U_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Renew Approval" Value="Approval_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Renew Reject" Value="Approval_R_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Cancel Approval" Value="Approval_C"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Cancel Reject" Value="Approval_C_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Hall and Class Approval" Value="Approval"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Hall and Class Reject" Value="Approval_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin New Holds" Value="NEW_HOLDS"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin New Request" Value="NEW_REQ"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin Upgrade Request" Value="Request_U"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin Renew Request" Value="Request_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin Cancel Request" Value="Request_C"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="New Bills" Value="Bills_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Bills Pay" Value="Bills_Pay"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="New Users" Value="Users_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Charges Discharge" Value="Charges_Discharge"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Request HALL" Value="REQ_HALL"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Request VISIT" Value="REQ_VISIT"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Hall Approval" Value="Approval_Hall"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Hall Reject" Value="Approval_Hall_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Visit Approval" Value="Approval_Visit"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Visit Reject" Value="Approval_Visit_R"></ej:DropDownListItem>

                                    </Items>
                                </ej:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 75%;">
                                <ej:RTE ID="RTE1" runat="server" ShowFooter="true" MinHeight="300px" EnableResize="false" MinWidth="20px" AllowEditing="true" Width="100%" Height="400px">
                                    <RTEContent>
                                    </RTEContent>
                                    <Tools Font="fontName,fontSize,fontColor,backgroundColor"
                                        Edit="findAndReplace"
                                        Styles="bold,italic,underline,strikethrough"
                                        Alignment="justifyLeft,justifyCenter,justifyRight,justifyFull"
                                        Lists="unorderedList,orderedList"
                                        Clipboard="cut,copy,paste"
                                        DoAction="undo,redo"
                                        Clear="clearFormat,clearAll"
                                        Links="createLink,removeLink"
                                        Images="image"
                                        Tables="createTable,addRowAbove,addRowBelow,addColumnLeft,addColumnRight,deleteRow,deleteColumn,deleteTable"
                                        Effects="superscript,subscript"
                                        Casing="upperCase,lowerCase"
                                        FormatStyle="format"
                                        View="fullScreen,zoomIn,zoomOut"
                                        Indenting="outdent,indent"
                                        Media="video"
                                        Print="print">
                                    </Tools>
                                </ej:RTE>
                                <style type="text/css">
                                    .customUnOrder, .customOrder {
                                        height: 25px;
                                    }

                                        .customUnOrder:after {
                                            content: "\e7f1";
                                            font-size: 20px;
                                            float: left;
                                            padding: 0px 4px 0px 4px;
                                        }

                                        .customOrder:after {
                                            content: "\e7f0";
                                            font-size: 20px;
                                            float: left;
                                            padding: 0px 4px 0px 4px;
                                        }
                                </style>
                            </td>
                            <td style="width: 25%;">
                                <ej:ListBox ID="selectTag" runat="server" AllowDragAndDrop="false" ClientSideOnSelect="Onselected" Height="400px">
                                    <Items>
                                        <ej:ListBoxItems Text="User ID" Value="id"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Full Name" Value="name"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Data Time Discharged" Value="date_time_discharged"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Email" Value="Email"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Mobile" Value="Mobile"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Library" Value="Library"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Language" Value="Language"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Title" Value="title"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Item ID" Value="itemid"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Alternative ID" Value="alternative_id"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_privilege_granted" Value="date_privilege_granted"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_privilege_expires" Value="date_privilege_expires"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Profile" Value="Profile"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="Status" Value="Status"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_time_due" Value="date_time_due"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_time_charged" Value="date_time_charged"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_placed" Value="date_placed"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_expires" Value="date_expires"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_responded" Value="date_responded"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="REQ_Type" Value="REQ_Type"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="REQ_Status" Value="REQ_Status"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="request_id" Value="request_id"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="bill_reason" Value="bill_reason"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="amount_billed" Value="amount_billed"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="date_billed" Value="date_billed"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="payment_amount" Value="payment_amount"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="payment_date" Value="payment_date"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="payment_type" Value="payment_type"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="approval" Value="approval"></ej:ListBoxItems>
                                        <ej:ListBoxItems Text="approval_r" Value="approval_r"></ej:ListBoxItems>
                                    </Items>
                                </ej:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" />
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <script>
                        var rte;
                        $(function () {
                            rte = $("#<%=RTE1.ClientID%>").data("ejRTE");
                        });
                        function Onselected(args) {
                            rte = $("#<%=RTE1.ClientID%>").data("ejRTE");
                    rte.pasteContent("||" + args.value + "||");
                }
                    </script>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
