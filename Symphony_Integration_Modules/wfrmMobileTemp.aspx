﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmMobileTemp.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmMobileTemp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table>
                        <tr>
                            <td colspan="2">
                                Select Mobile Template:
                                <ej:DropDownList ID="ddlMobileTemp" runat="server" Width="250px">
                                    <Items>
                                        <ej:DropDownListItem Text="User New Holds" Value="Holds_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Expired Holds" Value="Holds_Expired"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Canceled Holds" Value="Holds_Canceled"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Holds New Date" Value="Holds_New_Date"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Responded Requests" Value="Requests_Responded"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Canceled Requests" Value="Requests_Canceled"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="New Charges" Value="Charges_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Renew Charges" Value="Charges_Renew"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Upgrade Approvel" Value="Approval_U"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Upgrade Reject" Value="Approval_U_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Renew Approval" Value="Approval_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Renew Reject" Value="Approval_R_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Cancel Approval" Value="Approval_C"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Cancel Reject" Value="Approval_C_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Hall and Class Approval" Value="Approval"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Hall and Class Reject" Value="Approval_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin New Holds" Value="NEW_HOLDS"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin New Request" Value="NEW_REQ"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin Upgrade Request" Value="NewUser_U"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin Renew Request" Value="Request_R"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Admin Cancel Request" Value="Request_C"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="New Bills" Value="Bills_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Bills Pay" Value="Bills_Pay"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="New Users" Value="Users_New"></ej:DropDownListItem>
                                        <ej:DropDownListItem Text="Charges Discharge" Value="Charges_Discharge"></ej:DropDownListItem>
                                    </Items>
                                </ej:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtMobileTemp" runat="server" TextMode="MultiLine" Width="100%" Height="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" />
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                     <ej:Button ID="btnOpen" Type="Button" Text="Tags Map" ClientSideOnClick="btnclick" runat="server"></ej:Button>
                    <ej:Dialog ID="basicDialog" IsResponsive="True" Width="550" MinWidth="310px" MinHeight="215px" ClientSideOnClose="onDialogClose" Containment=".control" runat="server" Title="Tags Map">
                        <DialogContent>
                            <table>
                                <tr>
                                    <td></td>
                                    <td>||id||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||name||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_time_discharged||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||Email||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||Mobile||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||Library||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||Language||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||title||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||itemid||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||alternative_id||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_privilege_granted||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_privilege_expires||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||Profile||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||Status||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_time_due||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_time_charged||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_placed||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_expires||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_responded||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||REQ_Type||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||REQ_Status||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||request_id||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||bill_reason||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||amount_billed||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||date_billed||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||payment_amount||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||payment_date||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||payment_type||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||approval||</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>||approval_r||</td>
                                </tr>
                            </table>
                        </DialogContent>
                    </ej:Dialog>
                    <script>
                        function onDialogClose(args) {
                            $("#btnOpen").show();
                        }
                        function btnclick() {
                            var dialogObj = $("#basicDialog").data("ejDialog");
                            dialogObj.open();
                            $("#btnOpen").hide();
                        }
                        function pageLoad() {
                            var dialogObj = $("#basicDialog").data("ejDialog");
                            dialogObj.close();
                        }
                    </script>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
