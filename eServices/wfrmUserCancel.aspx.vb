﻿Imports ClassLibraries

Public Class wfrmUserCancel
    Inherits System.Web.UI.Page

    Dim gotoURL As String = ""
    Public objUserInfo As New Users.UserInfo
    Public objNewUserInfo As New Users.UserInfo
    Public PageTitle As String
    Public Row As String
    Public u As String
    Public Lang As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim User_ID As String
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            If Not Session("UserInfo") Is Nothing Then
                objUserInfo = Session("UserInfo")
                User_ID = objUserInfo.User_ID
            End If
            Lang = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
                objSymAdminSDH.locale = "en"
            End If

            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            PageTitle = rc.GetString("CancelUser", Culture_Used)
            If Lang = "Ara" Then
                gotoURL = "~/wfrmResulte.aspx?TermsID=Usrugd&Source=" & PageTitle & "&Msg=Request Sent Successfully&Lang=Ara"
            Else
                gotoURL = "~/wfrmResulte.aspx?TermsID=Usrugd&Source" & PageTitle & "&Msg=Request Sent Successfully"
            End If

            lblReason.Text = rc.GetString("Reason", Culture_Used)
            btnSubmit.Text = rc.GetString("_Continue", Culture_Used)

        Catch ex As Exception
            lblError.Text += ex.Message
        End Try

    End Sub

End Class