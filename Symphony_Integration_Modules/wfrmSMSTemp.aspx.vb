﻿Imports ClassLibraries
Public Class wfrmSMSTemp
    Inherits System.Web.UI.Page

    Dim _sqlObj As New SQLFunction

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim query As String = ""
            If dbType = "SQL" Then
                query = "UPDATE [Symphony_Integration].[dbo].[tbl_Notification_SMS_Temp] SET [TempValue] = N'" & txtSMSTemp.Text & "' WHERE [TempName] = '" & ddlSMSTemp.Value & "'"
            ElseIf dbType = "ORA" Then
                query = ""
            End If
            _sqlObj.exeNonQuery(query, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
            txtSMSTemp.Text = ""
        Catch ex As Exception
            Label2.Text = ex.Message
        End Try
    End Sub

    Private Sub ddlSMSTemp_ValueSelect(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs) Handles ddlSMSTemp.ValueSelect
        Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
        Dim dt As New DataTable
        Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
        Dim name As String = info.ToString
        Dim sqlScript As String = ""
        If dbType = "SQL" Then
            sqlScript = "SELECT [TempValue] FROM [Symphony_Integration].[dbo].[tbl_Notification_SMS_Temp] WHERE [TempName] = '" & ddlSMSTemp.Value & "'"
        ElseIf dbType = "ORA" Then
            sqlScript = ""
        End If
        dt = _sqlObj.fillDT(sqlScript, CommandType.Text, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
        If dt.Rows.Count > 0 Then
            txtSMSTemp.Text = dt.Rows(0).Item(0)
        End If
    End Sub
End Class