﻿Imports ClassLibraries

Public Class wfrmLibraryBranch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            objSymAdminSDH.locale = "en"
            objlookupPolicyListReq.policyType = "LIBR"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)

            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLIST = New Others.DDLList
                objDDLIST.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLIST.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlist.Add(objDDLIST)
            Next
            ddlLibrary.DataSource = objAllDDlist
            ddlLibrary.DataValueField = "Value"
            ddlLibrary.DataTextField = "Text"
            ddlLibrary.DataBind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub ddlLibrary_ValueSelect(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs) Handles ddlLibrary.ValueSelect
        Try
            Dim objewsOthersClient As New ewsOthers.IewsOthersClient
            Dim objLibraryBranch As New Others.LibraryBranch
            objLibraryBranch = objewsOthersClient.GetLibraryBranch(ddlLibrary.Value)
            txtAADDRESS.Text = objLibraryBranch.AAddress
            txtEADDRESS.Text = objLibraryBranch.EAddress
            txtADESCRIPTION.Text = objLibraryBranch.ADescription
            txtEDESCRIPTION.Text = objLibraryBranch.EDescription
            txtATITLE.Text = objLibraryBranch.ATitle
            txtETITLE.Text = objLibraryBranch.ETitle
            txtLATITUDE.Text = objLibraryBranch.Latitude
            txtLONGITUDE.Text = objLibraryBranch.Longitude
            txtTELEPHONE.Text = objLibraryBranch.Telephone
            txtWORKINGHOURS.Text = objLibraryBranch.WorkingHours
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim objewsOthersClient As New ewsOthers.IewsOthersClient
            Dim objLibraryBranch As New Others.LibraryBranch
            Dim Result As String = ""
            objewsOthersClient.InstLibraryBranch(ddlLibrary.Value, txtATITLE.Text, txtETITLE.Text, txtADESCRIPTION.Text, txtEDESCRIPTION.Text, txtAADDRESS.Text, txtEADDRESS.Text, txtTELEPHONE.Text, "", "", txtLATITUDE.Text, txtLONGITUDE.Text, txtWORKINGHOURS.Text, "0", "0", "0")
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class
