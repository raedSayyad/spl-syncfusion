﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmLogin.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Integration</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-switch.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
	<link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css"  />
    <asp:PlaceHolder runat="server">
        <script src="../Scripts/modernizr-2.6.2.js" type="text/javascript"></script>
    </asp:PlaceHolder>
    <%--<script src="../Scripts/jquery-3.0.0.min.js" type="text/javascript"></script>--%>
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <%--<script src="Scripts/jquery-1.11.0.min.js"></script>--%>
    <script src="Scripts/bootstrap-switch.js"></script>
    <%--<script src="../Scripts/js/jquery.min.js" type="text/javascript"></script>--%>
    <script src="../Scripts/js/skel.min.js" type="text/javascript"></script>
    <script src="../Scripts/js/skel-layers.min.js" type="text/javascript"></script>
    <script src="../Scripts/js/init.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <style>
        #footer {
    background: #383b43;
    padding: 0em 0em 0em;
    position: absolute;
    bottom: 0;
    width: 100%;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
            <header id="header">
                <h1><a href="index.html"><img src="../images/Custom_LoginLogo.gif" style="margin-top: 15px;"/></a></h1>
            </header>
            <section id="main" class="wrapper"">
                <div class="container">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server">
                                <div id="lgDivparent">
                                    <div class="lgDivright">
                                        Username:<br />
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        Password:<br />
                                        <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                                        <br />
                                        <div style="text-align: left;">
                                            <asp:Button ID="Button1" runat="server" Text="Login" />
                                        </div>
                                        <hr />
                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="lgDivleft">
                                        <img src="Images/integration-service.png" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server" Visible="false">
                                <asp:HiddenField ID="hdfUserID" runat="server" />
                                Auth Key:
                                <br />
                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                <br />
                                <asp:Button ID="Button2" runat="server" Text="Submit" />
                                <hr />
                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                        <ProgressTemplate>
                            <div id="IMGDIV1" align="center" valign="middle" class="ModalPopupBG" runat="server">
                                <div style="width: 80%; margin-top: 30%;">
                                    <div id="result" style="font-family: Tahoma; font-size: 0.9em; color: Black; margin-top: 30px; padding-bottom: 5px">
                                    </div>
                                    <div id="progressbar" style="width: 100%; height: 20px"></div>
                                    <br />
                                </div>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                 </div>
            </section>
            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="8u 12u$(medium)">
                            <ul class="copyright">
                                <li>&copy;<%: DateTime.Now.Year %> Naseej. All rights reserved.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        <script type="text/javascript">
            window.history.forward(1);
        </script>
    </form>
</body>
</html>
