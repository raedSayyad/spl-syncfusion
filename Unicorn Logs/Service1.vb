Imports System.IO

Public Class UnLogs
    Public Array_Setting() As String = Nothing
    Public Array_Commands() As String = Nothing
    Dim LogFile As String = ""
    Dim SettFile As String = ""
    Dim AllFiles As Boolean = False
    Dim ComFile As String = ""
    Dim Time_To_Run As String = ""
    Dim DB As String = ""
    Dim Encoding As String = ""
    Dim Query_Log As String = ""
    Dim ConStr As String = ""
    Dim InProcess As Boolean = False
    Dim Service_Path_Array() As String = Nothing
    Dim Service_Path As String = ""

    Protected Overrides Sub OnStart(ByVal args() As String)
        SaveTextToFile(LogFile, Now & " | Service Start")
        SaveTextToFile(LogFile, Now & " | Loading Config file")
        Get_Config()
        SaveTextToFile(LogFile, Now & " | Config Loaded")
        ' DebugMode()
        SaveTextToFile(LogFile, Now & " | Check Transaction Status Process")
        Check_ReqTrn()
        SaveTextToFile(LogFile, Now & " | Check Transaction Process Done")
        Timer1.Enabled = True
        Timer1.Interval = 60000
        Timer1.Start()
    End Sub

    <Conditional("DEBUG")>
    Shared Sub DebugMode()
        If Not Debugger.IsAttached Then
            Debugger.Launch()
        End If

        Debugger.Break()
    End Sub

    Protected Overrides Sub OnStop()
        Timer1.Enabled = False
        SaveTextToFile(LogFile, Now & " | Service Stop")
    End Sub

    Public Function Get_Config() As String
        Dim strLine As String = ""
        Try
            Service_Path_Array = Nothing
            Service_Path = ""
            Service_Path_Array = System.Reflection.Assembly.GetEntryAssembly().Location.ToString.Split("\")
            If Service_Path_Array IsNot Nothing Then
                For i As Integer = 0 To Service_Path_Array.Length - 2
                    Service_Path &= Service_Path_Array(i) & "\"
                Next
            End If
            SettFile = Service_Path & "Setting.txt"
            Array_Setting = Nothing
            Dim COunter As Integer = 0
            Dim objStreamReader As StreamReader = New StreamReader(SettFile)
            Do While Not strLine Is Nothing
                Try
                    strLine = objStreamReader.ReadLine
                    If strLine <> "" Then
                        ReDim Preserve Array_Setting(COunter)
                        Array_Setting(COunter) = strLine.Split("=")(1)
                        COunter += 1
                    End If
                Catch ex As Exception
                    SaveTextToFile(Array_Setting(0), Now & " : " & ex.Message)
                End Try
            Loop
            objStreamReader.Close()
            If Array_Setting IsNot Nothing Then
                If Array_Setting.Length > 0 Then
                    LogFile = Array_Setting(0)
                    Query_Log = Array_Setting(1)
                End If
            End If
            SaveTextToFile(LogFile, Now & " : RequestTrans Service Started." & Now)
            For i As Integer = 0 To Array_Setting.Length - 1
                SaveTextToFile(Array_Setting(0), Now & " : " & Array_Setting(i))
            Next
        Catch ex As IOException
            SaveTextToFile(Array_Setting(0), Now & " : " & ex.Message)
        End Try
        Return strLine
    End Function

    Public Sub Check_ReqTrn()
        Try

            'SaveTextToFile(LogFile, "Start")
            Dim srvReq As New RequestTransaction.RequestTransaction
            'SaveTextToFile(LogFile, "WS")
            Dim srvEpay As New epayTransStatus.epaynmwebservice
            'SaveTextToFile(LogFile, "ePay")
            'DebugMode()



            Dim RequestID As String = ""
            Dim SPCODE As String = ""
            Dim SERVCODE As String = ""
            Dim SPTRN As String = ""
            Dim DEGTRN As String = ""
            Dim TRANSDATE As String = ""
            Dim PAYMENTMETHOD As String = ""
            Dim MESSAGE As String = ""
            Dim MESSAGECODE As String = ""

            Dim ds1 As New DataSet
            'SaveTextToFile(LogFile, srvReq.Url)
            SaveTextToFile(LogFile, Now & " | Load Uncheck Transaction")
            ds1 = srvReq.SelectAll_DS()
            If ds1.Tables.Count = 0 Then
                SaveTextToFile(LogFile, Now & " | Transaction Count: 0")
            End If
            If ds1.Tables.Count <> 0 Then
                SaveTextToFile(LogFile, Now & " | Transaction Count: " + CStr(ds1.Tables(0).Rows.Count.ToString))
                If ds1.Tables(0).Rows.Count <> 0 Then
                    SaveTextToFile(LogFile, Now & " | Transaction Check List Loop")
                    For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                        srvReq.UpdateLock(ds1.Tables(0).Rows(i).Item(2).ToString, 1)
                        SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' DB Record Locked")
                        Dim response As String = srvEpay.getTransactionStatus(ds1.Tables(0).Rows(i).Item(0).ToString, ds1.Tables(0).Rows(i).Item(1).ToString, ds1.Tables(0).Rows(i).Item(2).ToString)
                        SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' Status Response from DSG: " + response)
                        Dim xmlString As String = response
                        Dim sr As New System.IO.StringReader(xmlString)
                        Dim doc As New Xml.XmlDocument
                        doc.Load(sr)
                        Dim reader As New Xml.XmlNodeReader(doc)
                        While reader.Read()
                            Select Case reader.NodeType
                                Case Xml.XmlNodeType.Element
                                    If reader.Name = "SPCODE" Then
                                        SPCODE = reader.ReadElementContentAsString
                                    End If
                                    If reader.Name = "SERVCODE" Then
                                        SERVCODE = reader.ReadElementContentAsString
                                    End If
                                    If reader.Name = "SPTRN" Then
                                        SPTRN = reader.ReadElementContentAsString
                                    End If
                                    If reader.Name = "DEGTRN" Then
                                        DEGTRN = reader.ReadElementContentAsString
                                    End If
                                    If reader.Name = "TRANSDATE" Then
                                        TRANSDATE = reader.ReadElementContentAsString
                                        SaveTextToFile(LogFile, "TRANSDATE  :   " & TRANSDATE)
                                    End If
                                    If reader.Name = "PAYMENTMETHOD" Then
                                        PAYMENTMETHOD = reader.ReadElementContentAsString
                                    End If
                                    If reader.Name = "MESSAGE" Then
                                        MESSAGE = reader.ReadElementContentAsString
                                    End If
                                    If reader.Name = "MESSAGECODE" Then
                                        MESSAGECODE = reader.ReadElementContentAsString
                                    End If
                            End Select
                        End While
                        RequestID = ds1.Tables(0).Rows(i).Item(3).ToString
                        SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' RequestID: " + RequestID)
                        SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' TransDate" + TRANSDATE)
                        Dim trnresponse As New RequestTransaction.Response_Msg
                        Dim srvBill As New UserBills.UserBills
                        Dim resBill As UserBills.Reponse_Msg
                        Dim ds2 As DataSet = srvReq.SelectAllSuccessBill(SPTRN)
                        If ds2.Tables.Count <> 0 Then
                            Dim UserID As String = ds2.Tables(0).Rows(0).Item(0)
                            SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | UserID: " + UserID)
                            Dim srvUserOther As New UserOthersINFO.UserOtherInfo
                            Dim resUserOther As UserOthersINFO.Reponse_Msg = srvUserOther.CallUserOtherInfo("eng", UserID)
                            Dim Libr As String = resUserOther.LIBR
                            SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | LibraryID:" + Libr)
                            Dim BillN As String = ds2.Tables(0).Rows(0).Item(1)
                            SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Bill(s) Number: " + ds2.Tables(0).Rows(0).Item(1))
                            Dim BillNS() As String = Split(BillN, ",")
                            Dim BillA As String = ds2.Tables(0).Rows(0).Item(2)
                            SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Bill(s) Amount: " + ds2.Tables(0).Rows(0).Item(2))
                            Dim BillAS() As String = Split(BillA, ",")

                            If response.Contains("SUCCESS") Then
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Status: SUCCESS")
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Insert Response")
                                trnresponse = srvReq.Insert_Res(RequestID, SPCODE, SERVCODE, SPTRN, DEGTRN, TRANSDATE, PAYMENTMETHOD, MESSAGECODE, MESSAGE)
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Update Transaction Record in Request Table")
                                srvReq.Update(ds1.Tables(0).Rows(i).Item(3), "SUCCESS")
                                Dim dtstr As String = ""
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Set DateTime Format as per Sumphony Bill service standard")
                                Dim xdatetime() As String = TRANSDATE.Split(" ")
                                Dim xdate() As String = xdatetime(0).Split("/")
                                Dim xtime() As String = xdatetime(1).Split(":")

                                dtstr = xdate(2) + xdate(0) + xdate(1)
                                If xdatetime(2) = "PM" Then
                                    If xtime(0) = "" Then
                                        SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Error: xtime(0) Empty")
                                    Else
                                        If CInt(xtime(0)) < 12 Then
                                            xtime(0) = CStr(CInt(xtime(0)) + 12)
                                        End If
                                    End If
                                ElseIf xdatetime(2) = "AM" Then
                                    If xtime(0) = "12" Then
                                        xtime(0) = "00"
                                    End If
                                End If
                                dtstr = dtstr + xtime(0) + xtime(1) + xtime(2)

                                Dim Payment As String = PAYMENTMETHOD.ToUpper
                                Payment = Payment.Replace(" ", "")
                                If BillAS.Length > 1 Then
                                    SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Contains more than one bill")
                                    SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Bill Count: " + CStr(BillAS.Length - 2))
                                    For b As Integer = 0 To BillAS.Length - 2
                                        BillAS(b) = BillAS(b).Replace("AED", "")
                                        resBill = srvBill.UsePaymentsMethod("eng", UserID.Trim, dtstr.Trim, Libr.Trim, BillNS(b).Trim, BillAS(b).Trim, Payment.Trim)
                                        SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Pay Bill '" + BillNS(b) + "'Status:" + resBill.Msg)
                                    Next
                                ElseIf BillAS.Length = 1 Then
                                    SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Contains one bill Only")
                                    BillAS(0) = BillAS(0).Replace("AED", "")
                                    resBill = srvBill.UsePaymentsMethod("eng", UserID.Trim, dtstr.Trim, Libr.Trim, BillNS(0).Trim, BillAS(0).Trim, Payment.Trim)
                                    SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Pay Bill '" + BillNS(0) + "'Status:" + resBill.Msg)
                                End If

                            ElseIf response.Contains("FAILURE") Then
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Status: FAILURE")
                                trnresponse = srvReq.Insert_Res(RequestID, SPCODE, SERVCODE, SPTRN, DEGTRN, TRANSDATE, PAYMENTMETHOD, MESSAGECODE, MESSAGE)
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Insert Response")
                                srvReq.Update(ds1.Tables(0).Rows(i).Item(3), "FAILURE")
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Update Transaction Record in Request Table")
                            ElseIf response.Contains("Transaction In Progress") Then
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Status: Transaction In Progress")
                                srvReq.UpdateLock(ds1.Tables(0).Rows(i).Item(2), 0)
                                srvReq.Update(ds1.Tables(0).Rows(i).Item(3), "Under Process")
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Update Transaction Record in Request Table")
                                srvReq.UpdateLock(ds1.Tables(0).Rows(i).Item(2), 0)
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' Recoed Unlocked")

                            ElseIf response.Contains("SP Terminated") Then
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Status: SP Terminated")
                                trnresponse = srvReq.Insert_Res(RequestID, SPCODE, SERVCODE, SPTRN, DEGTRN, TRANSDATE, PAYMENTMETHOD, MESSAGECODE, MESSAGE)
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Insert Response")
                                srvReq.Update(ds1.Tables(0).Rows(i).Item(3), "Terminated")
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Update Transaction Record in Request Table")
                                '70011
                            ElseIf response.Contains("70011") Then
                                Dim format() = {"dd/MM/yyyy hh:mm:ss tt"}
                                Dim expenddt As Date = Date.ParseExact(TRANSDATE, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)
                                expenddt = expenddt.AddMinutes(30)
                                If Date.Now > expenddt Then
                                    SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Status: Invalid Transaction Search Criteria")
                                    srvReq.Update(ds1.Tables(0).Rows(i).Item(3), "Invalid Search Criteria")
                                    SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Update Transaction Record in Request Table")
                                    srvReq.UpdateLock(ds1.Tables(0).Rows(i).Item(2), 0)
                                    SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' Recoed Unlocked")
                                End If
                            Else
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | Status: " + MESSAGE)
                                srvReq.Update(ds1.Tables(0).Rows(i).Item(3), MESSAGE)
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' | DB Update Transaction Record in Request Table")
                                srvReq.UpdateLock(ds1.Tables(0).Rows(i).Item(2), 0)
                                SaveTextToFile(LogFile, Now & " | Transaction '" + ds1.Tables(0).Rows(i).Item(2).ToString + "' Recoed Unlocked")
                            End If
                        Else
                            srvReq.UpdateLock(ds1.Tables(0).Rows(i).Item(2).ToString, 0)
                        End If
                    Next
                Dim reqCon As New RequestTransaction.ConfirmTrans
                reqCon = srvReq.getConfirmDetails(RequestID)
                Dim reqePayCon As New ePayConfirm.ePayConfirm
                SaveTextToFile(LogFile, Now & " | Transaction '" + RequestID + "' | Send Confirmation to DSG")
                Dim resePayCon As String = reqePayCon.ConfirmTrans(reqCon.vDEGTrans, reqCon.vPaymentMethod, reqCon.vSPCode, reqCon.vServiceCode, reqCon.vSPTrans, reqCon.vTrxDate, reqCon.vMessage, reqCon.vMessage0)
                SaveTextToFile(LogFile, Now & " | Transaction '" + RequestID + "' CONFIRMATION Response : " & resePayCon)
            End If
            End If
        Catch ex As Exception
            SaveTextToFile(LogFile, Now & " | Check Transaction Process Error: " & ex.Message)
        End Try

    End Sub

    Public Function SaveTextToFile(ByVal FullPath As String, ByVal new_line As String) As Boolean
        Dim bAns As Boolean = False
        Dim objReader As StreamWriter
        Try
            objReader = New StreamWriter(FullPath.Trim, True, System.Text.Encoding.UTF8)
            objReader.Write(new_line & ControlChars.NewLine)
            objReader.Close()
            bAns = True
        Catch Ex As Exception

        End Try
        Return bAns
    End Function

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        Try
            ' Dim SysTime As String = Now.ToString("hh:mm tt")
            ' If SysTime = Time_To_Run Then
            If InProcess = False Then
                InProcess = True
                SaveTextToFile(LogFile, Now & " | Timer Restart")
                'DebugMode()
                Check_ReqTrn()
                System.Threading.Thread.Sleep(60000)
                InProcess = False
            End If
            '   Else

            '  End If
        Catch ex As Exception
            SaveTextToFile(LogFile, Now & " | Timer Error: " & ex.Message)
        End Try
    End Sub
End Class
'RequestTransaction
'http://eservices.dubaipubliclibrary.ae:8888/PaymentWS/requesttransaction.asmx?wsdl

'epayTransStatus
'https://epayment.qa.dubai.ae/ePayHub/epaynmservicewar/epaynmwebservice?WSDL