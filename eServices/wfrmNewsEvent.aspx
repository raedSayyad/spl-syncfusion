﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmNewsEvent.aspx.vb" Inherits="eServices.wfrmNewsEvent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%;">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js'  type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body style="width: 100%;height:100%!important"  id="PageBody" runat="server">
    <form id="form1" runat="server" style="width: 100%;height:100%!important">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%;height:100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div>
                        <ej:Grid ID="grdNews" runat="server" DetailsTemplate="#tabGridContents">
                            <ClientSideEvents DetailsDataBound="detailGridData" />
                            <Columns>
                                <ej:Column Field="ID" IsPrimaryKey="True" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ATitle" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ETITLE" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ADESCRIPTION" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="EDESCRIPTION" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ACTIVE" HeaderText="" Width="100" Visible="false" />
                            </Columns>
                        </ej:Grid>
                        <script src="../Scripts/jsondata.min.js"></script>
                        <script id="tabGridContents" type="text/x-jsrender">
                            <div class="tabcontrol">
                                <div id="detailChart{{:ID}}">
                                    <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </script>
                        <script type="text/javascript">
                            function detailGridData(e) {
                                var filteredData = e.data["ID"];
                                var data = ej.DataManager(window.ordersView).executeLocal(ej.Query().where("ID", "equal", parseInt(filteredData), true));
                                e.detailsElement.css("display", "");
                                e.detailsElement.css("height", "100%");
                                e.detailsElement.find(".tabcontrol").ejTab({ selectedItemIndex: 1 });
                            }
                        </script>
                    </div>
                    <hr />
                    <div>
                        <ej:Grid ID="grdEvent" runat="server" DetailsTemplate="#tabGridContents1">
                            <ClientSideEvents DetailsDataBound="detailGridData" />
                            <Columns>
                                <ej:Column Field="ID" IsPrimaryKey="True" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ATitle" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ETITLE" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ADESCRIPTION" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="EDESCRIPTION" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ACTIVE" HeaderText="" Width="100" Visible="false" />
                            </Columns>
                        </ej:Grid>
                        <script src="../Scripts/jsondata.min.js"></script>
                        <script id="tabGridContents1" type="text/x-jsrender">
                            <div class="tabcontrol1">
                                <div id="detailChart1{{:ID}}">
                                    <asp:Label ID="lblDescriptionE" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </script>
                        <script type="text/javascript">
                            function detailGridData(e) {
                                var filteredData = e.data["ID"];
                                var data = ej.DataManager(window.ordersView).executeLocal(ej.Query().where("ID", "equal", parseInt(filteredData), true));
                                e.detailsElement.css("display", "");
                                e.detailsElement.css("height", "100%");
                                e.detailsElement.find(".tabcontrol1").ejTab({ selectedItemIndex: 1 });
                            }
                        </script>
                    </div>
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
             <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
