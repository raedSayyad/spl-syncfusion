﻿Imports ClassLibraries

Public Class wfrmUserUpgReqDetail
    Inherits System.Web.UI.Page

    Public ddlCityInd As String = "0"
    Public ddlNatInd As String = "0"
    Public ddlTitleInd As String = "0"
    Public ddlLibraryInd As String = "0"
    Public ddlGenderInd As String = "0"
    Public ddlMemberTypeInd As String = "0"

    Public objUserUpgRequest As New Users.UserUpgRequest

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objUserUpgRequest = Session("UserUpgRequest")
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim UserID As String = objUserUpgRequest.User_ID
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                ddlCity.EnableRTL = True
                ddlNat.EnableRTL = True
                ddlTitle.EnableRTL = True
            Else
                objSymAdminSDH.locale = "en"
            End If

            Dim objMemType As New List(Of MemType)()
            objMemType.Add(New MemType(0, "public_user"))
            objMemType.Add(New MemType(1, "family"))
            objMemType.Add(New MemType(2, "academic"))
            objMemType.Add(New MemType(3, "postgraduate_student"))
            objMemType.Add(New MemType(4, "undergraduate_student"))
            objMemType.Add(New MemType(5, "special_user"))
            objMemType.Add(New MemType(6, "Special_Need"))
            objMemType.Add(New MemType(7, "Virtual_Card"))
            ddlMemType.DataSource = objMemType
            ddlMemType.DataValueField = "id"
            ddlMemType.DataTextField = "text"

            ddlMemType.DataBind()
            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            objlookupPolicyListReq.policyType = "CAT3"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLIST = New Others.DDLList
                objDDLIST.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLIST.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlist.Add(objDDLIST)
            Next
            ddlTitle.DataSource = objAllDDlist
            ddlTitle.DataValueField = "Value"
            ddlTitle.DataTextField = "Text"
            ddlTitle.DataBind()

            objlookupPolicyListReq.policyType = "LIBR"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTLib As New Others.DDLList
            Dim objAllDDlistLib As New List(Of Others.DDLList)

            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTLib = New Others.DDLList
                objDDLISTLib.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTLib.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistLib.Add(objDDLISTLib)
            Next
            ddlLibrary.DataSource = objAllDDlistLib
            ddlLibrary.DataValueField = "Value"
            ddlLibrary.DataTextField = "Text"
            ddlLibrary.DataBind()

            objlookupPolicyListReq.policyType = "CAT2"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTG As New Others.DDLList
            Dim objAllDDlistG As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTG = New Others.DDLList
                objDDLISTG.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTG.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistG.Add(objDDLISTG)
            Next
            ddlGender.DataSource = objAllDDlistG
            ddlGender.DataValueField = "Value"
            ddlGender.DataTextField = "Text"
            ddlGender.DataBind()

            objlookupPolicyListReq.policyType = "CAT4"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTN As New Others.DDLList
            Dim objAllDDlistN As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTN = New Others.DDLList
                objDDLISTN.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTN.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistN.Add(objDDLISTN)
            Next
            ddlNat.DataSource = objAllDDlistN
            ddlNat.DataValueField = "Value"
            ddlNat.DataTextField = "Text"
            ddlNat.DataBind()

            objlookupPolicyListReq.policyType = "CAT5"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTC As New Others.DDLList
            Dim objAllDDlistC As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTC = New Others.DDLList
                objDDLISTC.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTC.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistC.Add(objDDLISTC)
            Next
            ddlCity.DataSource = objAllDDlistC
            ddlCity.DataValueField = "Value"
            ddlCity.DataTextField = "Text"
            ddlCity.DataBind()

            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            If Not IsPostBack Then
                txtFullName.Text = objUserUpgRequest.FullName
                Dim DOB As String = objUserUpgRequest.DOB
                Dim DOBArr() As String = DOB.Split(" ")
                txtDOB.Text = DOBArr(0)
                txtCompany.Text = objUserUpgRequest.Employer
                txtNatID.Text = objUserUpgRequest.NationalID
                txtMobile.Text = objUserUpgRequest.Mobile
                txtTele.Text = objUserUpgRequest.HomePhone
                txtWorkPhone.Text = objUserUpgRequest.WorkPhone
                txtExt.Text = objUserUpgRequest.Ext
                txtFax.Text = objUserUpgRequest.Fax
                txtPostBox.Text = objUserUpgRequest.POBox
                txtWebSite.Text = objUserUpgRequest.WebSite
                hprlnkPersonalImg.NavigateUrl = objUserUpgRequest.PersonalPhoto
                imgPersonal.ImageUrl = objUserUpgRequest.PersonalPhoto
                hprlnkNatID1.NavigateUrl = objUserUpgRequest.NationalIDPhoto1
                imgNatID1.ImageUrl = objUserUpgRequest.NationalIDPhoto1
                hprlnkNatID2.NavigateUrl = objUserUpgRequest.NationalIDPhoto2
                imgNatID2.ImageUrl = objUserUpgRequest.NationalIDPhoto2
                If objUserUpgRequest.SNeeds.Contains("http") Then
                    lblSNeedImg.Visible = True
                    hprlnkSNeedImg.NavigateUrl = objUserUpgRequest.SNeeds
                    imgSNeed.ImageUrl = objUserUpgRequest.SNeeds
                Else
                    lblSNeedImg.Text = ""
                    hprlnkSNeedImg.Visible = False
                    imgSNeed.Visible = False
                    imgSNeed.CssClass = "style1"
                End If
                ddlMemberTypeInd = objUserUpgRequest.MemberType
                For c As Integer = 0 To objAllDDlistLib.Count - 1
                    If objAllDDlistLib(c).Value = objUserUpgRequest.Library Then
                        ddlLibraryInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlistG.Count - 1
                    If objAllDDlistG(c).Value = objUserUpgRequest.Gender Then
                        ddlGenderInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlist.Count - 1
                    If objAllDDlist(c).Value = objUserUpgRequest.JobTitle Then
                        ddlTitleInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlistN.Count - 1
                    If objAllDDlistN(c).Value = objUserUpgRequest.Nationality Then
                        ddlNatInd = CStr(c)
                    End If
                Next
                For c As Integer = 0 To objAllDDlistC.Count - 1
                    If objAllDDlistC(c).Value = objUserUpgRequest.City Then
                        ddlCityInd = CStr(c)
                    End If
                Next
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            Dim objResponse_Msg As New Responce_Msg
            Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim DOB As String = txtDOB.Text
            Dim DOBArr() As String = DOB.Split("/")
            DOB = ""
            If DOBArr(1).Length = 1 Then
                DOB += "0" + DOBArr(1) + "/"
            Else
                DOB += DOBArr(1) + "/"
            End If
            If DOBArr(0).Length = 1 Then
                DOB += "0" + DOBArr(0) + "/"
            Else
                DOB += DOBArr(0) + "/"
            End If
            DOB += DOBArr(2)
            objResponse_Msg = objewsUserRegClient.LoadUserUpgrade("1", "Ara", objUserUpgRequest.User_ID, ddlLibrary.Value, txtFullName.Text, DOB, txtMobile.Text, _
                                                                  ddlCity.Value, ddlMemType.Value, ddlNat.Value, ddlTitle.Value, txtNatID.Text, _
                                                                  hprlnkSNeedImg.NavigateUrl, txtCompany.Text, ddlGender.Value, hprlnkNatID1.NavigateUrl, _
                                                                  hprlnkNatID2.NavigateUrl, hprlnkPersonalImg.NavigateUrl, txtTele.Text, txtWorkPhone.Text, _
                                                                  txtExt.Text, txtPostBox.Text, txtFax.Text, txtWebSite.Text)
            If objResponse_Msg.UserId <> "" Then
                Dim result As String = objewsUserRegClient.UpdateUserUpgrade(objUserUpgRequest.ID, "2", " ", "ADMIN", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                If result <> "1" Then
                    lblError.Text += result + " || "
                Else
                    'MEMFEEACA||Acadimic Membership|
                    'MEMFEEFAM||Family Membership|
                    'MEMFEEINST||Institute Membership|
                    'MEMFEEKID||Kids Membership|
                    'MEMFEEPSTD||Postgraduate Membership|
                    'MEMFEEPUB||Public  Membership|
                    'MEMFEESPN||Special Need Membership|
                    'MEMFEESTD||Student Membership|

                    'objMemType.Add(New MemType(0, "public_user"))
                    'objMemType.Add(New MemType(1, "family"))
                    'objMemType.Add(New MemType(2, "academic"))
                    'objMemType.Add(New MemType(3, "postgraduate_student"))
                    'objMemType.Add(New MemType(4, "undergraduate_student"))
                    'objMemType.Add(New MemType(5, "special_user"))
                    'objMemType.Add(New MemType(6, "Special_Need"))
                    If ddlMemType.Value = 0 Then
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "150.00", "MEMFEEPUB")
                    ElseIf ddlMemType.Value = 1 Then
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "300.00", "MEMFEEFAM")
                    ElseIf ddlMemType.Value = 2 Then
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "1000.00", "MEMFEEACA")
                    ElseIf ddlMemType.Value = 3 Then
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "150.00", "MEMFEEPSTD")
                    ElseIf ddlMemType.Value = 4 Then
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "150.00", "MEMFEESTD")
                    ElseIf ddlMemType.Value = 5 Then
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "150.00", "MEMFEEPUB")
                    ElseIf ddlMemType.Value = 6 Then
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "150.00", "MEMFEESPN")
                    ElseIf ddlMemType.Value = 7 Then 'Added By yamen for Virtual Membership
                        objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "26.25", "MEMFEEVIR")
                    End If
                    '  objResponse_Msg = objewsUserRegClient.UserBillsMethod("eng", objUserUpgRequest.User_ID, "", ddlLibrary.Value, "150.00", "PRIVILEGE")
                    If objResponse_Msg.Code <> "" Then
                        objewsUserRegClient.InsUsersOPT(objUserUpgRequest.User_ID, "0", "0", "0", "2", " ", "1", "Admin", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                        Response.Redirect("~/wfrmResulte.aspx?Title=User Upgrade&Text=Approved Successfuly and Bill Added&Url=wfrmUserUpgReq.aspx")
                        lblError.Text = "Approved Successfuly and Bill Added"
                    Else
                        objewsUserRegClient.InsUsersOPT(objUserUpgRequest.User_ID, "0", "0", "0", "2", " ", "1", "Admin", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                        Response.Redirect("~/wfrmResulte.aspx?Title=User Upgrade&Text=Approved Successfuly and Bill NOT Added, Report to system administrator&Url=wfrmUserUpgReq.aspx")
                        lblError.Text = "Approved Successfuly and Bill NOT Added"
                    End If
                End If
            Else
                    lblError.Text = objResponse_Msg.Msg
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmUserUpgReq.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmUserUpgReq.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Upg&Lang=Ara")
            Else
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Upg")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Class MemType
        Public Property text() As String
            Get
                Return m_text
            End Get
            Set(value As String)
                m_text = value
            End Set
        End Property
        Private m_text As String
        Public Property id() As Integer
            Get
                Return m_id
            End Get
            Set(value As Integer)
                m_id = value
            End Set
        End Property
        Private m_id As Integer
        Public Sub New(id As Integer, txt As String)
            Me.id = id
            Me.text = txt
        End Sub
    End Class

End Class