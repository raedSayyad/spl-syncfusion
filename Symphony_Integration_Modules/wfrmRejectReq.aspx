﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmRejectReq.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmRejectReq" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <section>
                    <div class="Row uniform 50%">
                        <div class="4=u 12u$(3)">
                            <asp:Label ID="lblReason" runat="server" Text="Reason"></asp:Label>
                            </div>
                    <div class="4=u$ 12u$(3)">
                            <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Width="350" Height="250"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row uniform 50%">
                        <div class="12u$(3)">
                            <asp:Button ID="btnReject" runat="server" Text="Reject" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                        </div>
                    </div>
                </section>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
