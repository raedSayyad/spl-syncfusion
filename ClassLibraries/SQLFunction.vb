﻿Imports Oracle.DataAccess.Client
Imports System.Data.Odbc

Public Class SQLFunction

    Public Class DataSetRes
        Public DS As New DataSet
        Public ErrorMsg As String
    End Class


    Public Function fillDT(ByVal sqlTxt As String, ByVal cmdtype As CommandType, ByVal ConnStr As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim conn As New OracleConnection
            conn.ConnectionString = ConnStr
            Dim cmd As New OracleCommand
            cmd.CommandType = cmdtype
            cmd.Connection = conn
            cmd.CommandText = sqlTxt
            Dim sqlad As New OracleDataAdapter
            sqlad.SelectCommand = cmd
            sqlad.Fill(dt)
            Return dt
        Catch ex As Exception
            Dim dt As New DataTable
            dt.Columns.Add("error", GetType(String))
            dt.Rows.Add(ex.Message)
            Return dt
        End Try
    End Function

    Public Function fillDT_ODBC(ByVal sqlTxt As String, ByVal cmdtype As CommandType, ByVal ConnStr As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim conn As New OdbcConnection
            conn.ConnectionString = ConnStr
            Dim cmd As New OdbcCommand
            cmd.CommandType = cmdtype
            cmd.Connection = conn
            cmd.CommandText = sqlTxt
            Dim sqlad As New OdbcDataAdapter
            sqlad.SelectCommand = cmd
            sqlad.Fill(dt)
            Return dt
        Catch ex As Exception

        End Try

    End Function

    Public Function exeNonQuery(ByVal query As String, ByVal ConnStr As String) As String
        Try
            Dim conn As New OracleConnection(ConnStr)
            Dim cmd As New OracleCommand
            cmd.CommandText = query
            cmd.CommandType = CommandType.Text
            cmd.Connection = conn
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
            Return "1"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function Get_DataSet(ByVal Query As String, ByVal ConnStr As String) As DataSetRes
        Dim DS As New DataSetRes
        Try

            Dim Con As New OracleConnection
            Dim DA As New OracleDataAdapter
            Con = New OracleConnection(ConnStr) 'Unicorn_Notification.ConStr)
            Con.Open()
            DA = New OracleDataAdapter(Query, Con)
            DA.Fill(DS.DS)
            Con.Close()
            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : QUERY : " & Query)
            'End If
            DS.ErrorMsg = ""
            Return DS
        Catch ex As OracleException
            'Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : ERROR : " & ex.Message)
            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : QUERY : " & Query)
            'End If
            DS.ErrorMsg = ex.Message
            Return DS
        End Try
        Return Nothing
    End Function

    Public Function Get_Info_DataSet(ByVal Query As String, ByVal ConnStr As String) As DataSet
        Dim DS As New DataSet
        Try

            Dim Con As New OracleConnection
            Dim DA As New OracleDataAdapter
            Con = New OracleConnection(ConnStr) 'Unicorn_Notification.ConStr)
            Con.Open()
            DA = New OracleDataAdapter(Query, Con)
            DA.Fill(DS)
            Con.Close()
            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : QUERY : " & Query)
            'End If
            ' DS.ErrorMsg = ""
            Return DS
        Catch ex As OracleException
            'Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : ERROR : " & ex.Message)
            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : QUERY : " & Query)
            'End If
            '   DS.ErrorMsg = ex.Message
            Return Nothing
        End Try
        Return Nothing
    End Function

    Public Function Get_Info_string(ByVal Query As String, ByVal ConnStr As String) As String

        Try
            Dim Str As String = Nothing
            Dim ds As New DataSet
            Dim Con As OracleConnection
            Dim DA As OracleDataAdapter

            Con = New OracleConnection(ConnStr) 'Unicorn_Notification.ConStr)
            DA = New OracleDataAdapter(Query, Con)
            Con.Open()
            DA.Fill(ds)

            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : QUERY : " & Query)
            'End If
            If ds IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Str = ds.Tables(0).Rows(0)(0)
                    If Str IsNot Nothing Then
                        Con.Close()
                        Return Str
                    Else
                        Con.Close()
                        Return Nothing
                    End If
                End If
            End If
            Con.Close()
            Return Nothing
        Catch ex As OracleException
            'Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : ERROR : " & ex.Message)
            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : QUERY : " & Query)
            'End If
            Return Nothing
        End Try
        Return Nothing
    End Function

    Public Function Update_Info(ByVal Query As String, ByVal ConnStr As String) As Boolean
        Try
            Dim Con As OracleConnection
            Dim Com As OracleCommand
            Con = New OracleConnection(ConnStr) 'Unicorn_Notification.ConStr)
            Con.Open()
            'If Unicorn_Notification.Query_Log = "1" Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : " & Query)
            'End If
            Com = New OracleCommand(Query, Con)
            Com.ExecuteNonQuery()
            Con.Close()
            Return True

        Catch ex As OracleException
            'Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : ERROR : " & ex.Message)
            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now.Date & " : QUERY : " & Query)
            'End If
            Return False
        End Try
        Return False
    End Function
End Class
