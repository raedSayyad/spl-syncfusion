﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmContent.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmContent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table>
                        <tr>
                            <td colspan="3">Select Content Template:
                                <ej:DropDownList ID="ddlContentTemp" runat="server" Width="250px">
                                </ej:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="width: 75%;">Arabic Content
                                <ej:RTE ID="RTE1" runat="server" ShowFooter="true" MinHeight="300px" EnableResize="false" MinWidth="20px" AllowEditing="true" Width="100%" Height="400px">
                                    <RTEContent>
                                    </RTEContent>
                                    <Tools Font="fontName,fontSize,fontColor,backgroundColor"
                                        Edit="findAndReplace"
                                        Styles="bold,italic,underline,strikethrough"
                                        Alignment="justifyLeft,justifyCenter,justifyRight,justifyFull"
                                        Lists="unorderedList,orderedList"
                                        Clipboard="cut,copy,paste"
                                        DoAction="undo,redo"
                                        Clear="clearFormat,clearAll"
                                        Links="createLink,removeLink"
                                        Images="image"
                                        Tables="createTable,addRowAbove,addRowBelow,addColumnLeft,addColumnRight,deleteRow,deleteColumn,deleteTable"
                                        Effects="superscript,subscript"
                                        Casing="upperCase,lowerCase"
                                        FormatStyle="format"
                                        View="fullScreen,zoomIn,zoomOut"
                                        Indenting="outdent,indent"
                                        Media="video"
                                        Print="print">
                                    </Tools>
                                </ej:RTE>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="width: 75%;">English Content:
                                <ej:RTE ID="RTE2" runat="server" ShowFooter="true" MinHeight="300px" EnableResize="false" MinWidth="20px" AllowEditing="true" Width="100%" Height="400px">
                                    <RTEContent>
                                    </RTEContent>
                                    <Tools Font="fontName,fontSize,fontColor,backgroundColor"
                                        Edit="findAndReplace"
                                        Styles="bold,italic,underline,strikethrough"
                                        Alignment="justifyLeft,justifyCenter,justifyRight,justifyFull"
                                        Lists="unorderedList,orderedList"
                                        Clipboard="cut,copy,paste"
                                        DoAction="undo,redo"
                                        Clear="clearFormat,clearAll"
                                        Links="createLink,removeLink"
                                        Images="image"
                                        Tables="createTable,addRowAbove,addRowBelow,addColumnLeft,addColumnRight,deleteRow,deleteColumn,deleteTable"
                                        Effects="superscript,subscript"
                                        Casing="upperCase,lowerCase"
                                        FormatStyle="format"
                                        View="fullScreen,zoomIn,zoomOut"
                                        Indenting="outdent,indent"
                                        Media="video"
                                        Print="print">
                                    </Tools>
                                </ej:RTE>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: center;">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" />
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                    <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
