﻿Imports System.Text
Imports System.Security.Cryptography
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Text.RegularExpressions
Imports System.Configuration

Public Class Common
    Public Function EmailValidation(ByVal Email As String) As Boolean
        Try
            Dim pattern As String = "^[a-z][a-z|0-9|]*([_|-][a-z|0-9]+)*([.][a-z|0-9]+([_|-][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$"
            Dim match As Match = Regex.Match(Email.Trim(), pattern, RegexOptions.IgnoreCase)
            If (match.Success) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Fields_Validation(ByVal Field As String) As Boolean
        Try
            If Field Is Nothing Then
                Return False
            Else
                If Field.Trim = "" Then
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ImgExt_Validation(ByVal Field As String) As Boolean
        Try
            Dim ExtArray() As String = ConfigurationSettings.AppSettings("Photo_Ext").Split(",")
            For i As Integer = 0 To ExtArray.Length - 1
                If Field.ToUpper = ExtArray(i).ToUpper Then
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function GetUserDetails(ByVal cmd As String, ByVal Input As String) As String
        Try

            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True

            pProcess.Start()

            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput

            MyStreamWriter.WriteLine(cmd)

            MyStreamWriter.WriteLine(Input.ToString.ToUpper + System.Environment.NewLine)
            pProcess.StandardInput.Close()

            Dim output As String = pProcess.StandardOutput.ReadToEnd()

            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()

            Return output
        Catch ex As Exception
            Return ""
            Logs(Now.Date, ex.Message)
        End Try
    End Function

    Public Function Resize_Img(ByVal img_byte As Byte()) As Byte()
        'Try

        '    Using MemoryStream1 As New System.IO.MemoryStream(img_byte)

        '        Using Bitmap1 As System.Drawing.Bitmap = System.Drawing.Bitmap.FromStream(MemoryStream1)

        '            Dim Width1 As Integer = Bitmap1.Width
        '            Dim Height1 As Integer = Bitmap1.Height

        '            Dim Width2 As Integer = 90
        '            Dim Height2 As Integer = Height1 * Width2 / Width1

        '            Using Bitmap2 As System.Drawing.Bitmap = New System.Drawing.Bitmap(Width2, Height2)

        '                Using Graphics1 As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(Bitmap2)

        '                    Graphics1.DrawImage(Bitmap1, 0, 0, Width2, Height2)

        '                End Using

        '                Using MemoryStream2 As New System.IO.MemoryStream

        '                    Bitmap2.Save(MemoryStream2, System.Drawing.Imaging.ImageFormat.Png)

        '                    Return MemoryStream2.ToArray

        '                End Using

        '            End Using

        '        End Using

        '    End Using

        'Catch ex As Exception
        '    Return Nothing
        '    Logs(Now.Date, ex.Message)
        'End Try
    End Function

    Public Function SaveTextToFile(ByVal strData As String, ByVal FullPath As String) As String

        Dim objReader As StreamWriter
        Try


            objReader = New StreamWriter(FullPath, True, System.Text.Encoding.UTF8)
            objReader.Write(strData)
            objReader.Write(System.Environment.NewLine)
            objReader.Close()
            Return Nothing
        Catch Ex As Exception
            Return Ex.Message
        End Try
    End Function

    Sub DeleteFiles(ByVal FilePath As String, ByVal File_Name As String)
        Try

            If File.Exists(FilePath + File_Name) = True Then
                File.Delete(FilePath + File_Name)
            End If
        Catch ex As Exception
            Logs(Now.Date, ex.Message)
        End Try
    End Sub

    Sub Logs(ByVal Error_Date As String, ByVal Msg As String)

    End Sub

    Public Function Encrypt(ByVal plainText As String, ByVal key As String) As String

        Dim passPhrase As String = key '"D53FF45281B76EEBBF8EA5C513D4387D7743E61FE35E20C762DC412A8657837D7F3408593CDBDA37158ACCDCAACE9C38618C6C92F3439D6EC133585A3693B6ED"
        Dim saltValue As String = "SaltValue$^#"
        Dim hashAlgorithm As String = "SHA1"
        Dim passwordIterations As Integer = 2
        Dim initVector As String = "@1B2c3D4e5F6g7H8"
        Dim keySize As Integer = 256
        Dim initVectorBytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim saltValueBytes As Byte() = Encoding.ASCII.GetBytes(saltValue)
        Dim plainTextBytes As Byte() = Encoding.UTF8.GetBytes(plainText)
        Dim password As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(passPhrase, saltValueBytes, passwordIterations)
        Dim keyBytes As Byte() = password.GetBytes(keySize \ 8)
        Dim symmetricKey As New RijndaelManaged()
        symmetricKey.Mode = CipherMode.CBC
        Dim encryptor As ICryptoTransform = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)
        Dim memoryStream As New MemoryStream()
        Dim cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)
        cryptoStream.FlushFinalBlock()
        Dim cipherTextBytes As Byte() = memoryStream.ToArray()
        memoryStream.Close()
        cryptoStream.Close()
        Dim cipherText As String = Convert.ToBase64String(cipherTextBytes)
        Return cipherText
    End Function

    Public Function Decrypt(ByVal cipherText As String) As String
        Dim passPhrase As String = "D653FF45281B76EEBBF8EA5C513D4387D7743E61FE35E20C962DC412A8657837D7F3408593CDBDA37158ACCDCAACE9C38618C6C92F3439D6EC133585A3693B6ED"
        Dim saltValue As String = "SaltValue$^#"
        Dim hashAlgorithm As String = "SHA1"
        Dim passwordIterations As Integer = 2
        Dim initVector As String = "@1B2c3D4e5F6g7H8"
        Dim keySize As Integer = 256
        Dim initVectorBytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim saltValueBytes As Byte() = Encoding.ASCII.GetBytes(saltValue)
        Dim cipherTextBytes As Byte() = Convert.FromBase64String(cipherText)
        Dim password As Rfc2898DeriveBytes = New Rfc2898DeriveBytes(passPhrase, saltValueBytes, passwordIterations)
        Dim keyBytes As Byte() = password.GetBytes(keySize \ 8)
        Dim symmetricKey As New RijndaelManaged()
        symmetricKey.Mode = CipherMode.CBC
        Dim decryptor As ICryptoTransform = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)
        Dim memoryStream As New MemoryStream(cipherTextBytes)
        Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
        Dim plainTextBytes As Byte() = New Byte(cipherTextBytes.Length - 1) {}
        Dim decryptedByteCount As Integer = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length)
        memoryStream.Close()
        cryptoStream.Close()
        Dim plainText As String = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount)
        Return plainText
    End Function
End Class
