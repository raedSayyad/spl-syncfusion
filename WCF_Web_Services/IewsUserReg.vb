﻿Imports System.ServiceModel
Imports ClassLibraries
Imports ClassLibraries.Users

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IewsUserReg" in both code and config file together.
<ServiceContract()>
Public Interface IewsUserReg

    <OperationContract()>
    Function BasicUserReg(ByVal RequestType As String, ByVal LANG As String, ByVal FullName As String, ByVal Pin As String, ByVal Email As String) As Responce_Msg

    <OperationContract()>
    Function UserPin(ByVal User_ID As String) As UserPin

    <OperationContract()>
    Function UserInfo(ByVal User_ID As String) As UserInfo

    <OperationContract()>
    Function UserExist(ByVal User_ID As String) As UserExist

    <OperationContract()>
    Function UserRegType(ByVal User_ID As String, ByVal Library As String, ByVal Status As String, ByVal RegType As String) As UserRegType

    <OperationContract()>
    Function LoadUserUpgrade(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                             , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                             , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                             , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                             , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                             , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                             ) As Responce_Msg
    <OperationContract()>
    Function LoadUserRenewal(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                             , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                             , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                             , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                             , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                             , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                             ) As Responce_Msg
    <OperationContract()>
    Function LoadUserRemove(ByVal LANG As String, ByVal User_ID As String) As Responce_Msg

    <OperationContract()>
    Function Users_Email_ID(ByVal Email As String, ByVal Pin As String, ByVal Lang As String) As String

    <OperationContract()>
    Function InsUserUpgrade(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                             , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                             , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                             , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                             , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                             , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                             , ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String _
                             ) As Responce_Msg
    <OperationContract()>
    Function InsUserRenewal(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                            , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                            , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                            , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                            , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                            , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                            , ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String _
                            ) As Responce_Msg
    <OperationContract()>
    Function InsUserCancel(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Reason As String _
                            , ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String _
                            ) As Responce_Msg

    <OperationContract()>
    Function SelectUserUpgrade() As List(Of Users.UserUpgRequest)

    <OperationContract()>
    Function SelectUserRenewal() As List(Of Users.UserRenRequest)

    <OperationContract()>
    Function SelectUserCancel() As List(Of Users.UserCancelRequest)


    <OperationContract()>
    Function UpdateUserUpgrade(ByVal ID As String, ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateUpdate As String) As String

    <OperationContract()>
    Function UpdateUserRenewal(ByVal ID As String, ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateUpdate As String) As String

    <OperationContract()>
    Function UpdateUserCancel(ByVal ID As String, ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateUpdate As String) As String

    <OperationContract()>
    Function ForgetPassword(ByVal Email As String) As String

    <OperationContract()>
    Function ReserveUserDetails(ByVal RequestType As String, ByVal User_ID As String, ByVal LIBR As String, ByVal FullName As String, ByVal Title As String, ByVal Company As String, ByVal Nationality As String, _
                                ByVal Mobile As String, ByVal Telephone As String, ByVal Fax As String, ByVal PostBox As String, ByVal Email As String, _
                                ByVal City As String, ByVal MemberShipType As String, ByVal LANG As String) As Responce_Msg

    <OperationContract()>
    Function ReserveUserInfo(ByVal User_ID As String, ByVal LANG As String) As Users.ReseveUserInfo

    <OperationContract()>
    Function UserPayment(ByVal LANG As String, ByVal UserID As String, ByVal Payment_DateTime As String, ByVal Library As String, ByVal Bill_Number As String, ByVal Bill_Amount As String, ByVal Payment_Type As String) As Responce_Msg

    <OperationContract()>
    Function UserBillsMethod(ByVal LANG As String, ByVal UserID As String, ByVal ItemID As String, ByVal Library As String, ByVal Amount As String, ByVal Reason As String) As Responce_Msg

    <OperationContract()>
    Function UserActivate(ByVal Email As String) As String

    <OperationContract()>
    Function GetUserActivate(ByVal Email As String, ByVal ActivationCode As String) As String

    <OperationContract()>
    Function UptUserActivate(ByVal Email As String, ByVal ActivationCode As String) As String

    <OperationContract()>
    Function Users_Email(ByVal UserID As String) As String

    <OperationContract()>
    Function GetActivated(ByVal Email As String) As String

    <OperationContract()>
    Function InsUsersOPT(ByVal User_ID As String, ByVal flag_Email As String, ByVal flag_SMS As String, ByVal flag_Mobile As String, ByVal Status As String, ByVal Reason As String, ByVal OPT As String, ByVal Admin_ID As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String
End Interface
