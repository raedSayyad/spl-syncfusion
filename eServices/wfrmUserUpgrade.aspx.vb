﻿Imports ClassLibraries

Public Class wfrmUserUpgrade
    Inherits System.Web.UI.Page

    Dim gotoURL As String = ""
    Public ddlCityInd As String = "0"
    Public ddlNatInd As String = "0"
    Public ddlTitleInd As String = "0"
    Public ddlLibraryInd As String = "0"
    Public ddlGenderInd As String = "0"
    Public ddlMemberTypeInd As String = "0"
    Public objUserInfo As New Users.UserInfo
    Public objNewUserInfo As New Users.UserInfo
    Public Lang As String
    Public WizHdrClass As String
    Public ChooseLBL As String

    Public academic As String
    Public postgraduate_student As String
    Public public_user As String
    Public special_user As String
    Public undergraduate_student As String
    Public Special_Need As String
    Public PageTitle As String
    Public Row As String
    Public u As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim UserID As String = objUserInfo.User_ID
            Lang = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                WizHdrClass = "AraWizHder"
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                ddlCity.EnableRTL = True
                ddlNat.EnableRTL = True
                ddlTitle.EnableRTL = True
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
                WizHdrClass = "EngWizHder"
                objSymAdminSDH.locale = "en"
            End If

            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            PageTitle = rc.GetString("UpgradeUser", Culture_Used)
            Dim SucMsg As String = rc.GetString("UpgradeUserSuccess", Culture_Used)
            If Lang = "Ara" Then
                gotoURL = "~/wfrmResulte.aspx?TermsID=Usrugd&Source=" & PageTitle & "&Msg=" & SucMsg & "&Lang=Ara"
            Else
                gotoURL = "~/wfrmResulte.aspx?TermsID=Usrugd&Source" & PageTitle & "&Msg=" & SucMsg
            End If

            Dim ReqSent As String = rc.GetString("UpgradeReqSent", Culture_Used)
            Dim ReqApp As String = rc.GetString("UpgradeReqApp", Culture_Used)
            If Not Session("UserInfo") Is Nothing Then
                objUserInfo = Session("UserInfo")
                Dim User_ID As String = objUserInfo.User_ID
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                objUserInfo = objEwsUserClient.UserInfo(User_ID)
                Dim RegType As String = objUserInfo.RegType
                If RegType.Trim = "2" Then
                    Response.Redirect("~/wfrmResulte.aspx?TermsID=Usrugd&Source=" & PageTitle & "&Msg=" & ReqSent)
                ElseIf RegType.Trim = "3" Then
                    Response.Redirect("~/wfrmResulte.aspx?TermsID=Usrugd&Source=" & PageTitle & "&Msg=" & ReqApp)
                End If
            Else
                If Lang = "Ara" Then
                    Response.Redirect("~/wfrmLogin.aspx?Lang=Ara&goto=/uhtbin/cgisirsi.exe/x/x/0/1/32/X")
                Else
                    Response.Redirect("~/wfrmLogin.aspx?goto=/uhtbin/cgisirsi.exe/x/x/0/1/32/X")
                End If
            End If
            If Not Session("NewUserInfo") Is Nothing Then
                objNewUserInfo = Session("NewUserInfo")
            End If


            lblFulName.Text = rc.GetString("FullName", Culture_Used)
            lblDOB.Text = rc.GetString("DOB", Culture_Used)
            lblTitle.Text = rc.GetString("Title", Culture_Used)
            lblCompany.Text = rc.GetString("Company", Culture_Used)
            lblNat.Text = rc.GetString("Nationallity", Culture_Used)
            lblMobile.Text = rc.GetString("Mobile", Culture_Used)
            lblTele.Text = rc.GetString("Tele", Culture_Used)
            lblFax.Text = rc.GetString("Fax", Culture_Used)
            lblPostBox.Text = rc.GetString("POBox", Culture_Used)
            lblCity.Text = rc.GetString("City", Culture_Used)
            btnSubmit.Text = rc.GetString("_Continue", Culture_Used)
            Wizard1.StartNextButtonText = rc.GetString("_Next", Culture_Used)

            Dim StrtNextBTN As Button = TryCast(Wizard1.FindControl("StartNavigationTemplateContainerID").FindControl("btnNext"), Button)
            StrtNextBTN.Text = rc.GetString("_Next", Culture_Used)
            Dim STRTCancelBTN As Button = TryCast(Wizard1.FindControl("StartNavigationTemplateContainerID").FindControl("btnCancel"), Button)
            STRTCancelBTN.Text = rc.GetString("_Cancel", Culture_Used)

            Dim StpNextBTN As Button = TryCast(Wizard1.FindControl("StepNavigationTemplateContainerID").FindControl("btnNext"), Button)
            StpNextBTN.Text = rc.GetString("_Next", Culture_Used)
            Dim StpCancelBTN As Button = TryCast(Wizard1.FindControl("StepNavigationTemplateContainerID").FindControl("btnCancel"), Button)
            StpCancelBTN.Text = rc.GetString("_Cancel", Culture_Used)
            Dim StpPreviousBTN As Button = TryCast(Wizard1.FindControl("StepNavigationTemplateContainerID").FindControl("btnPrevious"), Button)
            StpPreviousBTN.Text = rc.GetString("_Previous", Culture_Used)

            Dim FnshFinishBTN As Button = TryCast(Wizard1.FindControl("FinishNavigationTemplateContainerID").FindControl("btnFinish"), Button)
            FnshFinishBTN.Text = rc.GetString("_Finish", Culture_Used)
            Dim FnshCancelBTN As Button = TryCast(Wizard1.FindControl("FinishNavigationTemplateContainerID").FindControl("btnCancel"), Button)
            FnshCancelBTN.Text = rc.GetString("_Cancel", Culture_Used)
            Dim FnshPreviousBTN As Button = TryCast(Wizard1.FindControl("FinishNavigationTemplateContainerID").FindControl("btnPrevious"), Button)
            FnshPreviousBTN.Text = rc.GetString("_Previous", Culture_Used)

            lblLibrary.Text = rc.GetString("Library", Culture_Used)
            ChooseLBL = rc.GetString("Choose", Culture_Used)

            Dim objMemType As New List(Of MemType)()
            objMemType.Add(New MemType(0, rc.GetString("public_user", Culture_Used)))
            objMemType.Add(New MemType(1, rc.GetString("family", Culture_Used)))
            objMemType.Add(New MemType(2, rc.GetString("academic", Culture_Used)))
            objMemType.Add(New MemType(3, rc.GetString("postgraduate_student", Culture_Used)))
            objMemType.Add(New MemType(4, rc.GetString("undergraduate_student", Culture_Used)))
            objMemType.Add(New MemType(5, rc.GetString("special_user", Culture_Used)))
            objMemType.Add(New MemType(6, rc.GetString("Special_Need", Culture_Used)))
            objMemType.Add(New MemType(7, rc.GetString("Virtual_Membership", Culture_Used))) ' By Yamen (Add the new Membership Type)
            ddlMemType.DataSource = objMemType
            ddlMemType.DataValueField = "id"
            ddlMemType.DataTextField = "text"
            ddlMemType.DataBind()

            lblNatID.Text = rc.GetString("NationalID", Culture_Used)
            lblGender.Text = rc.GetString("Gender", Culture_Used)
            Wizard1.WizardSteps(0).Title = rc.GetString("GeneralInfo", Culture_Used)
            Wizard1.WizardSteps(1).Title = rc.GetString("ContactDetail", Culture_Used)
            Wizard1.WizardSteps(2).Title = rc.GetString("Document", Culture_Used)
            lblWorkPhone.Text = rc.GetString("WorkPhone", Culture_Used)
            lblExt.Text = rc.GetString("Extenstion", Culture_Used)
            lblWebSite.Text = rc.GetString("WebSite", Culture_Used)
            lblPersonalImg.Text = rc.GetString("PersonalPhoto", Culture_Used)
            lblNatID1.Text = rc.GetString("NatIDPhoto1", Culture_Used)
            lblNatID2.Text = rc.GetString("NatIDPhoto2", Culture_Used)
            lblMemType.Text = rc.GetString("MemShipType", Culture_Used)
            lblSNeedImg.Text = rc.GetString("SNeedPhoto", Culture_Used)
            'Page.ClientScript.RegisterStartupScript(Me.GetType(), "Script", "swithWizClass();", True)
            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            objlookupPolicyListReq.policyType = "CAT3"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLIST = New Others.DDLList
                objDDLIST.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLIST.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlist.Add(objDDLIST)
            Next
            objAllDDlist = objAllDDlist.OrderBy(Function(x) x.Text).ToList()
            ddlTitle.DataSource = objAllDDlist 'objOthersClient.LookUp("CAT3", Lang)
            ddlTitle.DataValueField = "Value"
            ddlTitle.DataTextField = "Text"
            ddlTitle.DataBind()

            objlookupPolicyListReq.policyType = "LIBR"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTLib As New Others.DDLList
            Dim objAllDDlistLib As New List(Of Others.DDLList)

            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTLib = New Others.DDLList
                objDDLISTLib.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTLib.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistLib.Add(objDDLISTLib)
            Next
            objAllDDlistLib = objAllDDlistLib.OrderBy(Function(x) x.Text).ToList()
            ddlLibrary.DataSource = objAllDDlistLib ' objOthersClient.LookUp("LIBR", Lang)
            ddlLibrary.DataValueField = "Value"
            ddlLibrary.DataTextField = "Text"
            ddlLibrary.DataBind()

            objlookupPolicyListReq.policyType = "CAT2"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTG As New Others.DDLList
            Dim objAllDDlistG As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTG = New Others.DDLList
                objDDLISTG.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTG.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistG.Add(objDDLISTG)
            Next
            objAllDDlistG = objAllDDlistG.OrderBy(Function(x) x.Text).ToList()
            ddlGender.DataSource = objAllDDlistG
            ddlGender.DataValueField = "Value"
            ddlGender.DataTextField = "Text"
            ddlGender.DataBind()

            objlookupPolicyListReq.policyType = "CAT4"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTN As New Others.DDLList
            Dim objAllDDlistN As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTN = New Others.DDLList
                objDDLISTN.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTN.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistN.Add(objDDLISTN)
            Next
            objAllDDlistN = objAllDDlistN.OrderBy(Function(x) x.Text).ToList()
            ddlNat.DataSource = objAllDDlistN
            ddlNat.DataValueField = "Value"
            ddlNat.DataTextField = "Text"
            ddlNat.DataBind()

            objlookupPolicyListReq.policyType = "CAT5"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTC As New Others.DDLList
            Dim objAllDDlistC As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTC = New Others.DDLList
                objDDLISTC.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTC.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistC.Add(objDDLISTC)
            Next
            objAllDDlistC = objAllDDlistC.OrderBy(Function(x) x.Text).ToList()
            ddlCity.DataSource = objAllDDlistC
            ddlCity.DataValueField = "Value"
            ddlCity.DataTextField = "Text"
            ddlCity.DataBind()

            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            If Not IsPostBack Then
                Wizard1.ActiveStepIndex = 0
                Dim DOB As String
                If Not objUserInfo.DOB.Trim Is Nothing And objUserInfo.DOB.Trim <> "NEVER" Then
                    DOB = objUserInfo.DOB.Trim
                    DOB = DOB.Substring(4, 2) + "/" + DOB.Substring(6, 2) + "/" + DOB.Substring(0, 4)
                    dpDOB.Value = Date.Parse(DOB)
                    'hdfdDOB.Value = Date.Parse(DOB)
                End If
                If objUserInfo.FullName.Trim <> "" Then
                    txtFullName.Text = objUserInfo.FullName
                End If
                If objUserInfo.Employer.Trim <> "" Then
                    txtCompany.Text = objUserInfo.Employer
                End If
                If objUserInfo.NationalID.Trim <> "" Then
                    txtNatID.Text = objUserInfo.NationalID
                End If
                If objUserInfo.Mobile.Trim <> "" Then
                    txtMobile.Text = objUserInfo.Mobile
                End If
                If objUserInfo.HomePhone.Trim <> "" Then
                    txtTele.Text = objUserInfo.HomePhone
                End If
                If objUserInfo.WorkPhone.Trim <> "" Then
                    txtWorkPhone.Text = objUserInfo.WorkPhone
                End If
                If objUserInfo.Ext.Trim <> "" Then
                    txtExt.Text = objUserInfo.Ext
                End If
                If objUserInfo.Fax.Trim <> "" Then
                    txtFax.Text = objUserInfo.Fax
                End If
                If objUserInfo.POBox.Trim <> "" Then
                    txtPostBox.Text = objUserInfo.POBox
                End If
                If objUserInfo.WebSite.Trim <> "" Then
                    txtWebSite.Text = objUserInfo.WebSite
                End If
                If objUserInfo.MemberType.Trim <> "" Then
                    ddlMemberTypeInd = objUserInfo.MemberType
                End If
                If objUserInfo.Library.Trim <> "" Then
                    For c As Integer = 0 To objAllDDlistLib.Count - 1
                        If objAllDDlistLib(c).Value = objUserInfo.Library.Trim Then
                            ddlLibraryInd = CStr(c)
                        End If
                    Next
                End If
                If objUserInfo.Gender.Trim <> "" Then
                    For c As Integer = 0 To objAllDDlistG.Count - 1
                        If objAllDDlistG(c).Value = objUserInfo.Gender.Trim Then
                            ddlGenderInd = CStr(c)
                        End If
                    Next
                End If
                If objUserInfo.JobTitle.Trim <> "" Then
                    For c As Integer = 0 To objAllDDlist.Count - 1
                        If objAllDDlist(c).Value = objUserInfo.JobTitle.Trim Then
                            ddlTitleInd = CStr(c)
                        End If
                    Next
                End If
                If objUserInfo.Nationality.Trim <> "" Then
                    For c As Integer = 0 To objAllDDlistN.Count - 1
                        If objAllDDlistN(c).Value = objUserInfo.Nationality.Trim Then
                            ddlNatInd = CStr(c)
                        End If
                    Next
                End If
                If objUserInfo.City.Trim <> "" Then
                    For c As Integer = 0 To objAllDDlistC.Count - 1
                        If objAllDDlistC(c).Value = objUserInfo.City.Trim Then
                            ddlCityInd = CStr(c)
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            lblError.Text += ex.Message '+ "|" + objUserInfo.Employer.Trim + "|"
        End Try
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim tsDOB As String
            Dim tstsDOBArr1() As String = dpDOB.Value.ToString.Split(" ")
            Dim tstsDOBArr2() As String = tstsDOBArr1(0).Split("/")
            If tstsDOBArr2(1).Length = 1 Then
                tstsDOBArr2(1) = "0" + tstsDOBArr2(1)
            End If
            If tstsDOBArr2(0).Length = 1 Then
                tstsDOBArr2(0) = "0" + tstsDOBArr2(0)
            End If
            tsDOB = tstsDOBArr2(1) + "/" + tstsDOBArr2(0) + "/" + tstsDOBArr2(2) + ":01:00:00AM"

            Dim resMsg As New ClassLibraries.Responce_Msg
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim img As New FileUpload
            Dim imgByte As Byte() = Nothing
            Dim Personal As String

            img = CType(updPersonalImg, FileUpload)
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                Dim File As HttpPostedFile = updPersonalImg.PostedFile
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            Dim imgPersonal As String = System.IO.Path.GetExtension(updPersonalImg.FileName).ToString
            'imgPersonal = imgPersonal.Substring(1, 3)
            resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "1", imgPersonal, imgByte)
            Personal = resMsg.Code

            Dim National1 As String
            img = New FileUpload
            img = CType(updNatID1, FileUpload)
            imgByte = Nothing
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                Dim File As HttpPostedFile = updNatID1.PostedFile
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            Dim imgNational1 As String = System.IO.Path.GetExtension(updNatID1.FileName).ToString
            'imgNational1 = imgNational1.Substring(1, 3)
            resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "2", imgNational1, imgByte)
            National1 = resMsg.Code
            Dim National2 As String
            img = New FileUpload
            img = CType(updNatID2, FileUpload)
            imgByte = Nothing
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                Dim File As HttpPostedFile = updNatID2.PostedFile
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            Dim imgNational2 As String = System.IO.Path.GetExtension(updNatID2.FileName).ToString
            'imgNational2 = imgNational2.Substring(1, 3)
            resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "22", imgNational2, imgByte)
            National2 = resMsg.Code


            Dim SNEEDURL As String
            If ddlMemType.Value = "4" Then
                img = New FileUpload
                img = CType(updSNeedImg, FileUpload)
                imgByte = Nothing
                If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                    Dim File As HttpPostedFile = updSNeedImg.PostedFile
                    imgByte = New Byte(File.ContentLength - 1) {}
                    File.InputStream.Read(imgByte, 0, File.ContentLength)
                End If
                Dim imgSNeed As String = System.IO.Path.GetExtension(updSNeedImg.FileName).ToString
                'imgSNeed = imgSNeed.Substring(1, 3)
                resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "3", imgSNeed, imgByte)
                SNEEDURL = resMsg.Code
            Else
                SNEEDURL = " "
            End If

            Dim objResponse_Msg As New Responce_Msg
            Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
            objResponse_Msg = objewsUserRegClient.InsUserUpgrade("1", "Ara", objUserInfo.User_ID, ddlLibrary.Value, txtFullName.Text, tsDOB, txtMobile.Text, ddlCity.Value, ddlMemType.Value, ddlNat.Value, ddlTitle.Value, txtNatID.Text, SNEEDURL, txtCompany.Text, ddlGender.Value, National1, National2, Personal, txtTele.Text, txtWorkPhone.Text, txtExt.Text, txtPostBox.Text, txtFax.Text, txtWebSite.Text, "1", " ", " ", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
            Dim objUserRegType As New Users.UserRegType
            If objResponse_Msg.Msg = "1" Then
                objUserRegType = objewsUserRegClient.UserRegType(objUserInfo.User_ID, ConfigurationManager.AppSettings("DEFAULT_LIBR"), "OK", "2")
                If objUserRegType.Execute = True Then
                    Response.Redirect(gotoURL)
                    'lblError.Text = "Request Sent Successfully"
                Else
                    lblError.Text += objUserRegType.objException.msg + " || "
                End If
            Else
                lblError.Text += objResponse_Msg.Msg
            End If

        Catch ex As Exception
            lblError.Text = ex.Message '+ " || " + ex.StackTrace
        End Try
    End Sub

    'Protected Sub ddlMemType_ValueSelect(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs)
    '    Try
    '        HiddenField1.Value = e.Value
    '        lblError.Text += "DDLMEM exe"
    '        If Not ddlMemType.Value Is Nothing Then
    '            If ddlMemType.Value = "3" Then
    '                updSNeedImg.Enabled = True
    '                updSNeedImg.Visible = True
    '                RequiredFieldValidator14.Enabled = True
    '                lblSNeedUpl.Attributes.Remove("style")
    '                lblSNeedUpl.Attributes.Add("style", "display: none;")
    '                lblSNeedImg.Visible = True
    '            Else
    '                lblSNeedImg.Visible = False
    '                lblSNeedUpl.Attributes.Remove("style")
    '                'lblSNeedUpl.Attributes.Add("class", "file-upload")
    '                updSNeedImg.Enabled = False
    '                updSNeedImg.Visible = False
    '                RequiredFieldValidator14.Enabled = False
    '            End If
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub Wizard1_FinishButtonClick(sender As Object, e As WizardNavigationEventArgs) Handles Wizard1.FinishButtonClick
        Try
            Dim resMsg As New ClassLibraries.Responce_Msg
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim img As New FileUpload
            Dim imgByte As Byte() = Nothing
            Dim Personal As String

            img = CType(updPersonalImg, FileUpload)
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                Dim File As HttpPostedFile = updPersonalImg.PostedFile
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            Dim imgPersonal As String = System.IO.Path.GetExtension(updPersonalImg.FileName).ToString
            'imgPersonal = imgPersonal.Substring(1, 3)
            resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "1", imgPersonal, imgByte)
            Personal = resMsg.Code

            Dim National1 As String
            img = New FileUpload
            img = CType(updNatID1, FileUpload)
            imgByte = Nothing
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                Dim File As HttpPostedFile = updNatID1.PostedFile
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            Dim imgNational1 As String = System.IO.Path.GetExtension(updNatID1.FileName).ToString
            'imgNational1 = imgNational1.Substring(1, 3)
            resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "2", imgNational1, imgByte)
            National1 = resMsg.Code
            Dim National2 As String
            img = New FileUpload
            img = CType(updNatID2, FileUpload)
            imgByte = Nothing
            If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                Dim File As HttpPostedFile = updNatID2.PostedFile
                imgByte = New Byte(File.ContentLength - 1) {}
                File.InputStream.Read(imgByte, 0, File.ContentLength)
            End If
            Dim imgNational2 As String = System.IO.Path.GetExtension(updNatID2.FileName).ToString
            'imgNational2 = imgNational2.Substring(1, 3)
            resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "22", imgNational2, imgByte)
            National2 = resMsg.Code


            Dim SNEEDURL As String
            If ddlMemType.Value = "2" Or ddlMemType.Value = "3" Or ddlMemType.Value = "4" Or ddlMemType.Value = "6" Then
                img = New FileUpload
                img = CType(updSNeedImg, FileUpload)
                imgByte = Nothing
                If img.HasFile AndAlso Not img.PostedFile Is Nothing Then
                    Dim File As HttpPostedFile = updSNeedImg.PostedFile
                    imgByte = New Byte(File.ContentLength - 1) {}
                    File.InputStream.Read(imgByte, 0, File.ContentLength)
                End If
                Dim imgSNeed As String = System.IO.Path.GetExtension(updSNeedImg.FileName).ToString
                'imgSNeed = imgSNeed.Substring(1, 3)
                resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "3", imgSNeed, imgByte)
                SNEEDURL = resMsg.Code
            Else
                SNEEDURL = " "
            End If

            Dim objResponse_Msg As New Responce_Msg
            Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
            objResponse_Msg = objewsUserRegClient.InsUserUpgrade("1", "Ara", objUserInfo.User_ID, ddlLibrary.Value, objNewUserInfo.FullName, _
                                                                 objNewUserInfo.DOB, objNewUserInfo.Mobile, objNewUserInfo.City, ddlMemType.Value, _
                                                                 objNewUserInfo.Nationality, objNewUserInfo.JobTitle, objNewUserInfo.NationalID, SNEEDURL, _
                                                                 objNewUserInfo.Employer, objNewUserInfo.Gender, National1, National2, Personal, _
                                                                 objNewUserInfo.HomePhone, objNewUserInfo.WorkPhone, objNewUserInfo.Ext, objNewUserInfo.POBox, _
                                                                 objNewUserInfo.Fax, objNewUserInfo.WebSite, "1", " ", " ", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
            Dim objUserRegType As New Users.UserRegType
            If objResponse_Msg.Msg = "1" Then
                objUserRegType = objewsUserRegClient.UserRegType(objUserInfo.User_ID, ConfigurationManager.AppSettings("DEFAULT_LIBR"), "OK", "2")
                If objUserRegType.Execute = True Then
                    objewsUserRegClient.InsUsersOPT(objUserInfo.User_ID, "0", "0", "0", "2", " ", "6", objUserInfo.User_ID, Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                    Response.Redirect(gotoURL)
                    'lblError.Text = "Request Sent Successfully"
                Else
                    lblError.Text += objUserRegType.objException.msg + " || "
                End If
            Else
                lblError.Text += objResponse_Msg.Msg
            End If
        Catch ex As Exception
            lblError.Text += ex.Message
        End Try
    End Sub

    Private Sub Wizard1_NextButtonClick(sender As Object, e As WizardNavigationEventArgs) Handles Wizard1.NextButtonClick
        Try
            'objNewUserInfo = New Users.UserInfo
            If e.NextStepIndex = 1 Then
                Dim tsDOB As String
                Dim tstsDOBArr1() As String = dpDOB.Value.ToString.Split(" ")
                Dim tstsDOBArr2() As String = tstsDOBArr1(0).Split("/")
                If tstsDOBArr2(1).Length = 1 Then
                    tstsDOBArr2(1) = "0" + tstsDOBArr2(1)
                End If
                If tstsDOBArr2(0).Length = 1 Then
                    tstsDOBArr2(0) = "0" + tstsDOBArr2(0)
                End If
                tsDOB = tstsDOBArr2(1) + "/" + tstsDOBArr2(0) + "/" + tstsDOBArr2(2) + ":01:00:00AM"

                objNewUserInfo.FullName = txtFullName.Text
                objNewUserInfo.DOB = tsDOB
                objNewUserInfo.Gender = ddlGender.Value
                objNewUserInfo.Nationality = ddlNat.Value
                objNewUserInfo.NationalID = txtNatID.Text
                objNewUserInfo.JobTitle = ddlTitle.Value
                objNewUserInfo.Employer = txtCompany.Text

                Session("NewUserInfo") = objNewUserInfo
            ElseIf e.NextStepIndex = 2 Then
                objNewUserInfo.Mobile = txtMobile.Text
                objNewUserInfo.HomePhone = txtTele.Text
                objNewUserInfo.WorkPhone = txtWorkPhone.Text
                objNewUserInfo.Ext = txtExt.Text
                objNewUserInfo.Fax = txtFax.Text
                objNewUserInfo.POBox = txtPostBox.Text
                objNewUserInfo.WebSite = txtWebSite.Text
                objNewUserInfo.City = ddlCity.Value

                Session("NewUserInfo") = objNewUserInfo
            End If
            If Not IsValid Then
                e.Cancel = True
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    'Protected Sub FileUploadComplete(ByVal sender As Object, ByVal e As EventArgs)
    '    Try
    '        Dim resMsg As New ClassLibraries.Responce_Msg
    '        Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
    '        'Dim img As New FileUpload
    '        Dim imgByte As Byte() = Nothing
    '        'Dim Personal As String
    '        'img = CType(updPersonalImg, FileUpload)
    '        If updPersonalImg.HasFile AndAlso Not updPersonalImg.PostedFile Is Nothing Then
    '            Dim File As HttpPostedFile = updPersonalImg.PostedFile
    '            imgByte = New Byte(File.ContentLength - 1) {}
    '            File.InputStream.Read(imgByte, 0, File.ContentLength)
    '        End If
    '        Dim imgPersonal As String = System.IO.Path.GetExtension(updPersonalImg.FileName).ToString
    '        imgPersonal = imgPersonal.Substring(1, 3)
    '        resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "2", imgPersonal, imgByte)
    '        ' updPersonalImg.SaveAs(Server.MapPath("Images/" + "llkkjjdduu.jpg"))
    '        hfdPersonalImg.Value = resMsg.Code
    '        lblPersonalImg.Text = resMsg.Code
    '    Catch ex As Exception
    '        lblPersonalImg.Text = ex.Message
    '    End Try
    'End Sub

    'Protected Sub Upload1_Load(sender As Object, e As EventArgs)
    '    If Not Page.IsPostBack Then
    '        lblPersonalImg.Text = "Loaded"
    '        '  Me.EventLog.InnerHtml = "<span class='eventTitle'>UploadBox Created</span>." & vbCr & vbLf & "<br/>" + Me.EventLog.InnerHtml
    '    End If
    'End Sub
    'Protected Sub Upload1_Complete(sender As Object, e As Syncfusion.JavaScript.Web.UploadBoxCompleteEventArgs)
    '    '    Me.EventLog.InnerHtml = "<span class='eventTitle'>Files Successfully uploaded</span>." & vbCr & vbLf & "<br/>" + Me.EventLog.InnerHtml
    '    Try
    '        Dim resMsg As New ClassLibraries.Responce_Msg
    '        Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
    '        Dim imgByte As Byte() = Nothing
    '        If Upload1.HasFiles AndAlso Not Upload1.PostedFiles Is Nothing Then
    '            Dim File As HttpPostedFile = Upload1.PostedFiles(0)
    '            imgByte = New Byte(File.ContentLength - 1) {}
    '            File.InputStream.Read(imgByte, 0, File.ContentLength)
    '        End If
    '        Dim imgPersonal As String = System.IO.Path.GetExtension(Upload1.CustomFileDetails.Name).ToString
    '        imgPersonal = imgPersonal.Substring(1, 3)
    '        resMsg = objEwsOtherClient.ImageUpload(objUserInfo.User_ID, "2", imgPersonal, imgByte)
    '        hfdPersonalImg.Value = resMsg.Code
    '        lblPersonalImg.Text = resMsg.Code
    '    Catch ex As Exception
    '        lblPersonalImg.Text = ex.Message
    '    End Try

    'End Sub

    'Public Enum WizardNavigationTempContainer
    '    StartNavigationTemplateContainerID = 1
    '    StepNavigationTemplateContainerID = 2
    '    FinishNavigationTemplateContainerID = 3
    'End Enum
    'Private Function GetControlFromWizard(wizard As Wizard, wzdTemplate As WizardNavigationTempContainer, controlName As String) As Control
    '    Dim strCtrl As New System.Text.StringBuilder()
    '    strCtrl.Append(wzdTemplate)
    '    strCtrl.Append("$")
    '    strCtrl.Append(controlName)

    '    Return wizard.FindControl(strCtrl.ToString())
    'End Function

    Protected Sub Wizard1_PreRender(sender As Object, e As EventArgs) Handles Wizard1.PreRender
        Dim SideBarList As Repeater = TryCast(Wizard1.FindControl("HeaderContainer").FindControl("SideBarList"), Repeater)
        SideBarList.DataSource = Wizard1.WizardSteps
        SideBarList.DataBind()
    End Sub

    Class MemType
        Public Property text() As String
            Get
                Return m_text
            End Get
            Set(value As String)
                m_text = value
            End Set
        End Property
        Private m_text As String
        Public Property id() As Integer
            Get
                Return m_id
            End Get
            Set(value As Integer)
                m_id = value
            End Set
        End Property
        Private m_id As Integer
        Public Sub New(id As Integer, txt As String)
            Me.id = id
            Me.text = txt
        End Sub
    End Class

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Function GetClassForWizardStep(wizardStep As Object) As String
        Dim [step] As WizardStep = TryCast(wizardStep, WizardStep)

        If [step] Is Nothing Then
            Return ""
        End If
        Dim stepIndex As Integer = Wizard1.WizardSteps.IndexOf([step])

        If stepIndex < Wizard1.ActiveStepIndex Then
            Return "prevStep"
        ElseIf stepIndex > Wizard1.ActiveStepIndex Then
            Return "nextStep"
        Else
            Return "currentStep"
        End If
    End Function

    Protected Sub dpDOB_Select(sender As Object, e As Syncfusion.JavaScript.Web.DatePickerSelectEventArgs)
        Try
            hdfdDOB.Value = e.Value
            lblError.Text += "DOB exe"
        Catch ex As Exception
            lblError.Text += ex.Message
        End Try
    End Sub
End Class