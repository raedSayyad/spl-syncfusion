﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmVisitReq
    Inherits System.Web.UI.Page

    Public Detail As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim objAllVisitRequest As New List(Of Others.ReserveVisit)
            Dim objVisitRequest As New Others.ReserveVisit
            'lblError.Text += CStr(objEwsOtherClient.GetAllReserveVisit().Count)
            For i As Integer = 0 To objEwsOtherClient.GetAllReserveVisit().Count - 1
                objVisitRequest = New Others.ReserveVisit
                objVisitRequest = objEwsOtherClient.GetAllReserveVisit()(i)
                objAllVisitRequest.Add(objVisitRequest)
            Next
            VisitReqGrid.DataSource = objAllVisitRequest
            VisitReqGrid.DataBind()
            'lblError.Text += " Page 1 "
            Detail = "Detail"
        Catch ex As Exception
            lblError.Text = "Page Error: " + ex.Message
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim objAllVisitRequest As New Others.ReserveVisit
            For i As Integer = 0 To objEwsOtherClient.GetAllReserveVisit().Count - 1
                If i = CInt(selectedIndex) Then
                    objAllVisitRequest = New Others.ReserveVisit
                    objAllVisitRequest = objEwsOtherClient.GetAllReserveVisit()(i)
                End If
            Next
            Dim objUserInfo As New Users.UserInfo
            Dim objewUserReg As New ewsUserReg.IewsUserRegClient
            objUserInfo = objewUserReg.UserInfo(objAllVisitRequest.User_ID)
            Session("VisitUserInfo") = objUserInfo
            Session("VisitRequest") = objAllVisitRequest
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmVisitReqDetail.aspx?Lang=Ara&Mode=Edit")
            Else
                Response.Redirect("~/wfrmVisitReqDetail.aspx?Mode=Edit")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class