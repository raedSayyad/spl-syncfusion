﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmUserHallReservation
    Inherits System.Web.UI.Page
    Public objUserInfo As New Users.UserInfo
    Dim UserID As String '= "11523"
    Public Detail As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Session("UserInfo") Is Nothing Then
                objUserInfo = Session("UserInfo")
                Dim User_ID As String = objUserInfo.User_ID
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                objUserInfo = objEwsUserClient.UserInfo(User_ID)
            Else
                Response.Redirect("~/wfrmLogin.aspx")
            End If
            UserID = objUserInfo.User_ID
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
            Dim objAllUserHallReservation As New List(Of Others.ReserveHall)
            Dim objUserHallReservation As New Others.ReserveHall

            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            objlookupPolicyListReq.policyType = "LIBR"

            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                objSymAdminSDH.locale = "ar"
                HallsGrid.EnableRTL = True
            Else
                objSymAdminSDH.locale = "en"
            End If

            HallsGrid.Columns(2).HeaderText = rc.GetString("Library", Culture_Used)
            HallsGrid.Columns(3).HeaderText = rc.GetString("Purpose", Culture_Used)
            HallsGrid.Columns(4).HeaderText = rc.GetString("LectureType", Culture_Used)
            HallsGrid.Columns(5).HeaderText = rc.GetString("Attendence", Culture_Used)
            HallsGrid.Columns(10).HeaderText = rc.GetString("DateFrom", Culture_Used)
            HallsGrid.Columns(11).HeaderText = rc.GetString("DateTo", Culture_Used)
            HallsGrid.Columns(12).HeaderText = rc.GetString("Status", Culture_Used)
            HallsGrid.Columns(16).HeaderText = rc.GetString("Detail", Culture_Used)
            Detail = rc.GetString("Detail", Culture_Used)
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            For i As Integer = 0 To objEwsOthersClient.GetAllReserveHallByUserID(UserID).Count - 1
                objUserHallReservation = New Others.ReserveHall
                objUserHallReservation = objEwsOthersClient.GetAllReserveHallByUserID(UserID)(i)
                If objUserHallReservation.Status = "0" Then
                    objUserHallReservation.Status = rc.GetString("_New", Culture_Used)
                ElseIf objUserHallReservation.Status = "1" Then
                    objUserHallReservation.Status = rc.GetString("Updated", Culture_Used)
                ElseIf objUserHallReservation.Status = "2" Then
                    objUserHallReservation.Status = rc.GetString("Approved", Culture_Used)
                ElseIf objUserHallReservation.Status = "3" Then
                    objUserHallReservation.Status = rc.GetString("Rejected", Culture_Used)
                ElseIf objUserHallReservation.Status = "4" Then
                    objUserHallReservation.Status = rc.GetString("Canceled", Culture_Used)
                End If
                For x As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                    If objUserHallReservation.Library = objlookupPolicyInfoRes(x).policyID Then
                        objUserHallReservation.Library = objlookupPolicyInfoRes(x).policyDescription
                    End If
                Next
                objAllUserHallReservation.Add(objUserHallReservation)
            Next
            HallsGrid.DataSource = objAllUserHallReservation
            HallsGrid.DataBind()

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString
            Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
            Dim objUserHallReservation As New Others.ReserveHall
            For i As Integer = 0 To objEwsOthersClient.GetAllReserveHallByUserID(UserID).Count - 1
                If i = CInt(selectedIndex) Then
                    objUserHallReservation = New Others.ReserveHall
                    objUserHallReservation = objEwsOthersClient.GetAllReserveHallByUserID(UserID)(i)
                End If
            Next
            Session("UserHallReservation") = objUserHallReservation
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmHallReservation.aspx?Mode=Edit&Lang=Ara")
            Else
                Response.Redirect("~/wfrmHallReservation.aspx?Mode=Edit")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class