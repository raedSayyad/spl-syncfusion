﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmPaymentResponse.aspx.vb" Inherits="eServices.wfrmPaymentResponse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%=PageTitle%></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div style="width: 100%; height: 100%!important">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <h3><%=PageTitle%></h3>
                        <hr />
                        <section>
                            <div class="row uniform 50%">
                                <div class="4u 12u$(3)">
                                    <asp:Label ID="lblDepAmount" runat="server" Text="Library Amount"></asp:Label>
                                    <asp:TextBox ID="DepAmount" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="4u 12u$(3)">
                                    <asp:Label ID="lblTP_ExtraFees" runat="server" Text="Extra Fees"></asp:Label>
                                    <asp:TextBox ID="TP_ExtraFees" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="4u$ 12u$(3)">
                                    <asp:Label ID="lblTP_Amount" runat="server" Text="Total Amount"></asp:Label>
                                    <asp:TextBox ID="TP_Amount" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="4u 12u$(3)">
                                    <asp:Label ID="lblTP_PaymentDate" runat="server" Text="Payment Date"></asp:Label>
                                    <asp:TextBox ID="TP_PaymentDate" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="4u 12u$(3)">
                                    <asp:Label ID="lblTP_PayMethod" runat="server" Text="Payment Method"></asp:Label>
                                    <asp:TextBox ID="TP_PayMethod" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="4u$ 12u$(3)">
                                    <asp:Label ID="lblTP_ReceiptNo" runat="server" Text="Receipt Number"></asp:Label>
                                    <asp:TextBox ID="TP_ReceiptNo" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="4u 12u$(3)">
                                    <asp:Label ID="lblTP_RefNo" runat="server" Text="Reference Number"></asp:Label>
                                    <asp:TextBox ID="TP_RefNo" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="4u 12u$(3)">
                                    <asp:Label ID="lblTP_ResultCode" runat="server" Text="Result"></asp:Label>
                                    <asp:TextBox ID="TP_ResultCode" runat="server" ReadOnly="true" TextMode="MultiLine" Height="300px"></asp:TextBox>
                                    <asp:HiddenField ID="hdTP_ResultCode" runat="server"/>
                                </div>
                                <div class="4u$ 12u$(3)">
                                    <asp:HiddenField ID="TP_SecHash" runat="server" />
                                </div>
                                <div class="4u$ 12u$(3)">
                                    <asp:Button ID="btnContinue" runat="server" Text="Continue" />
                                </div>
                                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                            </div>
                        </section>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                    <ProgressTemplate>
                        <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                            <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
        </div>
    </form>
</body>
</html>
