﻿Imports System.IO
Imports ClassLibraries
Imports ClassLibraries.Users
Imports ClassLibraries.SQLFunction

' NOTE: You can use the "Rename" command on the context menu to change the class name "ewsUserReg" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ewsUserReg.svc or ewsUserReg.svc.vb at the Solution Explorer and start debugging.
Public Class ewsUserReg
    Implements IewsUserReg

    Dim Common_Obj As New Common
    Dim FileName_Users As String = ""
    Dim User_ID As String = ""
    Dim Obj_EncDec As New EncDec
    Dim Obj_Internet As New InternetOperation

    Public Function BasicUserReg(ByVal RequestType As String, ByVal LANG As String, ByVal FullName As String, ByVal Pin As String, ByVal Email As String) As Responce_Msg Implements IewsUserReg.BasicUserReg
        Try

            Dim Res As Responce_Msg = New Responce_Msg
            Dim IsUser_Exist As Boolean = False
            Dim Alt_User_ID As String = ""
            User_ID = ""
            Alt_User_ID = Email.ToUpper
            FileName_Users = ""
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If LANG = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If
            '//////////////////////Call to check if User ALT ID Already Exist or not ?!!///////////////////////////////////////////////////
            Dim pProcess_Alt_ID As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess_Alt_ID.StartInfo.FileName = "cmd.exe"
            pProcess_Alt_ID.StartInfo.UseShellExecute = False
            pProcess_Alt_ID.StartInfo.RedirectStandardOutput = True
            pProcess_Alt_ID.StartInfo.RedirectStandardInput = True
            pProcess_Alt_ID.StartInfo.RedirectStandardError = True
            pProcess_Alt_ID.StartInfo.CreateNoWindow = True
            pProcess_Alt_ID.Start()
            pProcess_Alt_ID.StandardInput.AutoFlush = True
            Dim MyStreamWriter_Alt_ID As StreamWriter = pProcess_Alt_ID.StandardInput
            MyStreamWriter_Alt_ID.WriteLine("D:\SIRSI\Unicorn\Bin\seluser.exe -iE")
            MyStreamWriter_Alt_ID.WriteLine(Alt_User_ID.ToUpper + System.Environment.NewLine)
            pProcess_Alt_ID.StandardInput.Close()
            Dim output_Alt_ID As String = pProcess_Alt_ID.StandardOutput.ReadToEnd()
            Dim Output_Error_Alt_ID As String = pProcess_Alt_ID.StandardError.ReadToEnd()
            If output_Alt_ID.Contains("|") = True Then
                If FileName_Users <> "" Then
                    Common_Obj.DeleteFiles("", FileName_Users)
                End If
                IsUser_Exist = True
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserAlreadyExist", Culture_Used)
                Res.UserId = ""
                Return Res
            Else
                IsUser_Exist = False
            End If
            '//////////////////////Get User SEQ !!///////////////////////////////////////////////////
            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True
            pProcess.Start()
            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput
            MyStreamWriter.WriteLine("D:\SIRSI\Unicorn\Bin\sirsisql.exe")
            MyStreamWriter.WriteLine(ConfigurationManager.AppSettings("User_Key_Q") + System.Environment.NewLine)
            pProcess.StandardInput.Close()
            Dim output As String = pProcess.StandardOutput.ReadLine
            output = pProcess.StandardOutput.ReadLine
            output = pProcess.StandardOutput.ReadLine
            output = pProcess.StandardOutput.ReadLine
            output = pProcess.StandardOutput.ReadLine
            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()
            If output.Contains("|") = True Then
                User_ID = output.Replace("|", "")
                User_ID = User_ID + 1
            Else
                If FileName_Users <> "" Then
                    Common_Obj.DeleteFiles("", FileName_Users)
                End If
                IsUser_Exist = True
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserReg_Error", Culture_Used) + Output_Error
                Res.UserId = ""
                Return Res
            End If
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            If Common_Obj.Fields_Validation(RequestType) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_RequestType_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            End If

            If Common_Obj.Fields_Validation(FullName) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_FullName_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            End If
            If Common_Obj.Fields_Validation(Email) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Email_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            ElseIf Email.Trim <> "" Then
                If Common_Obj.EmailValidation(Email) = False Then
                    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Email_Invalid_Syntax", Culture_Used)
                    Res.UserId = ""
                    Return Res
                End If
            End If
            '//////////////////////Construct User File/////////////////////////////////////////////////////////////////////////////////
            FileName_Users = Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & "_" & User_ID & "_" & "User" & ".txt"
            FileName_Users = ConfigurationManager.AppSettings("Files_Path_P") + FileName_Users
            Common_Obj.SaveTextToFile("*** DOCUMENT BOUNDARY ***", FileName_Users)
            Common_Obj.SaveTextToFile("FORM=LDUSER", FileName_Users)
            Dim USERID As String = ".USER_ID.   |a" & User_ID.Trim.ToUpper
            Common_Obj.SaveTextToFile(USERID, FileName_Users)
            Dim USER_ALT_ID As String = ConfigurationManager.AppSettings("USER_ALT_ID") & "   |a" & Alt_User_ID.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_ALT_ID, FileName_Users)
            Dim USER_NAME As String = ConfigurationManager.AppSettings("FullName") & "   |a" & FullName.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_NAME, FileName_Users)
            Dim USER_PIN As String = ".USER_PIN.   |a" & Pin.ToUpper
            Common_Obj.SaveTextToFile(USER_PIN, FileName_Users)
            Dim USER_STATUS As String = ".USER_STATUS.   |aBARRED"
            Common_Obj.SaveTextToFile(USER_STATUS, FileName_Users)
            Dim USER_PROFILE As String = ConfigurationManager.AppSettings("Profile") & "   |a" & ConfigurationManager.AppSettings("BR_Profile")
            Common_Obj.SaveTextToFile(USER_PROFILE, FileName_Users)
            Dim USER_PRIV_GRANTED As String = ".USER_PRIV_GRANTED.   |a" & Now.Year & Now.Month & Now.Day
            Common_Obj.SaveTextToFile(USER_PRIV_GRANTED, FileName_Users)
            Dim USER_PRIV_EXPIRES As String = ".USER_PRIV_EXPIRES.   |aNEVER" '& (Now.Year + 10) & Now.Month & Now.Day
            Common_Obj.SaveTextToFile(USER_PRIV_EXPIRES, FileName_Users)
            Dim USER_ADDR1_BEGIN As String = ".USER_ADDR1_BEGIN."
            Common_Obj.SaveTextToFile(USER_ADDR1_BEGIN, FileName_Users)
            Dim REG_TYPE As String = ConfigurationManager.AppSettings("REG_TYPE") & "   |a1"
            Common_Obj.SaveTextToFile(REG_TYPE, FileName_Users)
            If Email IsNot Nothing Then
                Dim USER_Email As String = ConfigurationManager.AppSettings("Email") & "   |a" & Email.ToString
                Common_Obj.SaveTextToFile(USER_Email, FileName_Users)
            End If
            If RequestType IsNot Nothing Then
                Dim USER_RequestType As String = ConfigurationManager.AppSettings("RequestType") & "   |a" & RequestType.ToString
                Common_Obj.SaveTextToFile(USER_RequestType, FileName_Users)
            End If
            If IsUser_Exist = False Then
                Dim USER_ADDR1_END As String = ".USER_ADDR1_END."
                Common_Obj.SaveTextToFile(USER_ADDR1_END, FileName_Users)
                '/////////////////////////////////// Start Loading Users///////////////////////////////////
                Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
                pProcess_Users.StartInfo.FileName = "cmd.exe"
                pProcess_Users.StartInfo.WorkingDirectory = "D:\SIRSI\Unicorn\Bin\"
                pProcess_Users.StartInfo.UseShellExecute = False
                pProcess_Users.StartInfo.RedirectStandardOutput = True
                pProcess_Users.StartInfo.RedirectStandardInput = True
                pProcess_Users.StartInfo.RedirectStandardError = True
                pProcess_Users.StartInfo.CreateNoWindow = True
                pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
                pProcess_Users.Start()
                Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput
                MyStreamWriter_Users.WriteLine("loadflatuser.exe -n -mc -l""ADMIN|TTY0"" -y" & ConfigurationManager.AppSettings("DEFAULT_LIBR").ToUpper & " < """ & FileName_Users & """")
                pProcess_Users.StandardInput.Close()
                Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()
                If Output_UserReg.Contains("1 $<new> $<user> ") = True Then
                    If FileName_Users <> "" Then
                        'Common_Obj.DeleteFiles("", FileName_Users)
                    End If
                    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserReg_Success", Culture_Used)
                    Res.UserId = User_ID
                    Return Res
                Else
                    If FileName_Users <> "" Then
                        'Common_Obj.DeleteFiles("", FileName_Users)
                    End If
                    If Output_UserReg.Contains("**") = True Then
                        Dim Start_Index As Double = Output_UserReg.IndexOf("**")
                        Dim Last_Index As Double = 0
                        If Output_UserReg.Contains("*** DOCUMENT BOUNDARY ***") = True Then
                            Last_Index = Output_UserReg.IndexOf("*** DOCUMENT BOUNDARY ***")
                        End If
                        If Last_Index <> 0 Then
                            Res.Msg = Output_UserReg.Substring(Start_Index, Last_Index - Start_Index)
                        Else
                            Res.Msg = Output_UserReg
                        End If
                        Res.UserId = ""
                        Common_Obj.Logs(Date.Now, Res.Msg)
                        Return Res
                    End If
                    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserReg_Error", Culture_Used) + Output_UserReg
                    Res.UserId = ""
                    Common_Obj.Logs(Date.Now, Output_UserReg)
                    Return Res
                End If
            End If
            Common_Obj.Logs(Date.Now, "User : " & User_ID & " :Basic Registration Error : End of File.")
            Res.Msg = "Error : End of File."
            Res.UserId = ""
            Return Res
        Catch ex As Exception
            If FileName_Users <> "" Then
                'Common_Obj.DeleteFiles("", FileName_Users)
            End If
            Common_Obj.Logs(Date.Now, "User : " & User_ID & " :Basic Registration Error : " & ex.Message)
            Dim Res As Responce_Msg = New Responce_Msg
            Res.Msg = ex.Message & " - Error Line : " & ex.StackTrace()
            Res.UserId = ""
            Return Res
        End Try
    End Function

    Public Function UserInfo(ByVal User_ID As String) As UserInfo Implements IewsUserReg.UserInfo
        Dim objUserInfo As New UserInfo
        Try
            Dim output_userexit As String = Common_Obj.GetUserDetails("D:\SIRSI\Unicorn\Bin\seluser.exe -iB", User_ID)
            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True
            pProcess.Start()
            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput
            MyStreamWriter.WriteLine("echo " & User_ID & "|seluser -iB|dumpflatuser")
            MyStreamWriter.WriteLine(System.Environment.NewLine)
            pProcess.StandardInput.Close()
            Dim output As String = pProcess.StandardOutput.ReadToEnd
            Dim output_array() As String = output.Split(System.Environment.NewLine)
            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()
            objUserInfo.Library = ""
            objUserInfo.Email = ""
            objUserInfo.FullName = ""
            objUserInfo.Profile = ""
            objUserInfo.DOB = ""
            objUserInfo.MemberType = ""
            objUserInfo.Nationality = ""
            objUserInfo.JobTitle = ""
            objUserInfo.NationalID = ""
            objUserInfo.SNeeds = ""
            objUserInfo.Employer = ""
            objUserInfo.City = ""
            objUserInfo.Gender = ""
            objUserInfo.NationalIDPhoto1 = ""
            objUserInfo.NationalIDPhoto2 = ""
            objUserInfo.PersonalPhoto = ""
            objUserInfo.HomePhone = ""
            objUserInfo.WorkPhone = ""
            objUserInfo.Ext = ""
            objUserInfo.Mobile = ""
            objUserInfo.POBox = ""
            objUserInfo.Fax = ""
            objUserInfo.Zip = ""
            objUserInfo.WebSite = ""
            objUserInfo.RegType = ""
            objUserInfo.ReqType = ""
            objUserInfo.ExpireDate = ""

            Dim objSql As New SQLFunction
            Dim ds As New DataSetRes
            Dim SQLStr As String = "select * from User_Info where id = '" & User_ID & "'"
            ds = objSql.Get_DataSet(SQLStr, ConfigurationManager.AppSettings("SymConnStr"))
            objUserInfo.FullName = If(IsDBNull(ds.DS.Tables(0).Rows(0).Item(0)), "", ds.DS.Tables(0).Rows(0).Item(0))
            objUserInfo.Employer = If(IsDBNull(ds.DS.Tables(0).Rows(0).Item(1)), "", ds.DS.Tables(0).Rows(0).Item(1))

            For i As Integer = 0 To output_array.Length - 1
                'If output_array(i).Contains(".USER_NAME.   |a") Then
                '    objUserInfo.FullName = output_array(i).Replace(".USER_NAME.   |a", "")
                '    objUserInfo.FullName = objUserInfo.FullName.Replace(vbLf, [String].Empty)
                '    'Else
                '    '    objUserInfo.FullName = ""
                'End If
                If output_array(i).Contains(".USER_LIBRARY.   |a") Then
                    objUserInfo.Library = output_array(i).Replace(".USER_LIBRARY.   |a", "")
                    objUserInfo.Library = objUserInfo.Library.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Library = ""
                End If
                If output_array(i).Contains(".USER_PROFILE.   |a") Then
                    objUserInfo.Profile = output_array(i).Replace(".USER_PROFILE.   |a", "")
                    objUserInfo.Profile = objUserInfo.Profile.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Profile = ""
                End If
                If output_array(i).Contains(".USER_PRIV_EXPIRES.   |a") Then
                    objUserInfo.ExpireDate = output_array(i).Replace(".USER_PRIV_EXPIRES.   |a", "")
                    objUserInfo.ExpireDate = objUserInfo.ExpireDate.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.ExpireDate = ""
                End If
                If output_array(i).Contains(".USER_BIRTH_DATE.   |a") Then
                    objUserInfo.DOB = output_array(i).Replace(".USER_BIRTH_DATE.   |a", "")
                    objUserInfo.DOB = objUserInfo.DOB.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.DOB = ""
                End If
                If output_array(i).Contains(".USER_CATEGORY3.   |a") Then
                    objUserInfo.JobTitle = output_array(i).Replace(".USER_CATEGORY3.   |a", "")
                    objUserInfo.JobTitle = objUserInfo.JobTitle.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.JobTitle = ""
                End If
                If output_array(i).Contains(".USER_CATEGORY4.   |a") Then
                    objUserInfo.Nationality = output_array(i).Replace(".USER_CATEGORY4.   |a", "")
                    objUserInfo.Nationality = objUserInfo.Nationality.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Nationality = ""
                End If
                If output_array(i).Contains(".USER_CATEGORY5.   |a") Then
                    objUserInfo.City = output_array(i).Replace(".USER_CATEGORY5.   |a", "")
                    objUserInfo.City = objUserInfo.City.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.City = ""
                End If
                If output_array(i).Contains(".REG_TYPE. |a") Then
                    objUserInfo.RegType = output_array(i).Replace(".REG_TYPE. |a", "")
                    objUserInfo.RegType = objUserInfo.RegType.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.RegType = ""
                End If
                If output_array(i).Contains(".EMAIL. |a") Then
                    objUserInfo.Email = output_array(i).Replace(".EMAIL. |a", "")
                    objUserInfo.Email = objUserInfo.Email.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Email = ""
                End If
                If output_array(i).Contains(".HOMEPHONE. |a") Then
                    objUserInfo.HomePhone = output_array(i).Replace(".HOMEPHONE. |a", "")
                    objUserInfo.HomePhone = objUserInfo.HomePhone.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.HomePhone = ""
                End If
                If output_array(i).Contains(".PHONE. |a") Then
                    objUserInfo.Mobile = output_array(i).Replace(".PHONE. |a", "")
                    objUserInfo.Mobile = objUserInfo.Mobile.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Mobile = ""
                End If
                If output_array(i).Contains(".ZIP. |a") Then
                    objUserInfo.Zip = output_array(i).Replace(".ZIP. |a", "")
                    objUserInfo.Zip = objUserInfo.Zip.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Zip = ""
                End If
                If output_array(i).Contains(".FAX. |a") Then
                    objUserInfo.Fax = output_array(i).Replace(".FAX. |a", "")
                    objUserInfo.Fax = objUserInfo.Fax.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Fax = ""
                End If
                'If output_array(i).Contains(".COMPANY. |a") Then
                '    objUserInfo.Employer = output_array(i).Replace(".COMPANY. |a", "")
                '    objUserInfo.Employer = objUserInfo.Employer.Replace(vbLf, [String].Empty)
                '    'Else
                '    '    objUserInfo.Employer = ""
                'End If
                If output_array(i).Contains(".RQSTTYPE. |a") Then
                    objUserInfo.ReqType = output_array(i).Replace(".RQSTTYPE. |a", "")
                    objUserInfo.ReqType = objUserInfo.ReqType.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.ReqType = ""
                End If
                If output_array(i).Contains(".USER_CATEGORY2.   |a") Then
                    objUserInfo.Gender = output_array(i).Replace(".USER_CATEGORY2. |a", "")
                    objUserInfo.Gender = objUserInfo.Gender.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Gender = ""
                End If
                If output_array(i).Contains(".WORKPHONE. |a") Then
                    objUserInfo.WorkPhone = output_array(i).Replace(".WORKPHONE. |a", "")
                    objUserInfo.WorkPhone = objUserInfo.WorkPhone.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.WorkPhone = ""
                End If
                If output_array(i).Contains(".EXTENSION. |a") Then
                    objUserInfo.Ext = output_array(i).Replace(".EXTENSION. |a", "")
                    objUserInfo.Ext = objUserInfo.Ext.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.Ext = ""
                End If
                If output_array(i).Contains(".ZIP. |a") Then
                    objUserInfo.POBox = output_array(i).Replace(".ZIP. |a", "")
                    objUserInfo.POBox = objUserInfo.POBox.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.POBox = ""
                End If
                If output_array(i).Contains(".WEBSITE. |a") Then
                    objUserInfo.WebSite = output_array(i).Replace(".WEBSITE. |a", "")
                    objUserInfo.WebSite = objUserInfo.WebSite.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.WebSite = ""
                End If
                If output_array(i).Contains(".MEMTYP. |a") Then
                    objUserInfo.MemberType = output_array(i).Replace(".MEMTYP. |a", "")
                    objUserInfo.MemberType = objUserInfo.MemberType.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.MemberType = ""
                End If
                If output_array(i).Contains(".NAT_ID. |a") Then
                    objUserInfo.NationalID = output_array(i).Replace(".NAT_ID. |a", "")
                    objUserInfo.NationalID = objUserInfo.NationalID.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.NationalID = ""
                End If
                If output_array(i).Contains(".PER_PHOTO. |a") Then
                    objUserInfo.PersonalPhoto = output_array(i).Replace(".PER_PHOTO. |a", "")
                    objUserInfo.PersonalPhoto = objUserInfo.PersonalPhoto.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.PersonalPhoto = ""
                End If
                If output_array(i).Contains(".NAT_PH1. |a") Then
                    objUserInfo.NationalIDPhoto1 = output_array(i).Replace(".NAT_PH1. |a", "")
                    objUserInfo.NationalIDPhoto1 = objUserInfo.NationalIDPhoto1.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.NationalIDPhoto1 = ""
                End If
                If output_array(i).Contains(".NAT_PH2. |a") Then
                    objUserInfo.NationalIDPhoto2 = output_array(i).Replace(".NAT_PH2. |a", "")
                    objUserInfo.NationalIDPhoto2 = objUserInfo.NationalIDPhoto2.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.NationalIDPhoto2 = ""
                End If
                If output_array(i).Contains(".SNeeds. |a") Then
                    objUserInfo.SNeeds = output_array(i).Replace(".SNeeds. |a", "")
                    objUserInfo.SNeeds = objUserInfo.SNeeds.Replace(vbLf, [String].Empty)
                    'Else
                    '    objUserInfo.SNeeds = ""
                End If
            Next

            If objUserInfo.Nationality <> "" Or objUserInfo.City <> "" Or objUserInfo.City <> "" Or objUserInfo.Profile <> "" Then
                objUserInfo.User_ID = User_ID
                Return objUserInfo
            Else
                If Output_Error.Contains("**") = True Then
                    objUserInfo.objException.msg = "Error" + Output_Error
                    objUserInfo.User_ID = ""
                    Return objUserInfo
                Else
                    objUserInfo.objException.msg = ""
                    objUserInfo.User_ID = User_ID
                    Return objUserInfo
                End If
            End If
            objUserInfo.objException.msg = "Error : End of File."
            Return objUserInfo
        Catch ex As Exception
            objUserInfo.objException.msg = ex.Message
            ' objUserInfo.User_ID = ""
            Return objUserInfo
        End Try
    End Function

    Public Function UserExist(ByVal User_ID As String) As UserExist Implements IewsUserReg.UserExist
        Dim objUserExist As New UserExist
        Try
            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True

            pProcess.Start()

            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput

            MyStreamWriter.WriteLine("D:\SIRSI\Unicorn\Bin\seluser.exe -iB")

            MyStreamWriter.WriteLine(User_ID.ToString.ToUpper + System.Environment.NewLine)
            pProcess.StandardInput.Close()

            Dim output As String = pProcess.StandardOutput.ReadToEnd()

            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()
            If output.Contains("|") = False Then
                objUserExist.ext = False
            Else
                objUserExist.ext = True
            End If
            Return objUserExist
        Catch ex As Exception
            objUserExist.ext = False
            objUserExist.objException.msg = ex.Message
            Return objUserExist
        End Try
    End Function

    Public Function UserRegType(ByVal User_ID As String, ByVal Library As String, ByVal Status As String, ByVal RegType As String) As UserRegType Implements IewsUserReg.UserRegType
        Dim objUserRegType As New UserRegType
        Dim FileName_Users As String = ""
        Try
            FileName_Users = Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & "_" & User_ID & "_" & "RegType" & ".txt"
            FileName_Users = ConfigurationManager.AppSettings("Files_Path_P") + FileName_Users

            Common_Obj.SaveTextToFile("*** DOCUMENT BOUNDARY ***", FileName_Users)
            Common_Obj.SaveTextToFile("FORM=LDUSER", FileName_Users)

            Dim USERID_Type As String = ".USER_ID.   |a" & User_ID.Trim.ToUpper
            Common_Obj.SaveTextToFile(USERID_Type, FileName_Users)

            Dim USER_STATUS_Type As String = ".USER_STATUS.   |a" & Status
            Common_Obj.SaveTextToFile(USER_STATUS_Type, FileName_Users)

            Dim USER_ADDR1_BEGIN_Type As String = ".USER_ADDR1_BEGIN."
            Common_Obj.SaveTextToFile(USER_ADDR1_BEGIN_Type, FileName_Users)

            Dim REG_TYPE_Type As String = ".REG_TYPE. |a" & RegType
            Common_Obj.SaveTextToFile(REG_TYPE_Type, FileName_Users)

            Dim USER_ADDR1_END_Type As String = ".USER_ADDR1_END."
            Common_Obj.SaveTextToFile(USER_ADDR1_END_Type, FileName_Users)

            Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess_Users.StartInfo.FileName = "cmd.exe"
            pProcess_Users.StartInfo.WorkingDirectory = "D:\SIRSI\Unicorn\Bin\"
            pProcess_Users.StartInfo.UseShellExecute = False
            pProcess_Users.StartInfo.RedirectStandardOutput = True
            pProcess_Users.StartInfo.RedirectStandardInput = True
            pProcess_Users.StartInfo.RedirectStandardError = True
            pProcess_Users.StartInfo.CreateNoWindow = True
            pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
            pProcess_Users.Start()

            Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput

            MyStreamWriter_Users.WriteLine("loadflatuser.exe -aU -bU -mu -l""ADMIN|TTY0"" -y" & Library.ToUpper & " < """ & FileName_Users & """")
            pProcess_Users.StandardInput.Close()

            Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()

            If Output_UserReg.Contains("1 $<D222> $<user> $(1418)") = True Then
                If FileName_Users <> "" Then
                    objUserRegType.Execute = True
                    objUserRegType.objException.msg = ""
                    'Common_Obj.DeleteFiles("", FileName_Users)
                End If
                Return objUserRegType
            Else
                If Output_UserReg.Contains("**Invalid") = True Then
                    Dim Start_Index As Double = Output_UserReg.IndexOf("**Invalid")
                    Dim Last_Index As Double = 0
                    If Output_UserReg.Contains("*** DOCUMENT BOUNDARY ***") = True Then
                        Last_Index = Output_UserReg.IndexOf("*** DOCUMENT BOUNDARY ***")
                    End If
                    If Last_Index <> 0 Then
                        objUserRegType.objException.msg = Output_UserReg.Substring(Start_Index, Last_Index - Start_Index)
                    Else
                        objUserRegType.objException.msg = Output_UserReg
                    End If
                    objUserRegType.Execute = False
                    Return objUserRegType
                End If
                If FileName_Users <> "" Then
                    'Common_Obj.DeleteFiles("", FileName_Users)
                End If
                objUserRegType.objException.msg = Output_UserReg
                objUserRegType.Execute = False
                Return objUserRegType
            End If
        Catch ex As Exception
            objUserRegType.objException.msg = ex.Message + "######"
            Return objUserRegType
        End Try
    End Function

    Public Function LoadUserUpgrade(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                                    , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                                    , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                                    , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                                    , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                                    , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                                    ) As Responce_Msg Implements IewsUserReg.LoadUserUpgrade
        Dim objResponce_Msg As New Responce_Msg

        Try
            ' Dim Res As Reponse_Msg = New Reponse_Msg
            Dim IsUser_Exist As Boolean = False
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If LANG = "ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If

            '////////////////////// Check if birth date > now and calcualte Age ///////////////////////////////////////////////////
            Dim curTime As DateTime = Date.Now
            Dim birthDate As Date = DateTime.ParseExact(DOB, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US")) 'CDate(DOB)

            If birthDate > Now.Date Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_BirthDate_Now", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If

            'Dim elapsed As TimeSpan
            'Dim Age As String = ""
            'elapsed = curTime - birthDate
            'Age = elapsed.Days / 365.25

            Dim Child_Age As Double = ConfigurationManager.AppSettings("CHILD_AGE_MAX")
            '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            FileName_Users = ""

            '////////////////////// Start Validation ///////////////////////////////////////////////////
            If Common_Obj.Fields_Validation(RequestType) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_RequestType_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Common_Obj.Fields_Validation(Library) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_LIBR_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Common_Obj.Fields_Validation(NationalIDPhoto1) = False Or Common_Obj.Fields_Validation(NationalIDPhoto2) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_NATID_Byte_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Common_Obj.Fields_Validation(PersonalPhoto) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Photo_Byte_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If IsNumeric(MemberType) = True Then
                If MemberType < 1 Or MemberType > 3 Then
                    ' objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Membership_Type_Invalid", Culture_Used)
                    '   objResponce_Msg.UserId = ""
                    '  Return objResponce_Msg
                End If
            ElseIf IsNumeric(MemberType) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Membership_Type_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If MemberType <> 2 Then 'Not Company
                If Common_Obj.Fields_Validation(FullName) = False Then
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_FullName_Invalid", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If
            End If
            If Common_Obj.Fields_Validation(Nationality) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Nationality_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If MemberType = 1 Then ' Individual
                If Common_Obj.Fields_Validation(Gender) = False Then
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Gender_Invalid", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If
                If IsDate(birthDate) = False Then
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_DOB_Invalid", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If
            End If
            If Common_Obj.Fields_Validation(NationalID) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Passport_No_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If IsNumeric(Mobile) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Mob_No_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Mobile = 0 Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Mob_No_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            '///////////////////////////////////////////////////////////////////////////////////////////////////////
            FileName_Users = Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & "_" & User_ID & "_" & "User" & ".txt"
            FileName_Users = ConfigurationManager.AppSettings("Files_Path_P") + FileName_Users
            '//////////////////////Construct User File ////////////////////////////////////////////////////////////////////////////////

            Common_Obj.SaveTextToFile("*** DOCUMENT BOUNDARY ***", FileName_Users)
            Common_Obj.SaveTextToFile("FORM=LDUSER", FileName_Users)

            Dim USERID As String = ".USER_ID.   |a" & User_ID.Trim.ToUpper
            Common_Obj.SaveTextToFile(USERID, FileName_Users)

            Dim USER_NAME As String = ConfigurationManager.AppSettings("FullName") & "   |a" & FullName.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_NAME, FileName_Users)

            Dim USER_LIB As String = ConfigurationManager.AppSettings("LIBR") & "   |a" & Library.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_LIB, FileName_Users)

            Dim USER_STATUS As String = ".USER_STATUS.   |aOK"
            Common_Obj.SaveTextToFile(USER_STATUS, FileName_Users)

            Dim USER_PROFILE As String = ConfigurationManager.AppSettings("Profile") & "   |a"
            ' Dim USER_PROFILE_FINAL As String = ""
            If MemberType = 0 Then ' UPRF_public_user
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_public_user").ToUpper
                'ElseIf MemberType = 1 Then
                '  If Age > Child_Age Then
                'USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_ADULT")
                'ElseIf Age < Child_Age Then
                'USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_CHILD")
                'End If
            ElseIf MemberType = 1 Then ' Family
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_FAMILY").ToUpper
            ElseIf MemberType = 2 Then ' UPRF_academic
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_academic").ToUpper
            ElseIf MemberType = 3 Then ' UPRF_postgraduate_student
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_postgraduate_student").ToUpper
            ElseIf MemberType = 4 Then ' UPRF_undergraduate_student
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_undergraduate_student").ToUpper
            ElseIf MemberType = 5 Then ' UPRF_special_user
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_special_user").ToUpper
            ElseIf MemberType = 6 Then ' UPRF_Special_User
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_Special_Need").ToUpper
            ElseIf MemberType = 7 Then ' UPRF_Special_Need
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_Virtual_Card").ToUpper
            Else
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_public_user").ToUpper
            End If
            'USER_PROFILE_FINAL = USER_PROFILE.Replace(ConfigurationManager.AppSettings("Profile") & "   |a", "")
            Common_Obj.SaveTextToFile(USER_PROFILE, FileName_Users)

            Dim USER_PRIV_GRANTED As String = ".USER_PRIV_GRANTED.   |a" & Now.Year & Now.Month & Now.Day
            Common_Obj.SaveTextToFile(USER_PRIV_GRANTED, FileName_Users)

            Dim USER_PRIV_EXPIRES As String = ".USER_PRIV_EXPIRES.   |a"
            If MemberType = 7 Then
                USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Now.AddYears(1).Year & Now.Month & Now.AddDays(-1).Day
            Else
                USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & "NEVER"
            End If
            'birthDate.ToString("dd/MM/yyyy").Split("/")(2) & birthDate.ToString("dd/MM/yyyy").Split("/")(1) & birthDate.ToString("dd/MM/yyyy").Split("/")(0)
            'Dim Child_Date As New Date
            'If Age > Child_Age Then
            'USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Now.AddYears(5).Year & Now.Month & Now.Day
            'Else
            'Dim Number_of_Days As Double = (Child_Age * 12 * 30) - (Age * 12 * 30)
            'If Number_of_Days < 1826.25 Then
            '    Child_Date = CDate(Now.AddDays((Child_Age * 12 * 30) - (Age * 12 * 30)))
            '    USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Child_Date.Year & Child_Date.Month.ToString("00") & Child_Date.Day.ToString("00")
            'Else
            '    USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Now.AddYears(5).Year & Now.Month.ToString("00") & Now.Day.ToString("00")
            'End If
            'End If
            Common_Obj.SaveTextToFile(USER_PRIV_EXPIRES, FileName_Users)

            Dim USER_Nationality As String = ConfigurationManager.AppSettings("Nationality") & "   |a" & Nationality.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_Nationality, FileName_Users)

            Dim USER_Gender As String = ConfigurationManager.AppSettings("Gender") & "   |a" & Gender.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_Gender, FileName_Users)

            If JobTitle IsNot Nothing Then
                Dim USER_Profession As String = ConfigurationManager.AppSettings("JobTitle") & "   |a" & JobTitle.Trim.ToUpper
                Common_Obj.SaveTextToFile(USER_Profession, FileName_Users)
            End If

            Dim USER_Emirate As String = ConfigurationManager.AppSettings("City") & "   |a" & City.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_Emirate, FileName_Users)

            Dim USER_DOB As String = ConfigurationManager.AppSettings("DOB") & "   |a" & birthDate.ToString("dd/MM/yyyy").Split("/")(2) & birthDate.ToString("dd/MM/yyyy").Split("/")(1) & birthDate.ToString("dd/MM/yyyy").Split("/")(0)
            Common_Obj.SaveTextToFile(USER_DOB, FileName_Users)

            Dim USER_ADDR1_BEGIN As String = ".USER_ADDR1_BEGIN."
            Common_Obj.SaveTextToFile(USER_ADDR1_BEGIN, FileName_Users)

            Dim REG_TYPE As String = ConfigurationManager.AppSettings("REG_TYPE") & "   |a3"
            Common_Obj.SaveTextToFile(REG_TYPE, FileName_Users)

            If Employer IsNot Nothing Then
                Dim USER_Employer As String = ConfigurationManager.AppSettings("Company") & "   |a" & Employer.Trim.ToUpper
                Common_Obj.SaveTextToFile(USER_Employer, FileName_Users)
            End If

            Dim USER_Tel_Home As String = ConfigurationManager.AppSettings("Telephone") & "   |a" & HomePhone.ToString
            Common_Obj.SaveTextToFile(USER_Tel_Home, FileName_Users)

            Dim USER_WorkPhone As String = ConfigurationManager.AppSettings("Tel_Office") & "   |a" & WorkPhone.ToString
            Common_Obj.SaveTextToFile(USER_WorkPhone, FileName_Users)

            If Ext IsNot Nothing Then
                Dim USER_Tel_Ext As String = ConfigurationManager.AppSettings("TelExt") & "   |a" & Ext.ToString
                Common_Obj.SaveTextToFile(USER_Tel_Ext, FileName_Users)
            End If

            Dim USER_Mob_No As String = ConfigurationManager.AppSettings("Mobile") & "   |a" & Mobile.ToString
            Common_Obj.SaveTextToFile(USER_Mob_No, FileName_Users)

            Dim USER_PO_Box As String = ConfigurationManager.AppSettings("PostBox") & "   |a" & POBox.ToString
            Common_Obj.SaveTextToFile(USER_PO_Box, FileName_Users)

            Dim USER_Fax As String = ConfigurationManager.AppSettings("Fax") & "   |a" & Fax.ToString
            Common_Obj.SaveTextToFile(USER_Fax, FileName_Users)

            If WebSite IsNot Nothing Then
                Dim USER_Website As String = ConfigurationManager.AppSettings("Website") & "   |a" & WebSite.ToString
                Common_Obj.SaveTextToFile(USER_Website, FileName_Users)
            End If

            Dim USER_Membership_Type As String = ConfigurationManager.AppSettings("Membership_Type") & "   |a" & MemberType.ToString
            Common_Obj.SaveTextToFile(USER_Membership_Type, FileName_Users)

            If RequestType IsNot Nothing Then
                Dim USER_RequestType As String = ConfigurationManager.AppSettings("RequestType") & "   |a" & RequestType.ToString
                Common_Obj.SaveTextToFile(USER_RequestType, FileName_Users)
            End If

            If NationalID IsNot Nothing Then
                Dim USER_NAT_ID As String = ConfigurationManager.AppSettings("NAT_ID") & "   |a" & NationalID.ToString
                Common_Obj.SaveTextToFile(USER_NAT_ID, FileName_Users)
            End If


            If IsUser_Exist = False Then ' User does  exist

                Dim USER_PHOTO_USER As String = ConfigurationManager.AppSettings("PHOTO_USER") & "   |a" & PersonalPhoto.ToString
                Common_Obj.SaveTextToFile(USER_PHOTO_USER, FileName_Users)

                Dim USER_NATID_COPY1 As String = ConfigurationManager.AppSettings("NationalIDPhoto1") & "   |a" & NationalIDPhoto1.ToString
                Common_Obj.SaveTextToFile(USER_NATID_COPY1, FileName_Users)

                Dim USER_NATID_COPY2 As String = ConfigurationManager.AppSettings("NationalIDPhoto2") & "   |a" & NationalIDPhoto2.ToString
                Common_Obj.SaveTextToFile(USER_NATID_COPY2, FileName_Users)

                If SNeeds.Trim <> "" And SNeeds.Trim <> "(null)" Then
                    Dim USER_SNEEDS_Copy As String = ConfigurationManager.AppSettings("SNEEDS") & "   |a" & SNeeds.ToString
                    Common_Obj.SaveTextToFile(USER_SNEEDS_Copy, FileName_Users)
                End If
                If SNeeds.Trim <> "" And SNeeds.Trim <> "(null)" Then
                    Dim USER_SNeeds_Val As String = ConfigurationManager.AppSettings("SNEEDS_URL") & "   |a1"
                    Common_Obj.SaveTextToFile(USER_SNeeds_Val, FileName_Users)
                End If

                Dim USER_ADDR1_END As String = ".USER_ADDR1_END."
                Common_Obj.SaveTextToFile(USER_ADDR1_END, FileName_Users)

                '///////////////////////////////////////////////////////////////////////////////////////////////////////

                '/////////////////////////////////// Start Loading User file to update Reg_Type =3///////////////////////////////////////////////

                Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
                pProcess_Users.StartInfo.FileName = "cmd.exe"
                pProcess_Users.StartInfo.WorkingDirectory = "D:\SIRSI\Unicorn\Bin\"
                pProcess_Users.StartInfo.UseShellExecute = False
                pProcess_Users.StartInfo.RedirectStandardOutput = True
                pProcess_Users.StartInfo.RedirectStandardInput = True
                pProcess_Users.StartInfo.RedirectStandardError = True
                pProcess_Users.StartInfo.CreateNoWindow = True
                pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
                pProcess_Users.Start()

                Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput

                MyStreamWriter_Users.WriteLine("loadflatuser.exe -aU -bU -mu -l""ADMIN|TTY0"" -y" & ConfigurationManager.AppSettings("DEFAULT_LIBR").ToUpper & " < """ & FileName_Users & """")
                pProcess_Users.StandardInput.Close()

                Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()

                If Output_UserReg.Contains("1 $<D222> $<user> $(1418)") = True Then ' Load Success
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserReg_Updt_Success", Culture_Used)
                    objResponce_Msg.UserId = User_ID
                    Return objResponce_Msg
                Else ' Error in user Load file
                    If FileName_Users <> "" Then
                        '  Common_Obj.DeleteFiles("", FileName_Users)
                    End If

                    If Output_UserReg.Contains("**Invalid") = True Then
                        Dim Start_Index As Double = Output_UserReg.IndexOf("**Invalid")
                        Dim Last_Index As Double = 0
                        If Output_UserReg.Contains("*** DOCUMENT BOUNDARY ***") = True Then
                            Last_Index = Output_UserReg.IndexOf("*** DOCUMENT BOUNDARY ***")
                        End If
                        If Last_Index <> 0 Then
                            objResponce_Msg.Msg = Output_UserReg.Substring(Start_Index, Last_Index - Start_Index)
                        Else
                            objResponce_Msg.Msg = Output_UserReg
                        End If
                        objResponce_Msg.UserId = ""
                        Return objResponce_Msg
                    End If
                    Common_Obj.Logs(Now.Date, Output_UserReg)
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserReg_Error", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If

            End If

            'Common_Obj.Logs(Date.Now, "User : " & User_ID & " : Registration Error : End of File.") 'End of File Reached
            objResponce_Msg.Msg = "Error : End of File."
            objResponce_Msg.UserId = ""
            Return objResponce_Msg
        Catch ex As Exception
            If FileName_Users <> "" Then
                ' Common_Obj.DeleteFiles("", FileName_Users)
            End If
            'Common_Obj.Logs(Date.Now, "User : " & User_ID & " ,  " & " : Registration Error : " & ex.Message)
            If ex.Message.Contains("Could not find file") = True Then
                objResponce_Msg.Msg = "Error in Loading Files , files not found"
            Else
                objResponce_Msg.Msg = ex.Message & " - Error Line : " & ex.StackTrace()
            End If
            objResponce_Msg.UserId = ""
            Return objResponce_Msg
        End Try
    End Function

    Public Function LoadUserRenewal(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                                    , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                                    , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                                    , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                                    , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                                    , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                                    ) As Responce_Msg Implements IewsUserReg.LoadUserRenewal
        Dim objResponce_Msg As New Responce_Msg
        Try
            ' Dim Res As Reponse_Msg = New Reponse_Msg
            Dim IsUser_Exist As Boolean = False
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If LANG = "ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If

            '////////////////////// Check if birth date > now and calcualte Age ///////////////////////////////////////////////////
            Dim curTime As DateTime = Date.Now
            Dim birthDate As Date = DateTime.ParseExact(DOB, "dd/MM/yyyy", New System.Globalization.CultureInfo("en-US")) 'CDate(DOB)

            If birthDate > Now.Date Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_BirthDate_Now", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If

            'Dim elapsed As TimeSpan
            'Dim Age As String = ""
            'elapsed = curTime - birthDate
            'Age = elapsed.Days / 365.25

            Dim Child_Age As Double = ConfigurationManager.AppSettings("CHILD_AGE_MAX")
            '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            FileName_Users = ""

            '////////////////////// Start Validation ///////////////////////////////////////////////////
            If Common_Obj.Fields_Validation(RequestType) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_RequestType_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Common_Obj.Fields_Validation(Library) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_LIBR_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Common_Obj.Fields_Validation(NationalIDPhoto1) = False Or Common_Obj.Fields_Validation(NationalIDPhoto2) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_NATID_Byte_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Common_Obj.Fields_Validation(PersonalPhoto) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Photo_Byte_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If IsNumeric(MemberType) = True Then
                If MemberType < 1 Or MemberType > 3 Then
                    ' objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Membership_Type_Invalid", Culture_Used)
                    '   objResponce_Msg.UserId = ""
                    '  Return objResponce_Msg
                End If
            ElseIf IsNumeric(MemberType) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Membership_Type_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If MemberType <> 2 Then 'Not Company
                If Common_Obj.Fields_Validation(FullName) = False Then
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_FullName_Invalid", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If
            End If
            If Common_Obj.Fields_Validation(Nationality) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Nationality_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If MemberType = 1 Then ' Individual
                If Common_Obj.Fields_Validation(Gender) = False Then
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Gender_Invalid", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If
                If IsDate(birthDate) = False Then
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_DOB_Invalid", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If
            End If
            If Common_Obj.Fields_Validation(NationalID) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Passport_No_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If IsNumeric(Mobile) = False Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Mob_No_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            If Mobile = 0 Then
                objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_Mob_No_Invalid", Culture_Used)
                objResponce_Msg.UserId = ""
                Return objResponce_Msg
            End If
            '///////////////////////////////////////////////////////////////////////////////////////////////////////
            FileName_Users = Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & "_" & User_ID & "_" & "User" & ".txt"
            FileName_Users = ConfigurationManager.AppSettings("Files_Path_P") + FileName_Users
            '//////////////////////Construct User File ////////////////////////////////////////////////////////////////////////////////

            Common_Obj.SaveTextToFile("*** DOCUMENT BOUNDARY ***", FileName_Users)
            Common_Obj.SaveTextToFile("FORM=LDUSER", FileName_Users)

            Dim USERID As String = ".USER_ID.   |a" & User_ID.Trim.ToUpper
            Common_Obj.SaveTextToFile(USERID, FileName_Users)

            Dim USER_NAME As String = ConfigurationManager.AppSettings("FullName") & "   |a" & FullName.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_NAME, FileName_Users)

            Dim USER_LIB As String = ConfigurationManager.AppSettings("LIBR") & "   |a" & Library.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_LIB, FileName_Users)

            Dim USER_STATUS As String = ".USER_STATUS.   |aOK"
            Common_Obj.SaveTextToFile(USER_STATUS, FileName_Users)

            Dim USER_PROFILE As String = ConfigurationManager.AppSettings("Profile") & "   |a"
            ' Dim USER_PROFILE_FINAL As String = ""
            If MemberType = 0 Then ' UPRF_public_user
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_public_user").ToUpper
                'ElseIf MemberType = 1 Then
                '  If Age > Child_Age Then
                'USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_ADULT")
                'ElseIf Age < Child_Age Then
                'USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_CHILD")
                'End If
            ElseIf MemberType = 1 Then ' Family
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_FAMILY").ToUpper
            ElseIf MemberType = 2 Then ' UPRF_academic
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_academic").ToUpper
            ElseIf MemberType = 3 Then ' UPRF_postgraduate_student
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_postgraduate_student").ToUpper
            ElseIf MemberType = 4 Then ' UPRF_undergraduate_student
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_undergraduate_student").ToUpper
            ElseIf MemberType = 5 Then ' UPRF_special_user
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_special_user").ToUpper
            ElseIf MemberType = 6 Then ' UPRF_Special_Need
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_Special_Need").ToUpper
            ElseIf MemberType = 7 Then ' UPRF_Virtual_Card
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_Virtual_Card").ToUpper
            Else
                USER_PROFILE = USER_PROFILE & ConfigurationManager.AppSettings("UPRF_public_user").ToUpper
            End If
            'USER_PROFILE_FINAL = USER_PROFILE.Replace(ConfigurationManager.AppSettings("Profile") & "   |a", "")
            Common_Obj.SaveTextToFile(USER_PROFILE, FileName_Users)

            Dim USER_PRIV_GRANTED As String = ".USER_PRIV_GRANTED.   |a" & Now.Year & Now.Month & Now.Day
            Common_Obj.SaveTextToFile(USER_PRIV_GRANTED, FileName_Users)

            Dim USER_PRIV_EXPIRES As String = ".USER_PRIV_EXPIRES.   |a"
            'Dim Child_Date As New Date
            'If Age > Child_Age Then
            If MemberType = 7 Then
                USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Now.AddYears(1).Year & Now.Month & Now.AddDays(-1).Day
            Else
                USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Now.AddYears(5).Year & Now.Month & Now.Day
            End If
            USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Now.AddYears(5).Year & Now.Month & Now.Day
            'Else
            'Dim Number_of_Days As Double = (Child_Age * 12 * 30) - (Age * 12 * 30)
            'If Number_of_Days < 1826.25 Then
            '    Child_Date = CDate(Now.AddDays((Child_Age * 12 * 30) - (Age * 12 * 30)))
            '    USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Child_Date.Year & Child_Date.Month.ToString("00") & Child_Date.Day.ToString("00")
            'Else
            '    USER_PRIV_EXPIRES = USER_PRIV_EXPIRES & Now.AddYears(5).Year & Now.Month.ToString("00") & Now.Day.ToString("00")
            'End If
            'End If
            Common_Obj.SaveTextToFile(USER_PRIV_EXPIRES, FileName_Users)

            Dim USER_Nationality As String = ConfigurationManager.AppSettings("Nationality") & "   |a" & Nationality.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_Nationality, FileName_Users)

            Dim USER_Gender As String = ConfigurationManager.AppSettings("Gender") & "   |a" & Gender.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_Gender, FileName_Users)

            If JobTitle IsNot Nothing Then
                Dim USER_Profession As String = ConfigurationManager.AppSettings("JobTitle") & "   |a" & JobTitle.Trim.ToUpper
                Common_Obj.SaveTextToFile(USER_Profession, FileName_Users)
            End If

            Dim USER_Emirate As String = ConfigurationManager.AppSettings("City") & "   |a" & City.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_Emirate, FileName_Users)

            Dim USER_DOB As String = ConfigurationManager.AppSettings("DOB") & "   |a" & birthDate.ToString("dd/MM/yyyy").Split("/")(2) & birthDate.ToString("dd/MM/yyyy").Split("/")(1) & birthDate.ToString("dd/MM/yyyy").Split("/")(0)
            Common_Obj.SaveTextToFile(USER_DOB, FileName_Users)

            Dim USER_ADDR1_BEGIN As String = ".USER_ADDR1_BEGIN."
            Common_Obj.SaveTextToFile(USER_ADDR1_BEGIN, FileName_Users)

            Dim REG_TYPE As String = ConfigurationManager.AppSettings("REG_TYPE") & "   |a3"
            Common_Obj.SaveTextToFile(REG_TYPE, FileName_Users)

            If Employer IsNot Nothing Then
                Dim USER_Employer As String = ConfigurationManager.AppSettings("Company") & "   |a" & Employer.Trim.ToUpper
                Common_Obj.SaveTextToFile(USER_Employer, FileName_Users)
            End If

            Dim USER_Tel_Home As String = ConfigurationManager.AppSettings("Telephone") & "   |a" & HomePhone.ToString
            Common_Obj.SaveTextToFile(USER_Tel_Home, FileName_Users)

            Dim USER_WorkPhone As String = ConfigurationManager.AppSettings("Tel_Office") & "   |a" & WorkPhone.ToString
            Common_Obj.SaveTextToFile(USER_WorkPhone, FileName_Users)

            If Ext IsNot Nothing Then
                Dim USER_Tel_Ext As String = ConfigurationManager.AppSettings("TelExt") & "   |a" & Ext.ToString
                Common_Obj.SaveTextToFile(USER_Tel_Ext, FileName_Users)
            End If

            Dim USER_Mob_No As String = ConfigurationManager.AppSettings("Mobile") & "   |a" & Mobile.ToString
            Common_Obj.SaveTextToFile(USER_Mob_No, FileName_Users)

            Dim USER_PO_Box As String = ConfigurationManager.AppSettings("PostBox") & "   |a" & POBox.ToString
            Common_Obj.SaveTextToFile(USER_PO_Box, FileName_Users)

            Dim USER_Fax As String = ConfigurationManager.AppSettings("Fax") & "   |a" & Fax.ToString
            Common_Obj.SaveTextToFile(USER_Fax, FileName_Users)

            If WebSite IsNot Nothing Then
                Dim USER_Website As String = ConfigurationManager.AppSettings("Website") & "   |a" & WebSite.ToString
                Common_Obj.SaveTextToFile(USER_Website, FileName_Users)
            End If

            Dim USER_Membership_Type As String = ConfigurationManager.AppSettings("Membership_Type") & "   |a" & MemberType.ToString
            Common_Obj.SaveTextToFile(USER_Membership_Type, FileName_Users)

            If RequestType IsNot Nothing Then
                Dim USER_RequestType As String = ConfigurationManager.AppSettings("RequestType") & "   |a" & RequestType.ToString
                Common_Obj.SaveTextToFile(USER_RequestType, FileName_Users)
            End If

            If NationalID IsNot Nothing Then
                Dim USER_NAT_ID As String = ConfigurationManager.AppSettings("NAT_ID") & "   |a" & NationalID.ToString
                Common_Obj.SaveTextToFile(USER_NAT_ID, FileName_Users)
            End If


            If IsUser_Exist = False Then ' User does  exist

                Dim USER_PHOTO_USER As String = ConfigurationManager.AppSettings("PHOTO_USER") & "   |a" & PersonalPhoto.ToString
                Common_Obj.SaveTextToFile(USER_PHOTO_USER, FileName_Users)

                Dim USER_NATID_COPY1 As String = ConfigurationManager.AppSettings("NationalIDPhoto1") & "   |a" & NationalIDPhoto1.ToString
                Common_Obj.SaveTextToFile(USER_NATID_COPY1, FileName_Users)

                Dim USER_NATID_COPY2 As String = ConfigurationManager.AppSettings("NationalIDPhoto2") & "   |a" & NationalIDPhoto2.ToString
                Common_Obj.SaveTextToFile(USER_NATID_COPY2, FileName_Users)

                If SNeeds.Trim <> "" And SNeeds.Trim <> "(null)" Then
                    Dim USER_SNEEDS_Copy As String = ConfigurationManager.AppSettings("SNEEDS") & "   |a" & SNeeds.ToString
                    Common_Obj.SaveTextToFile(USER_SNEEDS_Copy, FileName_Users)
                End If
                If SNeeds.Trim <> "" And SNeeds.Trim <> "(null)" Then
                    Dim USER_SNeeds_Val As String = ConfigurationManager.AppSettings("SNEEDS_URL") & "   |a1"
                    Common_Obj.SaveTextToFile(USER_SNeeds_Val, FileName_Users)
                End If

                Dim USER_ADDR1_END As String = ".USER_ADDR1_END."
                Common_Obj.SaveTextToFile(USER_ADDR1_END, FileName_Users)

                '///////////////////////////////////////////////////////////////////////////////////////////////////////

                '/////////////////////////////////// Start Loading User file to update Reg_Type =3///////////////////////////////////////////////

                Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
                pProcess_Users.StartInfo.FileName = "cmd.exe"
                pProcess_Users.StartInfo.WorkingDirectory = "D:\SIRSI\Unicorn\Bin\"
                pProcess_Users.StartInfo.UseShellExecute = False
                pProcess_Users.StartInfo.RedirectStandardOutput = True
                pProcess_Users.StartInfo.RedirectStandardInput = True
                pProcess_Users.StartInfo.RedirectStandardError = True
                pProcess_Users.StartInfo.CreateNoWindow = True
                pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
                pProcess_Users.Start()

                Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput

                MyStreamWriter_Users.WriteLine("loadflatuser.exe -aU -bU -mu -l""ADMIN|TTY0"" -y" & ConfigurationManager.AppSettings("DEFAULT_LIBR").ToUpper & " < """ & FileName_Users & """")
                pProcess_Users.StandardInput.Close()

                Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()

                If Output_UserReg.Contains("1 $<D222> $<user> $(1418)") = True Then ' Load Success
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserReg_Updt_Success", Culture_Used)
                    objResponce_Msg.UserId = User_ID
                    Return objResponce_Msg
                Else ' Error in user Load file
                    If FileName_Users <> "" Then
                        '  Common_Obj.DeleteFiles("", FileName_Users)
                    End If

                    If Output_UserReg.Contains("**Invalid") = True Then
                        Dim Start_Index As Double = Output_UserReg.IndexOf("**Invalid")
                        Dim Last_Index As Double = 0
                        If Output_UserReg.Contains("*** DOCUMENT BOUNDARY ***") = True Then
                            Last_Index = Output_UserReg.IndexOf("*** DOCUMENT BOUNDARY ***")
                        End If
                        If Last_Index <> 0 Then
                            objResponce_Msg.Msg = Output_UserReg.Substring(Start_Index, Last_Index - Start_Index)
                        Else
                            objResponce_Msg.Msg = Output_UserReg
                        End If
                        objResponce_Msg.UserId = ""
                        Return objResponce_Msg
                    End If
                    Common_Obj.Logs(Now.Date, Output_UserReg)
                    objResponce_Msg.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserReg_Error", Culture_Used)
                    objResponce_Msg.UserId = ""
                    Return objResponce_Msg
                End If

            End If

            'Common_Obj.Logs(Date.Now, "User : " & User_ID & " : Registration Error : End of File.") 'End of File Reached
            objResponce_Msg.Msg = "Error : End of File."
            objResponce_Msg.UserId = ""
            Return objResponce_Msg
        Catch ex As Exception
            If FileName_Users <> "" Then
                ' Common_Obj.DeleteFiles("", FileName_Users)
            End If
            'Common_Obj.Logs(Date.Now, "User : " & User_ID & " ,  " & " : Registration Error : " & ex.Message)
            If ex.Message.Contains("Could not find file") = True Then
                objResponce_Msg.Msg = "Error in Loading Files , files not found"
            Else
                objResponce_Msg.Msg = ex.Message & " - Error Line : " & ex.StackTrace()
            End If
            objResponce_Msg.UserId = ""
            Return objResponce_Msg
        End Try
    End Function

    Public Function UserRemMethod(ByVal LANG As String, ByVal User_ID As String) As Responce_Msg Implements IewsUserReg.LoadUserRemove
        Try
            Dim Res As Responce_Msg = New Responce_Msg
            Dim IsUser_Exist As Boolean = False
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If LANG = "ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If
            If Common_Obj.Fields_Validation(User_ID) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserID_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            End If
            '//////////////////////Call to check if User  ID Already Exist or not ?!!///////////////////////////////////////////////////
            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            'pProcess.StartInfo.Arguments = "-iBV"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True
            pProcess.Start()
            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput
            MyStreamWriter.WriteLine("D:\SIRSI\Unicorn\Bin\seluser.exe -iB")
            MyStreamWriter.WriteLine(User_ID.ToUpper + System.Environment.NewLine)
            pProcess.StandardInput.Close()
            Dim output As String = pProcess.StandardOutput.ReadToEnd()
            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()
            If output.Contains("|") = True Then
                IsUser_Exist = True
            Else
                IsUser_Exist = False
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserNotExist", Culture_Used)
                Res.UserId = ""
                Return Res
            End If
            '//////////////////////Remove User !!///////////////////////////////////////////////////
            Dim pProcess_Rem As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess_Rem.StartInfo.FileName = "cmd.exe"
            'pProcess.StartInfo.Arguments = "-iBV"
            pProcess_Rem.StartInfo.UseShellExecute = False
            pProcess_Rem.StartInfo.RedirectStandardOutput = True
            pProcess_Rem.StartInfo.RedirectStandardInput = True
            pProcess_Rem.StartInfo.RedirectStandardError = True
            pProcess_Rem.StartInfo.CreateNoWindow = True
            pProcess_Rem.Start()
            pProcess_Rem.StandardInput.AutoFlush = True
            Dim MyStreamWriter_Rem As StreamWriter = pProcess_Rem.StandardInput
            MyStreamWriter_Rem.WriteLine("echo " & User_ID & "|D:\SIRSI\Unicorn\Bin\seluser.exe -iB|D:\SIRSI\Unicorn\Bin\remuser.exe")
            pProcess_Rem.StandardInput.Close()
            Dim output_Rem As String = pProcess_Rem.StandardOutput.ReadToEnd()
            Dim Output_Error_Rem As String = pProcess_Rem.StandardError.ReadToEnd()
            If output_Rem.Contains("|") = True Then
                Res.Msg = "User Deleted Successfully"
                Res.UserId = User_ID
                Return Res
            Else
                Res.Msg = "Error in User Deletion"
                Res.UserId = ""
                Return Res
            End If
            ' Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Success", Culture_Used)
            Common_Obj.Logs(Date.Now, "User : " & User_ID & " :User Deletion Error : End of File.")
            Res.Msg = "Error : End of File."
            Res.UserId = ""
            Return Res
        Catch ex As Exception
            ' Common_Obj.Logs(Date.Now, "User : " & User_ID & " :Basic Registration Error : " & ex.Message)
            Dim Res As Responce_Msg = New Responce_Msg
            Res.Msg = ex.Message & " - Error Line : " & ex.StackTrace()
            Res.UserId = ""
            Return Res
        End Try
    End Function

    Public Function Users_Email_ID(ByVal Email As String, ByVal Pin As String, ByVal Lang As String) As String Implements IewsUserReg.Users_Email_ID
        Try
            Dim objSQL As New SQLFunction
            Dim uDT As New DataTable
            uDT = objSQL.fillDT("select id from (select (select id from users where users.address_offset_1 = userxinfo.offset) id from userxinfo where entry_number ='9007' and entry='" & Email & "' )a WHERE id IS NOT NULL", CommandType.Text, ConfigurationManager.AppSettings("SymConnStr"))
            If uDT.Rows.Count > 0 Then
                Return uDT.Rows(0).Item(0)
            Else
                Return "User Not Found."
            End If
        Catch ex As Exception
            Return ex.Message + ex.StackTrace
        End Try
    End Function

    Public Function InsUserUpgrade(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                            , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                            , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                            , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                            , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                            , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                            , ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String _
                            ) As Responce_Msg Implements IewsUserReg.InsUserUpgrade
        Dim objResponse_Msg As New Responce_Msg
        Dim objSql As New SQLFunction
        Dim result As String = objSql.exeNonQuery("INSERT INTO UserUpgradeRequest ( User_ID, FullName, DOB, City, Library,  MemberType, Nationality, JobTitle," & _
                           " NationalID, SNeeds, Employer, Gender, NationalIDPhoto1, NationalIDPhoto2, PersonalPhoto, HomePhone," & _
                           " WorkPhone, Ext,  Mobile, POBox, Fax, WebSite, Status, RejectMsg, AdminName, DateCreate, DateUpdate)" & _
                           " VALUES ('" & User_ID & "','" & FullName & "',to_date('" & DOB & "', 'dd/mm/yyyy:hh:mi:ssam'),'" & City & "','" & Library & "','" & MemberType & "','" & Nationality & "','" & JobTitle & "','" & NationalID & _
                           "','" & SNeeds & "','" & Employer & "','" & Gender & "','" & NationalIDPhoto1 & "','" & NationalIDPhoto2 & "','" & PersonalPhoto & _
                           "','" & HomePhone & "','" & WorkPhone & "','" & Ext & "','" & Mobile & "','" & POBox & "','" & Fax & _
                           "','" & WebSite & "','" & Status & "',' ',' ',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
        objResponse_Msg.Msg = result
        Return objResponse_Msg
    End Function

    Public Function InsUserRenewal(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Library As String _
                            , ByVal FullName As String, ByVal DOB As String, ByVal Mobile As String, ByVal City As String _
                            , ByVal MemberType As String, ByVal Nationality As String, ByVal JobTitle As String, ByVal NationalID As String _
                            , ByVal SNeeds As String, ByVal Employer As String, ByVal Gender As String, ByVal NationalIDPhoto1 As String _
                            , ByVal NationalIDPhoto2 As String, ByVal PersonalPhoto As String, ByVal HomePhone As String, ByVal WorkPhone As String _
                            , ByVal Ext As String, ByVal POBox As String, ByVal Fax As String, ByVal WebSite As String _
                            , ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String _
                            ) As Responce_Msg Implements IewsUserReg.InsUserRenewal
        Dim objResponse_Msg As New Responce_Msg
        Dim objSql As New SQLFunction
        Dim result As String = objSql.exeNonQuery("INSERT INTO UserRenewalRequest ( User_ID, FullName, DOB, City, Library,  MemberType, Nationality, JobTitle," & _
                           " NationalID, SNeeds, Employer, Gender, NationalIDPhoto1, NationalIDPhoto2, PersonalPhoto, HomePhone," & _
                           " WorkPhone, Ext,  Mobile, POBox, Fax, WebSite, Status, RejectMsg, AdminName, DateCreate, DateUpdate)" & _
                           " VALUES ('" & User_ID & "','" & FullName & "',to_date('" & DOB & "', 'dd/mm/yyyy:hh:mi:ssam'),'" & City & "','" & Library & "','" & MemberType & "','" & Nationality & "','" & JobTitle & "','" & NationalID & _
                           "','" & SNeeds & "','" & Employer & "','" & Gender & "','" & NationalIDPhoto1 & "','" & NationalIDPhoto2 & "','" & PersonalPhoto & _
                           "','" & HomePhone & "','" & WorkPhone & "','" & Ext & "','" & Mobile & "','" & POBox & "','" & Fax & _
                           "','" & WebSite & "','" & Status & "',' ',' ',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
        objResponse_Msg.Msg = result
        Return objResponse_Msg
    End Function

    Public Function InsUserCancel(ByVal RequestType As String, ByVal LANG As String, ByVal User_ID As String, ByVal Reason As String _
                            , ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateCreate As String, ByVal DateUpdate As String _
                            ) As Responce_Msg Implements IewsUserReg.InsUserCancel
        Dim objResponse_Msg As New Responce_Msg
        Dim objSql As New SQLFunction
        Dim result As String = objSql.exeNonQuery("INSERT INTO UserCancelRequest ( User_ID, Reason, Status, AdminName, DateCreate, DateUpdate)" & _
                           " VALUES ('" & User_ID & "','" & Reason & "','" & Status & "',' ',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
        objResponse_Msg.Msg = result
        Return objResponse_Msg
    End Function

    Public Function SelectUserUpgrade() As List(Of Users.UserUpgRequest) Implements IewsUserReg.SelectUserUpgrade
        Dim objUserUpgRequest As New Users.UserUpgRequest
        Dim objAllUserUpgRequest As New List(Of Users.UserUpgRequest)
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        Try
            ds = objSql.Get_Info_DataSet("SELECT ID, User_ID, FullName, DOB, City, Library,  MemberType, Nationality, JobTitle," & _
                           " NationalID, SNeeds, Employer, Gender, NationalIDPhoto1, NationalIDPhoto2, PersonalPhoto, HomePhone," & _
                           " WorkPhone, Ext,  Mobile, POBox, Fax, WebSite, Status, RejectMsg, AdminName, DateCreate, DateUpdate FROM UserUpgradeRequest WHERE Status = '1' ORDER BY ID DESC ", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objUserUpgRequest = New Users.UserUpgRequest
                objUserUpgRequest.ID = ds.Tables(0).Rows(i).Item(0)
                objUserUpgRequest.User_ID = ds.Tables(0).Rows(i).Item(1)
                objUserUpgRequest.FullName = ds.Tables(0).Rows(i).Item(2)
                objUserUpgRequest.DOB = ds.Tables(0).Rows(i).Item(3)
                objUserUpgRequest.City = ds.Tables(0).Rows(i).Item(4)
                objUserUpgRequest.Library = ds.Tables(0).Rows(i).Item(5)
                objUserUpgRequest.MemberType = ds.Tables(0).Rows(i).Item(6)
                objUserUpgRequest.Nationality = ds.Tables(0).Rows(i).Item(7)
                objUserUpgRequest.JobTitle = ds.Tables(0).Rows(i).Item(8)
                objUserUpgRequest.NationalID = ds.Tables(0).Rows(i).Item(9)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(10)) Then
                    objUserUpgRequest.SNeeds = ds.Tables(0).Rows(i).Item(10)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(11)) Then
                    objUserUpgRequest.Employer = ds.Tables(0).Rows(i).Item(11)
                End If
                objUserUpgRequest.Gender = ds.Tables(0).Rows(i).Item(12)
                objUserUpgRequest.NationalIDPhoto1 = ds.Tables(0).Rows(i).Item(13)
                objUserUpgRequest.NationalIDPhoto2 = ds.Tables(0).Rows(i).Item(14)
                objUserUpgRequest.PersonalPhoto = ds.Tables(0).Rows(i).Item(15)
                objUserUpgRequest.HomePhone = ds.Tables(0).Rows(i).Item(16)
                objUserUpgRequest.WorkPhone = ds.Tables(0).Rows(i).Item(17)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(18)) Then
                    objUserUpgRequest.Ext = ds.Tables(0).Rows(i).Item(18)
                End If
                objUserUpgRequest.Mobile = ds.Tables(0).Rows(i).Item(19)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(20)) Then
                    objUserUpgRequest.POBox = ds.Tables(0).Rows(i).Item(20)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(21)) Then
                    objUserUpgRequest.Fax = ds.Tables(0).Rows(i).Item(21)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(22)) Then
                    objUserUpgRequest.WebSite = ds.Tables(0).Rows(i).Item(22)
                End If
                objUserUpgRequest.Status = ds.Tables(0).Rows(i).Item(23)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(24)) Then
                    objUserUpgRequest.RejectMsg = ds.Tables(0).Rows(i).Item(24)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(25)) Then
                    objUserUpgRequest.AdminName = ds.Tables(0).Rows(i).Item(25)
                End If
                objUserUpgRequest.DateCreate = ds.Tables(0).Rows(i).Item(26)
                objUserUpgRequest.DateUpdate = ds.Tables(0).Rows(i).Item(27)
                objAllUserUpgRequest.Add(objUserUpgRequest)
            Next
            Return objAllUserUpgRequest
        Catch ex As Exception
            objAllUserUpgRequest = New List(Of UserUpgRequest)
            objUserUpgRequest = New Users.UserUpgRequest
            objUserUpgRequest.objException.msg = ex.Message
            objUserUpgRequest.objException.code = "1001"
            objAllUserUpgRequest.Add(objUserUpgRequest)
            Return objAllUserUpgRequest
        End Try
    End Function

    Public Function SelectUserRenewal() As List(Of Users.UserRenRequest) Implements IewsUserReg.SelectUserRenewal
        Dim objUserRenRequest As New Users.UserRenRequest
        Dim objAllUserRenRequest As New List(Of Users.UserRenRequest)
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        Try
            ds = objSql.Get_Info_DataSet("SELECT ID, User_ID, FullName, DOB, City, Library,  MemberType, Nationality, JobTitle," & _
                           " NationalID, SNeeds, Employer, Gender, NationalIDPhoto1, NationalIDPhoto2, PersonalPhoto, HomePhone," & _
                           " WorkPhone, Ext,  Mobile, POBox, Fax, WebSite, Status, RejectMsg, AdminName, DateCreate, DateUpdate FROM UserRenewalRequest WHERE Status = '1' ORDER BY ID DESC ", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objUserRenRequest = New Users.UserRenRequest
                objUserRenRequest.ID = ds.Tables(0).Rows(i).Item(0)
                objUserRenRequest.User_ID = ds.Tables(0).Rows(i).Item(1)
                objUserRenRequest.FullName = ds.Tables(0).Rows(i).Item(2)
                objUserRenRequest.DOB = ds.Tables(0).Rows(i).Item(3)
                objUserRenRequest.City = ds.Tables(0).Rows(i).Item(4)
                objUserRenRequest.Library = ds.Tables(0).Rows(i).Item(5)
                objUserRenRequest.MemberType = ds.Tables(0).Rows(i).Item(6)
                objUserRenRequest.Nationality = ds.Tables(0).Rows(i).Item(7)
                objUserRenRequest.JobTitle = ds.Tables(0).Rows(i).Item(8)
                objUserRenRequest.NationalID = ds.Tables(0).Rows(i).Item(9)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(10)) Then
                    objUserRenRequest.SNeeds = ds.Tables(0).Rows(i).Item(10)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(11)) Then
                    objUserRenRequest.Employer = ds.Tables(0).Rows(i).Item(11)
                End If
                objUserRenRequest.Gender = ds.Tables(0).Rows(i).Item(12)
                objUserRenRequest.NationalIDPhoto1 = ds.Tables(0).Rows(i).Item(13)
                objUserRenRequest.NationalIDPhoto2 = ds.Tables(0).Rows(i).Item(14)
                objUserRenRequest.PersonalPhoto = ds.Tables(0).Rows(i).Item(15)
                objUserRenRequest.HomePhone = ds.Tables(0).Rows(i).Item(16)
                objUserRenRequest.WorkPhone = ds.Tables(0).Rows(i).Item(17)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(18)) Then
                    objUserRenRequest.Ext = ds.Tables(0).Rows(i).Item(18)
                End If
                objUserRenRequest.Mobile = ds.Tables(0).Rows(i).Item(19)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(20)) Then
                    objUserRenRequest.POBox = ds.Tables(0).Rows(i).Item(20)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(21)) Then
                    objUserRenRequest.Fax = ds.Tables(0).Rows(i).Item(21)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(22)) Then
                    objUserRenRequest.WebSite = ds.Tables(0).Rows(i).Item(22)
                End If
                objUserRenRequest.Status = ds.Tables(0).Rows(i).Item(23)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(24)) Then
                    objUserRenRequest.RejectMsg = ds.Tables(0).Rows(i).Item(24)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(25)) Then
                    objUserRenRequest.AdminName = ds.Tables(0).Rows(i).Item(25)
                End If
                objUserRenRequest.DateCreate = ds.Tables(0).Rows(i).Item(26)
                objUserRenRequest.DateUpdate = ds.Tables(0).Rows(i).Item(27)
                objAllUserRenRequest.Add(objUserRenRequest)
            Next
            Return objAllUserRenRequest
        Catch ex As Exception
            objAllUserRenRequest = New List(Of UserRenRequest)
            objUserRenRequest = New Users.UserRenRequest
            objUserRenRequest.objException.msg = ex.Message
            objUserRenRequest.objException.code = "1001"
            objAllUserRenRequest.Add(objUserRenRequest)
            Return objAllUserRenRequest
        End Try
    End Function

    Public Function SelectUserCancel() As List(Of Users.UserCancelRequest) Implements IewsUserReg.SelectUserCancel
        Dim objUserCancelRequest As New Users.UserCancelRequest
        Dim objAllUserCancelRequest As New List(Of Users.UserCancelRequest)
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        Try
            ds = objSql.Get_Info_DataSet("SELECT * FROM UserCancelRequest WHERE Status = '1' ORDER BY ID DESC ", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objUserCancelRequest = New Users.UserCancelRequest
                objUserCancelRequest.ID = ds.Tables(0).Rows(i).Item(0)
                objUserCancelRequest.User_ID = ds.Tables(0).Rows(i).Item(1)
                objUserCancelRequest.Reason = ds.Tables(0).Rows(i).Item(2)
                objUserCancelRequest.Status = ds.Tables(0).Rows(i).Item(3)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(24)) Then
                    objUserCancelRequest.RejectMsg = ds.Tables(0).Rows(i).Item(4)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(25)) Then
                    objUserCancelRequest.AdminName = ds.Tables(0).Rows(i).Item(5)
                End If
                objUserCancelRequest.DateCreate = ds.Tables(0).Rows(i).Item(6)
                objUserCancelRequest.DateUpdate = ds.Tables(0).Rows(i).Item(7)
                objAllUserCancelRequest.Add(objUserCancelRequest)
            Next
            Return objAllUserCancelRequest
        Catch ex As Exception
            objAllUserCancelRequest = New List(Of UserCancelRequest)
            objUserCancelRequest = New Users.UserCancelRequest
            objUserCancelRequest.objException.msg = ex.Message
            objUserCancelRequest.objException.code = "1001"
            objAllUserCancelRequest.Add(objUserCancelRequest)
            Return objAllUserCancelRequest
        End Try

    End Function

    Public Function UpdateUserUpgrade(ByVal ID As String, ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateUpdate As String) As String Implements IewsUserReg.UpdateUserUpgrade
        Dim objResponse_Msg As New Responce_Msg
        Dim objSql As New SQLFunction
        Dim result As String = objSql.exeNonQuery("UPDATE UserUpgradeRequest SET Status = '" & Status & "',RejectMsg='" & RejectMsg & "',DateUpdate = to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam')  WHERE ID = '" & ID & "' ", ConfigurationManager.AppSettings("ESConnStr"))
        Return result
    End Function

    Public Function UpdateUserRenewal(ByVal ID As String, ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateUpdate As String) As String Implements IewsUserReg.UpdateUserRenewal
        Dim objResponse_Msg As New Responce_Msg
        Dim objSql As New SQLFunction
        Dim result As String = objSql.exeNonQuery("UPDATE UserRenewalRequest SET Status = '" & Status & "',RejectMsg='" & RejectMsg & "',DateUpdate = to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam')) WHERE ID = '" & ID & "' ", ConfigurationManager.AppSettings("ESConnStr"))
        Return result
    End Function

    Public Function UpdateUserCancel(ByVal ID As String, ByVal Status As String, ByVal RejectMsg As String, ByVal AdminName As String, ByVal DateUpdate As String) As String Implements IewsUserReg.UpdateUserCancel
        Dim objResponse_Msg As New Responce_Msg
        Dim objSql As New SQLFunction
        Dim result As String = objSql.exeNonQuery("UPDATE UserCancelRequest SET Status = '" & Status & "',RejectMsg='" & RejectMsg & "',DateUpdate = to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam')) WHERE ID = '" & ID & "' ", ConfigurationManager.AppSettings("ESConnStr"))
        Return result
    End Function

    Public Function ForgetPassword(ByVal Email As String) As String Implements IewsUserReg.ForgetPassword
        Try
            Dim objSQL As New SQLFunction
            Dim uDT As New DataTable
            Dim r As New Random
            uDT = objSQL.fillDT("select * from (select (select id from users where users.address_offset_1 = userxinfo.offset) id from userxinfo where entry_number ='9007' and entry='" & Email & "') where id is not null", CommandType.Text, ConfigurationManager.AppSettings("SymConnStr"))
            If uDT.Rows.Count > 0 Then
                Dim AuthKey As String = "SPL" + Obj_EncDec.RandomString(r)
                'Dim queryRes As String = objSQL.exeNonQuery("UPDATE USERS SET PIN='" & AuthKey & "' WHERE ID='" & uDT.Rows(0).Item(0) & "'", ConfigurationManager.AppSettings("SymConnStr"))
                FileName_Users = Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & "_" & uDT.Rows(0).Item(0) & "_" & "UserResP" & ".txt"
                FileName_Users = ConfigurationManager.AppSettings("Files_Path_P") + FileName_Users
                Common_Obj.SaveTextToFile("*** DOCUMENT BOUNDARY ***", FileName_Users)
                Common_Obj.SaveTextToFile("FORM=LDUSER", FileName_Users)
                Dim USERID As String = ".USER_ID.   |a" & uDT.Rows(0).Item(0)
                Common_Obj.SaveTextToFile(USERID, FileName_Users)
                Dim USER_PIN As String = ".USER_PIN.   |a" & AuthKey.ToUpper
                Common_Obj.SaveTextToFile(USER_PIN, FileName_Users)

                Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
                pProcess_Users.StartInfo.FileName = "cmd.exe"
                pProcess_Users.StartInfo.WorkingDirectory = "D:\SIRSI\Unicorn\Bin\"
                pProcess_Users.StartInfo.UseShellExecute = False
                pProcess_Users.StartInfo.RedirectStandardOutput = True
                pProcess_Users.StartInfo.RedirectStandardInput = True
                pProcess_Users.StartInfo.RedirectStandardError = True
                pProcess_Users.StartInfo.CreateNoWindow = True
                pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
                pProcess_Users.Start()

                Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput

                MyStreamWriter_Users.WriteLine("loadflatuser.exe -aU -bU -mu -l""ADMIN|TTY0"" -y" & ConfigurationManager.AppSettings("DEFAULT_LIBR").ToUpper & " < """ & FileName_Users & """")
                pProcess_Users.StandardInput.Close()

                Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()

                If Output_UserReg.Contains("1 $<D222> $<user> $(1418)") = True Then
                    Obj_Internet.SendStringEmail(ConfigurationManager.AppSettings("MailUser"), ConfigurationManager.AppSettings("MailPassowrd"), ConfigurationManager.AppSettings("MailUser"), ConfigurationManager.AppSettings("MailServerIP"), ConfigurationManager.AppSettings("MailServerPort"), "Your New Password is : " + AuthKey, Email, "Sharjah Public Library - Reset Password", "")
                    Return "New Password sent to your Email"
                Else
                    Return Output_UserReg
                End If
            Else
                Return "User Not Found."
            End If
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    Public Function ReserveUserDetails(ByVal RequestType As String, ByVal User_ID As String, ByVal LIBR As String, ByVal FullName As String, ByVal Title As String, ByVal Company As String, ByVal Nationality As String, _
                                ByVal Mobile As String, ByVal Telephone As String, ByVal Fax As String, ByVal PostBox As String, ByVal Email As String, _
                                ByVal City As String, ByVal MemberShipType As String, ByVal LANG As String) As Responce_Msg Implements IewsUserReg.ReserveUserDetails
        Try
            Dim Res As Responce_Msg = New Responce_Msg
            Dim IsUser_Exist As Boolean = False
            Dim curTime As DateTime = Date.Now
            FileName_Users = ""

            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If LANG = "ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If
            '//////////////////////Call to check if User ID Already Exist or not ?!!///////////////////////////////////////////////////
            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            'pProcess.StartInfo.Arguments = "-iBV"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True

            pProcess.Start()

            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput
            MyStreamWriter.WriteLine("D:\SIRSI\Unicorn\Bin\seluser.exe -iB")

            MyStreamWriter.WriteLine(User_ID.ToUpper + System.Environment.NewLine)
            pProcess.StandardInput.Close()

            Dim output As String = pProcess.StandardOutput.ReadToEnd()

            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()

            If output.Contains("|") = True Then
                IsUser_Exist = True
            Else
                IsUser_Exist = False
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserNotExist", Culture_Used)
                Res.UserId = ""
                Return Res
            End If

            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            If Common_Obj.Fields_Validation(RequestType) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_RequestType_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            End If

            If Common_Obj.Fields_Validation(LIBR) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_LIBR_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            End If


            If IsNumeric(MemberShipType) = True Then
                If MemberShipType < 0 Or MemberShipType > 6 Then
                    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Membership_Type_Invalid", Culture_Used)
                    Res.UserId = ""
                    Return Res
                End If
            ElseIf IsNumeric(MemberShipType) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Membership_Type_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            End If

            If MemberShipType <> 2 Then
                If Common_Obj.Fields_Validation(FullName) = False Then
                    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_FullName_Invalid", Culture_Used)
                    Res.UserId = ""
                    Return Res
                End If
            End If

            If Common_Obj.Fields_Validation(Nationality) = False Then
                Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Nationality_Invalid", Culture_Used)
                Res.UserId = ""
                Return Res
            End If


            'If IsNumeric(Mobile) = False Then
            '    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Mob_No_Invalid", Culture_Used)
            '    Res.UserId = ""
            '    Return Res
            'End If
            '//////////////////////Construct User File/////////////////////////////////////////////////////////////////////////////////
            FileName_Users = Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & "_" & User_ID & "_" & "User" & ".txt"
            FileName_Users = ConfigurationManager.AppSettings("Files_Path_P") + FileName_Users

            Common_Obj.SaveTextToFile("*** DOCUMENT BOUNDARY ***", FileName_Users)
            Common_Obj.SaveTextToFile("FORM=LDUSER", FileName_Users)

            Dim USERID As String = ".USER_ID.   |a" & User_ID.Trim.ToUpper
            Common_Obj.SaveTextToFile(USERID, FileName_Users)

            Dim USER_NAME As String = ConfigurationManager.AppSettings("FullName") & "   |a" & FullName.Trim.ToUpper

            Common_Obj.SaveTextToFile(USER_NAME, FileName_Users)

            Dim USER_LIB As String = ConfigurationManager.AppSettings("LIBR") & "   |a" & LIBR.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_LIB, FileName_Users)

            If Nationality IsNot Nothing Then
                Dim USER_Nationality As String = ConfigurationManager.AppSettings("Nationality") & "   |a" & Nationality.Trim.ToUpper
                Common_Obj.SaveTextToFile(USER_Nationality, FileName_Users)
            End If

            If Title IsNot Nothing Then
                Dim USER_Title As String = ConfigurationManager.AppSettings("JobTitle") & "   |a" & Title.Trim.ToUpper
                Common_Obj.SaveTextToFile(USER_Title, FileName_Users)
            End If

            If City IsNot Nothing Then
                Dim USER_City As String = ConfigurationManager.AppSettings("City") & "   |a" & City.Trim.ToUpper
                Common_Obj.SaveTextToFile(USER_City, FileName_Users)
            End If


            Dim USER_ADDR1_BEGIN As String = ".USER_ADDR1_BEGIN."
            Common_Obj.SaveTextToFile(USER_ADDR1_BEGIN, FileName_Users)

            Dim USER_Telephone As String = ConfigurationManager.AppSettings("Telephone") & "   |a" & Telephone.ToString
            Common_Obj.SaveTextToFile(USER_Telephone, FileName_Users)

            Dim USER_Mobile As String = ConfigurationManager.AppSettings("Mobile") & "   |a" & Mobile.ToString
            Common_Obj.SaveTextToFile(USER_Mobile, FileName_Users)

            Dim USER_PostBox As String = ConfigurationManager.AppSettings("PostBox") & "   |a" & PostBox.ToString
            Common_Obj.SaveTextToFile(USER_PostBox, FileName_Users)

            Dim USER_Fax As String = ConfigurationManager.AppSettings("Fax") & "   |a" & Fax.ToString
            Common_Obj.SaveTextToFile(USER_Fax, FileName_Users)

            If Company IsNot Nothing Then
                Dim USER_Company As String = ConfigurationManager.AppSettings("Company") & "   |a" & Company.ToString
                Common_Obj.SaveTextToFile(USER_Company, FileName_Users)
            End If

            If RequestType IsNot Nothing Then
                Dim USER_RequestType As String = ConfigurationManager.AppSettings("RequestType") & "   |a" & RequestType.ToString
                Common_Obj.SaveTextToFile(USER_RequestType, FileName_Users)
            End If

            If IsUser_Exist = True Then

                Dim USER_ADDR1_END As String = ".USER_ADDR1_END."
                Common_Obj.SaveTextToFile(USER_ADDR1_END, FileName_Users)
                '/////////////////////////////////// Start Loading Users///////////////////////////////////
                Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
                pProcess_Users.StartInfo.FileName = "cmd.exe"
                pProcess_Users.StartInfo.WorkingDirectory = "D:\SIRSI\Unicorn\Bin\"
                pProcess_Users.StartInfo.UseShellExecute = False
                pProcess_Users.StartInfo.RedirectStandardOutput = True
                pProcess_Users.StartInfo.RedirectStandardInput = True
                pProcess_Users.StartInfo.RedirectStandardError = True
                pProcess_Users.StartInfo.CreateNoWindow = True
                pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
                pProcess_Users.Start()

                Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput
                MyStreamWriter_Users.WriteLine("loadflatuser.exe -aU -bU -n -mu -l""ADMIN|TTY0"" -y" & ConfigurationManager.AppSettings("DEFAULT_LIBR").ToUpper & " < """ & FileName_Users & """")
                pProcess_Users.StandardInput.Close()
                Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()

                If Output_UserReg.Contains("1 $<D222> $<user>") = True Or Output_UserReg.Contains("1 $<existing> $<user>") = True Then
                    If FileName_Users <> "" Then
                        Common_Obj.DeleteFiles("", FileName_Users)
                    End If
                    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserMod_Success", Culture_Used)
                    Res.UserId = User_ID
                    Return Res
                Else
                    If FileName_Users <> "" Then
                        Common_Obj.DeleteFiles("", FileName_Users)
                    End If
                    If Output_UserReg.Contains("**") = True Then
                        Dim Start_Index As Double = Output_UserReg.IndexOf("**")
                        Dim Last_Index As Double = 0
                        If Output_UserReg.Contains("*** DOCUMENT BOUNDARY ***") = True Then
                            Last_Index = Output_UserReg.IndexOf("*** DOCUMENT BOUNDARY ***")
                        End If

                        If Last_Index <> 0 Then
                            Res.Msg = Output_UserReg.Substring(Start_Index, Last_Index - Start_Index)
                        Else
                            Res.Msg = Output_UserReg
                        End If

                        Res.UserId = ""
                        Common_Obj.Logs(Date.Now, Res.Msg)
                        Return Res
                    End If
                    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserMod_Error", Culture_Used)
                    Res.UserId = ""
                    Common_Obj.Logs(Date.Now, Output_UserReg)
                    Return Res
                End If
                '//////////////////////////////////////////////////////////////////////////////////////////
            End If
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ' Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Success", Culture_Used)
            Common_Obj.Logs(Date.Now, "User : " & USERID & " : Modification Error : End of File.")
            Res.Msg = "Error : End of File."
            Res.UserId = ""
            Return Res
        Catch ex As Exception
            If FileName_Users <> "" Then
                Common_Obj.DeleteFiles("", FileName_Users)
            End If
            Common_Obj.Logs(Date.Now, "User : " & User_ID & " : Modification Error : " & ex.Message)
            Dim Res As Responce_Msg = New Responce_Msg
            Res.Msg = ex.Message & " - Error Line : " & ex.StackTrace()
            Res.UserId = ""
            Return Res
        End Try
    End Function

    Public Function ReserveUserInfo(ByVal User_ID As String, ByVal LANG As String) As Users.ReseveUserInfo Implements IewsUserReg.ReserveUserInfo
        Dim objReserveUserInfo As New Users.ReseveUserInfo
        Dim objSQL As New SQLFunction
        Dim dt As New DataTable
        Dim sqlStr As String = "select id,name_key,(select policy_Name from policy where policy_type = 'LIBR' and policy_Number=users.library) library," & _
        " (select entry from userxinfo where userxinfo.offset=users.address_offset_1 and entry_number='9007') email," & _
        " (select entry from userxinfo where userxinfo.offset=users.address_offset_1 and entry_number='9025') Mobile," & _
        " (select entry from userxinfo where userxinfo.offset=users.address_offset_1 and entry_number='9009') Telephone," & _
        " (select entry from userxinfo where userxinfo.offset=users.address_offset_1 and entry_number='9006') Fax," & _
        " (select entry from userxinfo where userxinfo.offset=users.address_offset_1 and entry_number='9004') POBox," & _
        " (select entry from userxinfo where userxinfo.offset=users.address_offset_1 and entry_number='9028') Company," & _
        " (select entry from userxinfo where userxinfo.offset=users.address_offset_1 and entry_number='9038') MemType," & _
        " (select policy_Name from policy where policy_type = 'CAT3' and policy_Number=(select value from ucat where USER_KEY = users.USER_KEY and CATEGORY = '3' ) ) Title," & _
        " (select policy_Name from policy where policy_type = 'CAT4' and policy_Number=(select value from ucat where USER_KEY = users.USER_KEY and CATEGORY = '4' ) ) Nat," & _
        " (select policy_Name from policy where policy_type = 'CAT5' and policy_Number=(select value from ucat where USER_KEY = users.USER_KEY and CATEGORY = '5' ) ) City" & _
        " from USERS where id='" & User_ID & "'"
        dt = objSQL.fillDT(sqlStr, CommandType.Text, ConfigurationManager.AppSettings("SymConnStr"))
        If dt.Rows.Count > 0 Then
            objReserveUserInfo.User_ID = If(Not IsDBNull(dt.Rows(0).Item(0)), dt.Rows(0).Item(0), "")
            objReserveUserInfo.FullName = If(Not IsDBNull(dt.Rows(0).Item(1)), dt.Rows(0).Item(1), "")
            objReserveUserInfo.Title = If(Not IsDBNull(dt.Rows(0).Item(10)), dt.Rows(0).Item(10), "")
            objReserveUserInfo.Company = If(Not IsDBNull(dt.Rows(0).Item(8)), dt.Rows(0).Item(8), "")
            objReserveUserInfo.Nationality = If(Not IsDBNull(dt.Rows(0).Item(11)), dt.Rows(0).Item(11), "")
            objReserveUserInfo.Mobile = If(Not IsDBNull(dt.Rows(0).Item(4)), dt.Rows(0).Item(4), "")
            objReserveUserInfo.Telephone = If(Not IsDBNull(dt.Rows(0).Item(5)), dt.Rows(0).Item(5), "")
            objReserveUserInfo.Fax = If(Not IsDBNull(dt.Rows(0).Item(6)), dt.Rows(0).Item(6), "")
            objReserveUserInfo.PostBox = If(Not IsDBNull(dt.Rows(0).Item(7)), dt.Rows(0).Item(7), "")
            objReserveUserInfo.Email = If(Not IsDBNull(dt.Rows(0).Item(3)), dt.Rows(0).Item(3), "")
            objReserveUserInfo.City = If(Not IsDBNull(dt.Rows(0).Item(12)), dt.Rows(0).Item(12), "")
            objReserveUserInfo.MemberShipType = If(Not IsDBNull(dt.Rows(0).Item(9)), dt.Rows(0).Item(9), "")
            Return objReserveUserInfo
        End If
        Return Nothing
    End Function

    Public Function UserPayment(ByVal LANG As String, ByVal UserID As String, ByVal Payment_DateTime As String, ByVal Library As String, ByVal Bill_Number As String, ByVal Bill_Amount As String, ByVal Payment_Type As String) As Responce_Msg Implements IewsUserReg.UserPayment
        Dim res As New Responce_Msg
        Try
            UserID = UserID.ToUpper
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If LANG = "ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If

            '//////////////////////Call to check if User  ID Already Exist or not ?!!///////////////////////////////////////////////////

            Dim output_userexit As String = Common_Obj.GetUserDetails("D:\SIRSI\Unicorn\Bin\seluser.exe -iB", UserID)
            If output_userexit.Contains("|") = False Then
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserNotExist", Culture_Used)
                res.Code = ""
                Return res
            End If

            '//////////////////////Validation///////////////////////////////////////////////////
            If Common_Obj.Fields_Validation(Library) = False Then
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_LIBR_Invalid", Culture_Used)
                res.Code = ""
                Return res
            End If
            If Common_Obj.Fields_Validation(Bill_Number) = False Then
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Bill_Number_Invalid", Culture_Used)
                res.Code = ""
                Return res
            End If
            If Common_Obj.Fields_Validation(Payment_DateTime) = False Then
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Payment_DateTime_Invalid", Culture_Used)
                res.Code = ""
                Return res
            End If
            If Common_Obj.Fields_Validation(Bill_Amount) = False Then
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Bill_Amount_Invalid", Culture_Used)
                res.Code = ""
                Return res
            End If
            If Common_Obj.Fields_Validation(Payment_Type) = False Then
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Payment_Type_Invalid", Culture_Used)
                res.Code = ""
                Return res
            End If
            ' res.Code += "a"

            res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Payment_Type_Invalid", Culture_Used)

            'Dim Bill_Amount_Array() As String = Bill_Amount.Split(".")
            'If Bill_Amount_Array.Length = 1 Then
            '    Bill_Amount = Bill_Amount & ".00"
            'End If
            'If Bill_Amount_Array.Length = 2 Then
            '    If Bill_Amount_Array(1).Length = 1 Then
            '        Bill_Amount = Bill_Amount & "0"
            '    End If
            'End If


            'If Bill_Amount = False Then
            '    res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Payment_Type_Invalid", Culture_Used)
            '    res.Code = ""
            '    Return res
            'End If

            '/////////////////////////////////// Start Loading Payment ///////////////////////////////////
            ' res.Code += "b"
            Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess_Users.StartInfo.FileName = "cmd.exe"
            pProcess_Users.StartInfo.UseShellExecute = False
            pProcess_Users.StartInfo.RedirectStandardOutput = True
            pProcess_Users.StartInfo.RedirectStandardInput = True
            pProcess_Users.StartInfo.RedirectStandardError = True
            pProcess_Users.StartInfo.CreateNoWindow = True
            pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
            pProcess_Users.Start()

            pProcess_Users.StandardInput.AutoFlush = True
            Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput
            MyStreamWriter_Users.WriteLine("apiserver.exe")

            Dim PaymentReq As String = ""
            'E201710230434000012R ^S54PYFFSIRSI^FECENTRAL^FcNONE^FWSIRSI^FDPCGUI-DISP^UO11539^BJ5^BNAED214.00^BA10.00^BECREDITCARD^BHAED10.00^z2AUTOPAY^usY^dC3^Fv3000000^^O
            'E231020170346360010R ^S39PYFFSIRSI^FECENTRAL^FcNONE^FWSIRSI^FDPCGUI-DISP^UO11539^BJ1^BNAEDA15.00^BA15.00^BECREDITCARD^z2AUTOPAY^usY^dC3^Fv3000000^^O
            PaymentReq = "E" & Payment_DateTime & "0010R ^S39PYFFSIRSI^FE" & Library.Trim & "^FcNONE^FWSIRSI^FDPCGUI-DISP^UO" & UserID & "^BJ" & Bill_Number & "^BN" & Bill_Amount & "^BA" & Bill_Amount.Replace("AED", "") & "^BE" & Payment_Type & "^BH" & Bill_Amount & "^z2AUTOPAY^usY^dC3^Fv3000000^^O"
            MyStreamWriter_Users.WriteLine(PaymentReq + System.Environment.NewLine)
            'res.Code += "c" + "|||" + PaymentReq + "|||"
            pProcess_Users.StandardInput.Close()
            Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()
            Dim Output As String = pProcess_Users.StandardOutput.ReadToEnd()

            If Output.Contains("^UO") = True Then
                res.Msg = "Payment_Success"
                res.Code = Bill_Amount
                Return res
            Else
                '      res.Code += Output + "}*}" + Output_UserReg
                Dim Start_Index As Double = Output.IndexOf("MA$")
                Dim Last_Index As Double = Output.IndexOf("^MN")
                If Last_Index <> 0 Then
                    res.Msg = Output.Substring(Start_Index, Last_Index - Start_Index)
                    res.Msg = res.Msg.Replace("MA", "")
                    Dim output_Msg As String = Common_Obj.GetUserDetails("Translate", res.Msg) ' Translate Msg
                    Dim Start_Index_Msg As Double = output_Msg.IndexOf("inetsrv>")
                    Dim Last_Index_Msg As Double = output_Msg.LastIndexOf("c:\")
                    If Last_Index_Msg <> 0 Then
                        res.Msg = output_Msg.Substring(Start_Index_Msg, Last_Index_Msg - Start_Index_Msg)
                        res.Msg = res.Msg.Replace("inetsrv>Translate", "")
                    Else
                        res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserBills_Error", Culture_Used)
                    End If
                Else
                    res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserBills_Error", Culture_Used)
                End If
                '  Res.Msg = Output_UserReg
                res.Code = "1"
                Return res
            End If


            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            ' Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Success", Culture_Used)
            'Common_Obj.Logs(Now.Date, "Error : End of File.")
            res.Msg = "Error : End of File."
            res.Code = "2"
            Return res
        Catch ex As Exception
            res.Msg = ex.Message
            res.UserId = ""
            res.Code = "3"
            Return res
        End Try
    End Function

    Public Function UserBillsMethod(ByVal LANG As String, ByVal UserID As String, ByVal ItemID As String, ByVal Library As String, ByVal Amount As String, ByVal Reason As String) As Responce_Msg Implements IewsUserReg.UserBillsMethod
        Dim res As New Responce_Msg
        Try
            FileName_Users = ""
            UserID = UserID.ToUpper
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If LANG = "ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If

            '//////////////////////Call to check if User  ID Already Exist or not ?!!///////////////////////////////////////////////////

            Dim output_userexit As String = Common_Obj.GetUserDetails("D:\SIRSI\Unicorn\Bin\seluser.exe -iB", UserID)
            If output_userexit.Contains("|") = False Then
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserNotExist", Culture_Used)
                res.Code = ""
                Return res
            End If

            '//////////////////////Validation///////////////////////////////////////////////////
            'If Common_Obj.Fields_Validation(ItemID) = False Then
            '    Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_ItemID_Invalid", Culture_Used)
            '      Res.Amount = ""
            '    Return Res
            'End If

            '//////////////////////Construct User File/////////////////////////////////////////////////////////////////////////////////

            FileName_Users = Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & "_" & UserID & "_" & "UserBill" & ".txt"
            FileName_Users = ConfigurationManager.AppSettings("Files_Path_P") + FileName_Users

            Common_Obj.SaveTextToFile("*** DOCUMENT BOUNDARY ***", FileName_Users)
            Common_Obj.SaveTextToFile("FORM=LDBILL", FileName_Users)

            Dim USER_ID As String = ".USER_ID.   |a" & UserID.Trim.ToUpper
            Common_Obj.SaveTextToFile(USER_ID, FileName_Users)

            If ItemID IsNot Nothing Then
                If ItemID.Trim <> "" Then
                    Dim User_Item_ID As String = ".ITEM_ID. |a" & ItemID.ToString
                    Common_Obj.SaveTextToFile(User_Item_ID, FileName_Users)
                End If
            End If

            Dim BILL_LIBRARY As String = ".BILL_LIBRARY.   |a" & Library.Trim.ToUpper
            Common_Obj.SaveTextToFile(BILL_LIBRARY, FileName_Users)

            Dim User_DateCharged As String = ".BILL_DB. |a" & Now.Date.Year.ToString("0000") & Now.Date.Month.ToString("00") & Now.Date.Day.ToString("00")
            Common_Obj.SaveTextToFile(User_DateCharged, FileName_Users)

            '/////////////////////////////////////////Get Bill Amount fro Reason////////////////////////////////////////////////////////////////

            'Dim Amount As String = ""

            'Dim conn As New SqlConnection
            'Dim cmd As New SqlCommand
            'Dim dr As SqlDataReader
            'Try
            '    conn = New SqlConnection(ConfigurationManager.ConnectionStrings("DB_Con_SC").ConnectionString)
            '    cmd = New SqlCommand("sp_dcaa_bills_types", conn)
            '    cmd.CommandType = CommandType.StoredProcedure

            '    cmd.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ""
            '    cmd.Parameters.Add("@Type", SqlDbType.NVarChar).Value = Reason
            '    cmd.Parameters.Add("@AraDsrp", SqlDbType.NVarChar).Value = ""
            '    cmd.Parameters.Add("@EngDsrp", SqlDbType.NVarChar).Value = ""
            '    cmd.Parameters.Add("@Amount", SqlDbType.NVarChar).Value = ""
            '    cmd.Parameters.Add("@Statment", SqlDbType.NVarChar).Value = "Select"

            '    conn.Open()
            '    dr = cmd.ExecuteReader
            '    While (dr.Read())
            '        Amount = dr.GetValue(4)
            '    End While
            '    dr.Close()

            'Catch ex As Exception
            '    res.Msg = ex.Message
            '    res.Amount = ""
            '    Return res
            'Finally
            '    cmd = Nothing
            '    conn.Close()
            '    conn = Nothing
            'End Try

            '/////////////////////////////////// Start Loading Users///////////////////////////////////
            If Amount.Trim = "" Then
                res.Msg = "No Amount fornd for this Reason"
                res.Code = ""
                Return res
            Else
                Dim BILL_AMOUNT As String = ".BILL_AMOUNT.   |a" & Amount.Trim
                Common_Obj.SaveTextToFile(BILL_AMOUNT, FileName_Users)

                Dim BILL_REASON As String = ".BILL_REASON.   |a" & Reason.Trim
                Common_Obj.SaveTextToFile(BILL_REASON, FileName_Users)
            End If


            '/////////////////////////////////// Start Loading Bills///////////////////////////////////

            Dim pProcess_Users As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess_Users.StartInfo.FileName = "cmd.exe"
            pProcess_Users.StartInfo.WorkingDirectory = "D:\SIRSI\Unicorn\Bin\"
            pProcess_Users.StartInfo.UseShellExecute = False
            pProcess_Users.StartInfo.RedirectStandardOutput = True
            pProcess_Users.StartInfo.RedirectStandardInput = True
            pProcess_Users.StartInfo.RedirectStandardError = True
            pProcess_Users.StartInfo.CreateNoWindow = True
            pProcess_Users.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8
            pProcess_Users.Start()


            Dim MyStreamWriter_Users As System.IO.StreamWriter = pProcess_Users.StandardInput

            MyStreamWriter_Users.WriteLine("loadbill.exe -l""ADMIN|TTY0"" -y" & ConfigurationManager.AppSettings("DEFAULT_LIBR").ToUpper & " < """ & FileName_Users & """")
            pProcess_Users.StandardInput.Close()
            Dim Output_UserReg As String = pProcess_Users.StandardError.ReadToEnd()
            If Output_UserReg.Contains("1 $<bill> $(1419)") = True Then
                If FileName_Users <> "" Then
                    Common_Obj.DeleteFiles("", FileName_Users)
                End If
                res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Bills_Success", Culture_Used)
                res.Code = Amount
                Return res
            Else

                If FileName_Users <> "" Then
                    '    Common_Obj.DeleteFiles("", FileName_Users)
                End If


                Dim Start_Index As Double = 15
                Dim Last_Index As Double = 0
                If Output_UserReg.Contains("Symphony") = True Then
                    Last_Index = Output_UserReg.IndexOf("Symphony")
                End If
                If Last_Index <> 0 Then
                    res.Msg = Output_UserReg.Substring(Start_Index, Last_Index - Start_Index)
                Else
                    res.Msg = Resources.Messages.ResourceManager.GetString("Msg_UserBills_Error", Culture_Used)
                End If
                res.Msg = Output_UserReg
                res.Code = ""
                Common_Obj.Logs(Now.Date, res.Msg)
                Return res
            End If


            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            ' Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Success", Culture_Used)
            Common_Obj.Logs(Now.Date, "Error : End of File.")
            res.Msg = "Error : End of File."
            res.Code = ""
            Return res
        Catch ex As Exception
            res.Msg = ex.Message
            res.UserId = ""
            res.Code = "3"
            Return res
        End Try
    End Function

    Public Function UserPin(ByVal User_ID As String) As UserPin Implements IewsUserReg.UserPin
        Dim objUserPin As New UserPin
        Try
            Dim output_userexit As String = Common_Obj.GetUserDetails("D:\SIRSI\Unicorn\Bin\seluser.exe -iB", User_ID)
            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True
            pProcess.Start()
            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput
            MyStreamWriter.WriteLine("echo " & User_ID & "|seluser -iB|dumpflatuser")
            MyStreamWriter.WriteLine(System.Environment.NewLine)
            pProcess.StandardInput.Close()
            Dim output As String = pProcess.StandardOutput.ReadToEnd
            Dim output_array() As String = output.Split(System.Environment.NewLine)
            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()
            objUserPin.Pin = ""

            For i As Integer = 0 To output_array.Length - 1
                If output_array(i).Contains(".USER_PIN.   |a") Then
                    objUserPin.Pin = output_array(i).Replace(".USER_PIN.   |a", "")
                    objUserPin.Pin = objUserPin.Pin.Replace(vbLf, [String].Empty)
                End If
            Next

            If Output_Error.Contains("**") = True Then
                objUserPin.objException.msg = "Error" + Output_Error
                objUserPin.User_ID = ""
                Return objUserPin
            Else
                objUserPin.objException.msg = ""
                objUserPin.User_ID = User_ID
                Return objUserPin
            End If

            objUserPin.objException.msg = "Error : End of File."
            objUserPin.User_ID = ""
            Return objUserPin
        Catch ex As Exception
            objUserPin.objException.msg = ex.Message
            objUserPin.User_ID = ""
            Return objUserPin
        End Try
    End Function

    Public Function UserActivate(ByVal Email As String) As String Implements IewsUserReg.UserActivate
        Try
            Dim objSQL As New SQLFunction
            Dim uDT As New DataTable
            Dim objEncDec As New EncDec
            Dim r As New Random
            uDT = objSQL.fillDT("SELECT TEMPVALUE FROM tbl_notification_email_temp WHERE TEMPNAME='NewReg'", CommandType.Text, ConfigurationManager.AppSettings("ESConnStr"))
            Dim ActivationCode As String = objEncDec.RandomStringMinMax(r, 25, 30)
            Dim insertActivation As String = objSQL.exeNonQuery("INSERT INTO USERACTIVATION (Email,ActivationCode,Status) VALUES ('" & Email & "','" & ActivationCode & "','1')", ConfigurationManager.AppSettings("ESConnStr"))
            Dim emailText As String = uDT.Rows(0).Item(0)
            Dim serviceURL As String = ConfigurationManager.AppSettings("EservicesURL")
            emailText = emailText.Replace("ACTURLENG", serviceURL + "wfrmActivateUser?ActivationCode=" & ActivationCode & "&Email=" & Email)
            emailText = emailText.Replace("ACTURLARA", serviceURL + "wfrmActivateUser?Lang=Ara&ActivationCode=" & ActivationCode & "&Email=" & Email)
            Dim emailStr As String = Obj_Internet.SendStringEmail(ConfigurationManager.AppSettings("MailUser"), ConfigurationManager.AppSettings("MailPassowrd"), ConfigurationManager.AppSettings("MailUser"), ConfigurationManager.AppSettings("MailServerIP"), ConfigurationManager.AppSettings("MailServerPort"), emailText, Email, "Sharjah Public Library - Activate Account", "")
            Return "1" + emailStr
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function GetUserActivate(ByVal Email As String, ByVal ActivationCode As String) As String Implements IewsUserReg.GetUserActivate
        Try
            Dim objSQL As New SQLFunction
            Dim uDT As New DataTable
            uDT = objSQL.fillDT("SELECT * FROM USERACTIVATION WHERE Email = '" & Email & "' AND ActivationCode = '" & ActivationCode & "' AND Status = '1'", CommandType.Text, ConfigurationManager.AppSettings("ESConnStr"))
            If uDT.Rows.Count > 0 Then
                Return "1"
            Else
                Return "0"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function UptUserActivate(ByVal Email As String, ByVal ActivationCode As String) As String Implements IewsUserReg.UptUserActivate
        Try
            Dim objSQL As New SQLFunction
            objSQL.exeNonQuery("UPDATE USERACTIVATION SET Status='2' WHERE Email = '" & Email & "' AND ActivationCode = '" & ActivationCode & "'", ConfigurationManager.AppSettings("ESConnStr"))
           
            Return "1"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function Users_Email(ByVal UserID As String) As String Implements IewsUserReg.Users_Email
        Try
            'select entry from userxinfo where entry_number ='9007' and offset = (select address_offset_1 from users where id='12051')
            Dim objSQL As New SQLFunction
            Dim uDT As New DataTable
            uDT = objSQL.fillDT("select entry from userxinfo where entry_number ='9007' and offset = (select address_offset_1 from users where id='" & UserID & "')", CommandType.Text, ConfigurationManager.AppSettings("SymConnStr"))
            If uDT.Rows.Count > 0 Then
                Return uDT.Rows(0).Item(0)
            Else
                Return "0"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function GetActivated(ByVal Email As String) As String Implements IewsUserReg.GetActivated
        Try
            Dim objSQL As New SQLFunction
            Dim uDT As New DataTable
            uDT = objSQL.fillDT("SELECT Status FROM USERACTIVATION WHERE Email = '" & Email & "' ORDER BY ID DESC", CommandType.Text, ConfigurationManager.AppSettings("ESConnStr"))
            If uDT.Rows.Count > 0 Then
                Return uDT.Rows(0).Item(0)
            Else
                Return "0"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function InsUsersOPT(ByVal User_ID As String, ByVal flag_Email As String, ByVal flag_SMS As String, ByVal flag_Mobile As String, ByVal Status As String, ByVal Reason As String, ByVal OPT As String, ByVal Admin_ID As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String Implements IewsUserReg.InsUsersOPT
        Try
            Dim objSql As New SQLFunction
            'user_id,flag_email,flag_SMS,flag_Mobile,status,reason,opt,admin_id,datecreate,dateupdate
            Dim result As String = objSql.exeNonQuery("INSERT INTO users_opt ( User_ID, flag_Email, flag_SMS, flag_Mobile, Status, Reason, OPT, Admin_ID" & _
                               ", DateCreate, DateUpdate)" & _
                               " VALUES ('" & User_ID & "','" & flag_Email & "','" & flag_SMS & "','" & flag_Mobile & "','" & Status & "','" & Reason & "','" & OPT & "','" & Admin_ID & "'" & _
                               ",to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
            Return result
        Catch ex As Exception
            Return ex.Message 
        End Try
    End Function
End Class
