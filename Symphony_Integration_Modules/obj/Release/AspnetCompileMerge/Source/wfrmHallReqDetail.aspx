﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmHallReqDetail.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmHallReqDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style type="text/css">
        .photo img {
            width: 150px;
            height: 150px;
        }

        .photo, .details {
            border-color: #c4c4c4;
            border-style: solid;
        }

        .photo {
            border-width: 1px 0px 0px 0px;
        }

        .details {
            border-width: 1px 0px 0px 1px;
        }

            .details > table {
                width: 100%;
            }

        .CardHeader {
            font-weight: bolder;
        }

        td {
            padding: 2px 2px 3px 2px;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div>
                        <h4>User Information:</h4>
                        <br />
                        <section>
                            <div class="<%=Row%> uniform 50%">
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblFulName" runat="server" Text="Full Name"></asp:Label>
                                    <asp:TextBox ID="txtFullName" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                    <asp:TextBox ID="txtTitle" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
                                    <asp:TextBox ID="txtCompany" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                    <asp:TextBox ID="txtMobile" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblTele" runat="server" Text="Telephone"></asp:Label>
                                    <asp:TextBox ID="txtTele" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                    <asp:TextBox ID="txtFax" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblNat" runat="server" Text="Nationality"></asp:Label>
                                    <asp:TextBox ID="txtNat" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                    <asp:TextBox ID="txtCity" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblPostBox" runat="server" Text="PO BOX"></asp:Label>
                                    <asp:TextBox ID="txtPostBox" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </section>
                        <hr />
                        <h4>Reservation Information:</h4>
                        <br />
                        <section>
                            <div class="<%=Row%> uniform">
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblLibrary" runat="server" Text="Library"></asp:Label>
                                    <asp:TextBox ID="txtLibrary" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblPurpose" runat="server" Text="Purpose"></asp:Label>
                                    <asp:TextBox ID="txtPurpose" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblLecrType" runat="server" Text="Lecture Type"></asp:Label>
                                    <asp:TextBox ID="txtLecrType" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblDFrom" runat="server" Text="Date From"></asp:Label>&nbsp;&nbsp;
                                    <asp:TextBox ID="txtDFrom" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblDTo" runat="server" Text="Date To"></asp:Label>&nbsp;&nbsp;
                                    <asp:TextBox ID="txtDTo" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblAttnce" runat="server" Text="Attendance"></asp:Label>
                                    <asp:TextBox ID="txtAttnce" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblTFrom" runat="server" Text="Time From"></asp:Label>&nbsp;&nbsp;
                                    <asp:TextBox ID="txtTFrom" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%> 12<%=u%>$(3)">
                                    <asp:Label ID="lblTTo" runat="server" Text="Time To"></asp:Label>&nbsp;&nbsp;
                                    <asp:TextBox ID="txtTTo" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="4<%=u%>$ 12<%=u%>$(3)">
                                    <asp:Label ID="lblDevice" runat="server" Text="Devices"></asp:Label>
                                    <asp:TextBox ID="txtDevice" runat="server" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="12<%=u%>$">
                                    <asp:Label ID="lblInstructor" runat="server" Text="Instructors"></asp:Label>
                                    Instructor List
                                    <ej:Grid ID="EmployeesGrid" runat="server" AllowScrolling="True" RowTemplate="#templateData">
                                        <ScrollSettings Height="170" Width="100%"></ScrollSettings>
                                        <Columns>
                                            <ej:Column HeaderText="Photo" Width="30%" />
                                            <ej:Column HeaderText="Instructor Details" Width="65%" />
                                        </Columns>
                                    </ej:Grid>
                                    <script id="templateData" type="text/x-jsrender">
                                        <tr>
                                            <td class="photo">
                                                <a href="{{:PassportImage}}" target="_blank" >
                                                    <img src="{{:PassportImage}}" alt="{{ : InstructorName }}" />
                                                </a>
                                            </td>
                                            <td class="details">
                                                <table class="cardTable" cellpadding="3" cellspacing="2">
                                                    <colgroup>
                                                        <col width="50%">
                                                        <col width="50%">
                                                    </colgroup>
                                                    <tbody>
                                                        <tr>
                                                            <td class="CardHeader">Instructor Name </td>
                                                            <td>{{:InstructorName}} </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="CardHeader">Passpoer Number</td>
                                                            <td>{{:PassportNumber}} </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </script>
                                    <br />
                                </div>
                                <div class="12<%=u%>$">
                                    <asp:Button ID="btnApprove" runat="server" Text="Approve" />&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnReject" runat="server" Text="Reject" />&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                </div>
                                <div class="12<%=u%>$">
                                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </section>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
