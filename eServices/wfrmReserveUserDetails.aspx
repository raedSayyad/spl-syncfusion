﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmReserveUserDetails.aspx.vb" Inherits="eServices.wfrmReserveUserDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <section>
                        <div class="<%=Row%> uniform 50%">
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblFulName" runat="server" Text="Full Name"></asp:Label>
                                <asp:TextBox ID="txtFullName" runat="server"></asp:TextBox>
                            </div>
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                <ej:DropDownList ID="ddlTitle"  Height="35px" Width="100%" CssClass="CustomDDL" runat="server" SelectedIndex="0"></ej:DropDownList>
                            </div>
                            <div class="4<%=u%>$ 12<%=u%>$(3)">
                                <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
                                <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
                            </div>
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                            </div>
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblTele" runat="server" Text="Telephone"></asp:Label>
                                <asp:TextBox ID="txtTele" runat="server"></asp:TextBox>
                            </div>
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                <asp:TextBox ID="txtFax" runat="server"></asp:TextBox>
                            </div>
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblNat" runat="server" Text="Nationality"></asp:Label>
                                <ej:DropDownList ID="ddlNat"  Height="35px" Width="100%" CssClass="CustomDDL" runat="server" SelectedIndex="0"></ej:DropDownList>
                            </div>
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                <ej:DropDownList ID="ddlCity"  Height="35px" Width="100%" CssClass="CustomDDL" runat="server" ClientSideOnChange="onActiveIndexChange" SelectedIndex="0"></ej:DropDownList>
                            </div>
                            <div class="4<%=u%>$ 12<%=u%>$(3)">
                                <asp:Label ID="lblPostBox" runat="server" Text="PO BOX"></asp:Label>
                                <asp:TextBox ID="txtPostBox" runat="server"></asp:TextBox>
                            </div>
                            <div class="12<%=u%>$">
                                <asp:Button ID="btnContinue" runat="server" Text="Continue" />
                            </div>
                            <div class="12<%=u%>$">
                                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                            </div>
                            <script type="text/javascript">
                                var target;
                                $(function () {
                                    target = $('#<%=ddlTitle.ClientID%>').data("ejDropDownList");
                                    target.option({ selectedIndex: <%=ddlTitleInd%> });
                                    target = $('#<%=ddlNat.ClientID%>').data("ejDropDownList");
                                    target.option({ selectedIndex: <%=ddlNatInd%> });
                                    target = $('#<%=ddlCity.ClientID%>').data("ejDropDownList");
                                    target.option({ selectedIndex: <%=ddlCityInd%> });
                                });
                            </script>
                        </div>
                    </section>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
