'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option or rebuild the Visual Studio project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Class Strings
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Resources.Strings", Global.System.Reflection.[Assembly].Load("App_GlobalResources"))
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Academic.
        '''</summary>
        Friend Shared ReadOnly Property academic() As String
            Get
                Return ResourceManager.GetString("academic", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Approved.
        '''</summary>
        Friend Shared ReadOnly Property Approved() As String
            Get
                Return ResourceManager.GetString("Approved", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Attendence.
        '''</summary>
        Friend Shared ReadOnly Property Attendence() As String
            Get
                Return ResourceManager.GetString("Attendence", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Available.
        '''</summary>
        Friend Shared ReadOnly Property Available() As String
            Get
                Return ResourceManager.GetString("Available", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Back.
        '''</summary>
        Friend Shared ReadOnly Property Back() As String
            Get
                Return ResourceManager.GetString("Back", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Cancel.
        '''</summary>
        Friend Shared ReadOnly Property Cancel() As String
            Get
                Return ResourceManager.GetString("Cancel", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Canceled.
        '''</summary>
        Friend Shared ReadOnly Property Canceled() As String
            Get
                Return ResourceManager.GetString("Canceled", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Cancel Account.
        '''</summary>
        Friend Shared ReadOnly Property CancelUser() As String
            Get
                Return ResourceManager.GetString("CancelUser", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Choose.
        '''</summary>
        Friend Shared ReadOnly Property Choose() As String
            Get
                Return ResourceManager.GetString("Choose", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to City.
        '''</summary>
        Friend Shared ReadOnly Property City() As String
            Get
                Return ResourceManager.GetString("City", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Company.
        '''</summary>
        Friend Shared ReadOnly Property Company() As String
            Get
                Return ResourceManager.GetString("Company", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Confirm Password.
        '''</summary>
        Friend Shared ReadOnly Property Confirm_Password() As String
            Get
                Return ResourceManager.GetString("Confirm_Password", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Password not match.
        '''</summary>
        Friend Shared ReadOnly Property Confirm_Password_MSG() As String
            Get
                Return ResourceManager.GetString("Confirm_Password_MSG", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Contact Details.
        '''</summary>
        Friend Shared ReadOnly Property ContactDetail() As String
            Get
                Return ResourceManager.GetString("ContactDetail", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Date From.
        '''</summary>
        Friend Shared ReadOnly Property DateFrom() As String
            Get
                Return ResourceManager.GetString("DateFrom", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Select Date.
        '''</summary>
        Friend Shared ReadOnly Property DatePickerWaterMark() As String
            Get
                Return ResourceManager.GetString("DatePickerWaterMark", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Date To.
        '''</summary>
        Friend Shared ReadOnly Property DateTo() As String
            Get
                Return ResourceManager.GetString("DateTo", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Delete.
        '''</summary>
        Friend Shared ReadOnly Property Delete() As String
            Get
                Return ResourceManager.GetString("Delete", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Detail.
        '''</summary>
        Friend Shared ReadOnly Property Detail() As String
            Get
                Return ResourceManager.GetString("Detail", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Devices.
        '''</summary>
        Friend Shared ReadOnly Property Devices() As String
            Get
                Return ResourceManager.GetString("Devices", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Date of Birth.
        '''</summary>
        Friend Shared ReadOnly Property DOB() As String
            Get
                Return ResourceManager.GetString("DOB", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Documents.
        '''</summary>
        Friend Shared ReadOnly Property Document() As String
            Get
                Return ResourceManager.GetString("Document", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Email.
        '''</summary>
        Friend Shared ReadOnly Property Email() As String
            Get
                Return ResourceManager.GetString("Email", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Events.
        '''</summary>
        Friend Shared ReadOnly Property Events() As String
            Get
                Return ResourceManager.GetString("Events", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Extenstion.
        '''</summary>
        Friend Shared ReadOnly Property Extenstion() As String
            Get
                Return ResourceManager.GetString("Extenstion", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Family.
        '''</summary>
        Friend Shared ReadOnly Property family() As String
            Get
                Return ResourceManager.GetString("family", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Fax.
        '''</summary>
        Friend Shared ReadOnly Property Fax() As String
            Get
                Return ResourceManager.GetString("Fax", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Full Name.
        '''</summary>
        Friend Shared ReadOnly Property FullName() As String
            Get
                Return ResourceManager.GetString("FullName", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Gender.
        '''</summary>
        Friend Shared ReadOnly Property Gender() As String
            Get
                Return ResourceManager.GetString("Gender", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to General Information.
        '''</summary>
        Friend Shared ReadOnly Property GeneralInfo() As String
            Get
                Return ResourceManager.GetString("GeneralInfo", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Instructors.
        '''</summary>
        Friend Shared ReadOnly Property Instructors() As String
            Get
                Return ResourceManager.GetString("Instructors", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Instructor Name.
        '''</summary>
        Friend Shared ReadOnly Property Instructors_Name() As String
            Get
                Return ResourceManager.GetString("Instructors_Name", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Passport Image.
        '''</summary>
        Friend Shared ReadOnly Property Instructors_PassportI() As String
            Get
                Return ResourceManager.GetString("Instructors_PassportI", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Passport Number.
        '''</summary>
        Friend Shared ReadOnly Property Instructors_PassportN() As String
            Get
                Return ResourceManager.GetString("Instructors_PassportN", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Lecture Type.
        '''</summary>
        Friend Shared ReadOnly Property LectureType() As String
            Get
                Return ResourceManager.GetString("LectureType", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Library.
        '''</summary>
        Friend Shared ReadOnly Property Library() As String
            Get
                Return ResourceManager.GetString("Library", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Login.
        '''</summary>
        Friend Shared ReadOnly Property Login() As String
            Get
                Return ResourceManager.GetString("Login", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Medium.
        '''</summary>
        Friend Shared ReadOnly Property Medium() As String
            Get
                Return ResourceManager.GetString("Medium", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Membership Type.
        '''</summary>
        Friend Shared ReadOnly Property MemShipType() As String
            Get
                Return ResourceManager.GetString("MemShipType", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Mobile.
        '''</summary>
        Friend Shared ReadOnly Property Mobile() As String
            Get
                Return ResourceManager.GetString("Mobile", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to National ID Photo 1.
        '''</summary>
        Friend Shared ReadOnly Property NatIDPhoto1() As String
            Get
                Return ResourceManager.GetString("NatIDPhoto1", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to National ID Photo 2.
        '''</summary>
        Friend Shared ReadOnly Property NatIDPhoto2() As String
            Get
                Return ResourceManager.GetString("NatIDPhoto2", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to National ID.
        '''</summary>
        Friend Shared ReadOnly Property NationalID() As String
            Get
                Return ResourceManager.GetString("NationalID", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Nationallity.
        '''</summary>
        Friend Shared ReadOnly Property Nationallity() As String
            Get
                Return ResourceManager.GetString("Nationallity", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to News.
        '''</summary>
        Friend Shared ReadOnly Property News() As String
            Get
                Return ResourceManager.GetString("News", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Not Available.
        '''</summary>
        Friend Shared ReadOnly Property NotAvailable() As String
            Get
                Return ResourceManager.GetString("NotAvailable", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to The selected Date or Time Not Available, Please select another Date or Time..
        '''</summary>
        Friend Shared ReadOnly Property NotAvailableMsg() As String
            Get
                Return ResourceManager.GetString("NotAvailableMsg", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Password.
        '''</summary>
        Friend Shared ReadOnly Property Password() As String
            Get
                Return ResourceManager.GetString("Password", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Personal Photo.
        '''</summary>
        Friend Shared ReadOnly Property PersonalPhoto() As String
            Get
                Return ResourceManager.GetString("PersonalPhoto", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to PO Box.
        '''</summary>
        Friend Shared ReadOnly Property POBox() As String
            Get
                Return ResourceManager.GetString("POBox", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Postgraduate Student.
        '''</summary>
        Friend Shared ReadOnly Property postgraduate_student() As String
            Get
                Return ResourceManager.GetString("postgraduate_student", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Public.
        '''</summary>
        Friend Shared ReadOnly Property public_user() As String
            Get
                Return ResourceManager.GetString("public_user", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Purpose.
        '''</summary>
        Friend Shared ReadOnly Property Purpose() As String
            Get
                Return ResourceManager.GetString("Purpose", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Cancelation Reason.
        '''</summary>
        Friend Shared ReadOnly Property Reason() As String
            Get
                Return ResourceManager.GetString("Reason", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Register.
        '''</summary>
        Friend Shared ReadOnly Property Register() As String
            Get
                Return ResourceManager.GetString("Register", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Rejected.
        '''</summary>
        Friend Shared ReadOnly Property Rejected() As String
            Get
                Return ResourceManager.GetString("Rejected", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Renew Account.
        '''</summary>
        Friend Shared ReadOnly Property RenewUser() As String
            Get
                Return ResourceManager.GetString("RenewUser", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Reserve.
        '''</summary>
        Friend Shared ReadOnly Property Reserve() As String
            Get
                Return ResourceManager.GetString("Reserve", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Documentary for selected Membership.
        '''</summary>
        Friend Shared ReadOnly Property SNeedPhoto() As String
            Get
                Return ResourceManager.GetString("SNeedPhoto", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Special Need.
        '''</summary>
        Friend Shared ReadOnly Property Special_Need() As String
            Get
                Return ResourceManager.GetString("Special_Need", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Special User.
        '''</summary>
        Friend Shared ReadOnly Property special_user() As String
            Get
                Return ResourceManager.GetString("special_user", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Status.
        '''</summary>
        Friend Shared ReadOnly Property Status() As String
            Get
                Return ResourceManager.GetString("Status", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Strong.
        '''</summary>
        Friend Shared ReadOnly Property Strong() As String
            Get
                Return ResourceManager.GetString("Strong", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Submit.
        '''</summary>
        Friend Shared ReadOnly Property Submit() As String
            Get
                Return ResourceManager.GetString("Submit", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Telephone.
        '''</summary>
        Friend Shared ReadOnly Property Tele() As String
            Get
                Return ResourceManager.GetString("Tele", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Time From.
        '''</summary>
        Friend Shared ReadOnly Property TimeFrom() As String
            Get
                Return ResourceManager.GetString("TimeFrom", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Time To.
        '''</summary>
        Friend Shared ReadOnly Property TimeTo() As String
            Get
                Return ResourceManager.GetString("TimeTo", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Title.
        '''</summary>
        Friend Shared ReadOnly Property Title() As String
            Get
                Return ResourceManager.GetString("Title", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Undergraduate Student.
        '''</summary>
        Friend Shared ReadOnly Property undergraduate_student() As String
            Get
                Return ResourceManager.GetString("undergraduate_student", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Update.
        '''</summary>
        Friend Shared ReadOnly Property Update() As String
            Get
                Return ResourceManager.GetString("Update", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Updated.
        '''</summary>
        Friend Shared ReadOnly Property Updated() As String
            Get
                Return ResourceManager.GetString("Updated", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to User already upgraded..
        '''</summary>
        Friend Shared ReadOnly Property UpgradeReqApp() As String
            Get
                Return ResourceManager.GetString("UpgradeReqApp", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to User already submit upgrade request..
        '''</summary>
        Friend Shared ReadOnly Property UpgradeReqSent() As String
            Get
                Return ResourceManager.GetString("UpgradeReqSent", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Upgrade User.
        '''</summary>
        Friend Shared ReadOnly Property UpgradeUser() As String
            Get
                Return ResourceManager.GetString("UpgradeUser", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Very Strong.
        '''</summary>
        Friend Shared ReadOnly Property Very_Strong() As String
            Get
                Return ResourceManager.GetString("Very_Strong", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Website.
        '''</summary>
        Friend Shared ReadOnly Property WebSite() As String
            Get
                Return ResourceManager.GetString("WebSite", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Week.
        '''</summary>
        Friend Shared ReadOnly Property Week() As String
            Get
                Return ResourceManager.GetString("Week", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Work Phone.
        '''</summary>
        Friend Shared ReadOnly Property WorkPhone() As String
            Get
                Return ResourceManager.GetString("WorkPhone", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Cancel.
        '''</summary>
        Friend Shared ReadOnly Property _Cancel() As String
            Get
                Return ResourceManager.GetString("_Cancel", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Continue.
        '''</summary>
        Friend Shared ReadOnly Property _Continue() As String
            Get
                Return ResourceManager.GetString("_Continue", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Finish.
        '''</summary>
        Friend Shared ReadOnly Property _Finish() As String
            Get
                Return ResourceManager.GetString("_Finish", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to New.
        '''</summary>
        Friend Shared ReadOnly Property _New() As String
            Get
                Return ResourceManager.GetString("_New", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Next.
        '''</summary>
        Friend Shared ReadOnly Property _Next() As String
            Get
                Return ResourceManager.GetString("_Next", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Previous.
        '''</summary>
        Friend Shared ReadOnly Property _Previous() As String
            Get
                Return ResourceManager.GetString("_Previous", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
