﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmUserReg.aspx.vb" Inherits="eServices.wfrmUserReg" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <section>
                        <div class="<%=Row%> uniform 50%">
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblFulName" runat="server" Text="Full Name"></asp:Label>
                                <asp:TextBox ID="txtFullName" runat="server"></asp:TextBox>
                                <asp:Label ID="lblValdName" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                            <div class="4<%=u%>$ 12<%=u%>$(3)">
                                <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                <asp:Label ID="lblValdEmail" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" onKeyUp="checkPassStrength()"></asp:TextBox>
                                <asp:Label ID="lblPasswordStrength" runat="server"></asp:Label>
                                <asp:Label ID="lblValdPass" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                            <div class="4<%=u%>$ 12<%=u%>$(3)">
                                <asp:Label ID="lblConfPassword" runat="server" Text="Confirm Password"></asp:Label>
                                <asp:TextBox ID="txtConfPassword" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:Label ID="lblValdCPass" runat="server" ForeColor="Red" Text=""></asp:Label>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfPassword" ErrorMessage="CompareValidator" ForeColor="Red"><%=Password_not_match %></asp:CompareValidator>
                            </div>
                            <div class="12<%=u%>$">
                                <asp:Button ID="btnSubmit" runat="server" Text="Create Account" />
                            </div>
                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </section>
                </ContentTemplate>
            </asp:UpdatePanel>
             <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <script type="text/javascript">
                function checkPassStrength() {
                    var value = $('#<%=txtPassword.ClientID %>').val();
                    var score = 0;
                    var status = "";
                    var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-="
                    if (value.toString().length >= 8) {

                        if (/[a-z]/.test(value)) {
                            score += 1;
                        }
                        if (/[A-Z]/.test(value)) {
                            score += 1;
                        }
                        if (/\d/.test(value)) {
                            score += 1;
                        }
                        for (i = 0; i < specialChars.length; i++) {
                            if (value.indexOf(specialChars[i]) > -1) {
                                score += 1;
                            }
                        }
                    }
                    else {
                        score = 1;
                    }

                    if (score == 2) {
                        status = status = "<span style='color:#CCCC00'><%=Medium%></span>";
                    }
                    else if (score == 3) {
                        status = "<span style='color:#0DFF5B'><%=Strong%></span>";
                    }
                    else if (score >= 4) {
                        status = "<span style='color:#009933'><%=Very_Strong%></span>";
                    }
                    else {

                        status = "<span style='color:red'><%=Week%></span>";
                    }
                    if (value.toString().length > 0) {
                        $('#<%=lblPasswordStrength.ClientID %>').html("<span> " + status + "</span>");
                    }
                    else {
                        $('#<%=lblPasswordStrength.ClientID %>').html("");
                    }
                }
            </script>
        </div>
    </form>
</body>
</html>
