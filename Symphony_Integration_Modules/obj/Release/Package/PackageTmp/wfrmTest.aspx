﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmTest.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <h3>TEST PAGE</h3>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%--<ej:RTE ID="RTE2" runat="server" ToolsList="style,doAction,lists,images,links" IsResponsive="True" EnableHtmlEncode="True">
                <RTEContent>
                    <ul>
                        <li>The Rich Text Editor  (RTE) control is an easy to render in client side. </li>
                        <li>Customer easy to edit the contents and get the HTML content for the displayed content. </li>
                        <li>A rich text editor control provides users with a toolbar that helps them to apply rich text formats to the text entered  in the text area.</li>
                    </ul>
                </RTEContent>
                <Tools Styles="bold,italic,underline,strikethrough"
                    Lists="unorderedList,orderedList"
                    DoAction="undo,redo"
                    Links="createLink,removeLink"
                    Images="image">
                </Tools>
            </ej:RTE>--%>
            <asp:Button ID="Button1" runat="server" Text="Button" />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label> <br /> <br /> <br />
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBox1" ErrorMessage="Wrong Mobile Number Format" ForeColor="Red" ValidationExpression="[^\d]{10}" ValidationGroup="UserUpgrade"></asp:RegularExpressionValidator>--%>
            <asp:RequiredFieldValidator ID="DropDownValidator" runat="server" ControlToValidate="TextBox1"
                        ErrorMessage="Change Value"  ForeColor="#FF8000"></asp:RequiredFieldValidator>
            <br />
            <ej:DropDownList ID="ddlMemType" runat="server">
                <Items>
                    <ej:DropDownListItem Value="0" Text="Adult"></ej:DropDownListItem>
                    <ej:DropDownListItem Value="1" Text="Child"></ej:DropDownListItem>
                    <ej:DropDownListItem Value="2" Text="Education"></ej:DropDownListItem>
                    <ej:DropDownListItem Value="3" Text="Special Need"></ej:DropDownListItem>
                </Items>
            </ej:DropDownList>
            <br />
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="UserUpgrade" />
        
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        //$(document).ready(function () {
        //    $("iframe").mouseenter(function () {
        //        //alert("You entered iframe!");
        //        $(this).contents().find("body").attr("spellcheck", 'false');
        //        $(this).contents().find("body").attr("autocorrect", 'off');
        //        $(this).contents().find("body").attr("contenteditable", 'true');
        //        $(this).contents().find("body").attr("data-gramm_id", 'eb508303-e56b-d6c8-3bb1-f39935f4936d');
        //        $(this).contents().find("body").attr("data-gramm", 'true');
        //        $(this).contents().find("body").attr("data-gramm_editor", 'true');
        //        $(this).contents().find("body").attr("style", 'font-family: Times New Roman,Times,serif;color: #5C5C5C;word-wrap: break-word;');
        //        //alert("You entered iframe! 2");
        //        //$("iframe body").hide();
        //    });
        //});
        //var $iframe = $('#MainContent_RTE2_Iframe');
        //$iframe.ready(function () {
        //    $iframe.contents().find("body").attr("data-gramm_editor", 'true');
        //});
        //function pageLoad() {
        //    $("#MainContent_RTE2_Iframe").contents().find("#body").attr("spellcheck", 'false');
        //    $("#MainContent_RTE2_Iframe").contents().find("#body").attr("autocorrect", 'off');
        //    $("#MainContent_RTE2_Iframe").contents().find("#body").attr("contenteditable", 'true');
        //    $("#MainContent_RTE2_Iframe").contents().find("#body").attr("data-gramm_id", 'eb508303-e56b-d6c8-3bb1-f39935f4936d');
        //    $("#MainContent_RTE2_Iframe").contents().find("#body").attr("data-gramm", 'true');
        //    $("#MainContent_RTE2_Iframe").contents().find("#body").attr("data-gramm_editor", 'true');
        //    $("#MainContent_RTE2_Iframe").contents().find("#body").attr("style", 'font-family: Times New Roman,Times,serif;color: #5C5C5C;word-wrap: break-word;');
        //};
    </script>
</asp:Content>
