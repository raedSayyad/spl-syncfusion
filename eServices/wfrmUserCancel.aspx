﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmUserCancel.aspx.vb" Inherits="eServices.wfrmUserCancel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%=PageTitle%></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" class="landing" runat="server">
    <form id="form1" enctype="multipart/form-data" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:HiddenField ID="hdfdDOB" runat="server" Value="" />
            <input type="hidden" name="hdfdDOB1" id="hdfdDOB1" value="" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <h3><%=PageTitle%></h3>
                    <section>
                        <div class="<%=Row%> uniform 50%">
                            <div class="4<%=u%> 12<%=u%>$(3)">
                                <asp:Label ID="lblReason" runat="server" Text="Cancelation Reason"></asp:Label>
                                <asp:TextBox ID="txtReason" runat="server" CausesValidation="true" required="required" TextMode="MultiLine" Height="400px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtReason" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserCancel"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </section>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div>
                <section>
                    <div class="row uniform 50%">
                        <div class="12<%=u%>$">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="UserCancel" />
                        </div>
                        <div class="12<%=u%>$">
                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </section>
            </div>
            <script>
                $.noConflict();
            </script>
        </div>
    </form>
</body>
</html>
