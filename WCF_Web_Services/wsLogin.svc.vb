﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "wsLogin" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select wsLogin.svc or wsLogin.svc.vb at the Solution Explorer and start debugging.

Imports System.Security.Cryptography
Imports System.Text
Imports ClassLibraries

Public Class wsLogin
    Implements IwsLogin

    Public Function GetData(ByVal value As String) As String Implements IwsLogin.GetData
        Dim key As String = "D653FF45281B76EEBBF8EA5C513D4387D7743E61FE35E20C962DC412A8657837D7F3408593CDBDA37158ACCDCAACE9C38618C6C92F3439D6EC133585A3693B6ED"
        Dim enc As String = Obj_EncDec.Encrypt(value, key)
        Dim dec As String = Obj_EncDec.DecryptIn(enc)
        Return enc + "&&&" + dec
    End Function

    Dim Obj_EncDec As New EncDec
    Dim Obj_SQL As New SQLFunction
    Dim Obj_Internet As New InternetOperation

    Public Function Login(ByVal Username As String, ByVal Password As String) As String Implements IwsLogin.Login
        Try
            Dim decUsername As String = Obj_EncDec.DecryptIn(Username)
            Dim decPassword As String = Obj_EncDec.DecryptIn(Password)
            Dim key As String = "D653FF45281B76EEBBF8EA5C513D4387D7743R61FE35E20C962DC412A8657837D7F3408543CDBDA37158ACBDCAACE9C38618C6M92F3439D6EE133585H3693B6ED"
            Dim encPassword As String = Obj_EncDec.Encrypt(decPassword, key)
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim dt As New DataTable
            Dim sqlScript As String = ""
            If dbType = "SQL" Then
                sqlScript = "SELECT * FROM [Integration_Admin].[dbo].[Users] WHERE [Username] = '" & decUsername & "' AND [Password] = '" & encPassword & "'"
            ElseIf dbType = "ORA" Then
                sqlScript = ""
            End If
            If sqlScript <> "" Then
                dt = Obj_SQL.fillDT(sqlScript, CommandType.Text, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                If dt.Rows.Count = 1 Then
                    Return dt.Rows(0).Item(0).ToString
                Else
                    Return "False"
                End If
            Else
                Return "False"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function Session(ByVal Username As String, ByVal Password As String) As String Implements IwsLogin.Session
        Try
            
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function SetAuthKey(ByVal Username As String) As String Implements IwsLogin.SetAuthKey
        Try
            Dim r As New Random
            Dim AuthKey As String = Obj_EncDec.RandomString(r)
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim sqlScript As String = ""
            If dbType = "SQL" Then
                sqlScript = "INSERT INTO [Integration_Admin].[dbo].[AuthKeys] ([UserID],[AuthKey],[dtExpire],[isDeleted]) VALUES ('" & Username & "','" & AuthKey & "',Getdate(),0)"
            ElseIf dbType = "ORA" Then
                sqlScript = ""
            End If
            If sqlScript <> "" Then
                Obj_SQL.exeNonQuery(sqlScript, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                Obj_Internet.SendStringEmail(ConfigurationManager.AppSettings("MailUser"), ConfigurationManager.AppSettings("MailPassowrd"), ConfigurationManager.AppSettings("MailUser"), ConfigurationManager.AppSettings("MailServerIP"), ConfigurationManager.AppSettings("MailServerPort"), "Auth Key : " + AuthKey, Username, "Auth Key", "")
                Return AuthKey
            Else
                Return "False"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function GetAuthKey(ByVal AuthKey As String) As String Implements IwsLogin.GetAuthKey
        Try
            Dim r As New Random
            Dim dt As New DataTable
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim sqlScript As String = ""
            If dbType = "SQL" Then
                sqlScript = "SELECT * FROM [Integration_Admin].[dbo].[AuthKeys] WHERE [AuthKey] ='" & AuthKey & "'"
            ElseIf dbType = "ORA" Then
                sqlScript = ""
            End If
            If sqlScript <> "" Then
                dt = Obj_SQL.fillDT(sqlScript, CommandType.Text, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                If dt.Rows.Count = 1 Then
                    Dim exDate As DateTime = CDate(dt.Rows(0).Item(2))
                    exDate = exDate.AddMinutes(10)
                    If exDate > Now Then
                        Return dt.Rows(0).Item(0).ToString
                    Else
                        Return "Expired"
                    End If
                Else
                    Return "False"
                End If
            Else
                Return "False"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
End Class
