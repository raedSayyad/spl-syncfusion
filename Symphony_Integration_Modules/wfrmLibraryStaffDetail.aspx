﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmLibraryStaffDetail.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmLibraryStaffDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHead" runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-switch.css" rel="stylesheet" />
    <%--<script src="Scripts/jquery-1.11.0.min.js"></script>--%>
    <script src="Scripts/bootstrap-switch.js"></script>
    <script>
        function pageLoad() {
            $("#<%=CheckBox1.ClientID%>").bootstrapSwitch();
            <%--$("#<%=CheckBox2.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox3.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox4.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox5.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox6.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox7.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox8.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox9.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox10.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox11.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox12.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox13.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox14.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox15.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox16.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox17.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox18.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox19.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox20.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox21.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox22.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox23.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox24.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox25.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox26.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox27.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox28.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox29.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox30.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox31.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox32.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox33.ClientID%>").bootstrapSwitch();--%>
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <header class="major">
        <h3>Users Details</h3>
        <p></p>
    </header>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="wrapper style1 special">
                <div class="container">
                    <div class="row uniform 50%">
                        <div class="4u 12u$(medium)">
                            <asp:Label ID="lblUserID" runat="server" Text="User ID"></asp:Label>
                            <asp:TextBox ReadOnly="true" ID="txtUSerID" runat="server"></asp:TextBox>
                        </div>
                        <div class="4u 12u$(medium)">
                            <asp:Label ID="lblFulName" runat="server" Text="Full Name"></asp:Label>
                            <asp:TextBox ReadOnly="true" ID="txtFullName" runat="server"></asp:TextBox>
                        </div>
                        <div class="4u$ 12u$(medium)">
                            <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox ReadOnly="true" ID="txtEmail" runat="server"></asp:TextBox>
                        </div>
                        <div class="4u 12u$(medium)">
                            <asp:Label ID="lblLibrary" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox ReadOnly="true" ID="txtLibrary" runat="server"></asp:TextBox>
                        </div>
                        <div class="4u 12u$(medium)">
                            <asp:Label ID="lblActive" runat="server" Text="Active"></asp:Label>
                            <asp:CheckBox ID="CheckBox1" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                        </div>
                        <div class="4u$ 12u$(medium)">
                        </div>
                        <hr />
                        <div class="12u$">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                        </div>
                        <div class="12u$">
                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                        </div>
                        <%-- <h4>Pages Authentication</h4>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblSymphony" runat="server" Text="Symphony Admin"></asp:Label>
                                <asp:CheckBox ID="CheckBox2" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblNotification" runat="server" Text="Notification Admin"></asp:Label>
                                <asp:CheckBox ID="CheckBox3" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                            </div>
                            <div class="4u$ 12u$(medium)">
                                <asp:Label ID="Label2" runat="server" Text="Symphony Admin"></asp:Label>
                                <asp:CheckBox ID="CheckBox4" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="Label3" runat="server" Text="Symphony Admin"></asp:Label>
                                <asp:CheckBox ID="CheckBox5" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="Label4" runat="server" Text="Symphony Admin"></asp:Label>
                                <asp:CheckBox ID="CheckBox6" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                </div>
                             <div class="4u$ 12u$(medium)">
                                <asp:Label ID="Label5" runat="server" Text="Symphony Admin"></asp:Label>
                                <asp:CheckBox ID="CheckBox7" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                            </div>--%>
                    </div>
                </div>
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div id="IMGDIV1" align="center" valign="middle" class="ModalPopupBG" runat="server">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
