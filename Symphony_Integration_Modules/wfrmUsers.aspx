﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmUsers.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmUsers" %>

<%@ Register assembly="Syncfusion.EJ" namespace="Syncfusion.JavaScript.Models" tagprefix="ej" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <header class="major">
        <h3>Users Managment</h3>
        <p></p>
    </header>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
               <%-- <ej:Accordion ID="Accordion1" runat="server" SelectedItemIndex="-1" Collapsible="true">
                    <Items>
                        <ej:AccordionItem Text="User Upgrade Request">
                            <ContentSection>
                                 <iframe id="UserUpgrade" src="wfrmUserUpgReq.aspx" width="100%" height="730px"></iframe>
                            </ContentSection>
                        </ej:AccordionItem>
                        <ej:AccordionItem Text="User Renewal Request">
                            <ContentSection>
                                <iframe id="UserRenewal" src="wfrmUserRenReq.aspx" width="100%" height="730px"></iframe>
                            </ContentSection>
                        </ej:AccordionItem>
                        <ej:AccordionItem Text="User Cancel Request">
                            <ContentSection>
                                <iframe id="UserCancel" src="wfrmUserCancelReq.aspx" width="100%" height="730px"></iframe>
                            </ContentSection>
                        </ej:AccordionItem>
                    </Items>
                </ej:Accordion>--%>
                <ej:Grid ID="UserRenReqGrid" runat="server" ClientIDMode="Static" AllowPaging="True" OnServerRecordClick="onClick">
                        <ClientSideEvents RecordClick="RecordClick" />
                        <Columns>
                            <ej:Column Field="User_ID" HeaderText="User ID" IsPrimaryKey="True" Width="30" Visible="true"></ej:Column>
                            <ej:Column Field="Name" HeaderText="Full Name" Width="70" Visible="true"></ej:Column>
                            <ej:Column Field="Library" HeaderText="Library" Width="30" Visible="true"></ej:Column>
                            <ej:Column HeaderText="Details" Template="true" TemplateID="#buttonTemplate" TextAlign="Center" Width="75"></ej:Column>
                        </Columns>
                    </ej:Grid>
                    <script type="text/x-jsrender" id="buttonTemplate">
                        <button class="Details" name="Details"><%:Detail%></button>
                    </script>
                    <script type="text/javascript">
                        $(function () {
                            $(".Details").ejButton();
                        });
                    </script>
                    <script type="text/javascript">
                        $(function () {
                            $(".Details").click(function (e) {
                                triggerEvent(e);
                            });
                        });

                        function triggerEvent(e) {
                            var obj = $("#UserRenReqGrid").data("ejGrid");
                            var args = { currentTarget: e.currentTarget.name, selectedRecord: obj.getSelectedRecords(), selectedIndex: obj.model.selectedRowIndex };
                            obj._trigger("recordClick", args);
                        }

                        function RecordClick(e) {
                            if (e.currentTarget != "Details")
                                return false
                            else {
                                triggerEvent(e);
                            }
                        }
                    </script>
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                    <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>

