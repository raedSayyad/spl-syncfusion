﻿Imports System.Security.Cryptography
Imports ExtraClassLibraries
Imports ClassLibraries.Users
Imports ClassLibraries
Imports System.Net
Imports System.IO

Public Class wfrmPaymentResponse
    Inherits System.Web.UI.Page

    Public PageTitle As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            PageTitle = "Payment Response"
            DepAmount.Text = Request.QueryString("DepAmount")
            TP_Amount.Text = Request.QueryString("TP_Amount")
            TP_ExtraFees.Text = Request.QueryString("TP_ExtraFees")
            TP_PaymentDate.Text = Request.QueryString("TP_PaymentDate")
            TP_PayMethod.Text = Request.QueryString("TP_PayMethod")
            TP_ReceiptNo.Text = Request.QueryString("TP_ReceiptNo")
            TP_RefNo.Text = Request.QueryString("TP_RefNo")
            hdTP_ResultCode.Value = Request.QueryString("TP_ResultCode")
            TP_SecHash.Value = Request.QueryString("TP_SecHash")
            Select Case hdTP_ResultCode.Value
                Case "0"
                    TP_ResultCode.Text = "The Payment done successfully"
                Case "-1"
                    TP_ResultCode.Text = "Error: Wrong Encryption"
                Case "-2"
                    TP_ResultCode.Text = "Error Invalid Service amount"
                Case "-3"
                    TP_ResultCode.Text = "Error Invalid credentials to the typed account"
                Case "-4"
                    TP_ResultCode.Text = "Error: if the Payment is already done for the requested reference number, in this case the payment details will be returned back in the response parameters. "
                Case "-5"
                    TP_ResultCode.Text = "Error :If the user have not enough balance to proceed the payment"
                Case "-6"
                    TP_ResultCode.Text = "Error: if the TAHSEEL account used in payment is inactive."
                Case "-7"
                    TP_ResultCode.Text = "Error: Reserved to TAHSEEL"
                Case "-8"
                    TP_ResultCode.Text = "Error: Reserved to TAHSEEL"
                Case "-9"
                    TP_ResultCode.Text = "Error: Reserved to TAHSEEL"
                Case "-10"
                    TP_ResultCode.Text = "Error: If unexpected error occurred during the payment step."
                Case "-11"
                    TP_ResultCode.Text = "Error: if the user press cancel on Tahseel side"
                Case "-12"
                    TP_ResultCode.Text = "Error: Invalid Reference number"
                Case "-13"
                    TP_ResultCode.Text = "Error: reserved to TAHSEEL"
                Case "-14"
                    TP_ResultCode.Text = "ErrorBank refused the payment through the online credit card channel"

            End Select


            Dim resultparmeter As String = HttpUtility.UrlDecode(HttpContext.Current.Request.Url.AbsoluteUri)
            resultparmeter = resultparmeter.Replace(ConfigurationManager.AppSettings("TP_ReturnURL") + "?", "")
            Dim Pairs = resultparmeter.Split("&"c)
            Dim PayResult As New SortedDictionary(Of String, String)()
            Dim TSeckey As String = String.Empty
            For Each pair As String In Pairs
                Dim pairParts = pair.Split("="c)
                If Not pairParts(0).Equals("TP_SecHash", StringComparison.OrdinalIgnoreCase) Then
                    PayResult.Add(pairParts(0), pairParts(1))
                Else
                    TSeckey = pairParts(1)
                End If
            Next
            Dim SecureData As New StringBuilder()
            For Each kvp As KeyValuePair(Of String, String) In PayResult
                If Not [String].IsNullOrEmpty(kvp.Value.ToString()) Then
                    SecureData.AppendFormat("{0}={1}&", kvp.Key, kvp.Value.ToString())
                End If
            Next
            If SecureData.Length > 0 Then
                SecureData.Remove(SecureData.Length - 1, 1)
            End If

            'Dim ComputedSecKey As String = CreateSecureHash("3EC81BD69AF47CB52ED45AF62EA81CB9", SecureData.ToString())
            Dim ComputedSecKey As String = CreateSecureHash(ConfigurationManager.AppSettings("SecureHash"), SecureData.ToString())
            If ComputedSecKey.Equals(TSeckey, StringComparison.Ordinal) Then
                'Response.Write("Valid Response")
                Dim objewsUserReg As New ewsUserReg.IewsUserRegClient
                Dim objewsOther As New ewsOthers.IewsOthersClient
                Dim ds As New DataSet
                ds = objewsOther.GetUserPayments(TP_RefNo.Text)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim objUserInfo As New UserInfo
                        objUserInfo = objewsUserReg.UserInfo(ds.Tables(0).Rows(0).Item(1))
                        If hdTP_ResultCode.Value = "0" Then
                            objewsOther.UptUserPaymentWS(ds.Tables(0).Rows(0).Item(0), "0", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                            objewsOther.UptUserPaymentWP(ds.Tables(0).Rows(0).Item(0), "0", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                            Dim BillN As String = ds.Tables(0).Rows(0).Item(9)
                            Dim BillNArr() As String = BillN.Split(",")
                            Dim BillV As String = ds.Tables(0).Rows(0).Item(10)
                            Dim BillVArr() As String = BillV.Split(",")
                            ' Response.Write(BillVArr.Length)
                            For i As Integer = 0 To BillNArr.Length - 2
                                Dim res As New Responce_Msg
                                Dim pDate As DateTime = Date.ParseExact(TP_PaymentDate.Text, "dd/MM/yyyy HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                                res = objewsUserReg.UserPayment("eng", objUserInfo.User_ID, pDate.ToString("yyyyMMddHHmmss"), objUserInfo.Library, BillNArr(i), BillVArr(i), "CREDITCARD")
                                Dim Bill_Reason As String = objewsOther.GetUserBillReason(objUserInfo.User_ID, BillNArr(i))
                                If Bill_Reason = "MEMFEEPSTD" Or Bill_Reason = "MEMFEEPUB" Or Bill_Reason = "MEMFEESTD" Or Bill_Reason = "MEMFEEKID" Or Bill_Reason = "MEMFEESPN" Or Bill_Reason = "MEMFEEFAM" Then
                                    Dim objTahseelAccountClient As New wsTahseelCard.TahseelAccountClient
                                    objTahseelAccountClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings("Card_Username")
                                    objTahseelAccountClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings("Card_Password")
                                    Dim objAdTahCardOrder As New wsTahseelCard.AddTahseelMemberCardOrder

                                    objAdTahCardOrder.MerchantID = ConfigurationManager.AppSettings("Card_MerchantID")
                                    objAdTahCardOrder.EntityID = ConfigurationManager.AppSettings("Card_EntityID")
                                    Dim objTahseelMemberCard As New wsTahseelCard.TahseelMemberCard
                                    objTahseelMemberCard.Address1 = objUserInfo.City
                                    objTahseelMemberCard.Address2 = objUserInfo.City
                                    If Bill_Reason = "MEMFEEPSTD" Or Bill_Reason = "MEMFEEPUB" Or Bill_Reason = "MEMFEESTD" Then
                                        objTahseelMemberCard.Amount = 150.0
                                        objTahseelMemberCard.CardType = wsTahseelCard.MemberType.Individual
                                    ElseIf Bill_Reason = "MEMFEEKID" Then
                                        objTahseelMemberCard.Amount = 150.0
                                        objTahseelMemberCard.CardType = wsTahseelCard.MemberType.Child
                                    ElseIf Bill_Reason = "MEMFEESPN" Then
                                        objTahseelMemberCard.Amount = 150.0
                                        objTahseelMemberCard.CardType = wsTahseelCard.MemberType.Special_Needs
                                    ElseIf Bill_Reason = "MEMFEEFAM" Then
                                        objTahseelMemberCard.Amount = 300.0
                                        objTahseelMemberCard.CardType = wsTahseelCard.MemberType.Family
                                    End If

                                    objTahseelMemberCard.BirthDate = DateTime.ParseExact(objUserInfo.DOB, "yyyymmdd", Globalization.CultureInfo.InvariantCulture)
                                    objTahseelMemberCard.City = objUserInfo.City
                                    objTahseelMemberCard.Email = objUserInfo.Email
                                    objTahseelMemberCard.EmirateID = objUserInfo.NationalID
                                    objTahseelMemberCard.Gender = objUserInfo.Gender
                                    Dim webClient = New WebClient()
                                    Dim imageBytes As Byte() = webClient.DownloadData(objUserInfo.PersonalPhoto)
                                    objTahseelMemberCard.MemberImage = imageBytes
                                    objTahseelMemberCard.MemberShipNo = objUserInfo.User_ID
                                    objTahseelMemberCard.Mobile = objUserInfo.Mobile
                                    objTahseelMemberCard.NameA = objUserInfo.FullName
                                    objTahseelMemberCard.NameE = objUserInfo.FullName
                                    objTahseelMemberCard.Nationality = objUserInfo.Nationality
                                    objTahseelMemberCard.PassportNo = ""
                                    objTahseelMemberCard.ReceiptNo = TP_ReceiptNo.Text

                                    objAdTahCardOrder.CardInfo = objTahseelMemberCard

                                    Dim objAddCardResponse As New wsTahseelCard.AddCardResponse
                                    objAddCardResponse = objTahseelAccountClient.AddTahseelMemberCardRequest(objAdTahCardOrder)

                                    If objAddCardResponse.ResultCode = 0 Then
                                        lblError.Text = "Tahseel Card Request Sent."
                                        'objAddCardResponse.ResultCode 
                                        'objAddCardResponse.ResultDescription 
                                        'objAddCardResponse.TahseelCardNumber 
                                        Dim saveCard As String = objewsOther.InstUserTahseelResponse(objUserInfo.User_ID, objUserInfo.Profile, objAddCardResponse.ResultCode, "Success", objAddCardResponse.TahseelCardNumber, Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                        If saveCard <> "1" Then
                                            lblError.Text += " " + saveCard
                                        End If

                                    Else
                                        lblError.Text = objAddCardResponse.ResultDescription + "|" + CStr(objAddCardResponse.ResultCode) & _
                                            "|" + CStr(objAdTahCardOrder.MerchantID) + "|" + CStr(objAdTahCardOrder.EntityID) & _
                                            "|" + objTahseelAccountClient.ClientCredentials.UserName.UserName + "|" + objTahseelAccountClient.ClientCredentials.UserName.Password & _
                                            "|" + objTahseelMemberCard.Address1 & _
                                            "|" + objTahseelMemberCard.Address2 + "|" + Bill_Reason & _
                                            "|" + CStr(objAdTahCardOrder.CardInfo.Amount) & _
                                            "|" + objAdTahCardOrder.CardInfo.BirthDate.ToString & _
                                            "|" + objAdTahCardOrder.CardInfo.City & _
                                            "|" + objAdTahCardOrder.CardInfo.Email & _
                                            "|" + objAdTahCardOrder.CardInfo.EmirateID & _
                                            "|" + objAdTahCardOrder.CardInfo.Gender & _
                                            "|" + objAdTahCardOrder.CardInfo.MemberShipNo & _
                                            "|" + objAdTahCardOrder.CardInfo.Mobile & _
                                            "|" + objAdTahCardOrder.CardInfo.NameA & _
                                            "|" + objAdTahCardOrder.CardInfo.NameE & _
                                            "|" + objAdTahCardOrder.CardInfo.Nationality & _
                                            "|" + objAdTahCardOrder.CardInfo.ReceiptNo
                                        Dim saveCard As String = objewsOther.InstUserTahseelResponse(objUserInfo.User_ID, objUserInfo.Profile, objAddCardResponse.ResultCode, objAddCardResponse.ResultDescription, "", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                    End If
                                End If
                                If res.Code = BillVArr(i) Then
                                    objewsOther.UptUserPaymentWP(ds.Tables(0).Rows(0).Item(0), "0", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                End If
                                'Response.Write(res.Code + " || " + res.Msg + "|| " + BillNArr(i) + "|" + BillVArr(i))
                            Next
                        Else
                            objewsOther.UptUserPaymentWP(ds.Tables(0).Rows(0).Item(0), hdTP_ResultCode.Value, Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                            objewsOther.UptUserPaymentWS(ds.Tables(0).Rows(0).Item(0), "1", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                            objewsOther.UptUserPaymentWP(ds.Tables(0).Rows(0).Item(0), "1", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                        End If
                        objewsOther.InstUserPaymentRes(objUserInfo.User_ID, DepAmount.Text, TP_Amount.Text, TP_ExtraFees.Text, TP_PaymentDate.Text, TP_PayMethod.Text, TP_ReceiptNo.Text, TP_RefNo.Text, hdTP_ResultCode.Value, TP_SecHash.Value, Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))

                    Else
                        lblError.Text = "System Error"
                    End If
                Else
                    lblError.Text = "System Error"
                End If
            Else
                lblError.Text = "invalid Response!"
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Public Function CreateSecureHash(SecurityKey As String, RawData As String) As String
        Dim convertedHash As Byte()
        convertedHash = New Byte(SecurityKey.Length / 2 - 1) {}
        For i As Integer = 0 To SecurityKey.Length / 2 - 1
            convertedHash(i) = CByte(Int32.Parse(SecurityKey.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber))
        Next
        Dim hexHash As String = ""
        Using hasher As New HMACSHA256(convertedHash)
            Dim hashValue As Byte() = hasher.ComputeHash(Encoding.UTF8.GetBytes(RawData))
            For Each b As Byte In hashValue
                hexHash += b.ToString("X2")
            Next
        End Using
        Return hexHash
    End Function

    Private Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Try
            Response.Redirect("https://catalog.shjlib.gov.ae/uhtbin/cgisirsi.exe/x/CENTRAL/x/30")
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class