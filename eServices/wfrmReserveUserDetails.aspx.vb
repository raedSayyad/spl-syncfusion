﻿Imports ClassLibraries

Public Class wfrmReserveUserDetails
    Inherits System.Web.UI.Page

    Dim gotoURL As String = ""
    Public ddlCityInd As String = "0"
    Public ddlNatInd As String = "0"
    Public ddlTitleInd As String = "0"
    Public objUserInfo As New Users.UserInfo
    Dim objReserveUserInfo As New Users.ReseveUserInfo
    Public Row As String
    Public u As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Session("UserInfo") Is Nothing Then
                objUserInfo = Session("UserInfo")
                Dim User_ID As String = objUserInfo.User_ID
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                objUserInfo = objEwsUserClient.UserInfo(User_ID)
            Else
                Response.Redirect("~/wfrmLogin.aspx")
            End If
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim UserID As String = objUserInfo.User_ID
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                ddlCity.EnableRTL = True
                ddlNat.EnableRTL = True
                ddlTitle.EnableRTL = True
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
                objSymAdminSDH.locale = "en"
            End If

            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            lblFulName.Text = rc.GetString("FullName", Culture_Used)
            lblTitle.Text = rc.GetString("Title", Culture_Used)
            lblCompany.Text = rc.GetString("Company", Culture_Used)
            lblNat.Text = rc.GetString("Nationallity", Culture_Used)
            lblMobile.Text = rc.GetString("Mobile", Culture_Used)
            lblTele.Text = rc.GetString("Tele", Culture_Used)
            lblFax.Text = rc.GetString("Fax", Culture_Used)
            lblPostBox.Text = rc.GetString("POBox", Culture_Used)
            lblCity.Text = rc.GetString("City", Culture_Used)
            btnContinue.Text = rc.GetString("_Continue", Culture_Used)

            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            objlookupPolicyListReq.policyType = "CAT3"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLIST = New Others.DDLList
                objDDLIST.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLIST.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlist.Add(objDDLIST)
            Next
            ddlTitle.DataSource = objAllDDlist 'objOthersClient.LookUp("CAT3", Lang)
            ddlTitle.DataValueField = "Value"
            ddlTitle.DataTextField = "Text"
            ddlTitle.DataBind()

            objlookupPolicyListReq.policyType = "CAT4"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTN As New Others.DDLList
            Dim objAllDDlistN As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTN = New Others.DDLList
                objDDLISTN.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTN.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistN.Add(objDDLISTN)
            Next
            ddlNat.DataSource = objAllDDlistN
            ddlNat.DataValueField = "Value"
            ddlNat.DataTextField = "Text"
            ddlNat.DataBind()

            objlookupPolicyListReq.policyType = "CAT5"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLISTC As New Others.DDLList
            Dim objAllDDlistC As New List(Of Others.DDLList)
            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLISTC = New Others.DDLList
                objDDLISTC.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLISTC.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlistC.Add(objDDLISTC)
            Next
            ddlCity.DataSource = objAllDDlistC
            ddlCity.DataValueField = "Value"
            ddlCity.DataTextField = "Text"
            ddlCity.DataBind()

            Dim Terms As String = Request.QueryString("TermsID")
            Dim Mode As String = Request.QueryString("Mode")
            If Terms = "Visit_Reservation" Then
                If Lang = "Ara" Then
                    gotoURL = "~/wfrmVisitReservation.aspx?Mode=" & Mode & "&Lang=Ara"
                Else
                    gotoURL = "~/wfrmVisitReservation.aspx?Mode=" & Mode & ""
                End If
            ElseIf Terms = "Hall_Reservation" Then
                If Lang = "Ara" Then
                    gotoURL = "~/wfrmHallReservation.aspx?Mode=" & Mode & "&Lang=Ara"
                Else
                    gotoURL = "~/wfrmHallReservation.aspx?Mode=" & Mode & ""
                End If
            End If
            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            objReserveUserInfo = objEwsUserRegClient.ReserveUserInfo(UserID, Lang)
            If Not objReserveUserInfo Is Nothing Then
                txtFullName.Text = If(Not IsDBNull(objReserveUserInfo.FullName), objReserveUserInfo.FullName, "")
                txtCompany.Text = If(Not IsDBNull(objReserveUserInfo.Company), objReserveUserInfo.Company, "")
                txtFax.Text = If(Not IsDBNull(objReserveUserInfo.Fax), objReserveUserInfo.Fax, "")
                txtMobile.Text = If(Not IsDBNull(objReserveUserInfo.Mobile), objReserveUserInfo.Mobile, "")
                txtPostBox.Text = If(Not IsDBNull(objReserveUserInfo.PostBox), objReserveUserInfo.PostBox, "")
                txtTele.Text = If(Not IsDBNull(objReserveUserInfo.Telephone), objReserveUserInfo.Telephone, "")
                For c As Integer = 0 To objAllDDlist.Count - 1
                    If Not IsDBNull(objReserveUserInfo.Title) Then
                        If objAllDDlist(c).Value = objReserveUserInfo.Title Then
                            ddlTitleInd = CStr(c)
                        End If
                    End If
                Next
                For c As Integer = 0 To objAllDDlistN.Count - 1
                    If Not IsDBNull(objReserveUserInfo.Nationality) Then
                        If objAllDDlistN(c).Value = objReserveUserInfo.Nationality Then
                            ddlNatInd = CStr(c)
                        End If
                    End If
                Next
                For c As Integer = 0 To objAllDDlistC.Count - 1
                    If Not IsDBNull(objReserveUserInfo.City) Then
                        If objAllDDlistC(c).Value = objReserveUserInfo.City Then
                            ddlCityInd = CStr(c)
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim UserID As String = objUserInfo.User_ID '"11523" 'Request.QueryString("Lang")
            Dim Libr As String = objUserInfo.Library '"CENTRAL" 'Request.QueryString("Lang")
            Dim MemType As String = objUserInfo.Profile
            If MemType = ConfigurationManager.AppSettings("UPRF_public_user").ToUpper Then ' UPRF_public_user
                MemType = 0
            ElseIf MemType = ConfigurationManager.AppSettings("UPRF_FAMILY").ToUpper Then ' Family
                MemType = 1
            ElseIf MemType = ConfigurationManager.AppSettings("UPRF_academic").ToUpper Then ' UPRF_academic
                MemType = 2
            ElseIf MemType = ConfigurationManager.AppSettings("UPRF_postgraduate_student").ToUpper Then ' UPRF_postgraduate_student
                MemType = 3
            ElseIf MemType = ConfigurationManager.AppSettings("UPRF_undergraduate_student").ToUpper Then ' UPRF_undergraduate_student
                MemType = 4
            ElseIf MemType = ConfigurationManager.AppSettings("UPRF_special_user").ToUpper Then ' UPRF_special_user
                MemType = 5
            ElseIf MemType = ConfigurationManager.AppSettings("UPRF_Special_Need").ToUpper Then ' UPRF_Special_Need
                MemType = 6
            Else
                MemType = 0
            End If
            '"1" 'Request.QueryString("Lang")
            Dim Email As String = objUserInfo.Email '"yazeedkloub@gmail.com"

            Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
            Dim res As New ClassLibraries.Responce_Msg
            res = objEwsUserClient.ReserveUserDetails("1", UserID, Libr, txtFullName.Text, ddlTitle.Value, txtCompany.Text, ddlNat.Value, txtMobile.Text, _
                                                txtTele.Text, txtFax.Text, txtPostBox.Text, Email, ddlCity.Value, MemType, Lang)
            If res.UserId <> "" Then
                Response.Redirect(gotoURL)
            Else
                lblError.Text = res.Msg
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


End Class