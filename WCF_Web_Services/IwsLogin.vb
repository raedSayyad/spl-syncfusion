﻿Imports System.ServiceModel

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IwsLogin" in both code and config file together.
<ServiceContract()>
Public Interface IwsLogin

    <OperationContract()>
    Function GetData(ByVal value As String) As String

    <OperationContract()>
    Function Login(ByVal Username As String, ByVal Password As String) As String

    <OperationContract()>
    Function Session(ByVal Username As String, ByVal key As String) As String

    <OperationContract()>
    Function SetAuthKey(ByVal Username As String) As String

    <OperationContract()>
    Function GetAuthKey(ByVal AuthKey As String) As String

End Interface
