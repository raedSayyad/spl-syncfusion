﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmUserCancelReq
    Inherits System.Web.UI.Page

    Public Detail As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objAllUserUpgRequest As New List(Of Users.UserCancelRequest)
            Dim objUserUpgRequest As New Users.UserCancelRequest
            lblError.Text += CStr(objEwsUserRegClient.SelectUserUpgrade().Count)
            For i As Integer = 0 To objEwsUserRegClient.SelectUserUpgrade().Count - 1
                objUserUpgRequest = New Users.UserCancelRequest
                objUserUpgRequest = objEwsUserRegClient.SelectUserCancel()(i)
                objAllUserUpgRequest.Add(objUserUpgRequest)
            Next
            UserCancelReqGrid.DataSource = objAllUserUpgRequest
            UserCancelReqGrid.DataBind()
            lblError.Text += " Page 1 "
            Detail = "Detail"
        Catch ex As Exception
            lblError.Text += ex.Message + " Page Error"
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objUserUpgRequest As New Users.UserCancelRequest
            For i As Integer = 0 To objEwsUserRegClient.SelectUserUpgrade().Count - 1
                If i = CInt(selectedIndex) Then
                    objUserUpgRequest = New Users.UserCancelRequest
                    objUserUpgRequest = objEwsUserRegClient.SelectUserCancel()(i)
                End If
            Next
            Session("UserUpgRequest") = objUserUpgRequest
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmUserUpgReqDetail.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmUserUpgReqDetail.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub
End Class