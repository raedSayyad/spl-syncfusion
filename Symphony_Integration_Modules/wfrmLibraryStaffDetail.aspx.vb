﻿Imports ClassLibraries

Public Class wfrmLibraryStaffDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objLibraryStaff As New Others.LibraryStaff
            objLibraryStaff = Session("LibraryStaffDetail")
            txtFullName.Text = objLibraryStaff.Name
            txtUSerID.Text = objLibraryStaff.User_ID
            txtLibrary.Text = objLibraryStaff.Library
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmUsers.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmUsers.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class