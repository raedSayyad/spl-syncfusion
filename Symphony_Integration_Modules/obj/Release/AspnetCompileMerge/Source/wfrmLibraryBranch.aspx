﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmLibraryBranch.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmLibraryBranch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <table>
                        <tr>
                            <td colspan="3">Select Library Template:
                                <ej:DropDownList ID="ddlLibrary" runat="server" Width="250px">
                                </ej:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblATITLE" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtATITLE" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblETITLE" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtETITLE" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblADESCRIPTION" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtADESCRIPTION" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblEDESCRIPTION" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtEDESCRIPTION" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAADDRESS" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtAADDRESS" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblEADDRESS" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtEADDRESS" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTELEPHONE" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtTELEPHONE" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLATITUDE" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtLATITUDE" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLONGITUDE" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtLONGITUDE" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblWORKINGHOURS" runat="server" Text="Label"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtWORKINGHOURS" runat="server"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: center;">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" />
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                    <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>

