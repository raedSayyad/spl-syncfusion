﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmUserVisitReservation
    Inherits System.Web.UI.Page

    Dim UserID As String = "11523"
    Public Detail As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
            Dim objAllUserVisitReservation As New List(Of Others.ReserveVisit)
            Dim objUserVisitReservation As New Others.ReserveVisit

            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            objlookupPolicyListReq.policyType = "LIBR"

            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                objSymAdminSDH.locale = "ar"
                VisitsGrid.EnableRTL = True
            Else
                objSymAdminSDH.locale = "en"
            End If

            VisitsGrid.Columns(2).HeaderText = rc.GetString("Library", Culture_Used)
            VisitsGrid.Columns(3).HeaderText = rc.GetString("Purpose", Culture_Used)
            VisitsGrid.Columns(4).HeaderText = rc.GetString("DateFrom", Culture_Used)
            VisitsGrid.Columns(5).HeaderText = rc.GetString("DateTo", Culture_Used)
            VisitsGrid.Columns(6).HeaderText = rc.GetString("Status", Culture_Used)
            VisitsGrid.Columns(10).HeaderText = rc.GetString("Detail", Culture_Used)
            Detail = rc.GetString("Detail", Culture_Used)
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            For i As Integer = 0 To objEwsOthersClient.GetAllReserveVisitByUserID(UserID).Count - 1
                objUserVisitReservation = New Others.ReserveVisit
                objUserVisitReservation = objEwsOthersClient.GetAllReserveVisitByUserID(UserID)(i)
                If objUserVisitReservation.Status = "0" Then
                    objUserVisitReservation.Status = rc.GetString("_New", Culture_Used)
                ElseIf objUserVisitReservation.Status = "1" Then
                    objUserVisitReservation.Status = rc.GetString("Updated", Culture_Used)
                ElseIf objUserVisitReservation.Status = "2" Then
                    objUserVisitReservation.Status = rc.GetString("Approved", Culture_Used)
                ElseIf objUserVisitReservation.Status = "3" Then
                    objUserVisitReservation.Status = rc.GetString("Rejected", Culture_Used)
                ElseIf objUserVisitReservation.Status = "4" Then
                    objUserVisitReservation.Status = rc.GetString("Canceled", Culture_Used)
                End If
                For x As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                    If objUserVisitReservation.Library = objlookupPolicyInfoRes(x).policyID Then
                        objUserVisitReservation.Library = objlookupPolicyInfoRes(x).policyDescription
                    End If
                Next
                objAllUserVisitReservation.Add(objUserVisitReservation)
            Next
            VisitsGrid.DataSource = objAllUserVisitReservation
            VisitsGrid.DataBind()

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            'Dim currentTarget As String = e.Arguments("currentTarget").ToString  'returns current target details
            'Dim selectedRecord As Object = e.Arguments("selectedRecord")  'yields selected record details
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
            Dim objUserVisitReservation As New Others.ReserveVisit
            For i As Integer = 0 To objEwsOthersClient.GetAllReserveVisitByUserID(UserID).Count - 1
                If i = CInt(selectedIndex) Then
                    objUserVisitReservation = New Others.ReserveVisit
                    objUserVisitReservation = objEwsOthersClient.GetAllReserveVisitByUserID(UserID)(i)
                End If
            Next
            'If objUserVisitReservation.Status = "0" Or objUserVisitReservation.Status = "1" Then
            Session("UserVisitReservation") = objUserVisitReservation
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmVisitReservation.aspx?Mode=Edit&Lang=Ara")
            Else
                Response.Redirect("~/wfrmVisitReservation.aspx?Mode=Edit")
            End If
            'Else
            'lblError.Text = "Selected Reservation can't be updated." + selectedIndex
            'lblError.ForeColor = Drawing.Color.Red
            'End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub Update(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            'Dim currentTarget As String = e.Arguments("currentTarget").ToString  'returns current target details
            'Dim selectedRecord As Object = e.Arguments("selectedRecord")  'yields selected record details
            Dim selectedID As String '= e.Arguments.ToString   'e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            lblError.Text += "Arg: " + e.Arguments.ToString
            Dim KeyVal As Dictionary(Of String, Object) = e.Arguments("Data")
            For Each KeyV As KeyValuePair(Of String, Object) In KeyVal
                If KeyV.Key = "ID" Then
                    selectedID = CStr(KeyV.Value)
                End If
            Next
            lblError.Text += selectedID
            Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
            Dim objUserVisitReservation As New Others.ReserveVisit
            For i As Integer = 0 To objEwsOthersClient.GetAllReserveVisitByUserID(UserID).Count - 1
                If objEwsOthersClient.GetAllReserveVisitByUserID(UserID)(i).ID = CInt(selectedID) Then
                    objUserVisitReservation = New Others.ReserveVisit
                    objUserVisitReservation = objEwsOthersClient.GetAllReserveVisitByUserID(UserID)(i)
                End If
            Next
            If objUserVisitReservation.Status = "0" Or objUserVisitReservation.Status = "0" Then
                Session("UserVisitReservation") = objUserVisitReservation
                Dim Lang As String = Request.QueryString("Lang")
                If Lang = "Ara" Then
                    Response.Redirect("~/wfrmVisitReservation.aspx?Mode=Edit&Lang=Ara")
                Else
                    Response.Redirect("~/wfrmVisitReservation.aspx?Mode=Edit")
                End If
            Else
                lblError.Text = "Selected Reservation can't be updated." + selectedID
                lblError.ForeColor = Drawing.Color.Red
            End If
        Catch ex As Exception
            lblError.Text += ex.Message
        End Try
    End Sub

End Class