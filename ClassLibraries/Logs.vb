﻿Public Class Logs
    Dim Obj_SQL As New SQLFunction

    Public Function ProcessLog(ByVal type As String, ByVal text As String, ByVal DB As String, ByVal ODBC_Con_Str As String)
        If DB = "SQL" Then
            Obj_SQL.exeNonQuery("INSERT INTO [Symphony_Integration].[dbo].[tbl_Notification_Process_Log] ([Type],[DataTime],[Text]) VALUES ('" & type & "',GETDATE(),'" & text & "')", ODBC_Con_Str)
        Else
            Obj_SQL.exeNonQuery("INSERT INTO tbl_Notification_Process_Log (Type,DataTime,Text) VALUES ('" & type & "',sysdate,'" & text & "')", ODBC_Con_Str)
        End If
    End Function

    Public Function ErrorLog(ByVal type As String, ByVal text As String, ByVal DB As String, ByVal ODBC_Con_Str As String)
        If DB = "SQL" Then
            Obj_SQL.exeNonQuery("INSERT INTO [Symphony_Integration].[dbo].[tbl_Notification_Error_Log] ([Type],[DataTime],[Text]) VALUES ('" & type & "',GETDATE(),'" & text & "')", ODBC_Con_Str)
        Else
            'sysdate
            Obj_SQL.exeNonQuery("INSERT INTO tbl_Notification_Error_Log (Type,DataTime,Text) VALUES ('" & type & "',sysdate,'" & text & "')", ODBC_Con_Str)
        End If
    End Function



End Class
