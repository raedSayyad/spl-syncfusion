﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmContents.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmContents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <header class="major">
        <h3>Users Managment</h3>
        <p></p>
    </header>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <ej:Accordion ID="Accordion1" runat="server" SelectedItemIndex="-1" Collapsible="true">
                    <Items>
                        <ej:AccordionItem Text="Terms and Condition">
                            <ContentSection>
                                <iframe id="Terms" src="wfrmContent.aspx" width="100%" height="1080px"></iframe>
                            </ContentSection>
                        </ej:AccordionItem>
                        <ej:AccordionItem Text="News">
                            <ContentSection>
                                <iframe id="UserRenewal" src="" width="100%" height="1080px"></iframe>
                            </ContentSection>
                        </ej:AccordionItem>
                        <ej:AccordionItem Text="Library Branch">
                            <ContentSection>
                                <iframe id="LibraryBranch" src="wfrmLibraryBranch.aspx" width="100%" height="1080px"></iframe>
                            </ContentSection>
                        </ej:AccordionItem>
                    </Items>
                </ej:Accordion>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                    <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
