﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmTestWOM.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmTestWOM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='<%= Page.ResolveClientUrl("~/Scripts/CodeMirror/codemirror.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveClientUrl("~/Scripts/CodeMirror/javascript.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveClientUrl("~/Scripts/CodeMirror/htmlmixed.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveClientUrl("~/Scripts/CodeMirror/xml.js")%>' type="text/javascript"></script>
    <link href='<%= Page.ResolveClientUrl("~/Scripts/CodeMirror/codemirror.min.css")%>'></link>
    <link href='<%= Page.ResolveClientUrl("~/Scripts/CodeMirror/css.js")%>'></link>--%>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Button ID="Button1" runat="server" Text="Login" />
                <br />
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                <hr />
                 <asp:Button ID="Button2" runat="server" Text="Check" />
                <br />
                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                <hr />
                <asp:Button ID="Button3" runat="server" Text="logout" />
                <br />
                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                <%--<div>
                    <ej:RTE ID="rteSample" AllowEditing="true" Width="100%" ToolsList="customTools" runat="server" MinWidth="200px">
                        <RTEContent>
                            &lt;p&gt;&lt;b&gt;Description:&lt;/b&gt;&lt;/p&gt;    
    &lt;p&gt;The Rich Text Editor (RTE) control is an easy to render in
    client side. Customer easy to edit the contents and get the HTML content for
    the displayed content. A rich text editor control provides users with a toolbar
    that helps them to apply rich text formats to the text entered in the text
    area. &lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Functional
    Specifications/Requirements:&lt;/b&gt;&lt;/p&gt;
    &lt;ol&gt;&lt;li&gt;&lt;p&gt;Provide
    the tool bar support, it’s also customizable.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Options
    to get the HTML elements with styles.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Support
    to insert image from a defined path.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Footer
    elements and styles(tag / Element information , Action button (Upload, Cancel))&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Re-size
    the editor support. &lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Provide
    efficient public methods and client side events.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Keyboard
    navigation support.&lt;/p&gt;&lt;/li&gt;&lt;/ol&gt;
                        </RTEContent>
                        <Tools>
                            <CustomTools>
                                <ej:CustomTools Action="showDialog" Css="codeInsert" Name="codeInsert" Text="codeInsert" Tooltip="Insert code snippets" />
                            </CustomTools>
                        </Tools>
                    </ej:RTE>

                    <div id="TargetList">
                        <ul>
                            <li>Java Script</li>
                            <li>HTML</li>
                            <li>CSS</li>
                        </ul>
                    </div>
                    <ej:Dialog ID="customSourceCode" Title="Paste you code and inset to RTE" ShowOnInit="false" EnableModal="true" Width="596" EnableResize="false" runat="server">
                        <DialogContent>
                            <table>
                                <tr>
                                    <td id="dropselect" style="width: 100px">Select type :
                                    </td>
                                    <td>
                                        <div>
                                            <ej:DropDownList ID="languageList" TargetID="TargetList" SelectedItemIndex="0" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <textarea id="srcCode" style="width: 550px; height: 250px">
                                            <div id="srcArea"></div>
                                </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="samplebtn" colspan="2">
                                        <div class="e-rte-button e-fieldseparate">
                                            <ej:Button ID="insert" Type="Button" Text="Insert" runat="server" ClientSideOnClick="customBtnClick"></ej:Button>
                                            <ej:Button ID="cancel" Type="Button" Text="Cancel" runat="server" ClientSideOnClick="customBtnClick"></ej:Button>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </DialogContent>
                    </ej:Dialog>
                    <script src="../Scripts/CodeMirror/codemirror.js" type="text/javascript"></script>
                    <script src="../Scripts/CodeMirror/javascript.js" type="text/javascript"></script>
                    <script src="../Scripts/CodeMirror/css.js" type="text/javascript"></script>
                    <script src="../Scripts/CodeMirror/xml.js" type="text/javascript"></script>
                    <script src="../Scripts/CodeMirror/htmlmixed.js" type="text/javascript"></script>
                    <script type="text/javascript">
                        var rteObj;
                        window.onload = function () {
                            //Add text for custom tool bar element.
                            $("div.codeInsert").html("Insert code");
                            //load the CodeMirror css in the iframe
                            $("#<%=rteSample.ClientID%>_Iframe").contents().find("head").append($("<link href='../Scripts/CodeMirror/codemirror.min.css' rel='stylesheet'></link>"))
            rteObj = $("#<%=rteSample.ClientID%>").data("ejRTE");
            $("#<%=customSourceCode.ClientID%>").ejDialog({ enableResize: false, EnableModal: true, shoeOnInit: false, width: "auto" });
            $("#<%=languageList.ClientID%>").ejDropDownList({ selectItemByIndex: 0 });
            $("#<%=customSourceCode.ClientID%>").find(".e-rte-btn").ejButton({ click: "customBtnClick" });
        };
        function showDialog() {
            var dialogobj = $('#<%=customSourceCode.ClientID%>').data('ejDialog');
            $('#srcCode').val('').show();
            dialogobj.open();
            $('#srcCode').focus();
        }
        function customBtnClick(args) {
            if (this._id == "<%=insert.ClientID%>") {
                var scrLang, lang = $("#<%=languageList.ClientID%>").val();
                if (lang == "Java Script")
                    scrLang = "javascript";
                else if (lang == "HTML")
                    scrLang = "text/html";
                else
                    scrLang = "css";
                var htmlEditor = CodeMirror.fromTextArea($("#srcCode")[0], {
                    lineNumbers: false,
                    mode: scrLang
                });
                var codeTags = $("#<%=customSourceCode.ClientID%>").find(".CodeMirror");
                rteObj.executeCommand("inserthtml", codeTags[0].outerHTML);
                $(rteObj.getDocument()).find(".CodeMirror div.CodeMirror-cursor").css({ "display": "none" })
                codeTags.remove();
            }
            $("#<%=customSourceCode.ClientID%>").ejDialog("close");
        }
                    </script>
                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
