﻿Imports System.ServiceModel
Imports System.Web.Script.Serialization
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports ClassLibraries

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IewsUserControl" in both code and config file together.
<ServiceContract()>
Public Interface IewsUserControl

    <OperationContract()>
    Function ExistUser(ByVal UserID As String) As UserExt

    <DataContract()>
    Class UserExt
        <DataMember>
        Public Property ext As String
            Get
                Return m_ext
            End Get
            Set(value As String)
                m_ext = value
            End Set
        End Property
        Private m_ext As String
    End Class

    
End Interface
