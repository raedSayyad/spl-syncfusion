﻿Imports System.Security.Cryptography
Imports ExtraClassLibraries

Public Class wfrmPayment
    Inherits System.Web.UI.Page
    Public PageTitle As String
    Public ServNam As String = ConfigurationManager.AppSettings("ServNam")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            PageTitle = "Payment Summary"
            Dim Lang As String = Request.Form("pglang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "English" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                btnPay.Text = "إدفع"
                lblAmmount.Text = "القيمة"
                PageTitle = "تفاصيل الدفع"
            End If
            ' btnPay.Text = ""
            ' lblAmmount = ""

            'PageTitle = "Payment"
            UserID.Value = Request.Form("userid")
            billN.Value = Request.Form("billN")
            billV.Value = Request.Form("billV")
            billR.Value = Request.Form("billR")
            Dim reasonStr As String = billR.Value
            Dim reasonStrArr As String() = Nothing
            reasonStrArr = reasonStr.Split(New String() {","}, StringSplitOptions.None)

            Dim BillVArr() As String = billV.Value.Split(",")
            Dim BillsAmount As Double = 0
            For i As Integer = 0 To BillVArr.Length - 2
                BillsAmount = BillsAmount + CDbl(BillVArr(i).Replace("AED", ""))
            Next
            billVFinal.Value = BillsAmount
            Dim bStr As String = Request.Form("billN")
            Dim bstrarr As String() = Nothing
            bstrarr = bStr.Split(New String() {","}, StringSplitOptions.None)

            Dim valueStr As String = Request.Form("billV")
            Dim valuestrarr As String() = Nothing
            valuestrarr = valueStr.Split(New String() {","}, StringSplitOptions.None)

            Dim objewsOthersClient As New ewsOthers.IewsOthersClient

            Dim ds As New DataSet
            ds = objewsOthersClient.GetUserPaymentsRequests(UserID.Value)

            Dim ds1 As New DataSet()
            Dim dt1 As New DataTable("Bills")
            ds1.Tables.Add(dt1)
            If Lang = "English" Then
                ds1.Tables("Bills").Columns.Add("رقم الفاتوره")
                ds1.Tables("Bills").Columns.Add("السبب")
                ds1.Tables("Bills").Columns.Add("قيمة الفاتوره")
                ds1.Tables("Bills").Columns.Add("الحالة")
            Else
                ds1.Tables("Bills").Columns.Add("Bill Number")
                ds1.Tables("Bills").Columns.Add("Reason")
                ds1.Tables("Bills").Columns.Add("Bill Value")
                ds1.Tables("Bills").Columns.Add("Status")
            End If
            'ds1.Tables("Bills").Columns.Add(New DataColumn("Selected", GetType(Boolean)))
            For y As Integer = 0 To bstrarr.Length - 2
                Dim row As DataRow = ds1.Tables("Bills").NewRow()
                row(0) = bstrarr(y)
                row(1) = reasonStrArr(y)
                row(2) = valuestrarr(y)
                row(3) = "Unpaid"
                'row(3) = 1
                ds1.Tables("Bills").Rows.Add(row)
                For x As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim vObj As Object = ds.Tables(0).Rows(x)(9)
                    Dim vStr As String = Convert.ToString(vObj)
                    Dim vstrarr As String() = Nothing
                    vstrarr = vStr.Split(New String() {","}, StringSplitOptions.None)
                    For i As Integer = 0 To vstrarr.Length - 1
                        ds1.Tables("Bills").Rows(y)(2) = valuestrarr(y)
                        If vstrarr(i) = bstrarr(y) Then
                            Dim vObj1 As Object = ds.Tables(0).Rows(x)(11)
                            Dim vStr1 As String = Convert.ToString(vObj1)
                            Dim vObj2 As Object = ds.Tables(0).Rows(x)(12)
                            Dim vStr2 As String = Convert.ToString(vObj2)
                            If vStr1 = "-1" And vStr2 = "-1" Then
                                If Lang = "English" Then
                                    ds1.Tables("Bills").Rows(y)(3) = "قيد التنفيذ"
                                Else
                                    ds1.Tables("Bills").Rows(y)(3) = "Under Progress"
                                End If
                                ' ds1.Tables("Bills").Rows(y)(3) = 0
                            ElseIf vStr1 = "0" Or vStr2 = "0" Then
                                If Lang = "English" Then
                                    ds1.Tables("Bills").Rows(y)(3) = "مدفوعة"
                                Else
                                    ds1.Tables("Bills").Rows(y)(3) = "Paid"
                                End If
                                ' ds1.Tables("Bills").Rows(y)(3) = 0
                                'ElseIf vStr1 <> "" Then
                                '    If Lang = "English" Then
                                '        ds1.Tables("Bills").Rows(y)(3) = "قيد الانتظار"
                                '    Else
                                '        ds1.Tables("Bills").Rows(y)(3) = "Pending"
                                '    End If
                                '    '  ds1.Tables("Bills").Rows(y)(3) = 0
                            Else
                                If Lang = "English" Then
                                    ds1.Tables("Bills").Rows(y)(3) = "غير مدفوعة"
                                Else
                                    ds1.Tables("Bills").Rows(y)(3) = "Unpaid"
                                End If
                                '   ds1.Tables("Bills").Rows(y)(3) = 1
                            End If
                        End If
                    Next
                Next
            Next

            grvBills.DataSource = ds1
            grvBills.DataBind()

            Dim amount As Double = 0

            Dim Str As String = Request.Form("billV")
            Dim strarr As String() = Nothing
            strarr = Str.Split(New String() {","}, StringSplitOptions.None)
            For i As Integer = 0 To strarr.Length - 2
                strarr(i) = strarr(i).Replace("AED", "")
                Dim result As Double
                Double.TryParse(strarr(i), result)
            Next
            For j As Integer = 0 To ds1.Tables("Bills").Rows.Count - 1
                If ds1.Tables("Bills").Rows(j)(3) = "Unpaid" Or ds1.Tables("Bills").Rows(j)(3) = "غير مدفوعة" Then
                    Dim billAmount As String = Convert.ToString(ds1.Tables("Bills").Rows(j)(2))
                    billNFinal.Value = billNFinal.Value + ds1.Tables("Bills").Rows(j)(0) + ","
                    billVFinal.Value = billVFinal.Value + ds1.Tables("Bills").Rows(j)(2) + ","
                    billAmount = billAmount.Replace("AED", "")
                    Dim result As Double
                    Double.TryParse(billAmount, result)
                    amount = amount + result
                    billAmount = ""
                End If
            Next
            txtAmount.Text = Convert.ToString(amount)
            If txtAmount.Text = "0" Then
                If Lang = "English" Then
                    LBLMsg.Text = "لا يوجد فواتير للدفع"
                Else
                    LBLMsg.Text = "No Bills to Pay, Check Payment Summary"
                End If

                btnPay.Enabled = False
            Else
                LBLMsg.Text = ""
                btnPay.Enabled = True
            End If
        Catch ex As Exception
            LBLMsg.Text = ex.Message
        End Try

    End Sub

    Public Function CreateSecureHash(SecurityKey As String, RawData As String) As String
        Dim convertedHash As Byte()
        convertedHash = New Byte(SecurityKey.Length / 2 - 1) {}
        For i As Integer = 0 To SecurityKey.Length / 2 - 1
            convertedHash(i) = CByte(Int32.Parse(SecurityKey.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber))
        Next
        Dim hexHash As String = ""
        Using hasher As New HMACSHA256(convertedHash)
            Dim hashValue As Byte() = hasher.ComputeHash(Encoding.UTF8.GetBytes(RawData))
            For Each b As Byte In hashValue
                hexHash += b.ToString("X2")
            Next
        End Using
        Return hexHash
    End Function

    Private Sub btnPay_Click(sender As Object, e As EventArgs) Handles btnPay.Click
        Try
            Dim Lang As String = Request.Form("pglang")
            Dim TahseelLNG As String = "en-US"
            If Lang = "English" Then
                TahseelLNG = "en-US"
            Else
                TahseelLNG = "ar-AE"
            End If
            Dim st As New StringBuilder(), st2 As StringBuilder = New System.Text.StringBuilder()
            Dim stSecurData As New StringBuilder()
            Dim SecureData As New SortedList(New VPCStringComparer())
            'SecureData.Add("TP_InternalDep", "")
            SecureData.Add("TP_BranchId", "1")
            Dim trnsRef As String
            trnsRef = DateTime.Now.ToString.Replace("/", "").Replace(":", "").Replace(" ", "").Replace("AM", "").Replace("PM", "")
            Response.Write(trnsRef)
            Dim ServiceInfo As String = ""
            Dim billVStr As String = billV.Value
            Dim billVstrarr As String() = Nothing
            billVstrarr = billVStr.Split(New String() {","}, StringSplitOptions.None)
            Dim reasonStr As String = billR.Value
            Dim reasonStrArr As String() = Nothing
            reasonStrArr = reasonStr.Split(New String() {","}, StringSplitOptions.None)

            '<add key="OverDue" value ="001"/>
            '<add key="Damage" value ="002"/>
            '<add key="KIDMEM" value ="004"/>
            '<add key="SNEEDMEM" value ="005"/>
            '<add key="FAMILYMEM" value ="006"/>
            '<add key="INSTACAMEM" value ="007"/>
            '<add key="PUBLICMEM" value ="008"/>
            '<add key="OTHER" value ="009"/>

            'MEMFEEACA||Acadimic Membership|
            'MEMFEEFAM||Family Membership|
            'MEMFEEINST||Institute Membership|
            'MEMFEEKID||Kids Membership|
            'MEMFEEPSTD||Postgraduate Membership|
            'MEMFEEPUB||Public  Membership|
            'MEMFEESPN||Special Need Membership|
            'MEMFEESTD||Student Membership|
            For i As Integer = 0 To reasonStrArr.Length - 2
                If reasonStrArr(i) = "MEMFEEACA" Or reasonStrArr(i) = "MEMFEEINST" Then
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                        "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("INSTACAMEM") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                ElseIf reasonStrArr(i) = "MEMFEEFAM" Then
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                       "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("FAMILYMEM") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                ElseIf reasonStrArr(i) = "MEMFEEKID" Then
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                       "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("KIDMEM") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                ElseIf reasonStrArr(i) = "MEMFEEPSTD" Or reasonStrArr(i) = "MEMFEEPUB" Or reasonStrArr(i) = "MEMFEESTD" Then
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                       "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("PUBLICMEM") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                ElseIf reasonStrArr(i) = "MEMFEESPN" Then
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                       "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("SNEEDMEM") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                ElseIf reasonStrArr(i) = "OVERDUE" Then
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                       "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("OverDue") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                ElseIf reasonStrArr(i) = "DAMAGE" Then
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                       "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("Damage") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                Else
                    'OTHER
                    ServiceInfo += ConfigurationManager.AppSettings("TP_Department_Code") + "#" + ConfigurationManager.AppSettings("TP_BranchId") + _
                       "#" + ConfigurationManager.AppSettings("TP_Main_Service_Code") + "#" + ConfigurationManager.AppSettings("OTHER") + "#" + billVstrarr(i).Replace("AED", "").Replace(".00", "") + "#" + "1"
                End If
                If i > 0 Then
                    ServiceInfo += "|"
                End If
            Next
            SecureData.Add("TP_RefNo", CStr(trnsRef))
            SecureData.Add("TP_Merchant", ConfigurationManager.AppSettings("TP_Merchant")) '"4222473")
            SecureData.Add("TP_Language", TahseelLNG)
            SecureData.Add("TP_ServiceInfo", ServiceInfo) '"102#1#001#001#" & txtAmount.Text & "#1")
            SecureData.Add("TP_PayerName", UserID.Value)
            SecureData.Add("TP_ReturnURL", ConfigurationManager.AppSettings("TP_ReturnURL")) '"https://catalog.shjlib.gov.ae/eservices/wfrmPaymentResponse")

            For Each kvp As DictionaryEntry In SecureData
                st.AppendFormat("{0}={1}&", kvp.Key, kvp.Value.ToString())
                st2.AppendFormat("{0}={1}&", kvp.Key, HttpUtility.UrlEncode(kvp.Value.ToString()))
            Next
            If st.Length > 0 Then
                st.Remove(st.Length - 1, 1)
            End If
            Dim objewsOthersClient As New ewsOthers.IewsOthersClient
            Dim str As String = objewsOthersClient.InstUserPayment(UserID.Value, ConfigurationManager.AppSettings("TP_BranchId"), ConfigurationManager.AppSettings("TP_Merchant"), _
                                                                  CStr(trnsRef), TahseelLNG, "102#1#001#001#" & txtAmount.Text & "#1", _
                                                                 UserID.Value, ConfigurationManager.AppSettings("TP_ReturnURL"), billN.Value, billV.Value, _
                                                                "-1", "-1", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
            'If str = "1" Then
            'Dim TSecKey As String = CreateSecureHash("3EC81BD69AF47CB52ED45AF62EA81CB9", st.ToString())
            Dim TSecKey As String = CreateSecureHash(ConfigurationManager.AppSettings("SecureHash"), st.ToString())

            Dim payURL As String = [String].Format("{0}?{1}&TP_SecHash={2}", ConfigurationManager.AppSettings("TahseelURL"), st2.ToString(), TSecKey)

            Response.Redirect(payURL)
            'Else
            LBLMsg.Text = "Payment system not available | " + payURL
            'End If
        Catch ex As Exception
            LBLMsg.Text = ex.Message
        End Try
    End Sub
End Class