﻿Imports ClassLibraries
Public Class wfrmEmailTemp
    Inherits System.Web.UI.Page

    Dim _sqlObj As New SQLFunction

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'basicDialog.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim query As String = ""
            If dbType = "SQL" Then
                query = "UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Email_Temp] SET [TempValue] = N'" & RTE1.Value & "' WHERE [TempName] = '" & ddlEmailTemp.Value & "'"
            ElseIf dbType = "ORA" Then
                query = "UPDATE tbl_Notification_Email_Temp SET TempValue = N'" & RTE1.Value & "' WHERE TempName = '" & ddlEmailTemp.Value & "'"
            End If
            Dim objEwsNotification As New ewsNotification.IewsNotificationClient
            objEwsNotification.exeNonQuery(query)
            RTE1.Value = ""
        Catch ex As Exception
            Label2.Text = ex.Message
        End Try
    End Sub

    Private Sub ddlEmailTemp_ValueSelect(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs) Handles ddlEmailTemp.ValueSelect
        Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
        Dim dt As New DataSet
        Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
        Dim name As String = info.ToString
        Dim sqlScript As String = ""
        If dbType = "SQL" Then
            sqlScript = "SELECT [TempValue] FROM [Symphony_Integration].[dbo].[tbl_Notification_Email_Temp] WHERE [TempName] = '" & ddlEmailTemp.Value & "'"
        ElseIf dbType = "ORA" Then
            sqlScript = "SELECT TempValue FROM tbl_Notification_Email_Temp WHERE TempName = '" & ddlEmailTemp.Value & "'"
        End If
        Dim objEwsNotification As New ewsNotification.IewsNotificationClient
        dt = objEwsNotification.GetConfig(sqlScript)
        If dt.Tables(0).Rows.Count > 0 Then
            RTE1.Value = dt.Tables(0).Rows(0).Item(0)
            RTE1.DataBind()
        End If
    End Sub
End Class