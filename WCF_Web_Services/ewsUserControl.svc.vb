﻿Imports System.IO
Imports ClassLibraries

' NOTE: You can use the "Rename" command on the context menu to change the class name "ewsUserControl" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ewsUserControl.svc or ewsUserControl.svc.vb at the Solution Explorer and start debugging.
Public Class ewsUserControl
    Implements IewsUserControl

    Dim Common_Obj As New Common

    <WebGet(UriTemplate:="/ExistUser?UserID={UserID}", ResponseFormat:=WebMessageFormat.Json, RequestFormat:=WebMessageFormat.Json, BodyStyle:=WebMessageBodyStyle.Wrapped)> _
    Public Function ExistUser(ByVal UserID As String) As IewsUserControl.UserExt Implements IewsUserControl.ExistUser
        Dim objUserExist As New IewsUserControl.UserExt
        Try
            Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
            pProcess.StartInfo.FileName = "cmd.exe"
            pProcess.StartInfo.UseShellExecute = False
            pProcess.StartInfo.RedirectStandardOutput = True
            pProcess.StartInfo.RedirectStandardInput = True
            pProcess.StartInfo.RedirectStandardError = True
            pProcess.StartInfo.CreateNoWindow = True

            pProcess.Start()

            pProcess.StandardInput.AutoFlush = True
            Dim MyStreamWriter As StreamWriter = pProcess.StandardInput

            MyStreamWriter.WriteLine("D:\SIRSI\Unicorn\Bin\seluser.exe -iB")

            MyStreamWriter.WriteLine(UserID.ToString.ToUpper + System.Environment.NewLine)
            pProcess.StandardInput.Close()

            Dim output As String = pProcess.StandardOutput.ReadToEnd()

            Dim Output_Error As String = pProcess.StandardError.ReadToEnd()
            If output.Contains("|") = False Then
                objUserExist.ext = "False"
            Else
                objUserExist.ext = "True"
            End If
            Return objUserExist
        Catch ex As Exception
            objUserExist.ext = "False"
            ' objUserExist.objException.msg = ex.Message
            Return objUserExist
        End Try
    End Function

    
End Class
