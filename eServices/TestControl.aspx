﻿<%@ Page Language="vb" AutoEventWireup="true" CodeBehind="TestControl.aspx.vb" Inherits="eServices.TestControl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
</head>
<body>

    <form id="form1" enctype="multipart/form-data" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <ej:DatePicker ID="dpDOB" EnablePersistence="true" Height="35px" Width="100%" CssClass="CustomDDL" runat="server" ></ej:DatePicker>
            <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:Wizard ID="Wizard1" runat="server" DisplaySideBar="false" DisplayCancelButton="true">
                        <WizardSteps>
                            <asp:WizardStep ID="WizardStep1" runat="server" Title="General Information">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <h3>Step1</h3>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:WizardStep>
                            <asp:WizardStep ID="WizardStep2" runat="server" Title="Contact Details">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <h3>Step2</h3>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:WizardStep>
                            <asp:WizardStep ID="WizardStep3" runat="server" Title="Documents">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="UserUpgrade" ControlToValidate="FileUpload1" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                                <asp:Button ID="btnFinish" runat="server" Text="Finish" CausesValidation="true" ValidationGroup="UserUpgrade" />
                            </asp:WizardStep>
                        </WizardSteps>
                        <%-- <HeaderTemplate>
                            <ul id="wizHeader">
                                <asp:Repeater ID="SideBarList" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a class="<%# GetClassForWizardStep(Container.DataItem) %>" title="<%#Eval("Name")%>"><%# Eval("Name")%></a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </HeaderTemplate>
                        <StartNavigationTemplate>
                            <table cellpadding="3" cellspacing="3">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnNext" runat="server" Text="Next" CausesValidation="true" ValidationGroup="UserUpgrade" CommandName="MoveNext" />
                                    </td>
                                </tr>
                            </table>
                        </StartNavigationTemplate>
                        <StepNavigationTemplate>
                            <table cellpadding="3" cellspacing="3">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnPrevious" runat="server" Text="Previous" CausesValidation="false" CommandName="MovePrevious" />
                                        <asp:Button ID="btnNext" runat="server" Text="Next" CausesValidation="true" ValidationGroup="UserUpgrade" CommandName="MoveNext" />
                                    </td>
                                </tr>
                            </table>
                        </StepNavigationTemplate>
                        <FinishNavigationTemplate>
                            <table cellpadding="3" cellspacing="3">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnPrevious" runat="server" Text="Previous" CausesValidation="false" CommandName="MovePrevious" />
                                        <asp:Button ID="btnFinish" runat="server" Text="Finish" CausesValidation="true" ValidationGroup="UserUpgrade" CommandName="MoveComplete" />
                                    </td>
                                </tr>
                            </table>
                        </FinishNavigationTemplate>
                        <FinishNavigationTemplate>--%>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel"  />
                          <%--  <asp:Button ID="btnPrevious" runat="server" Text="Previous" CausesValidation="false" CommandName="MovePrevious" />
                            <asp:Button ID="btnFinish" runat="server" Text="Finish" CausesValidation="true" ValidationGroup="UserUpgrade" CommandName="MoveComplete" />
                        </FinishNavigationTemplate>
                    </asp:Wizard>
                    <script type="text/javascript">
                    </script>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Wizard1$FinishNavigationTemplateContainerID$btnFinish" />
                    <asp:PostBackTrigger ControlID="Wizard1$WizardStep3$FileUpload1$btnFinish" />
                </Triggers>
            </asp:UpdatePanel>--%>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
