﻿Imports System.ServiceProcess
Imports System.IO
Imports System.Data.Odbc
Imports System.Reflection
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Configuration
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports ClassLibraries
Imports Symphony_Integration_Modules

Public Class wfrmNotification
    Inherits System.Web.UI.Page

    Dim _sqlObj As New SQLFunction
    Dim _winOpObj As New WindowsOperation

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            If IsPostBack = False Then
                PanelProcess.Visible = False
                Dim serv_exist As Boolean = False
                Dim servics As ServiceController() = ServiceController.GetServices()
                For Each service As ServiceController In servics
                    If service.ServiceName = "Unicorn_Notification" Then
                        serv_exist = True
                        PanelInstall.Visible = False
                        PanelManage.Visible = True
                        Exit For
                    Else
                        PanelInstall.Visible = True
                        PanelManage.Visible = False
                    End If
                Next
                If serv_exist = True Then
                    fillConfig()
                    fillSetting()
                    Dim servic As ServiceController = New ServiceController("Unicorn_Notification")
                    If Not servic Is Nothing Then
                        If servic.Status = ServiceControllerStatus.Running Then
                            tgbService.ToggleState = True
                            'ckbWinService.Checked = True
                        ElseIf servic.Status = ServiceControllerStatus.Stopped Then
                            tgbService.ToggleState = False
                            'ckbWinService.Checked = False
                        End If
                        lblWinSrvStatus.Text = servic.Status.ToString
                    Else
                        lblWinSrvStatus.Text = "Service Not Installed"
                    End If
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message + ex.StackTrace
        End Try
    End Sub

    Protected Sub btnDBCheck_Click(sender As Object, e As EventArgs) Handles btnDBCheck.Click
        Try
            System.Threading.Thread.Sleep(1000)
            Dim DBType As String = ""
            If ddlDBType.SelectedIndex = 0 Then
                DBType = "SQL"
            Else
                DBType = "ORA"
            End If
            ConfigurAppSetting("DB_Type", DBType)
            ConfigurAppSetting("SymConnStr", "DSN=" & txtDSN.Text.Trim & ";Uid=" & txtUserName.Text.Trim & ";Pwd=" & txtPassword.Text.Trim & ";")

            Dim conn As New OdbcConnection
            conn.ConnectionString = "DSN=" & txtDSN.Text.Trim & ";Uid=" & txtUserName.Text.Trim & ";Pwd=" & txtPassword.Text.Trim & ";" 'System.Configuration.ConfigurationManager.AppSettings("SymConnStr")
            conn.Open()
            If conn.State = ConnectionState.Open Then
                PanelDB.Visible = False
                PanelProcess.Visible = True
                lblDBCheck.Text = "Success"
            Else
                lblDBCheck.Text = "Connection cannott be established"
            End If
        Catch ex As Exception
            lblDBCheck.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnInstall_Click(sender As Object, e As EventArgs) Handles btnInstall.Click
        Try
            'Copying Files
            System.Threading.Thread.Sleep(10000)
            Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
            Dim name As String = info.ToString
            _winOpObj.CopyFolder(name + "App_Install_Source\Notification", "D:\SymWS\Notification")

            'Install Service
            System.Threading.Thread.Sleep(10000)
            Dim processResult As String = _winOpObj.Process("C:\Windows\Microsoft.NET\Framework64\v4.0.30319\", "InstallUtil.exe ""D:\SymWS\Notification\Unicorn Notification.exe""")

            'Prepare Database
            System.Threading.Thread.Sleep(10000)
            _sqlObj.exeNonQuery("USE [master] CREATE DATABASE [Symphony_Integration] ", System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
            _sqlObj.exeNonQuery("USE [master] CREATE DATABASE [Notification] ", System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
            'Service Configuration
            System.Threading.Thread.Sleep(10000)
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim script As String = ""
            If dbType = "SQL" Then
                script = File.ReadAllText(name + "SQL_Script\Notification\SQL_Init_Notification.sql")
            ElseIf dbType = "ORA" Then
                script = File.ReadAllText(name + "SQL_Script\Notification\ORA_Init_Notification.sql")
            End If
            Dim currentConnStr As String = System.Configuration.ConfigurationManager.AppSettings("SymConnStr")
            'Dim newConnStr As String = ""
            'Dim ConnStrPra() As String = currentConnStr.Split(";")
            'For i As Integer = 0 To ConnStrPra.Length - 2
            '    If ConnStrPra.Contains("Database") Then
            '        newConnStr += "Database=Symphony_Integration;"
            '    Else
            '        newConnStr += ConnStrPra(i) + ";"
            '    End If
            'Next

            _sqlObj.exeNonQuery(script, currentConnStr)

            If dbType = "SQL" Then
                Dim scripts() As String = File.ReadAllLines(name + "SQL_Script\Notification\SQL_Init_Notification_Triggers.sql")
                For t As Integer = 0 To scripts.Length - 1
                    _sqlObj.exeNonQuery(scripts(t), System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                    Label5.Text += "%%%%" + scripts(t) + "%%%%"
                Next
            End If

            System.Threading.Thread.Sleep(10000)
            System.Threading.Thread.Sleep(10000)
            Response.Redirect("~/wfrmNotification.aspx")
        Catch ex As Exception
            Label5.Text += ex.Message
        End Try
    End Sub

    Private Sub ConfigurAppSetting(Name As String, Value As String)
        Dim objConfig As System.Configuration.Configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
        Dim objAppsettings As AppSettingsSection = DirectCast(objConfig.GetSection("appSettings"), AppSettingsSection)
        If objAppsettings IsNot Nothing Then
            objAppsettings.Settings(Name).Value = Value
            objConfig.Save()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
           "var str2 = str.replace('hide','');" & _
          "document.getElementById('navButton').className=str2;", True)
        System.Threading.Thread.Sleep(3000)
        Try
            Dim query As String = "UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox1.Text.Trim & "' WHERE [Key] = 'Notification_WS_URL'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox3.Text.Trim & "' WHERE [Key] = 'Request_Type_Tag'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox4.Text.Trim & "' WHERE [Key] = 'Email_Tag'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox5.Text.Trim & "' WHERE [Key] = 'Mobile_Tag'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox6.Text.Trim & "' WHERE [Key] = 'Charges_Overdue_First_Notifications_Days'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox7.Text.Trim & "' WHERE [Key] = 'Charges_Overdue_Second_Notifications_Days'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox8.Text.Trim & "' WHERE [Key] = 'Charges_Overdue_Final_Notifications_Days'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox2.Text.Trim & "' WHERE [Key] = 'Push_Notification_WS_URL'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox9.Text.Trim & "' WHERE [Key] = 'EmailCC'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox10.Text.Trim & "' WHERE [Key] = 'HostIP'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox11.Text.Trim & "' WHERE [Key] = 'Port'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox12.Text.Trim & "' WHERE [Key] = 'Username'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox13.Text.Trim & "' WHERE [Key] = 'Password'" & _
                " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Value_Config] SET [Value] = '" & TextBox14.Text.Trim & "' WHERE [Key] = 'Email'"

            _sqlObj.exeNonQuery(query, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
            fillConfig()
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
          "var str2 = str.replace('hide','');" & _
         "document.getElementById('navButton').className=str2;", True)

            Dim cbx() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

            If CheckBox28.Checked Then
                cbx(28) = 1
            End If
            If CheckBox29.Checked Then
                cbx(29) = 1
            End If
            If CheckBox30.Checked Then
                cbx(30) = 1
            End If

            cbx(0).ToString()


            System.Threading.Thread.Sleep(3000)
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")

            query = ""
            If dbType = "SQL" Then
                query = " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(28).ToString & " where [key]='Charges_Overdue_First_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(29).ToString & " where [key]='Charges_Overdue_Second_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(30).ToString & " where [key]='Charges_Overdue_Final_Notifications'"
            ElseIf dbType = "ORA" Then
                query = ""
            End If

            _sqlObj.exeNonQuery(query, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))

            fillSetting()
        Catch ex As Exception
            Label6.Text = ex.Message
            lblError.Text = ex.Message
        End Try
    End Sub

    Public Function fillConfig()
        Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
        Dim dt As New DataSet
        Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
        Dim name As String = info.ToString
        Dim sqlScript As String = ""
        If dbType = "SQL" Then
            sqlScript = File.ReadAllText(name + "SQL_Script\Notification\SQLConfig.sql")
        ElseIf dbType = "ORA" Then
            sqlScript = File.ReadAllText(name + "SQL_Script\Notification\ORAConfig.sql")
        End If
        If sqlScript <> "" Then
            Dim objEwsNotification As New ewsNotification.IewsNotificationClient
            dt = objEwsNotification.GetConfig(sqlScript)
            If dt.Tables(0).Rows.Count > 0 Then

                TextBox1.Text = dt.Tables(0).Rows(2).Item(1).ToString
                TextBox2.Text = dt.Tables(0).Rows(11).Item(1).ToString
                TextBox3.Text = dt.Tables(0).Rows(5).Item(1).ToString
                TextBox4.Text = dt.Tables(0).Rows(6).Item(1).ToString
                TextBox5.Text = dt.Tables(0).Rows(7).Item(1).ToString
                TextBox6.Text = dt.Tables(0).Rows(8).Item(1).ToString
                TextBox7.Text = dt.Tables(0).Rows(9).Item(1).ToString
                TextBox8.Text = dt.Tables(0).Rows(10).Item(1).ToString
                TextBox9.Text = dt.Tables(0).Rows(13).Item(1).ToString
                TextBox10.Text = dt.Tables(0).Rows(14).Item(1).ToString
                TextBox11.Text = dt.Tables(0).Rows(15).Item(1).ToString
                TextBox12.Text = dt.Tables(0).Rows(16).Item(1).ToString
                TextBox13.Text = dt.Tables(0).Rows(17).Item(1).ToString
                TextBox14.Text = dt.Tables(0).Rows(18).Item(1).ToString
            End If
        End If
    End Function

    Public Function fillSetting()
        Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
        Dim dt As New DataSet
        dt = New DataSet
        Dim info As New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~"))
        Dim name As String = info.ToString

        Dim sqlScript As String = ""
        If dbType = "SQL" Then
            sqlScript = File.ReadAllText(name + "SQL_Script\Notification\SQLSetting.sql")
        ElseIf dbType = "ORA" Then
            sqlScript = File.ReadAllText(name + "SQL_Script\Notification\ORASetting.sql")
        End If
        If sqlScript <> "" Then
            Dim objEwsNotification As New ewsNotification.IewsNotificationClient
            dt = objEwsNotification.GetConfig(sqlScript)
            If dt.Tables(0).Rows.Count > 0 Then
                'Email
                CheckBox31.Checked = dt.Tables(0).Rows(2).Item(1)
                CheckBox1.Checked = dt.Tables(0).Rows(5).Item(1)
                CheckBox2.Checked = dt.Tables(0).Rows(14).Item(1)
                CheckBox3.Checked = dt.Tables(0).Rows(17).Item(1)
                CheckBox4.Checked = dt.Tables(0).Rows(20).Item(1)
                CheckBox5.Checked = dt.Tables(0).Rows(23).Item(1)
                CheckBox6.Checked = dt.Tables(0).Rows(26).Item(1)
                CheckBox7.Checked = dt.Tables(0).Rows(29).Item(1)
                CheckBox8.Checked = dt.Tables(0).Rows(32).Item(1)
                CheckBox9.Checked = dt.Tables(0).Rows(8).Item(1)
                'Mobile
                CheckBox32.Checked = dt.Tables(0).Rows(4).Item(1)
                CheckBox10.Checked = dt.Tables(0).Rows(7).Item(1)
                CheckBox11.Checked = dt.Tables(0).Rows(16).Item(1)
                CheckBox12.Checked = dt.Tables(0).Rows(19).Item(1)
                CheckBox13.Checked = dt.Tables(0).Rows(22).Item(1)
                CheckBox14.Checked = dt.Tables(0).Rows(25).Item(1)
                CheckBox15.Checked = dt.Tables(0).Rows(28).Item(1)
                CheckBox16.Checked = dt.Tables(0).Rows(31).Item(1)
                CheckBox17.Checked = dt.Tables(0).Rows(34).Item(1)
                CheckBox18.Checked = dt.Tables(0).Rows(10).Item(1)
                'SMS
                CheckBox33.Checked = dt.Tables(0).Rows(3).Item(1)
                CheckBox19.Checked = dt.Tables(0).Rows(6).Item(1)
                CheckBox20.Checked = dt.Tables(0).Rows(15).Item(1)
                CheckBox21.Checked = dt.Tables(0).Rows(18).Item(1)
                CheckBox22.Checked = dt.Tables(0).Rows(21).Item(1)
                CheckBox23.Checked = dt.Tables(0).Rows(24).Item(1)
                CheckBox24.Checked = dt.Tables(0).Rows(27).Item(1)
                CheckBox25.Checked = dt.Tables(0).Rows(30).Item(1)
                CheckBox26.Checked = dt.Tables(0).Rows(33).Item(1)
                CheckBox27.Checked = dt.Tables(0).Rows(9).Item(1)
                'Overdue
                CheckBox28.Checked = dt.Tables(0).Rows(11).Item(1)
                CheckBox29.Checked = dt.Tables(0).Rows(12).Item(1)
                CheckBox30.Checked = dt.Tables(0).Rows(13).Item(1)

            End If
        End If
    End Function

    Private Sub tgbService_Click(Sender As Object, e As Syncfusion.JavaScript.Web.ToggleButtonEventArgs) Handles tgbService.Click
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
                                            "var str2 = str.replace('hide','');" & _
                                            "document.getElementById('navButton').className=str2;", True)
        Dim servic As ServiceController = New ServiceController("Unicorn Notifications")
        If tgbService.ToggleState = True Then
            lblWinSrvStatus.Text = _winOpObj.StartWinService("Unicorn_Notification", 5)
            'ckbWinService.Checked = True
        Else
            lblWinSrvStatus.Text = _winOpObj.StopWinService("Unicorn_Notification", 5)
            'ckbWinService.Checked = False
        End If
    End Sub

    Private Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click
        Try
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
           "var str2 = str.replace('hide','');" & _
          "document.getElementById('navButton').className=str2;", True)

            Dim cbx() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            If CheckBox1.Checked Then
                cbx(1) = 1
            End If
            If CheckBox2.Checked Then
                cbx(2) = 1
            End If
            If CheckBox3.Checked Then
                cbx(3) = 1
            End If
            If CheckBox4.Checked Then
                cbx(4) = 1
            End If
            If CheckBox5.Checked Then
                cbx(5) = 1
            End If
            If CheckBox6.Checked Then
                cbx(6) = 1
            End If
            If CheckBox7.Checked Then
                cbx(7) = 1
            End If
            If CheckBox8.Checked Then
                cbx(8) = 1
            End If
            If CheckBox9.Checked Then
                cbx(9) = 1
            End If
            If CheckBox31.Checked Then
                cbx(31) = 1
            End If

            cbx(0).ToString()


            System.Threading.Thread.Sleep(3000)

            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")

            Dim query As String = ""
            Dim objEwsNotification As New ewsNotification.IewsNotificationClient

            If dbType = "SQL" Then
                query = " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(31).ToString & " where [key]='Users_New_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(1).ToString & " where [key]='Charges_New_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(9).ToString & " where [key]='Charges_Overdue_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(2).ToString & " where [key]='Holds_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(3).ToString & " where [key]='Charges_Discharge_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(4).ToString & " where [key]='Charges_Renew_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(5).ToString & " where [key]='Requests_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(6).ToString & " where [key]='Bills_NEW_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(7).ToString & " where [key]='Bills_PAY_Email_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(8).ToString & " where [key]='Approval_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
            ElseIf dbType = "ORA" Then
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(31).ToString & " where key='Users_New_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(1).ToString & " where key='Charges_New_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(9).ToString & " where key='Charges_Overdue_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(2).ToString & " where key='Holds_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(3).ToString & " where key='Charges_Discharge_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(4).ToString & " where key='Charges_Renew_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(5).ToString & " where key='Requests_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(6).ToString & " where key='Bills_NEW_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(7).ToString & " where key='Bills_PAY_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(8).ToString & " where key='Approval_Email_Notifications'"
                objEwsNotification.exeNonQuery(query)
            End If
           

            fillSetting()
        Catch ex As Exception
            lblEmailEr.Text = ex.Message
        End Try
    End Sub

    Private Sub btnMobile_Click(sender As Object, e As EventArgs) Handles btnMobile.Click
        Try
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
           "var str2 = str.replace('hide','');" & _
          "document.getElementById('navButton').className=str2;", True)

            Dim cbx() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            If CheckBox10.Checked Then
                cbx(10) = 1
            End If
            If CheckBox11.Checked Then
                cbx(11) = 1
            End If
            If CheckBox12.Checked Then
                cbx(12) = 1
            End If
            If CheckBox13.Checked Then
                cbx(13) = 1
            End If
            If CheckBox14.Checked Then
                cbx(14) = 1
            End If
            If CheckBox15.Checked Then
                cbx(15) = 1
            End If
            If CheckBox16.Checked Then
                cbx(16) = 1
            End If
            If CheckBox17.Checked Then
                cbx(17) = 1
            End If
            If CheckBox18.Checked Then
                cbx(18) = 1
            End If
            If CheckBox32.Checked Then
                cbx(32) = 1
            End If
            cbx(0).ToString()


            System.Threading.Thread.Sleep(3000)

            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")

            Dim query As String = ""
            If dbType = "SQL" Then
                query = " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(32).ToString & " where [key]='Users_New_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(10).ToString & " where [key]='Charges_New_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(18).ToString & " where [key]='Charges_Overdue_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(11).ToString & " where [key]='Holds_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(12).ToString & " where [key]='Charges_Discharge_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(13).ToString & " where [key]='Charges_Renew_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(14).ToString & " where [key]='Requests_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(15).ToString & " where [key]='Bills_NEW_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(16).ToString & " where [key]='Bills_PAY_Mobile_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(17).ToString & " where [key]='Approval_Mobile_Notifications'"
            ElseIf dbType = "ORA" Then
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(32).ToString & " where key='Users_New_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(10).ToString & " where key='Charges_New_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(18).ToString & " where key='Charges_Overdue_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(11).ToString & " where key='Holds_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(12).ToString & " where key='Charges_Discharge_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(13).ToString & " where key='Charges_Renew_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(14).ToString & " where key='Requests_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(15).ToString & " where key='Bills_NEW_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(16).ToString & " where key='Bills_PAY_Mobile_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(17).ToString & " where key='Approval_Mobile_Notifications'"
            End If
            Dim objEwsNotification As New ewsNotification.IewsNotificationClient
            objEwsNotification.exeNonQuery(query)

            fillSetting()
        Catch ex As Exception
            lblMobEr.Text = ex.Message
        End Try
    End Sub

    Private Sub btnSms_Click(sender As Object, e As EventArgs) Handles btnSms.Click
        Try
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
           "var str2 = str.replace('hide','');" & _
          "document.getElementById('navButton').className=str2;", True)

            Dim cbx() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

            If CheckBox19.Checked Then
                cbx(19) = 1
            End If
            If CheckBox20.Checked Then
                cbx(20) = 1
            End If
            If CheckBox21.Checked Then
                cbx(21) = 1
            End If
            If CheckBox22.Checked Then
                cbx(22) = 1
            End If
            If CheckBox23.Checked Then
                cbx(23) = 1
            End If
            If CheckBox24.Checked Then
                cbx(24) = 1
            End If
            If CheckBox25.Checked Then
                cbx(25) = 1
            End If
            If CheckBox26.Checked Then
                cbx(26) = 1
            End If
            If CheckBox27.Checked Then
                cbx(27) = 1
            End If
            If CheckBox33.Checked Then
                cbx(33) = 1
            End If
            cbx(0).ToString()


            System.Threading.Thread.Sleep(3000)

            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")

            Dim query As String = ""
            If dbType = "SQL" Then
                query = " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(33).ToString & " where [key]='Users_New_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(19).ToString & " where [key]='Charges_New_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(27).ToString & " where [key]='Charges_Overdue_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(28).ToString & " where [key]='Charges_Overdue_First_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(20).ToString & " where [key]='Holds_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(21).ToString & " where [key]='Charges_Discharge_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(22).ToString & " where [key]='Charges_Renew_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(23).ToString & " where [key]='Requests_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(24).ToString & " where [key]='Bills_NEW_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(25).ToString & " where [key]='Bills_PAY_SMS_Notifications'" & _
        " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(26).ToString & " where [key]='Approval_SMS_Notifications'"
            ElseIf dbType = "ORA" Then
                query = " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(33).ToString & " where key='Users_New_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(19).ToString & " where key='Charges_New_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(27).ToString & " where key='Charges_Overdue_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(28).ToString & " where key='Charges_Overdue_First_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(20).ToString & " where key='Holds_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(21).ToString & " where key='Charges_Discharge_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(22).ToString & " where key='Charges_Renew_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(23).ToString & " where key='Requests_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(24).ToString & " where key='Bills_NEW_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(25).ToString & " where key='Bills_PAY_SMS_Notifications'" & _
        " UPDATE tbl_Notification_Enable_Config SET Enable= " & cbx(26).ToString & " where key='Approval_SMS_Notifications'"
            End If
            Dim objEwsNotification As New ewsNotification.IewsNotificationClient
            objEwsNotification.exeNonQuery(query)

            fillSetting()
        Catch ex As Exception
            lblSmsEr.Text = ex.Message
        End Try
    End Sub

    'Private Sub btnOverDue_Click(sender As Object, e As EventArgs) Handles btnOverDue.Click
    '    Try
    '        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
    '       "var str2 = str.replace('hide','');" & _
    '      "document.getElementById('navButton').className=str2;", True)

    '        Dim cbx() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

    '        If CheckBox28.Checked Then
    '            cbx(28) = 1
    '        End If
    '        If CheckBox29.Checked Then
    '            cbx(29) = 1
    '        End If
    '        If CheckBox30.Checked Then
    '            cbx(30) = 1
    '        End If

    '        cbx(0).ToString()


    '        System.Threading.Thread.Sleep(3000)
    '        Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")

    '        Dim query As String = ""
    '        If dbType = "SQL" Then
    '            query = " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(28).ToString & " where [key]='Charges_Overdue_First_Notifications'" & _
    '    " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(29).ToString & " where [key]='Charges_Overdue_Second_Notifications'" & _
    '    " UPDATE [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config] SET [Enable]= " & cbx(30).ToString & " where [key]='Charges_Overdue_Final_Notifications'"
    '        ElseIf dbType = "ORA" Then
    '            query = ""
    '        End If

    '        _sqlObj.exeNonQuery(query, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))

    '        fillSetting()
    '    Catch ex As Exception
    '        lblOverEr.Text = ex.Message
    '    End Try
    'End Sub

    Private Sub ddlDBType_CheckedChange(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs) Handles ddlDBType.CheckedChange
        If ddlDBType.SelectedIndex = 0 Then

        ElseIf ddlDBType.SelectedIndex = 1 Then

        End If
    End Sub

End Class