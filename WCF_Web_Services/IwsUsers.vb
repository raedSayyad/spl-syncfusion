﻿Imports System.ServiceModel

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IwsUsers" in both code and config file together.
<ServiceContract()>
Public Interface IwsUsers

    <OperationContract()>
    Function AddUser(ByVal Username As String, ByVal Password As String, ByVal Email As String) As Boolean

End Interface
