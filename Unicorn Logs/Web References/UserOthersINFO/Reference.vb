﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
'
Namespace UserOthersINFO
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="UserOtherInfoSoap", [Namespace]:="http://tempuri.org/")>  _
    Partial Public Class UserOtherInfo
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private CallUserOtherInfoOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.RequestTransStatus.My.MySettings.Default.RequestTransStatus_UserOthersINFO_UserOtherInfo
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event CallUserOtherInfoCompleted As CallUserOtherInfoCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/UserOtherInfo", RequestElementName:="UserOtherInfo", RequestNamespace:="http://tempuri.org/", ResponseElementName:="UserOtherInfoResponse", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function CallUserOtherInfo(ByVal LANG As String, ByVal UserID As String) As <System.Xml.Serialization.XmlElementAttribute("UserOtherInfoResult")> Reponse_Msg
            Dim results() As Object = Me.Invoke("CallUserOtherInfo", New Object() {LANG, UserID})
            Return CType(results(0),Reponse_Msg)
        End Function
        
        '''<remarks/>
        Public Overloads Sub CallUserOtherInfoAsync(ByVal LANG As String, ByVal UserID As String)
            Me.CallUserOtherInfoAsync(LANG, UserID, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub CallUserOtherInfoAsync(ByVal LANG As String, ByVal UserID As String, ByVal userState As Object)
            If (Me.CallUserOtherInfoOperationCompleted Is Nothing) Then
                Me.CallUserOtherInfoOperationCompleted = AddressOf Me.OnCallUserOtherInfoOperationCompleted
            End If
            Me.InvokeAsync("CallUserOtherInfo", New Object() {LANG, UserID}, Me.CallUserOtherInfoOperationCompleted, userState)
        End Sub
        
        Private Sub OnCallUserOtherInfoOperationCompleted(ByVal arg As Object)
            If (Not (Me.CallUserOtherInfoCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent CallUserOtherInfoCompleted(Me, New CallUserOtherInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3056.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class Reponse_Msg
        
        Private msgField As String
        
        Private userIdField As String
        
        Private cAT1Field As String
        
        Private cAT2Field As String
        
        Private cAT3Field As String
        
        Private cAT4Field As String
        
        Private cAT5Field As String
        
        Private pINField As String
        
        Private s_NEEDSField As String
        
        Private pROFILEField As String
        
        Private lIBRField As String
        
        '''<remarks/>
        Public Property Msg() As String
            Get
                Return Me.msgField
            End Get
            Set
                Me.msgField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property UserId() As String
            Get
                Return Me.userIdField
            End Get
            Set
                Me.userIdField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property CAT1() As String
            Get
                Return Me.cAT1Field
            End Get
            Set
                Me.cAT1Field = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property CAT2() As String
            Get
                Return Me.cAT2Field
            End Get
            Set
                Me.cAT2Field = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property CAT3() As String
            Get
                Return Me.cAT3Field
            End Get
            Set
                Me.cAT3Field = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property CAT4() As String
            Get
                Return Me.cAT4Field
            End Get
            Set
                Me.cAT4Field = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property CAT5() As String
            Get
                Return Me.cAT5Field
            End Get
            Set
                Me.cAT5Field = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property PIN() As String
            Get
                Return Me.pINField
            End Get
            Set
                Me.pINField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property S_NEEDS() As String
            Get
                Return Me.s_NEEDSField
            End Get
            Set
                Me.s_NEEDSField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property PROFILE() As String
            Get
                Return Me.pROFILEField
            End Get
            Set
                Me.pROFILEField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property LIBR() As String
            Get
                Return Me.lIBRField
            End Get
            Set
                Me.lIBRField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0")>  _
    Public Delegate Sub CallUserOtherInfoCompletedEventHandler(ByVal sender As Object, ByVal e As CallUserOtherInfoCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3056.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class CallUserOtherInfoCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Reponse_Msg
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Reponse_Msg)
            End Get
        End Property
    End Class
End Namespace
