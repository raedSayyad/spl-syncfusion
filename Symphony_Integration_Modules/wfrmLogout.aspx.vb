﻿Public Class wfrmLogout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("Login") = "0"
        Session.Clear()
        Session.Abandon()
        FormsAuthentication.SignOut()
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1))
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetNoStore()
        Response.Redirect("wfrmLogin.aspx")
    End Sub

End Class