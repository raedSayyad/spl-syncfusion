﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmNotiProcessLog.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmNotiProcessLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>

        <div>
            <table>
                <tr>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <td>From:<ej:DateTimePicker ID="dtpPFrom" ClientSideOnChange="onSelectedF" runat="server"></ej:DateTimePicker>
                                <asp:HiddenField ID="hdfDtFm" runat="server" />
                            </td>
                            <td>To:
                                <ej:DateTimePicker ID="dtpPTo" ClientSideOnChange="onSelectedT" runat="server"></ej:DateTimePicker>
                                <asp:HiddenField ID="hdfDtTo" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnPFilter" runat="server" Text="Filter" />
                            </td>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </tr>
                <tr>
                    <td colspan="3">
                        <ej:Grid ID="grdProcessLog" runat="server" EnablePersistence="true" IsResponsive="true" AllowGrouping="True" OnServerWordExporting="FlatGrid_ServerWordExporting" OnServerPdfExporting="FlatGrid_ServerPdfExporting" OnServerExcelExporting="FlatGrid_ServerExcelExporting" AllowPaging="True" AllowSorting="true" AllowSearching="True">
                            <ToolbarSettings ShowToolbar="true" ToolbarItems="excelExport,wordExport,pdfExport,printGrid"></ToolbarSettings>
                            <PageSettings PageSize="10" />
                        </ej:Grid>
                    </td>
                </tr>
            </table>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>
        <script type="text/javascript" class="jsScript">
            function onSelectedF(args) {
                $("#<%=hdfDtFm.ClientID%>").val(args.value);
            }
            function onSelectedT(args) {
                $("#<%=hdfDtTo.ClientID%>").val(args.value);
            }
        </script>
    </form>
</body>
</html>
