﻿Imports ClassLibraries
Public Class wfrmContent
    Inherits System.Web.UI.Page

    Dim _sqlObj As New SQLFunction

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        Dim objContent As New Others.Content
        Dim objEwsOthersClient As New ewsOthers.IewsOthersClient

        Dim Content As New Others.Content
        Dim AllContent As New List(Of Others.Content)


        For i As Integer = 0 To objEwsOthersClient.GetAllContent.Count - 1
            If i = 0 Then
                Content = New Others.Content
                Content.Content = "Select One"
                Content.ID = "0"
                AllContent.Add(Content)
            End If
            Content = New Others.Content
            Content = objEwsOthersClient.GetAllContent(i)
            AllContent.Add(Content)
        Next
        ddlContentTemp.DataSource = AllContent
        ddlContentTemp.DataValueField = "ID"
        ddlContentTemp.DataTextField = "Content"
        ddlContentTemp.DataBind()
        If Not IsPostBack Then
            ddlContentTemp.SelectedIndex = 0
        End If
    End Sub

    Private Sub ddlContentTemp_ValueSelect(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs) Handles ddlContentTemp.ValueSelect
        Try
            lblError.Text = e.Value
            If e.Value = "0" Then
                RTE1.Value = ""
                RTE2.Value = ""
            Else
                Dim objContent As New Others.Content
                Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
                objContent = objEwsOthersClient.GetContent(e.Value)
                RTE1.Value = objContent.ADESCRIPTION
                RTE1.DataBind()
                RTE2.Value = objContent.EDESCRIPTION
                RTE2.DataBind()
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ddlContentTemp.Value + "|" + ddlContentTemp.Text
            If ddlContentTemp.Value = "0" Then
                lblError.Text = "Please Select Content Template"
            Else
                Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
                objEwsOthersClient.UptContent(ddlContentTemp.Value, ddlContentTemp.Text, RTE1.Value, RTE2.Value)
                lblError.Text = "Content Template Updated " + ddlContentTemp.Value + " | " + ddlContentTemp.Text + " | " + RTE1.Value + " | " + RTE2.Value
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class