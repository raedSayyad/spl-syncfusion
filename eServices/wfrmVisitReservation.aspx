﻿
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmVisitReservation.aspx.vb" Inherits="eServices.wfrmVisitReservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div>
                        <div class="<%=Row%> uniform 50%">
                            <div class="6<%=u%> 12<%=u%>$(4)">
                                <asp:Label ID="lblLibrary" runat="server" Text="Library"></asp:Label>
                                <ej:DropDownList ID="ddlLibrary" Height="35px" Width="100%" CssClass="CustomDDL" runat="server"></ej:DropDownList>
                                <asp:Label ID="lblValdDdl" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                            <div class="6<%=u%>$ 12<%=u%>$(4)">
                                <asp:Label ID="lblPurpose" runat="server" Text="Purpose"></asp:Label>
                                <asp:TextBox ID="txtPurpose" runat="server" AutoPostBack="True"></asp:TextBox>
                                <asp:Label ID="lblValdTxt" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                           <div class="6<%=u%> 12<%=u%>$(4)">
                                <asp:Label ID="lblDFrom" runat="server"  Text="Date From"></asp:Label>&nbsp;&nbsp;
                                    <ej:DatePicker ID="dpFrom" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" ClientSideOnBeforeDateCreate="disableDate"></ej:DatePicker>
                                <asp:Label ID="lblValdDpF" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                            <div class="6<%=u%>$ 12<%=u%>$(4)">
                                <asp:Label ID="lblDTo" runat="server" Text="Date To"></asp:Label>&nbsp;&nbsp;
                                    <ej:DatePicker ID="dpTo" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" ClientSideOnBeforeDateCreate="disableDate"></ej:DatePicker>
                                <asp:Label ID="lblValdDpT" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                            <div class="6<%=u%> 12<%=u%>$(4)">
                                <asp:Label ID="lblTFrom" runat="server" Text="Time From"></asp:Label>&nbsp;&nbsp;
                                    <ej:TimePicker ID="tpFrom" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" Interval="60" Value="08:00 AM">
                                        <DisableTimeRanges>
                                            <ej:DisableTimeRange StartTime="12:00 AM" EndTime="07:00 AM" />
                                            <ej:DisableTimeRange StartTime="05:00 PM" EndTime="11:00 PM" />
                                        </DisableTimeRanges>
                                    </ej:TimePicker>
                                <asp:Label ID="lblValdTpF" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>
                            <div class="6<%=u%>$ 12<%=u%>$(4)">
                                <asp:Label ID="lblTTo" runat="server" Text="Time To"></asp:Label>&nbsp;&nbsp;
                                    <ej:TimePicker ID="tpTo" runat="server" Height="35px" Width="100%" CssClass="CustomDDL" Interval="60" Value="05:00 PM">
                                        <DisableTimeRanges>
                                            <ej:DisableTimeRange StartTime="12:00 AM" EndTime="08:00 AM" />
                                            <ej:DisableTimeRange StartTime="06:00 PM" EndTime="11:00 PM" />
                                        </DisableTimeRanges>
                                    </ej:TimePicker>
                                <asp:Label ID="lblValdTpT" runat="server" ForeColor="Red" Text=""></asp:Label>
                                <asp:Label ID="lblAva" runat="server" ForeColor="Red" Text=""></asp:Label>
                            </div>

                            <div class="12<%=u%>$">
                                <asp:Button ID="btnBack" runat="server" Text="Back" /></td>
                                <asp:Button ID="btnReserve" runat="server" Text="Reseve" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible="false" />
                            </div>
                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                        </div>
                        <script>
                            function disableDate(args) {
                                if (args.date.getDay() == 5 || args.date.getDay() == 6) {
                                    //Removes the current month/other month class and adds the e-disable class to disable the dates.
                                    $(args.element).addClass('e-hidedate e-disable').removeClass('current-month other-month');
                                }
                            }
                        </script>
                        <script type="text/javascript">
                            var target;
                            $(function () {
                                target = $('#<%=ddlLibrary.ClientID%>').data("ejDropDownList");
                                target.option({ selectedIndex: <%=ddlLibraryV%> });
                            });
                            $("#tpFrom").ejTimePicker({  value : "<%=tpFromV%>" });
                            $("#tpTo").ejTimePicker({  value : "<%=tpToV%>" });
                        </script>
                        <asp:HiddenField ID="hfVisitID" runat="server" />
                        <asp:HiddenField ID="hdDateCreate" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
