﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmResulte.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmResulte" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <section class="wrapper style1 special">
                    <div class="container">
                        <div class="12u$">
                            <h4>
                                <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label></h4>
                            <br />
                            <br />
                            <br />
                            <asp:Label ID="lblText" runat="server" Text="Label"></asp:Label>
                            <br />
                            <br />
                            <asp:Button ID="btnBack" runat="server" Text="Back" />
                        </div>
                        <div class="12u$">
                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </section>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV1" align="center" valign="middle" class="ModalPopupBG" runat="server">
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
