﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmUserRenewal.aspx.vb" Inherits="eServices.wfrmUserRenewal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%=PageTitle%></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" class="landing" runat="server">
    <form id="form1" enctype="multipart/form-data" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="width: 100%; height: 100%!important">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <h3><%=PageTitle%></h3>
                    <asp:Wizard ID="Wizard1" runat="server" CssClass="EngWizard" DisplaySideBar="false" DisplayCancelButton="true">
                        <WizardSteps>
                            <asp:WizardStep ID="WizardStep1" runat="server" Title="General Information">
                                <section>
                                    <div class="<%=Row%> uniform 50%">
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblFulName" runat="server" Text="Full Name"></asp:Label>
                                            <asp:TextBox ID="txtFullName" runat="server" CausesValidation="true" required="required" Enabled="false" ></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ControlToValidate="txtFullName" ErrorMessage="Letter Only" ForeColor="Red" ValidationExpression="[a-zA-Z ]*" ValidationGroup="UserUpgrade"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFullName" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblDOB" runat="server" Text="Date of Birth"></asp:Label>
                                            <asp:TextBox ID="txtDOB" runat="server" CausesValidation="false" required="required" Enabled="false" ></asp:TextBox>
                                            <%--<ej:DatePicker ID="dpDOB" EnablePersistence="true" Enabled="false" Height="35px" Width="90%" CssClass="CustomDDL" runat="server"></ej:DatePicker>--%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDOB" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="4<%=u%>$ 12<%=u%>$(3)">
                                            <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                            <ej:DropDownList ID="ddlGender" Enabled="false" Height="35px" Width="90%" CssClass="CustomDDL" runat="server"></ej:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlGender" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblNat" runat="server" Text="Nationality"></asp:Label>
                                            <ej:DropDownList ID="ddlNat" Height="35px" Width="90%" CssClass="CustomDDL" runat="server"></ej:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlNat" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblNatID" runat="server" Text="National ID"></asp:Label>
                                            <asp:TextBox ID="txtNatID" runat="server" Enabled="false" CausesValidation="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtNatID" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic" ControlToValidate="txtNatID" ErrorMessage="Must be 15 digits only" ForeColor="Red" ValidationExpression="^\d{15}" ValidationGroup="UserUpgrade"></asp:RegularExpressionValidator>
                                        </div>
                                        <div class="4<%=u%>$ 12<%=u%>$(3)">
                                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                            <ej:DropDownList ID="ddlTitle" Height="35px" Width="90%" CssClass="CustomDDL" runat="server"></ej:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlTitle" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
                                            <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </section>
                            </asp:WizardStep>
                            <asp:WizardStep ID="WizardStep2" runat="server" Title="Contact Details">
                                <section>
                                    <div class="<%=Row%> uniform 50%">
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                            <asp:TextBox ID="txtMobile" runat="server" TextMode="Number"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtMobile" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic" ControlToValidate="txtMobile" ErrorMessage="Wrong Mobile Number Format" ForeColor="Red" ValidationExpression="^\d{10}" ValidationGroup="UserUpgrade"></asp:RegularExpressionValidator>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblTele" runat="server" Text="Telephone"></asp:Label>
                                            <asp:TextBox ID="txtTele" runat="server" TextMode="Number"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic" ControlToValidate="txtTele" ErrorMessage="Wrong Format" ForeColor="Red" ValidationExpression="^\d{9}" ValidationGroup="UserUpgrade"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtTele" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="4<%=u%>$ 12<%=u%>$(3)">
                                            <asp:Label ID="lblWorkPhone" runat="server" Text="Work Phone"></asp:Label>
                                            <asp:TextBox ID="txtWorkPhone" runat="server" TextMode="Number"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Display="Dynamic" ControlToValidate="txtWorkPhone" ErrorMessage="Wrong Format" ForeColor="Red" ValidationExpression="^\d{9}" ValidationGroup="UserUpgrade"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtWorkPhone" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblExt" runat="server" Text="Extenstion"></asp:Label>
                                            <asp:TextBox ID="txtExt" runat="server" TextMode="Number"></asp:TextBox>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                            <asp:TextBox ID="txtFax" runat="server" TextMode="Number"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" Display="Dynamic" ControlToValidate="txtFax" ErrorMessage="Wrong Format" ForeColor="Red" ValidationExpression="^\d{9}" ValidationGroup="UserUpgrade"></asp:RegularExpressionValidator>
                                        </div>
                                        <div class="4<%=u%>$ 12<%=u%>$(3)">
                                            <asp:Label ID="lblPostBox" runat="server" Text="PO BOX"></asp:Label>
                                            <asp:TextBox ID="txtPostBox" runat="server" TextMode="Number"></asp:TextBox>
                                        </div>
                                        <div class="4<%=u%> 12<%=u%>$(3)">
                                            <asp:Label ID="lblWebSite" runat="server" Text="Web Site"></asp:Label>
                                            <asp:TextBox ID="txtWebSite" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="4<%=u%>$ 12<%=u%>$(3)">
                                            <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                            <ej:DropDownList ID="ddlCity" Height="35px" Width="90%" CssClass="CustomDDL" runat="server" ClientSideOnChange="onActiveIndexChange"></ej:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlCity" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </section>
                            </asp:WizardStep>
                            <asp:WizardStep ID="WizardStep3" runat="server" Title="Documents">
                                <div class="<%=Row%> uniform 50%">
                                    <div class="4<%=u%> 12<%=u%>$(3)">
                                        <asp:Label ID="lblLibrary" runat="server" Text="Library"></asp:Label>
                                        <ej:DropDownList ID="ddlLibrary" Height="35px" Width="90%" CssClass="CustomDDL" runat="server"></ej:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLibrary" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="4<%=u%> 12<%=u%>$(3)">
                                        <asp:Label ID="lblPersonalImg" runat="server" Text="Personal Photo"></asp:Label><br />
                                        <label class="file-upload">
                                            <span class="fa fa-upload">
                                                <strong><%=ChooseLBL%></strong>
                                            </span>
                                            <asp:FileUpload ID="updPersonalImg" runat="server" />
                                        </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="updPersonalImg" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="4<%=u%>$ 12<%=u%>$(3)">
                                        <asp:Label ID="lblNatID1" runat="server" Text="National ID Front Photo"></asp:Label><br />
                                        <label class="file-upload">
                                            <span class="fa fa-upload">
                                                <strong><%=ChooseLBL%></strong>
                                            </span>
                                            <asp:FileUpload ID="updNatID1" runat="server" />
                                        </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="updNatID1" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                    </div>
                                    <%--<div class="4<%=u%> 12<%=u%>$(3)">
                                        <asp:Label ID="lblMemType" runat="server" Text="Membershipt Type"></asp:Label>
                                        <ej:DropDownList ID="ddlMemType" ClientSideOnChange="drpvaluechange" ClientSideOnCreate="drpcreate"
                                            Height="35px" Width="90%" CssClass="CustomDDL" runat="server">
                                        </ej:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="ddlMemType" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                    </div>--%>
                                    <asp:HiddenField ID="ddlMemType" runat="server" />
                                    <div class="4<%=u%> 12<%=u%>$(3)">
                                        <asp:Label ID="lblNatID2" runat="server" Text="National ID Back Photo"></asp:Label><br />
                                        <label class="file-upload">
                                            <span class="fa fa-upload">
                                                <strong><%=ChooseLBL%></strong>
                                            </span>
                                            <asp:FileUpload ID="updNatID2" runat="server" />
                                        </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="updNatID2" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="4<%=u%>$ 12<%=u%>$(3)">
                                        <asp:Label ID="lblSNeedImg" runat="server" Text="Special Need Photo" Style="display: none;"></asp:Label><br />
                                        <label id="lblSNeedUpl" class="file-upload" style="display: none;">
                                            <span class="fa fa-upload">
                                                <strong><%=ChooseLBL%></strong>
                                            </span>
                                            <asp:FileUpload ID="updSNeedImg" runat="server" />
                                        </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" Enable="false" ControlToValidate="updSNeedImg" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="UserUpgrade"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </asp:WizardStep>
                        </WizardSteps>
                        <HeaderTemplate>
                            <ul id="wizHeader" class="<%=WizHdrClass%>">
                                <asp:Repeater ID="SideBarList" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a class="<%# GetClassForWizardStep(Container.DataItem) %>" title="<%#Eval("Name")%>"><%# Eval("Name")%></a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </HeaderTemplate>
                        <StartNavigationTemplate>
                            <table cellpadding="3" cellspacing="3">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnNext" runat="server" Text="Next" CausesValidation="true" ValidationGroup="UserUpgrade" CommandName="MoveNext" />
                                    </td>
                                </tr>
                            </table>
                        </StartNavigationTemplate>
                        <StepNavigationTemplate>
                            <table cellpadding="3" cellspacing="3">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnPrevious" runat="server" Text="Previous" CausesValidation="false" CommandName="MovePrevious" />
                                        <asp:Button ID="btnNext" runat="server" Text="Next" CausesValidation="true" ValidationGroup="UserUpgrade" CommandName="MoveNext" />
                                    </td>
                                </tr>
                            </table>
                        </StepNavigationTemplate>
                        <FinishNavigationTemplate>
                            <table cellpadding="3" cellspacing="3">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnPrevious" runat="server" Text="Previous" CausesValidation="false" CommandName="MovePrevious" />
                                        <asp:Button ID="btnFinish" runat="server" Text="Finish" CausesValidation="true" ValidationGroup="UserUpgrade" CommandName="MoveComplete" />
                                    </td>
                                </tr>
                            </table>
                        </FinishNavigationTemplate>
                    </asp:Wizard>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Wizard1$FinishNavigationTemplateContainerID$btnFinish" />
                    <%--<asp:PostBackTrigger ControlID="Wizard1$WizardStep1$dpDOB" />--%>
                    <asp:PostBackTrigger ControlID="Wizard1$WizardStep1$ddlMemType" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div>
                <section>
                    <div class="row uniform 50%">
                        <div class="12<%=u%>$">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="false" ValidationGroup="UserUpgrade" />
                        </div>
                        <div class="12<%=u%>$">
                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </section>
                <script type="text/javascript" class="jsScript">
                    function drpcreate(sender) {
                        document.getElementById("lblSNeedUpl").style.display = 'none';
                        document.getElementById("<%=lblSNeedImg.ClientID%>").style.display = 'none';
                        ValidatorEnable(document.getElementById("<%=RequiredFieldValidator14.ClientID%>"), false);
                    }
                    function drpvaluechange(sender) {
                        var ddlmem = sender.value;
                        if ((ddlmem == '2') || (ddlmem == '3') || (ddlmem == '4') || (ddlmem == '6')) {
                            document.getElementById("lblSNeedUpl").style.display = 'inline-block';
                            document.getElementById("<%=lblSNeedImg.ClientID%>").style.display = 'inline';
                            ValidatorEnable(document.getElementById("<%=RequiredFieldValidator14.ClientID%>"), true);
                        }
                        else {
                            document.getElementById("lblSNeedUpl").style.display = 'none';
                            document.getElementById("<%=lblSNeedImg.ClientID%>").style.display = 'none';
                            ValidatorEnable(document.getElementById("<%=RequiredFieldValidator14.ClientID%>"), false);
                        }
                        //ddlmem = sender.value;
                    }
                    function evtpropscheckedevent(args) {
                        var target = $("#<%=ddlMemType.ClientID%>").data("ejDropDownList");
                        if (args.isChecked) {
                            switch (args.selectedValue) {
                                case "create": target.option(args.selectedValue, "drpcreate"); break;
                                case "change": target.option(args.selectedValue, "drpvaluechange"); break;
                            }
                        }
                        else {
                            target.option(args.selectedValue, null);
                        }
                    }
                </script>
                <script type="text/javascript">
                    var target;
                    $(function () {
                        target = $('#<%=ddlLibrary.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlLibraryInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlCity.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlCityInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlNat.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlNatInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlTitle.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlTitleInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlGender.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlGenderInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlMemType.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlMemberTypeInd%> });
                    });
                </script>
            </div>
            <script>
                $.noConflict();
            </script>
        </div>
    </form>
</body>
</html>
