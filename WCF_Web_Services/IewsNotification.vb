﻿Imports System.ServiceModel

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IewsNotification" in both code and config file together.
<ServiceContract()>
Public Interface IewsNotification

    <OperationContract()>
    Function GetConfig(ByVal Query As String) As DataSet

    <OperationContract()>
    Function exeNonQuery(ByVal Query As String) As String

End Interface
