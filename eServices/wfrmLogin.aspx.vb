﻿Imports ClassLibraries.Users

Public Class wfrmLogin
    Inherits System.Web.UI.Page

    Public ServNam As String = ConfigurationManager.AppSettings("ServNam")
    Public goto_url As String = ""

    Public Row As String
    Public u As String

    Public NewUser As String
    Public ChangePassword As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim GotoQ As String = Request.QueryString("goto")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If GotoQ <> "" Then
                Dim URL_SPLIT() As String = Nothing
                URL_SPLIT = GotoQ.Split("/")
                Dim URL_NEW As String = ""
                For i As Integer = 1 To URL_SPLIT.Length - 1
                    URL_NEW = URL_NEW + "/" + URL_SPLIT(i)
                    If i = 5 Then
                        URL_NEW = URL_NEW + "/57"
                    End If
                Next
                goto_url = URL_NEW
            Else
                goto_url = "/uhtbin/cgisirsi.exe/x/0/0/57/49/502/X"
            End If

            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            lblEmail.Text = rc.GetString("Email", Culture_Used)
            lblPassword.Text = rc.GetString("Password", Culture_Used)
            btnSubmit.Text = rc.GetString("Login", Culture_Used)
            NewUser = rc.GetString("SingUp", Culture_Used)
            ChangePassword = rc.GetString("ForgotPassword", Culture_Used)
            If Not Session("UserInfo") Is Nothing Then
                newuserreg.Attributes.Remove("style")
                newuserreg.Attributes.Add("style", "display: none;")
                forgotpass.Attributes.Remove("style")
                forgotpass.Attributes.Add("style", "display: none;")
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                Dim objUserInfo As New ClassLibraries.Users.UserInfo
                objUserInfo = Session("UserInfo")
                Session("user_web") = objUserInfo.User_ID
                Dim objUserPin As New UserPin
                objUserPin = objEwsUserClient.UserPin(objUserInfo.User_ID)
                Session("Pass") = objUserPin.Pin
                ' Response.Write(Session("user_web") + " || " + Session("Pass"))
                Dim url As String = GotoQ ' goto_url
                Response.Write("<script>window.open('" & url & "','_parent');</script>")
                lblEmail.Visible = False
                lblPassword.Visible = False
                txtEmail.Visible = False
                txtPassword.Visible = False
                btnSubmit.Visible = False

            Else
                Session("user_web") = ""
                Session("Pass") = ""
            End If
            HiddenField_UserID.Value = ""
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            Dim UserID As String = ""
            Dim UserEmail As String = ""
            Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
            Dim res As New ClassLibraries.Users.UserExist
            If txtEmail.Text.Trim.Contains("@") Then
                UserID = objEwsUserClient.Users_Email_ID(txtEmail.Text.Trim, "", "")
                UserEmail = txtEmail.Text.Trim
            Else
                UserID = txtEmail.Text.Trim
                UserEmail = objEwsUserClient.Users_Email(UserID)
            End If
            Dim activated As String = objEwsUserClient.GetActivated(UserEmail)

            res = objEwsUserClient.UserExist(UserID)
            If res.ext = True Then

                Dim objwsSymSecurityClient As New wsSymSecurity.SecurityEndpointClient
                Dim objAuthenticateUserRequest As New wsSymSecurity.AuthenticateUserRequest
                Dim objSymAdminSDH As New wsSymSecurity.SdHeader
                objAuthenticateUserRequest.login = UserID
                objAuthenticateUserRequest.password = txtPassword.Text
                objSymAdminSDH.clientID = "DS_CLIENT"
                Dim AuthBool As Boolean = objwsSymSecurityClient.authenticateUser(objSymAdminSDH, objAuthenticateUserRequest)
                If AuthBool = True Then
                    If UserID.Length = 5 Then
                        If activated = "2" Then
                            Session("user_web") = UserID
                            Session("Pass") = txtPassword.Text
                            HiddenField_UserID.Value = UserID
                            Dim objUserInfo As New ClassLibraries.Users.UserInfo
                            objUserInfo = objEwsUserClient.UserInfo(UserID)
                            Session("UserInfo") = objUserInfo
                        Else
                            lblError.Text = rc.GetString("NotActivated", Culture_Used)
                        End If
                    Else
                        Session("user_web") = UserID
                        Session("Pass") = txtPassword.Text
                        HiddenField_UserID.Value = UserID
                        Dim objUserInfo As New ClassLibraries.Users.UserInfo
                        objUserInfo = objEwsUserClient.UserInfo(UserID)
                        Session("UserInfo") = objUserInfo
                    End If

                Else
                    lblError.Text = rc.GetString("NotAuth", Culture_Used)
                End If
            Else
                lblError.Text = rc.GetString("NotExist", Culture_Used)
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class