﻿Imports ClassLibraries
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.IO

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Configuration
Imports System.Collections.Generic
Imports ClassLibraries.Others

' NOTE: You can use the "Rename" command on the context menu to change the class name "ewsOthers" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ewsOthers.svc or ewsOthers.svc.vb at the Solution Explorer and start debugging.
Public Class ewsOthers
    Implements IewsOthers

    '||||||||||||||||||||||||||||||||||||||||||||| Library Branch Start |||||||||||||||||||||||||||||||||||||||||||||
    Public Sub InstLibraryBranch(ByVal LibararyBranchID As String, ByVal ATitle As String, ByVal ETitle As String, _
                                  ByVal ADescription As String, ByVal EDescription As String, ByVal AAddress As String, _
                                  ByVal EAddress As String, ByVal Telephone As String, ByVal URL As String, _
                                  ByVal Email As String, ByVal Latitude As String, ByVal Longitude As String, _
                                  ByVal WorkingHours As String, Optional Likes As Integer = 0, _
                                  Optional Memebers As Integer = 0, Optional NumberOfWatchings As String = "") Implements IewsOthers.InstLibraryBranch
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("INSERT INTO LibraryBranchInfo(LibararyBranchID,ATitle,ETitle,ADescription,EDescription," & _
                           "AAddress,EAddress,Telephone,URL,Email,Latitude,Longitude,WorkingHours,Likes,Memebers," & _
                           "NumberOfWatchings) VALUES ('" & LibararyBranchID & "','" & ATitle & "','" & ETitle & _
                           "','" & ADescription & "','" & EDescription & "','" & AAddress & "','" & EAddress & "','" & Telephone & _
                           "','" & URL & "','" & Email & "','" & Latitude & "','" & Longitude & "','" & WorkingHours & "'," & Likes & _
                           "," & Memebers & ",'" & NumberOfWatchings & "')", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Sub UptLibraryBranch(ByVal LibararyBranchID As String, ByVal ATitle As String, ByVal ETitle As String, _
                                  ByVal ADescription As String, ByVal EDescription As String, ByVal AAddress As String, _
                                  ByVal EAddress As String, ByVal Telephone As String, ByVal URL As String, _
                                  ByVal Email As String, ByVal Latitude As String, ByVal Longitude As String, _
                                  ByVal WorkingHours As String, Optional Likes As Integer = 0, _
                                  Optional Memebers As Integer = 0, Optional NumberOfWatchings As String = "") Implements IewsOthers.UptLibraryBranch
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("UPDATE LibraryBranchInfo SET LibararyBranchID='" & LibararyBranchID & "',ATitle='" & ATitle & _
                           "',ETitle='" & ETitle & "',ADescription='" & ADescription & "',EDescription='" & EDescription & "'," & _
                           "AAddress='" & AAddress & "',EAddress='" & EAddress & "',Telephone='" & Telephone & _
                           "',URL='" & URL & "',Email='" & Email & "',Latitude='" & Latitude & "',Longitude='" & Longitude & _
                           "',WorkingHours='" & WorkingHours & "',Likes=" & Likes & ",Memebers=" & Memebers & "," & _
                           "NumberOfWatchings='" & NumberOfWatchings & "'", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Function GetLibraryBranch(ByVal LibararyBranchID As String) As Others.LibraryBranch Implements IewsOthers.GetLibraryBranch
        Dim objLibBrch As New Others.LibraryBranch
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT * FROM LibraryBranchInfo WHERE LibararyBranchID = '" & LibararyBranchID & "'", ConfigurationManager.AppSettings("ESConnStr"))
        objLibBrch.LibararyBranchID = ds.Tables(0).Rows(0).Item(0)
        objLibBrch.ATitle = ds.Tables(0).Rows(0).Item(1)
        objLibBrch.ETitle = ds.Tables(0).Rows(0).Item(2)
        objLibBrch.ADescription = ds.Tables(0).Rows(0).Item(3)
        objLibBrch.EDescription = ds.Tables(0).Rows(0).Item(4)
        objLibBrch.AAddress = ds.Tables(0).Rows(0).Item(5)
        objLibBrch.EAddress = ds.Tables(0).Rows(0).Item(6)
        objLibBrch.Telephone = ds.Tables(0).Rows(0).Item(7)
        objLibBrch.URL = ds.Tables(0).Rows(0).Item(8)
        objLibBrch.Email = ds.Tables(0).Rows(0).Item(9)
        objLibBrch.Latitude = ds.Tables(0).Rows(0).Item(10)
        objLibBrch.Longitude = ds.Tables(0).Rows(0).Item(11)
        objLibBrch.WorkingHours = ds.Tables(0).Rows(0).Item(12)
        objLibBrch.Likes = ds.Tables(0).Rows(0).Item(13)
        objLibBrch.Memebers = ds.Tables(0).Rows(0).Item(14)
        objLibBrch.NumberOfWatchings = ds.Tables(0).Rows(0).Item(15)
        Return objLibBrch
    End Function

    Public Function GetAllLibraryBranch() As List(Of Others.LibraryBranch) Implements IewsOthers.GetAllLibraryBranch

        Dim objLibBrch As New Others.LibraryBranch
        Dim objLibBrchs As New List(Of Others.LibraryBranch)
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("SELECT * FROM LibraryBranchInfo", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objLibBrch = New Others.LibraryBranch
                objLibBrch.LibararyBranchID = ds.Tables(0).Rows(i).Item(0)
                objLibBrch.ATitle = ds.Tables(0).Rows(i).Item(1)
                objLibBrch.ETitle = ds.Tables(0).Rows(i).Item(2)
                objLibBrch.ADescription = ds.Tables(0).Rows(i).Item(3)
                objLibBrch.EDescription = ds.Tables(0).Rows(i).Item(4)
                objLibBrch.AAddress = ds.Tables(0).Rows(i).Item(5)
                objLibBrch.EAddress = ds.Tables(0).Rows(i).Item(6)
                objLibBrch.Telephone = ds.Tables(0).Rows(i).Item(7)
                objLibBrch.URL = ds.Tables(0).Rows(i).Item(8)
                objLibBrch.Email = ds.Tables(0).Rows(i).Item(9)
                objLibBrch.Latitude = ds.Tables(0).Rows(i).Item(10)
                objLibBrch.Longitude = ds.Tables(0).Rows(i).Item(11)
                objLibBrch.WorkingHours = ds.Tables(0).Rows(i).Item(12)
                objLibBrch.Likes = ds.Tables(0).Rows(i).Item(13)
                objLibBrch.Memebers = ds.Tables(0).Rows(i).Item(14)
                objLibBrch.NumberOfWatchings = ds.Tables(0).Rows(i).Item(15)
                objLibBrchs.Add(objLibBrch)
            Next
            Return objLibBrchs
        Catch ex As Exception
            objLibBrch = New Others.LibraryBranch
            objLibBrch.LibararyBranchID = ex.Message
            objLibBrchs.Add(objLibBrch)
            Return objLibBrchs
        End Try

    End Function
    '||||||||||||||||||||||||||||||||||||||||||||| Library Branch End |||||||||||||||||||||||||||||||||||||||||||||

    '||||||||||||||||||||||||||||||||||||||||||||| News Start |||||||||||||||||||||||||||||||||||||||||||||
    Public Sub InstNews(ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer) Implements IewsOthers.InstNews
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("INSERT INTO NEWS(ATitle,ETitle,ADescription,EDescription,Active) VALUES ('" & ATitle & "','" & ETitle & "','" & ADescription & _
                           "','" & EDescription & "','" & Active & "')", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Sub UptNews(ByVal ID As String, ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer) Implements IewsOthers.UptNews
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("UPDATE NEWS SET ',ATitle='" & ATitle & _
                           "',ETitle='" & ETitle & "',ADescription='" & ADescription & "',EDescription='" & EDescription & "'," & _
                           "Active='" & Active & "' WHERE ID=" & ID & "", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Function GetNews(ByVal ID As String) As Others.News Implements IewsOthers.GetNews
        Dim objNews As New Others.News
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT ID,ATitle,ETitle,ADescription,EDescription,Active FROM NEWS WHERE ID = '" & ID & "'", ConfigurationManager.AppSettings("ESConnStr"))
        objNews.ID = ds.Tables(0).Rows(0).Item(0)
        objNews.ATitle = ds.Tables(0).Rows(0).Item(1)
        objNews.ETITLE = ds.Tables(0).Rows(0).Item(2)
        objNews.ADESCRIPTION = ds.Tables(0).Rows(0).Item(3)
        objNews.EDESCRIPTION = ds.Tables(0).Rows(0).Item(4)
        objNews.ACTIVE = ds.Tables(0).Rows(0).Item(5)
        Return objNews
    End Function

    Public Function GetAllNews() As List(Of Others.News) Implements IewsOthers.GetAllNews
        Dim objNews As New Others.News
        Dim objAllNews As New List(Of Others.News)
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("SELECT ID,ATitle,ETitle,ADescription,EDescription,Active FROM NEWS", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objNews = New Others.News
                objNews.ID = ds.Tables(0).Rows(i).Item(0)
                objNews.ATitle = ds.Tables(0).Rows(i).Item(1)
                objNews.ETITLE = ds.Tables(0).Rows(i).Item(2)
                objNews.ADESCRIPTION = ds.Tables(0).Rows(i).Item(3)
                objNews.EDESCRIPTION = ds.Tables(0).Rows(i).Item(4)
                objNews.ACTIVE = ds.Tables(0).Rows(i).Item(5)
                objAllNews.Add(objNews)
            Next
            Return objAllNews
        Catch ex As Exception

        End Try
    End Function
    '||||||||||||||||||||||||||||||||||||||||||||| News End |||||||||||||||||||||||||||||||||||||||||||||

    '||||||||||||||||||||||||||||||||||||||||||||| Events Start |||||||||||||||||||||||||||||||||||||||||||||
    Public Sub InstEvents(ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer) Implements IewsOthers.InstEvents
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("INSERT INTO EVENT(ATitle,ETitle,ADescription,EDescription,Active) VALUES ('" & ATitle & "','" & ETitle & "','" & ADescription & _
                           "','" & EDescription & "','" & Active & "')", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Sub UptEvents(ByVal ID As String, ByVal ATitle As String, ByVal ADescription As String, ByVal ETitle As String, ByVal EDescription As String, ByVal Active As Integer) Implements IewsOthers.UptEvents
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("UPDATE EVENT SET ',ATitle='" & ATitle & _
                           "',ETitle='" & ETitle & "',ADescription='" & ADescription & "',EDescription='" & EDescription & "'," & _
                           "Active='" & Active & "' WHERE ID=" & ID & "", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Function GetEvents(ByVal ID As String) As Others.Events Implements IewsOthers.GetEvents
        Dim objEvents As New Others.Events
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT ID,ATitle,ETitle,ADescription,EDescription,Active FROM EVENT WHERE ID = '" & ID & "'", ConfigurationManager.AppSettings("ESConnStr"))
        objEvents.ID = ds.Tables(0).Rows(0).Item(0)
        objEvents.ATitle = ds.Tables(0).Rows(0).Item(1)
        objEvents.ETITLE = ds.Tables(0).Rows(0).Item(2)
        objEvents.ADESCRIPTION = ds.Tables(0).Rows(0).Item(3)
        objEvents.EDESCRIPTION = ds.Tables(0).Rows(0).Item(4)
        objEvents.ACTIVE = ds.Tables(0).Rows(0).Item(5)
        Return objEvents
    End Function

    Public Function GetAllEVENTs() As List(Of Others.Events) Implements IewsOthers.GetAllEvents
        Dim objEvents As New Others.Events
        Dim objAllEvents As New List(Of Others.Events)
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("SELECT ID,ATitle,ETitle,ADescription,EDescription,Active FROM EVENT", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objEvents = New Others.Events
                objEvents.ID = ds.Tables(0).Rows(i).Item(0)
                objEvents.ATitle = ds.Tables(0).Rows(i).Item(1)
                objEvents.ETITLE = ds.Tables(0).Rows(i).Item(2)
                objEvents.ADESCRIPTION = ds.Tables(0).Rows(i).Item(3)
                objEvents.EDESCRIPTION = ds.Tables(0).Rows(i).Item(4)
                objEvents.ACTIVE = ds.Tables(0).Rows(i).Item(5)
                objAllEvents.Add(objEvents)
            Next
            Return objAllEvents
        Catch ex As Exception

        End Try
    End Function
    '||||||||||||||||||||||||||||||||||||||||||||| Events End |||||||||||||||||||||||||||||||||||||||||||||

    '||||||||||||||||||||||||||||||||||||||||||||| Content Start |||||||||||||||||||||||||||||||||||||||||||||
    Public Sub InsContent(ByVal Content As String, ByVal ADescription As String, ByVal EDescription As String) Implements IewsOthers.InsContent
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("INSERT INTO CONTENT(CONTENT,ADescription,EDescription) VALUES ('" & Content & "','" & ADescription & _
                           "','" & EDescription & "')", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Sub UptContent(ByVal ID As String, ByVal Content As String, ByVal ADescription As String, ByVal EDescription As String) Implements IewsOthers.UptContent
        Dim objSql As New SQLFunction
        objSql.exeNonQuery("UPDATE CONTENT SET CONTENT='" & Content & "',ADescription='" & ADescription & _
                           "',EDescription='" & EDescription & "'  WHERE ID=" & ID & "", ConfigurationManager.AppSettings("ESConnStr"))
    End Sub

    Public Function GetContent(ByVal ID As String) As Others.Content Implements IewsOthers.GetContent
        Dim objContent As New Others.Content
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT ID,CONTENT,ADescription,EDescription FROM CONTENT WHERE ID = '" & ID & "'", ConfigurationManager.AppSettings("ESConnStr"))
        objContent.ID = ds.Tables(0).Rows(0).Item(0)
        objContent.Content = ds.Tables(0).Rows(0).Item(1)
        objContent.ADESCRIPTION = ds.Tables(0).Rows(0).Item(2)
        objContent.EDESCRIPTION = ds.Tables(0).Rows(0).Item(3)
        Return objContent
    End Function

    Public Function GetAllContent() As List(Of Others.Content) Implements IewsOthers.GetAllContent
        Dim objContent As New Others.Content
        Dim objAllContent As New List(Of Others.Content)
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("SELECT ID,Content,ADescription,EDescription FROM Content", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objContent = New Others.Content
                objContent.ID = ds.Tables(0).Rows(i).Item(0)
                objContent.Content = ds.Tables(0).Rows(i).Item(1)
                objContent.ADESCRIPTION = ds.Tables(0).Rows(i).Item(2)
                objContent.EDESCRIPTION = ds.Tables(0).Rows(i).Item(3)
                objAllContent.Add(objContent)
            Next
            Return objAllContent
        Catch ex As Exception

        End Try
    End Function

    '||||||||||||||||||||||||||||||||||||||||||||| Content End |||||||||||||||||||||||||||||||||||||||||||||

    '||||||||||||||||||||||||||||||||||||||||||||| ReserveVisit Start |||||||||||||||||||||||||||||||||||||||||||||
    Public Function InsReserveVisit(ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, _
                               ByVal DateTo As String, ByVal Status As String, ByVal AdminName As String, ByVal DateCreate As String, _
                               ByVal DateUpdate As String) As String Implements IewsOthers.InsReserveVisit
        Dim objSql As New SQLFunction
        Dim str As String = objSql.exeNonQuery("INSERT INTO ReserveVisit(User_ID,Library,ReservPurpose,DateFrom,DateTo," & _
                           "Status,AdminName,DateCreate,DateUpdate) VALUES ('" & User_ID & "','" & Library & "','" & ReservPurpose & _
                           "',to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam'),'" & Status & "','" & _
                           AdminName & "',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
        Return str
    End Function

    Public Function UptReserveVisit(ByVal ID As String, ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, _
                               ByVal DateTo As String, ByVal Status As String, ByVal AdminName As String, ByVal DateCreate As String, _
                               ByVal DateUpdate As String) As String Implements IewsOthers.UptReserveVisit
        Dim objSql As New SQLFunction
        Dim str As String = objSql.exeNonQuery("UPDATE ReserveVisit SET User_ID='" & User_ID & "',Library='" & Library & _
                           "',ReservPurpose='" & ReservPurpose & "',DateFrom=to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam'),DateTo=to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam')," & _
                           "Status='" & Status & "',AdminName='" & AdminName & "',DateCreate=to_date('" & DateCreate & _
                           "', 'dd/mm/yyyy:hh:mi:ssam'),DateUpdate=to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam') WHERE ID=" & ID & "", ConfigurationManager.AppSettings("ESConnStr"))
        Return str
    End Function

    Public Function GetReserveVisit(ByVal ID As String) As Others.ReserveVisit Implements IewsOthers.GetReserveVisit
        Dim objReserveVisit As New Others.ReserveVisit
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT ID,User_ID,Library,ReservPurpose,DateFrom,DateTo," & _
                           "Status,AdminName,DateCreate,DateUpdate FROM ReserveVisit WHERE ID = '" & ID & "'", ConfigurationManager.AppSettings("ESConnStr"))
        objReserveVisit.ID = ds.Tables(0).Rows(0).Item(0)
        objReserveVisit.User_ID = ds.Tables(0).Rows(0).Item(1)
        objReserveVisit.Library = ds.Tables(0).Rows(0).Item(2)
        objReserveVisit.ReservPurpose = ds.Tables(0).Rows(0).Item(3)
        objReserveVisit.DateFrom = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(4))
        objReserveVisit.DateTo = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(5))
        objReserveVisit.Status = ds.Tables(0).Rows(0).Item(6)
        'objReserveVisit.AdminName = If(String.IsNullOrEmpty(ds.Tables(0).Rows(0).Item(7).ToString), "", ds.Tables(0).Rows(0).Item(7))
        objReserveVisit.DateCreate = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(8))
        objReserveVisit.DateUpdate = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(9))
        Return objReserveVisit
    End Function

    Public Function GetAllReserveVisitByUserID(ByVal User_ID As String) As List(Of Others.ReserveVisit) Implements IewsOthers.GetAllReserveVisitByUserID
        Dim objReserveVisit As New Others.ReserveVisit
        Dim objAllReserveVisit As New List(Of Others.ReserveVisit)
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT ID,User_ID,Library,ReservPurpose,DateFrom,DateTo," & _
                       "Status,AdminName,DateCreate,DateUpdate FROM ReserveVisit WHERE User_ID = '" & User_ID & "' ORDER BY ID", ConfigurationManager.AppSettings("ESConnStr"))
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            objReserveVisit = New Others.ReserveVisit
            objReserveVisit.ID = ds.Tables(0).Rows(i).Item(0)
            objReserveVisit.User_ID = ds.Tables(0).Rows(i).Item(1)
            objReserveVisit.Library = ds.Tables(0).Rows(i).Item(2)
            objReserveVisit.ReservPurpose = ds.Tables(0).Rows(i).Item(3)
            objReserveVisit.DateFrom = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(4))
            objReserveVisit.DateTo = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(5))
            objReserveVisit.Status = ds.Tables(0).Rows(i).Item(6)
            'objReserveVisit.AdminName = If(ds.Tables(0).Rows(i).Item(7).ToString Is Nothing, "", ds.Tables(0).Rows(i).Item(7))
            objReserveVisit.DateCreate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(8))
            objReserveVisit.DateUpdate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(9))
            objAllReserveVisit.Add(objReserveVisit)
        Next
        Return objAllReserveVisit
    End Function

    Public Function GetAllReserveVisit() As List(Of Others.ReserveVisit) Implements IewsOthers.GetAllReserveVisit
        Try
            Dim objReserveVisit As New Others.ReserveVisit
            Dim objAllReserveVisit As New List(Of Others.ReserveVisit)
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("SELECT ID,User_ID,Library,ReservPurpose,DateFrom,DateTo," & _
                           "Status,AdminName,DateCreate,DateUpdate FROM ReserveVisit WHERE STATUS <> '2' AND STATUS <> '3' ORDER BY ID", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objReserveVisit = New Others.ReserveVisit
                objReserveVisit.ID = ds.Tables(0).Rows(i).Item(0)
                objReserveVisit.User_ID = ds.Tables(0).Rows(i).Item(1)
                objReserveVisit.Library = ds.Tables(0).Rows(i).Item(2)
                objReserveVisit.ReservPurpose = ds.Tables(0).Rows(i).Item(3)
                objReserveVisit.DateFrom = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(4))
                objReserveVisit.DateTo = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(5))
                objReserveVisit.Status = ds.Tables(0).Rows(i).Item(6)
                objReserveVisit.AdminName = If(String.IsNullOrEmpty(ds.Tables(0).Rows(i).Item(7).ToString), "", ds.Tables(0).Rows(i).Item(7))
                'objReserveVisit.DateCreate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(8))
                objReserveVisit.DateUpdate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(9))
                objAllReserveVisit.Add(objReserveVisit)
            Next
            Return objAllReserveVisit
        Catch ex As Exception
            Dim objReserveVisit As New Others.ReserveVisit
            Dim objAllReserveVisit As New List(Of Others.ReserveVisit)
            objReserveVisit.User_ID = ex.Message
            objAllReserveVisit.Add(objReserveVisit)
            Return objAllReserveVisit
        End Try

    End Function

    Public Function ReserveVisitAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String) As Boolean Implements IewsOthers.ReserveVisitAva
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet(" SELECT ID FROM RESERVEVISIT WHERE LIBRARY='" & Library & "'  AND STATUS <> '3' AND ( " & _
                                         " DATEFROM <= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') OR " & _
                                         " DATEFROM <= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam')) ", ConfigurationManager.AppSettings("ESConnStr"))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function UpdateVisitAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String, ByVal ID As String) As Boolean Implements IewsOthers.UpdateVisitAva
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet(" SELECT ID FROM RESERVEVISIT WHERE LIBRARY='" & Library & "' AND ID <> " & ID & " AND " & _
                                         " DATEFROM <= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') OR " & _
                                         " DATEFROM <= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam') AND LIBRARY='" & Library & "' AND ID <> " & ID & " AND STATUS <> '3' ", ConfigurationManager.AppSettings("ESConnStr"))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function
    '||||||||||||||||||||||||||||||||||||||||||||| ReserveVisit End |||||||||||||||||||||||||||||||||||||||||||||

    '||||||||||||||||||||||||||||||||||||||||||||| ReserveHall Start |||||||||||||||||||||||||||||||||||||||||||||
    Public Function InsReserveHall(ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, _
                                   ByVal DateTo As String, ByVal LectureType As String, ByVal AttendanceNo As String, ByVal Devices As String, _
                                   ByVal InstructorName As String, ByVal InstructorPassport As String, ByVal InstructorPpNo As String, ByVal Status As String, ByVal AdminName As String, _
                                   ByVal DateCreate As String, ByVal DateUpdate As String) As String Implements IewsOthers.InsReserveHall
        Dim objSql As New SQLFunction
        Dim str As String = objSql.exeNonQuery("INSERT INTO ReserveHall(User_ID,Library,ReservPurpose,DateFrom,DateTo," & _
                           "LectureType,AttendanceNo,Devices,InstructorName,INSTRUCTORPASSPORT,InstructorPpNo,Status,AdminName,DateCreate," & _
                           "DateUpdate) VALUES ('" & User_ID & "','" & Library & "','" & ReservPurpose & _
                           "',to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam'),'" & LectureType & "','" & AttendanceNo & "','" & Devices & "','" & InstructorName & "','" & InstructorPassport & "','" & InstructorPpNo & "','" & Status & "','" & _
                           AdminName & "',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
        Return str
    End Function

    Public Function UptReserveHall(ByVal ID As String, ByVal User_ID As String, ByVal Library As String, ByVal ReservPurpose As String, ByVal DateFrom As String, _
                                   ByVal DateTo As String, ByVal LectureType As String, ByVal AttendanceNo As String, ByVal Devices As String, _
                                   ByVal InstructorName As String, ByVal InstructorPassport As String, ByVal InstructorPpNo As String, ByVal Status As String, ByVal AdminName As String, _
                                   ByVal DateCreate As String, ByVal DateUpdate As String) As String Implements IewsOthers.UptReserveHall
        Dim objSql As New SQLFunction
        Dim str As String = objSql.exeNonQuery("UPDATE ReserveHall SET User_ID='" & User_ID & "',Library='" & Library & _
                           "',ReservPurpose='" & ReservPurpose & "',DateFrom=to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam'),DateTo=to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam')," & _
                           "LectureType='" & LectureType & "',AttendanceNo='" & AttendanceNo & "',Devices='" & Devices & "',InstructorName='" & InstructorName & "',InstructorPassport='" & InstructorPassport & "',InstructorPpNo='" & InstructorPpNo & "',Status='" & Status & "',AdminName='" & AdminName & "',DateCreate=to_date('" & DateCreate & _
                           "', 'dd/mm/yyyy:hh:mi:ssam'),DateUpdate=to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam') WHERE ID=" & ID & "", ConfigurationManager.AppSettings("ESConnStr"))
        Return str
    End Function

    Public Function GetReserveHall(ByVal ID As String) As Others.ReserveHall Implements IewsOthers.GetReserveHall
        Dim objReserveHall As New Others.ReserveHall
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT ID,User_ID,Library,ReservPurpose,DateFrom,DateTo," & _
                           "LectureType,AttendanceNo,Devices,InstructorName,InstructorPassport,Status," & _
                           "AdminName,DateCreate,DateUpdate FROM ReserveHall WHERE ID = '" & ID & "'", ConfigurationManager.AppSettings("ESConnStr"))
        objReserveHall.ID = ds.Tables(0).Rows(0).Item(0)
        objReserveHall.User_ID = ds.Tables(0).Rows(0).Item(1)
        objReserveHall.Library = ds.Tables(0).Rows(0).Item(2)
        objReserveHall.ReservPurpose = ds.Tables(0).Rows(0).Item(3)
        objReserveHall.DateFrom = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(4))
        objReserveHall.DateTo = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(5))
        objReserveHall.LectureType = ds.Tables(0).Rows(0).Item(6)
        objReserveHall.AttendanceNo = ds.Tables(0).Rows(0).Item(7)
        objReserveHall.Devices = ds.Tables(0).Rows(0).Item(8)
        objReserveHall.InstructorName = ds.Tables(0).Rows(0).Item(9)
        objReserveHall.InstructorPassport = ds.Tables(0).Rows(0).Item(10)
        objReserveHall.InstructorPpNo = ds.Tables(0).Rows(0).Item(11)
        objReserveHall.Status = ds.Tables(0).Rows(0).Item(12)
        objReserveHall.DateCreate = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(14))
        objReserveHall.DateUpdate = Convert.ToDateTime(ds.Tables(0).Rows(0).Item(15))
        Return objReserveHall
    End Function

    Public Function GetAllReserveHallByUserID(ByVal User_ID As String) As List(Of Others.ReserveHall) Implements IewsOthers.GetAllReserveHallByUserID
        Dim objReserveHall As New Others.ReserveHall
        Dim objAllReserveHall As New List(Of Others.ReserveHall)
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("SELECT ID,User_ID,Library,ReservPurpose,DateFrom,DateTo, " & _
                               " LectureType,AttendanceNo,Devices,InstructorName,InstructorPassport,InstructorPpNo,Status, " & _
                               " AdminName,DateCreate,DateUpdate FROM ReserveHall WHERE User_ID = '" & User_ID & "'", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objReserveHall = New Others.ReserveHall
                objReserveHall.ID = ds.Tables(0).Rows(i).Item(0)
                objReserveHall.User_ID = ds.Tables(0).Rows(i).Item(1)
                objReserveHall.Library = ds.Tables(0).Rows(i).Item(2)
                objReserveHall.ReservPurpose = ds.Tables(0).Rows(i).Item(3)
                objReserveHall.DateFrom = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(4))
                objReserveHall.DateTo = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(5))
                objReserveHall.LectureType = ds.Tables(0).Rows(i).Item(6)
                objReserveHall.AttendanceNo = ds.Tables(0).Rows(i).Item(7)
                objReserveHall.Devices = ds.Tables(0).Rows(i).Item(8)
                objReserveHall.InstructorName = ds.Tables(0).Rows(i).Item(9)
                objReserveHall.InstructorPassport = ds.Tables(0).Rows(i).Item(10)
                objReserveHall.InstructorPpNo = ds.Tables(0).Rows(i).Item(11)
                objReserveHall.Status = ds.Tables(0).Rows(i).Item(12)
                objReserveHall.DateCreate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(14))
                objReserveHall.DateUpdate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(15))
                objAllReserveHall.Add(objReserveHall)
            Next
            Return objAllReserveHall
        Catch ex As Exception
            objReserveHall.User_ID = ex.Message
            objAllReserveHall.Add(objReserveHall)
            Return objAllReserveHall
        End Try
    End Function

    Public Function GetAllReserveHall() As List(Of Others.ReserveHall) Implements IewsOthers.GetAllReserveHall
        Try
            Dim objReserveHall As New Others.ReserveHall
            Dim objAllReserveHall As New List(Of Others.ReserveHall)
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("SELECT ID,User_ID,Library,ReservPurpose,DateFrom,DateTo," & _
                           "LectureType,AttendanceNo,Devices,InstructorName,InstructorPassport,INSTRUCTORPPNO,Status," & _
                           "AdminName,DateCreate,DateUpdate FROM ReserveHall WHERE STATUS <> '2' AND STATUS <> '3' ORDER BY ID ", ConfigurationManager.AppSettings("ESConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objReserveHall = New Others.ReserveHall
                objReserveHall.ID = ds.Tables(0).Rows(i).Item(0)
                objReserveHall.User_ID = ds.Tables(0).Rows(i).Item(1)
                objReserveHall.Library = ds.Tables(0).Rows(i).Item(2)
                objReserveHall.ReservPurpose = ds.Tables(0).Rows(i).Item(3)
                objReserveHall.DateFrom = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(4))
                objReserveHall.DateTo = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(5))
                objReserveHall.LectureType = ds.Tables(0).Rows(i).Item(6)
                objReserveHall.AttendanceNo = ds.Tables(0).Rows(i).Item(7)
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(8)) Then
                    objReserveHall.Devices = ds.Tables(0).Rows(i).Item(8)
                Else
                    objReserveHall.Devices = ""
                End If
                objReserveHall.InstructorName = ds.Tables(0).Rows(i).Item(9)
                objReserveHall.InstructorPassport = ds.Tables(0).Rows(i).Item(10)
                objReserveHall.InstructorPpNo = ds.Tables(0).Rows(i).Item(11)
                objReserveHall.Status = ds.Tables(0).Rows(i).Item(12)
                objReserveHall.DateCreate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(14))
                objReserveHall.DateUpdate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item(15))
                objAllReserveHall.Add(objReserveHall)
            Next
            Return objAllReserveHall
        Catch ex As Exception
            Dim objReserveHall As New Others.ReserveHall
            Dim objAllReserveHall As New List(Of Others.ReserveHall)
            objReserveHall.User_ID = ex.Message
            objAllReserveHall.Add(objReserveHall)
            Return objAllReserveHall
        End Try

    End Function

    Public Function ReserveHallAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String) As Boolean Implements IewsOthers.ReserveHallAva
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet(" SELECT ID FROM ReserveHall WHERE LIBRARY='" & Library & "'  AND STATUS <> '3' AND ( " & _
                                         " DATEFROM <= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') OR " & _
                                         " DATEFROM <= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam')) ", ConfigurationManager.AppSettings("ESConnStr"))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function UpdateHallAva(ByVal DateFrom As String, ByVal DateTo As String, ByVal Library As String, ByVal ID As String) As Boolean Implements IewsOthers.UpdateHallAva
        Try
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet(" SELECT ID FROM ReserveHall WHERE LIBRARY='" & Library & "' AND ID <> " & ID & " AND " & _
                                         " DATEFROM <= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateFrom & "', 'dd/mm/yyyy:hh:mi:ssam') OR " & _
                                         " DATEFROM <= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam') " & _
                                         " AND DATETO >= to_date('" & DateTo & "', 'dd/mm/yyyy:hh:mi:ssam') AND LIBRARY='" & Library & "' AND ID <> " & ID & " AND STATUS <> '3' ", ConfigurationManager.AppSettings("ESConnStr"))
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function
    '||||||||||||||||||||||||||||||||||||||||||||| ReserveHall End |||||||||||||||||||||||||||||||||||||||||||||

    '||||||||||||||||||||||||||||||||||||||||||||| HallDevices Start |||||||||||||||||||||||||||||||||||||||||||||
    Public Function InsHallDevices(ByVal AraDesc As String, ByVal EngDesc As String) As String Implements IewsOthers.InsHallDevices
        Dim objSql As New SQLFunction
        Dim str As String = objSql.exeNonQuery("INSERT INTO HallDevices(AraDesc,EngDesc) VALUES ('" & AraDesc & "','" & EngDesc & "')", ConfigurationManager.AppSettings("ESConnStr"))
        Return str
    End Function

    Public Function UptHallDevices(ByVal ID As String, ByVal AraDesc As String, ByVal EngDesc As String) As String Implements IewsOthers.UptHallDevices
        Dim objSql As New SQLFunction
        Dim str As String = objSql.exeNonQuery("UPDATE HallDevices SET AraDesc='" & AraDesc & "',EngDesc='" & EngDesc & " WHERE ID = " & ID, ConfigurationManager.AppSettings("ESConnStr"))
        Return str
    End Function

    Public Function GetAllHallDevices() As DataTable Implements IewsOthers.GetAllHallDevices
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT ID,AraDesc,EngDesc FROM HallDevices", ConfigurationManager.AppSettings("ESConnStr"))
        Dim dt As New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt
    End Function
    '||||||||||||||||||||||||||||||||||||||||||||| HallDevices End |||||||||||||||||||||||||||||||||||||||||||||

    Public Function LookUp(ByVal CATNUM As String, ByVal Lang As String) As List(Of Others.DDLList) Implements IewsOthers.LookUp
        Try
            Dim objSQL As New SQLFunction
            Dim dt As New DataTable
            dt = objSQL.fillDT("select Policy_name,f0 from policy where policy_type='" & CATNUM.Trim & "'", CommandType.Text, ConfigurationManager.AppSettings("SymConnStr"))
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)

            For i As Integer = 0 To dt.Rows.Count - 1
                objDDLIST = New Others.DDLList
                If Lang = "Ara" Then
                    objDDLIST.Text = dt.Rows(i).Item(1)
                Else
                    objDDLIST.Text = dt.Rows(i).Item(0)
                End If
                objDDLIST.Value = dt.Rows(i).Item(0)
                objAllDDlist.Add(objDDLIST)
            Next
            Return objAllDDlist
        Catch ex As Exception
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)
            objDDLIST.Value = "123"
            objDDLIST.Text = ex.Message
            objAllDDlist.Add(objDDLIST)
            Return objAllDDlist
        End Try

    End Function

    Dim Common_Obj As New Common
    Dim FileName_Image As String = ""
    Dim FileName As String = ""

    Public Function ImageUpload(ByVal User_ID As String, ByVal Type As String, ByVal Ext As String, ByVal Image_Byte As Byte()) As Responce_Msg Implements IewsOthers.ImageUpload
        Try

            Dim Res As Responce_Msg = New Responce_Msg

            If Type = "1" Then
                FileName = User_ID & Ext
            ElseIf Type = "2" Then
                FileName = User_ID & "_NATID1_" & Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & Ext
            ElseIf Type = "22" Then
                FileName = User_ID & "_NATID2_" & Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & Ext
            ElseIf Type = "3" Then
                FileName = User_ID & "_SNEEDS_" & Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & Ext
            ElseIf Type = "4" Then
                FileName = User_ID & "_PASSPORT_" & Now.Year & Now.Month & Now.Day & "_" & Now.Hour & Now.Minute & Now.Second & Ext
            End If

            Dim IsFileName_Image As Boolean = False
            If Image_Byte.Length <> 0 Then ' NATID Byte is OK
                IsFileName_Image = True
            End If

            If Common_Obj.Fields_Validation(Ext) = False Then '  Ext is Empty
                Res.Msg = "Ext Field is Empty"
                Res.Code = ""
                Return Res
            ElseIf Common_Obj.Fields_Validation(Ext) = True Then '  Ext is Not Empty
                If Common_Obj.ImgExt_Validation(Ext) = False Then '  Ext is Not Valid
                    Res.Msg = "Invalid Ext Type"
                    Res.Code = ""
                    Return Res
                End If
            End If

            Dim image As System.Drawing.Image = System.Drawing.Image.FromStream(New System.IO.MemoryStream(Image_Byte))
            Dim sz As New Size
            sz.Height = 540
            sz.Width = 350
            Dim newImage As Image = ResizeImage(image, sz)
            Dim imgCon As New ImageConverter()
            Dim imgByte As Byte()

            If Type = "1" Then
                imgByte = DirectCast(imgCon.ConvertTo(newImage, GetType(Byte())), Byte())
            Else
                imgByte = Image_Byte
            End If

            If FileName <> "" Then
                IsFileName_Image = False
                Dim ms As IO.MemoryStream = New IO.MemoryStream(imgByte)
                FileName_Image = FileName
                Dim fs As IO.FileStream = Nothing
                If Type = "1" Then
                    fs = New IO.FileStream(ConfigurationManager.AppSettings("Photo_Path_P") + FileName_Image, FileMode.Create)
                ElseIf Type = "2" Or Type = "22" Or Type = "3" Or Type = "4" Then
                    fs = New IO.FileStream(ConfigurationManager.AppSettings("Files_Path_P") + FileName_Image, FileMode.Create)
                Else
                    Res.Msg = "Type is wrong"
                    Res.Code = ""
                    Return Res
                End If

                ms.WriteTo(fs)
                ms.Close()
                fs.Close()
                fs.Dispose()
            Else
                Res.Msg = "FileName Empty"
                Res.Code = ""
                Return Res
            End If


            Res.Msg = ""
            If Type = "1" Then
                Res.Code = ConfigurationManager.AppSettings("Photo_Path") & FileName
            Else
                Res.Code = ConfigurationManager.AppSettings("Files_Path") & FileName
            End If
            Return Res


            '//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            ' Res.Msg = Resources.Messages.ResourceManager.GetString("Msg_Success", Culture_Used)
            Common_Obj.Logs(Date.Now, "File : " & FileName & " : FileUpload Error : End of File.")
            Res.Msg = "Error : End of File."
            Res.Code = ""
            Return Res
        Catch ex As Exception
            If FileName_Image <> "" Then
                '    Common_Obj.DeleteFiles(ConfigurationManager.AppSettings("Files_Path").ToUpper, FileName_Image)
            End If

            Common_Obj.Logs(Date.Now, "File : " & FileName & " : FileUpload Error : End of File.")
            Dim Res As Responce_Msg = New Responce_Msg
            Res.Msg = ex.Message & " - Error Line : " & ex.StackTrace()
            Res.Code = ""
            Return Res

        End Try

    End Function

    Public Function ResizeImage(ByVal image As Image, ByVal size As Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image
        Dim newWidth As Integer
        Dim newHeight As Integer
        If preserveAspectRatio Then
            Dim originalWidth As Integer = image.Width
            Dim originalHeight As Integer = image.Height
            Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
            Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
            Dim percent As Single
            If percentHeight < percentWidth Then
                percent = percentHeight
            Else
                percent = percentWidth
            End If
            newWidth = CInt(originalWidth * percent)
            newHeight = CInt(originalHeight * percent)
        Else
            newWidth = size.Width
            newHeight = size.Height
        End If
        Dim newImage As Image = New Bitmap(newWidth, newHeight)
        Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
            graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
            graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
        End Using
        Return newImage
    End Function

    Public Function imageUrlToBase64(ByVal URL As String) As String Implements IewsOthers.imageUrlToBase64
        Dim FileName() As String = URL.Split("/")
        Dim FilePath As String = ConfigurationManager.AppSettings("Files_Path_P") + FileName(FileName.Length - 1)
        Using image1 As Image = Image.FromFile(FilePath)
            Using m As New MemoryStream()
                image1.Save(m, image1.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                ' Convert byte[] to Base64 String
                Dim base64String As String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Public Function InstUserPayment(ByVal User_ID As String, ByVal TP_BranchId As String, ByVal TP_Merchant As String, ByVal TP_RefNo As String, _
                                  ByVal TP_Language As String, ByVal TP_ServiceInfo As String, ByVal TP_PayerName As String, _
                                  ByVal TP_ReturnURL As String, ByVal BillsNumber As String, ByVal BillsAmount As String, ByVal wpStatus As String, ByVal wsStatus As String, ByVal DateCreate As String, _
                                  ByVal DateUpdate As String) As String Implements IewsOthers.InstUserPayment
        Try
            Dim objSql As New SQLFunction
            Dim Str As String = objSql.exeNonQuery("INSERT INTO UserPaymentRequest(User_ID,TP_BranchId,TP_Merchant,TP_RefNo,TP_Language,TP_ServiceInfo," & _
                               "TP_PayerName,TP_ReturnURL,BillsNumber,BillsAmount,wpStatus,wsStatus,DateCreate,DateUpdate) VALUES ('" & User_ID & "','" & TP_BranchId & "','" & TP_Merchant & _
                               "','" & TP_RefNo & "','" & TP_Language & "','" & TP_ServiceInfo & "','" & TP_PayerName & "','" & TP_ReturnURL & "','" & BillsNumber & "','" & BillsAmount & "','" & wpStatus & _
                               "','" & wsStatus & "',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
            Return Str
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    Public Function UptUserPaymentWP(ByVal ID As String, ByVal Status As String, ByVal DateUpdate As String) As String Implements IewsOthers.UptUserPaymentWP
        Try
            Dim objSql As New SQLFunction
            Dim Str As String = objSql.exeNonQuery("UPDATE UserPaymentRequest SET wpStatus='" & Status & "',DateUpdate=to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam') WHERE ID=" & ID & "", ConfigurationManager.AppSettings("ESConnStr"))
            Return Str
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function UptUserPaymentWS(ByVal ID As String, ByVal Status As String, ByVal DateUpdate As String) As String Implements IewsOthers.UptUserPaymentWS
        Try
            Dim objSql As New SQLFunction
            Dim Str As String = objSql.exeNonQuery("UPDATE UserPaymentRequest SET wsStatus='" & Status & "',DateUpdate=to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam') WHERE ID=" & ID & "", ConfigurationManager.AppSettings("ESConnStr"))
            Return Str
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function GetUserPayments(ByVal TP_RefNo As String) As DataSet Implements IewsOthers.GetUserPayments
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT * FROM UserPaymentRequest WHERE TP_RefNo = '" & TP_RefNo & "'", ConfigurationManager.AppSettings("ESConnStr"))
        Return ds
    End Function

    Public Function GetUserBillReason(ByVal User_ID As String, ByVal Bill_Number As String) As String Implements IewsOthers.GetUserBillReason
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        Return objSql.Get_Info_string("select policy_name from policy where policy_number =(select reason from bill where USER_KEY= '" & User_ID & "' and bill_number='" & Bill_Number & "') and policy_type = 'BRSN'", ConfigurationManager.AppSettings("SymConnStr"))
        'ds.Tables(0).Rows(0).Item(0)
    End Function

    Public Function GetUserPaymentsRequests(ByVal User_ID As String) As DataSet Implements IewsOthers.GetUserPaymentsRequests
        Dim objLibBrch As New Others.LibraryBranch
        Dim objSql As New SQLFunction
        Dim ds As New DataSet
        ds = objSql.Get_Info_DataSet("SELECT * FROM UserPaymentRequest WHERE USER_ID = '" & User_ID & "'", ConfigurationManager.AppSettings("ESConnStr"))
        Return ds
    End Function

    Public Function InstUserPaymentRes(ByVal User_ID As String, ByVal DepAmount As String, ByVal TP_Amount As String, _
                                  ByVal TP_ExtraFees As String, ByVal TP_PaymentDate As String, ByVal TP_PayMethod As String, _
                                  ByVal TP_ReceiptNo As String, ByVal TP_RefNo As String, ByVal TP_ResultCode As String, ByVal TP_SecHash As String, ByVal DateCreate As String, _
                                  ByVal DateUpdate As String) As String Implements IewsOthers.InstUserPaymentRes
        Try
            Dim objSql As New SQLFunction
            Dim Str As String = objSql.exeNonQuery("INSERT INTO UserPaymentWPResponse(User_ID,DepAmount,TP_Amount,TP_ExtraFees,TP_PaymentDate," & _
                               "TP_PayMethod,TP_ReceiptNo,TP_RefNo,TP_ResultCode,TP_SecHash,DateCreate,DateUpdate) VALUES ('" & User_ID & "','" & DepAmount & _
                               "','" & TP_Amount & "','" & TP_ExtraFees & "',to_date('" & TP_PaymentDate & "', 'dd/mm/yyyy:hh:mi:ssam'),'" & TP_PayMethod & "','" & _
                               TP_ReceiptNo & "','" & TP_RefNo & "','" & TP_ResultCode & _
                               "','" & TP_SecHash & "',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
            Return Str
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    Public Function InstUserTahseelResponse(ByVal User_ID As String, ByVal MemberType As String, ByVal ResultCode As String, ByVal ResultDescription As String, ByVal TahseelCardNumber As String, ByVal DateCreate As String, ByVal DateUpdate As String) As String Implements IewsOthers.InstUserTahseelResponse
        Try
            Dim objSql As New SQLFunction
            Dim Str As String = objSql.exeNonQuery("INSERT INTO TahseelCardResponse(User_ID,MemberType,ResultCode,ResultDescription,TahseelCardNumber,DateCreate,DateUpdate) VALUES ('" & _
                                                   User_ID & "','" & MemberType & "','" & ResultCode & "','" & ResultDescription & "','" & TahseelCardNumber & _
                                                   "',to_date('" & DateCreate & "', 'dd/mm/yyyy:hh:mi:ssam'),to_date('" & DateUpdate & "', 'dd/mm/yyyy:hh:mi:ssam'))", ConfigurationManager.AppSettings("ESConnStr"))
            Return Str
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    Public Function GetLibraryStaff() As List(Of LibraryStaff) Implements IewsOthers.GetLibraryStaff
        Try
            Dim objLibraryStaff As New Others.LibraryStaff
            Dim objAllLibraryStaff As New List(Of Others.LibraryStaff)
            Dim objSql As New SQLFunction
            Dim ds As New DataSet
            ds = objSql.Get_Info_DataSet("select ID,name_key " & _
                                        " ,(select policy_name from policy where policy_number = users.library and policy_type='LIBR') as Library " & _
                                        " from users where profile not in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,30,62,63,64,65,66,67) ", ConfigurationManager.AppSettings("SymConnStr"))
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                objLibraryStaff = New Others.LibraryStaff
                objLibraryStaff.User_ID = ds.Tables(0).Rows(i).Item(0)
                objLibraryStaff.Name = ds.Tables(0).Rows(i).Item(1)
                objLibraryStaff.Library = ds.Tables(0).Rows(i).Item(2)
                objAllLibraryStaff.Add(objLibraryStaff)
            Next
            Return objAllLibraryStaff
        Catch ex As Exception
            Dim objLibraryStaff As New Others.LibraryStaff
            Dim objAllLibraryStaff As New List(Of Others.LibraryStaff)
            objLibraryStaff.User_ID = ex.Message
            objAllLibraryStaff.Add(objLibraryStaff)
            Return objAllLibraryStaff
        End Try
    End Function

End Class
