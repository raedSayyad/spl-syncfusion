﻿Public Class wfrmActivateUser
    Inherits System.Web.UI.Page

    Public Email As String
    Public ActivationCode As String
    Public Lang As String
    Public rc As System.Resources.ResourceManager

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Lang = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            rc = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))

            'Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
            Email = Request.QueryString("Email")
            ActivationCode = Request.QueryString("ActivationCode")
            'Dim GetActivation As String = objEwsUserClient.GetUserActivate(Email, ActivationCode)
            'If GetActivation = "1" Then
            '    Dim UpdActivation As String = objEwsUserClient.UptUserActivate(Email, ActivationCode)
            '    If UpdActivation = "1" Then
            '        lblError.Text = rc.GetString("Activated", Culture_Used)
            '        System.Threading.Thread.Sleep(10000)
            '        Response.Write("<script>window.open(' http://shjlib.gov.ae:8083/uhtbin/cgisirsi.exe/X/0/0/1/42/X ','_parent');</script>")
            '        System.Threading.Thread.Sleep(10000)
            '    Else
            '        lblError.Text = rc.GetString("SystemError", Culture_Used)
            '    End If
            'Else
            '    lblError.Text = rc.GetString("SystemError", Culture_Used)
            'End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub wfrmActivateUser_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Try
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
            Else
            End If
            rc = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
            'Email = Request.QueryString("Email")
            'ActivationCode = Request.QueryString("ActivationCode")
            Dim GetActivation As String = objEwsUserClient.GetUserActivate(Email, ActivationCode)
            If GetActivation = "1" Then
                Dim UpdActivation As String = objEwsUserClient.UptUserActivate(Email, ActivationCode)
                If UpdActivation = "1" Then
                    lblError.Text = rc.GetString("Activated", Culture_Used)
                    System.Threading.Thread.Sleep(10000)
                    'Response.Write("<script>window.open(' http://shjlib.gov.ae:8083/uhtbin/cgisirsi.exe/X/0/0/1/42/X ','_parent');</script>")
                    Response.Redirect("https://catalog.shjlib.gov.ae/uhtbin/cgisirsi.exe/X/0/0/1/42/X")
                    System.Threading.Thread.Sleep(10000)
                Else
                    lblError.Text = rc.GetString("SystemError", Culture_Used)
                End If
            Else
                lblError.Text = rc.GetString("SystemError", Culture_Used)
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class