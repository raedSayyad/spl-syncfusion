﻿Imports System.IO
Imports System.ServiceProcess

Public Class WindowsOperation
    Public Function CopyFolder(ByVal dirFrom As String, ByVal dirTo As String)
        Dim filePaths As String() = System.IO.Directory.GetFiles(dirFrom)
        If Not Directory.Exists(dirTo) Then
            Directory.CreateDirectory(dirTo)
        End If
        For Each fileName As [String] In filePaths
            Dim targetFolder As String = dirTo
            Dim fi As New System.IO.FileInfo(fileName)
            fi.CopyTo(System.IO.Path.Combine(targetFolder, fi.Name), True)
        Next
    End Function

    Public Function Process(ByVal WrkDir As String, ByVal wLine As String) As String
        Dim pProcess As System.Diagnostics.Process = New System.Diagnostics.Process()
        pProcess.StartInfo.FileName = "cmd.exe"
        pProcess.StartInfo.WorkingDirectory = WrkDir
        pProcess.StartInfo.UseShellExecute = False
        pProcess.StartInfo.RedirectStandardOutput = True
        pProcess.StartInfo.RedirectStandardInput = True
        pProcess.StartInfo.RedirectStandardError = True
        pProcess.StartInfo.CreateNoWindow = True
        pProcess.StartInfo.Verb = "runas"
        pProcess.Start()
        pProcess.StandardInput.AutoFlush = True
        Dim MyStreamWriter As StreamWriter = pProcess.StandardInput
        MyStreamWriter.WriteLine(wLine)
        pProcess.StandardInput.Close()
        Return pProcess.StandardOutput.ReadToEnd() + "|||" + pProcess.StandardError.ReadToEnd()
    End Function

    Public Function StartWinService(ByVal ServiceName As String, ByVal wiating As Integer) As String
        Dim servic As ServiceController = New ServiceController(ServiceName)
        If servic.Status = ServiceControllerStatus.Stopped Then
            servic.Start()
            servic.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(wiating))
            Dim wiatingSec As Integer = wiating * 1000
            System.Threading.Thread.Sleep(wiatingSec)
            Return servic.Status.ToString
        End If
    End Function

    Public Function StopWinService(ByVal ServiceName As String, ByVal wiating As Integer) As String
        Dim servic As ServiceController = New ServiceController(ServiceName)
        If servic.Status = ServiceControllerStatus.Running Then
            servic.Stop()
            servic.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(wiating))
            Dim wiatingSec As Integer = wiating * 1000
            System.Threading.Thread.Sleep(wiatingSec)
            Return servic.Status.ToString
        End If
    End Function

    Public Function SaveTextToFile(ByVal FullPath As String, ByVal new_line As String) As Boolean
        Dim bAns As Boolean = False
        Dim objReader As StreamWriter
        Try
            objReader = New StreamWriter(FullPath.Trim, True, System.Text.Encoding.UTF8)
            objReader.Write(new_line & ControlChars.NewLine)
            objReader.Close()
            bAns = True
        Catch Ex As Exception

        End Try
        Return bAns
    End Function
End Class
