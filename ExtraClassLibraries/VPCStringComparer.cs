﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Web;
using System.Security.Cryptography;

namespace ExtraClassLibraries
{

    public class VPCStringComparer : IComparer
    {


        public int Compare(Object a, Object b)
        {
            if (a == b) return 0;
            if (a == null) return -1;
            if (b == null) return 1;
            string sa = a as string;
            string sb = b as string;
            System.Globalization.CompareInfo myComparer = System.Globalization.CompareInfo.GetCompareInfo("en-US");
            if (sa != null && sb != null)
            {
                return myComparer.Compare(sa, sb, System.Globalization.CompareOptions.Ordinal);
            }
            throw new ArgumentException("a and b should be strings.");
        }

        //#region IComparer Members

        //int IComparer.Compare(object x, object y)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //#endregion
    }
}
