﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmApproval.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../Scripts/jsondata.min.js"></script>
    <script id="tabGridContents" type="text/x-jsrender">
        <div class="tabcontrol">
            <ul>
                <li><a href="#Details{{:ID}}">User Details</a>
                </li>
                <li><a href="#NatID{{:ID}}">National ID</a>
                </li>
                <li><a href="#SNeed{{:ID}}">Special Need</a>
                </li>
            </ul>
            <div id="Details{{:ID}}">
                <p>{{:ID}}</p>
            </div>
            <div id="NatID{{:ID}}"></div>
            <div id="SNeed{{:ID}}"></div>
        </div>
    </script>
    <script type="text/javascript">
        function detailGridData(e) {
            e.detailsElement.find(".tabcontrol").ejTab({ selectedItemIndex: 0 });
        }
    </script>
    <asp:Panel ID="Panel1" runat="server">
        <h5>Individual Requests</h5>
        <ej:Accordion ID="Accordion1" runat="server" SelectedItemIndex="-1" Collapsible="true">
            <Items>
                <ej:AccordionItem Text="User Upgrade Request">
                    <ContentSection>
                        <iframe id="UserUpgrade" src="wfrmUserUpgReq.aspx" width="100%" height="930px"></iframe>
                    </ContentSection>
                </ej:AccordionItem>
                <ej:AccordionItem Text="User Renewal Request">
                    <ContentSection>
                        <iframe id="UserRenewal" src="wfrmUserRenReq.aspx" width="100%" height="730px"></iframe>
                    </ContentSection>
                </ej:AccordionItem>
                <ej:AccordionItem Text="User Cancel Request">
                    <ContentSection>
                        <iframe id="UserCancel" src="wfrmUserCancelReq.aspx" width="100%" height="730px"></iframe>
                    </ContentSection>
                </ej:AccordionItem>
            </Items>
        </ej:Accordion>
        <hr />
    </asp:Panel>
    
    <asp:Panel ID="Panel2" runat="server">
        <h5>Company Requests</h5>
        <ej:Accordion ID="Accordion2" runat="server" SelectedItemIndex="-1" Collapsible="true">
            <Items>
                <ej:AccordionItem Text="Upgrade Requests">
                    <ContentSection>
                    </ContentSection>
                </ej:AccordionItem>
                <ej:AccordionItem Text="Renewal Requests">
                    <ContentSection>
                    </ContentSection>
                </ej:AccordionItem>
                <ej:AccordionItem Text="Cancelation Requests">
                    <ContentSection>
                    </ContentSection>
                </ej:AccordionItem>
            </Items>
        </ej:Accordion>
        <hr />
    </asp:Panel>
    
    <asp:Panel ID="Panel3" runat="server">
        <h5>Hall & Visit Requests</h5>
        <ej:Accordion ID="Accordion3" runat="server" SelectedItemIndex="-1" Collapsible="true">
            <Items>
                <ej:AccordionItem Text="Hall Requests">
                    <ContentSection>
                        <iframe id="Hall" src="wfrmHallReq.aspx" width="100%" height="730px"></iframe>
                    </ContentSection>
                </ej:AccordionItem>
                <ej:AccordionItem Text="Visit Requests">
                    <ContentSection>
                        <iframe id="Visit" src="wfrmVisitReq.aspx" width="100%" height="730px"></iframe>
                    </ContentSection>
                </ej:AccordionItem>
            </Items>
        </ej:Accordion>
        <hr />
    </asp:Panel>
</asp:Content>
