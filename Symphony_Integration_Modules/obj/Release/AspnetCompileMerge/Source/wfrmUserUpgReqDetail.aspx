﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmUserUpgReqDetail.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmUserUpgReqDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <%--<script>
        $.noConflict();
    </script>
    <script src="../Scripts/js/jquery.min.js"></script>
    <script src="../Scripts/js/skel.min.js"></script>
    <script src="../Scripts/js/skel-layers.min.js"></script>
    <script src="../Scripts/js/init.js"></script>--%>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <section class="wrapper style1 special">
                    <div class="container">
                        <div class="row uniform 50%">
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblLibrary" runat="server" Text="Library"></asp:Label>
                                <ej:DropDownList ID="ddlLibrary" Height="35px" Enabled="false" Width="100%" CssClass="CustomDDL" runat="server"></ej:DropDownList>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblMemType" runat="server" Text="Membershipt Type"></asp:Label>
                                <ej:DropDownList ID="ddlMemType" Height="35px" Enabled="false" Width="100%" CssClass="CustomDDL" runat="server">
                                    <Items>
                                        <ej:DropDownListItem Value="0" Text="Adult"></ej:DropDownListItem>
                                        <ej:DropDownListItem Value="1" Text="Child"></ej:DropDownListItem>
                                        <ej:DropDownListItem Value="2" Text="Education"></ej:DropDownListItem>
                                        <ej:DropDownListItem Value="3" Text="Special Need"></ej:DropDownListItem>
                                    </Items>
                                </ej:DropDownList>
                            </div>
                            <div class="4u$ 12u$(medium)">
                                <asp:Label ID="lblFulName" runat="server" Text="Full Name"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtFullName" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblDOB" runat="server" Text="Date of Birth"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtDOB" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                <ej:DropDownList ID="ddlTitle" Height="35px" Enabled="false" Width="100%" CssClass="CustomDDL" runat="server" SelectedIndex="0"></ej:DropDownList>
                            </div>
                            <div class="4u$ 12u$(medium)">
                                <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtCompany" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblNat" runat="server" Text="Nationality"></asp:Label>
                                <ej:DropDownList ID="ddlNat" Height="35px" Enabled="false" Width="100%" CssClass="CustomDDL" runat="server" SelectedIndex="0"></ej:DropDownList>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblNatID" runat="server" Text="National ID"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtNatID" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u$ 12u$(medium)">
                                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                <ej:DropDownList ID="ddlGender" Height="35px" Enabled="false" Width="100%" CssClass="CustomDDL" runat="server" SelectedIndex="0"></ej:DropDownList>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtMobile" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblTele" runat="server" Text="Telephone"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtTele" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u$ 12u$(medium)">
                                <asp:Label ID="lblWorkPhone" runat="server" Text="Work Phone"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtWorkPhone" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblExt" runat="server" Text="Extenstion"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtExt" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtFax" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u$ 12u$(medium)">
                                <asp:Label ID="lblPostBox" runat="server" Text="PO BOX"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtPostBox" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u 12u$(medium)">
                                <asp:Label ID="lblWebSite" runat="server" Text="Web Site"></asp:Label>
                                <asp:TextBox ReadOnly="true" ID="txtWebSite" runat="server"></asp:TextBox>
                            </div>
                            <div class="4u$ 12u$(medium)">
                                <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                <ej:DropDownList ID="ddlCity" Height="35px" Enabled="false" Width="100%" CssClass="CustomDDL" runat="server" ClientSideOnChange="onActiveIndexChange" SelectedIndex="0"></ej:DropDownList>
                            </div>
                            <div class="6u 12u$(4)">
                                <asp:Label ID="lblPersonalImg" runat="server" Text="Personal Photo"></asp:Label>
                                <asp:HyperLink ID="hprlnkPersonalImg" Target="_blank" runat="server">Image Link</asp:HyperLink>
                            </div>
                            <div class="6u$ 12u$(4)">
                                <asp:Label ID="lblNatID1" runat="server" Text="National ID Photo 1"></asp:Label>
                                <asp:HyperLink ID="hprlnkNatID1" Target="_blank" runat="server">Image Link</asp:HyperLink>
                            </div>
                            <div class="6u 12u$(4)">
                                <asp:Label ID="lblNatID2" runat="server" Text="National ID Photo 2"></asp:Label>
                                <asp:HyperLink ID="hprlnkNatID2" Target="_blank" runat="server">Image Link</asp:HyperLink>
                            </div>
                            <div class="6u$ 12u$(4)">
                                <asp:Label ID="lblSNeedImg" runat="server" Text="Special Need Photo"></asp:Label>
                                <asp:HyperLink ID="hprlnkSNeedImg" Target="_blank" runat="server">Image Link</asp:HyperLink>
                            </div>
                            <div class="12u$">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnReject" runat="server" Text="Reject" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                            </div>
                            <div class="12u$">
                                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </div>
                </section>
                <script type="text/javascript">
                    var target;
                    $(function () {
                        target = $('#<%=ddlLibrary.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlLibraryInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlCity.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlCityInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlNat.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlNatInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlTitle.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlTitleInd%> });
                    });
                    $(function () {
                        target = $('#<%=ddlGender.ClientID%>').data("ejDropDownList");
                            target.option({ selectedIndex: <%=ddlGenderInd%> });
                        });
                        $(function () {
                            target = $('#<%=ddlMemType.ClientID%>').data("ejDropDownList");
                        target.option({ selectedIndex: <%=ddlMemberTypeInd%> });
                    });
                </script>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV1" align="center" valign="middle" class="ModalPopupBG" runat="server">
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
