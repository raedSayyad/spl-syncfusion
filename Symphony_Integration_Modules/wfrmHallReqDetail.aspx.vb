﻿Imports System.Web.UI.WebControls
Imports Syncfusion.JavaScript.Web
Imports System.Net
Imports System.IO
Imports ClassLibraries

Public Class wfrmHallReqDetail
    Inherits System.Web.UI.Page

    Public objUserInfo As New Users.UserInfo
    Public objHAllRequest As New Others.ReserveHall
    Public Row As String
    Public u As String

    Public Passport_Number As String
    Public Instructor_Name As String
    Public Passport_Image As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Session("HallUserInfo") Is Nothing Then
                objUserInfo = Session("HallUserInfo")
                Dim User_ID As String = objUserInfo.User_ID
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                objUserInfo = objEwsUserClient.UserInfo(User_ID)
            End If

            objHAllRequest = Session("HallRequest")
            Dim Lang As String = Request.QueryString("Lang")
            Dim objOthersClient As New ewsOthers.IewsOthersClient
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            lblFulName.Text = rc.GetString("FullName", Culture_Used)
            lblTitle.Text = rc.GetString("Title", Culture_Used)
            lblCompany.Text = rc.GetString("Company", Culture_Used)
            lblNat.Text = rc.GetString("Nationallity", Culture_Used)
            lblMobile.Text = rc.GetString("Mobile", Culture_Used)
            lblTele.Text = rc.GetString("Tele", Culture_Used)
            lblFax.Text = rc.GetString("Fax", Culture_Used)
            lblPostBox.Text = rc.GetString("POBox", Culture_Used)
            lblCity.Text = rc.GetString("City", Culture_Used)
            lblLibrary.Text = rc.GetString("Library", Culture_Used)
            lblDFrom.Text = rc.GetString("DateFrom", Culture_Used)
            lblDTo.Text = rc.GetString("DateTo", Culture_Used)
            lblPurpose.Text = rc.GetString("Purpose", Culture_Used)
            lblTFrom.Text = rc.GetString("TimeFrom", Culture_Used)
            lblTTo.Text = rc.GetString("TimeTo", Culture_Used)
            lblLecrType.Text = rc.GetString("LectureType", Culture_Used)
            lblAttnce.Text = rc.GetString("Attendence", Culture_Used)
            lblDevice.Text = rc.GetString("Devices", Culture_Used)
            lblInstructor.Text = rc.GetString("Instructors", Culture_Used)
            InstructorsGrid.Columns(0).HeaderText = rc.GetString("Instructors_PassportN", Culture_Used)
            InstructorsGrid.Columns(1).HeaderText = rc.GetString("Instructors_PassportI", Culture_Used)
            InstructorsGrid.Columns(2).HeaderText = rc.GetString("Instructors_Name", Culture_Used)

            Passport_Number = rc.GetString("Instructors_PassportN", Culture_Used)
            Instructor_Name = rc.GetString("Instructors_Name", Culture_Used)
            Passport_Image = rc.GetString("Instructors_PassportI", Culture_Used)
            txtFullName.Text = objUserInfo.FullName
            txtTitle.Text = objUserInfo.JobTitle
            txtCompany.Text = objUserInfo.Employer
            txtNat.Text = objUserInfo.NationalID
            txtMobile.Text = objUserInfo.Mobile
            txtTele.Text = objUserInfo.HomePhone
            txtFax.Text = objUserInfo.Fax
            txtPostBox.Text = objUserInfo.POBox
            txtCity.Text = objUserInfo.City
            txtLibrary.Text = objHAllRequest.Library
            txtDFrom.Text = objHAllRequest.DateFrom
            txtDTo.Text = objHAllRequest.DateTo
            txtPurpose.Text = objHAllRequest.ReservPurpose
            txtLecrType.Text = objHAllRequest.LectureType
            txtAttnce.Text = objHAllRequest.AttendanceNo
            txtDevice.Text = objHAllRequest.Devices
            Dim Instructor As List(Of Instructors) = New List(Of Instructors)()
            Dim InstNames() As String = objHAllRequest.InstructorName.Split(",")
            Dim InstImg() As String = objHAllRequest.InstructorPassport.Split(",")
            Dim InstPNum() As String = objHAllRequest.InstructorPpNo.Split(",")
            For i As Integer = 0 To InstNames.Length - 2
                Instructor.Add(New Instructors(InstPNum(i), InstNames(i), InstImg(i)))
            Next
            Session("ExternalFormDataSource") = Instructor
            InstructorsGrid.DataSource = Instructor
            InstructorsGrid.DataBind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim FillInstructor As New List(Of Instructors)()
            FillInstructor = Session("ExternalFormDataSource")
            Dim InstName As String = ""
            Dim InstPass As String = ""
            Dim InstPassNo As String = ""
            For Each objInstructor As Instructors In FillInstructor
                InstName += objInstructor.InstructorN + ","
                InstPassNo += CStr(objInstructor.PassportNo) + ","
                InstPass += objInstructor.PassportImg + ","
            Next

            Dim result As String = ""
            result = objEwsOtherClient.UptReserveHall(objHAllRequest.ID, objUserInfo.User_ID, objUserInfo.Library, txtPurpose.Text, objHAllRequest.DateFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), objHAllRequest.DateTo.ToString("dd/MM/yyyy:hh:mm:sstt"), txtLecrType.Text, txtAttnce.Text, txtDevice.Text, InstName, InstPass, InstPassNo, "2", "Admin", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
            If result = "1" Then
                objewsUserRegClient.InsUsersOPT(objUserInfo.User_ID, "0", "0", "0", "2", " ", "11", objUserInfo.User_ID, Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                Response.Redirect("~/wfrmResulte.aspx?Title=Hall Reservation&Text=Approved Successfuly&Url=wfrmHallReq.aspx")
            Else
                lblError.Text += result
            End If
        Catch ex As Exception
            lblError.Text += ex.Message
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmHallReq.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmHallReq.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Hall&Lang=Ara")
            Else
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Hall")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    <Serializable> _
    Public Class Instructors

        Public Sub New()
        End Sub
        Public Sub New(passportNo As String, instructorN As String, passportImg As String)
            Me.PassportNo = passportNo
            Me.InstructorN = instructorN
            Me.PassportImg = passportImg
        End Sub
        Public Property PassportNo() As String
            Get
                Return m_PassportNo
            End Get
            Set(value As String)
                m_PassportNo = value
            End Set
        End Property
        Private m_PassportNo As String
        Public Property InstructorN() As String
            Get
                Return m_InstructorN
            End Get
            Set(value As String)
                m_InstructorN = value
            End Set
        End Property
        Private m_InstructorN As String
        Public Property PassportImg() As String
            Get
                Return m_PassportImg
            End Get
            Set(value As String)
                m_PassportImg = value
            End Set
        End Property
        Private m_PassportImg As String
    End Class

End Class
