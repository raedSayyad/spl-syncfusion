﻿Imports ClassLibraries
Imports System.IO


Public Class wfrmUserReg
    Inherits System.Web.UI.Page

    Public gotoURL As String = ""
    Public Week As String = ""
    Public Very_Strong As String = ""
    Public Strong As String = ""
    Public Medium As String = ""

    Public PageTitle As String
    Public Row As String
    Public u As String
    Public Password_not_match As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))



            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
            End If
            PageTitle = rc.GetString("RegUser", Culture_Used)
            Dim ReqSent As String = rc.GetString("RegReqSent", Culture_Used)
            If Lang = "Ara" Then
                gotoURL = "~/wfrmResulte.aspx?Source=" & PageTitle & "&Lang=Ara&Msg=" & ReqSent
            Else
                gotoURL = "~/wfrmResulte.aspx?Source=" & PageTitle & "&Msg=" & ReqSent
            End If

            lblFulName.Text = rc.GetString("FullName", Culture_Used)
            lblPassword.Text = rc.GetString("Password", Culture_Used)
            lblConfPassword.Text = rc.GetString("Confirm_Password", Culture_Used)
            lblEmail.Text = rc.GetString("EmailReg", Culture_Used)
            btnSubmit.Text = rc.GetString("Register", Culture_Used)
            Week = rc.GetString("Week", Culture_Used)
            Very_Strong = rc.GetString("Very_Strong", Culture_Used)
            Strong = rc.GetString("Strong", Culture_Used)
            Medium = rc.GetString("Medium", Culture_Used)
            Password_not_match = rc.GetString("Confirm_Password_MSG", Culture_Used)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
            Dim objResponce_Msg As New Responce_Msg
            Dim UserCheck As String = objEwsUserClient.Users_Email_ID(txtEmail.Text, "", Lang)
            If UserCheck = "User Not Found." Then
                objResponce_Msg = objEwsUserClient.BasicUserReg("1", Lang, txtFullName.Text, txtPassword.Text, txtEmail.Text)
                If objResponce_Msg.UserId <> "" Then
                    Dim activation As String = objEwsUserClient.UserActivate(txtEmail.Text)
                    Response.Redirect(gotoURL)
                Else
                    lblError.Text = objResponce_Msg.Msg
                End If
            Else
                lblError.Text = rc.GetString("Msg_UserAlreadyExist", Culture_Used)
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class