﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace srvLogin
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute(ConfigurationName:="srvLogin.IwsLogin")>  _
    Public Interface IwsLogin
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/GetData", ReplyAction:="http://tempuri.org/IwsLogin/GetDataResponse")>  _
        Function GetData(ByVal value As String) As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/GetData", ReplyAction:="http://tempuri.org/IwsLogin/GetDataResponse")>  _
        Function GetDataAsync(ByVal value As String) As System.Threading.Tasks.Task(Of String)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/Login", ReplyAction:="http://tempuri.org/IwsLogin/LoginResponse")>  _
        Function Login(ByVal Username As String, ByVal Password As String) As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/Login", ReplyAction:="http://tempuri.org/IwsLogin/LoginResponse")>  _
        Function LoginAsync(ByVal Username As String, ByVal Password As String) As System.Threading.Tasks.Task(Of String)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/Session", ReplyAction:="http://tempuri.org/IwsLogin/SessionResponse")>  _
        Function Session(ByVal Username As String, ByVal key As String) As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/Session", ReplyAction:="http://tempuri.org/IwsLogin/SessionResponse")>  _
        Function SessionAsync(ByVal Username As String, ByVal key As String) As System.Threading.Tasks.Task(Of String)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/SetAuthKey", ReplyAction:="http://tempuri.org/IwsLogin/SetAuthKeyResponse")>  _
        Function SetAuthKey(ByVal Username As String) As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/SetAuthKey", ReplyAction:="http://tempuri.org/IwsLogin/SetAuthKeyResponse")>  _
        Function SetAuthKeyAsync(ByVal Username As String) As System.Threading.Tasks.Task(Of String)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/GetAuthKey", ReplyAction:="http://tempuri.org/IwsLogin/GetAuthKeyResponse")>  _
        Function GetAuthKey(ByVal AuthKey As String) As String
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IwsLogin/GetAuthKey", ReplyAction:="http://tempuri.org/IwsLogin/GetAuthKeyResponse")>  _
        Function GetAuthKeyAsync(ByVal AuthKey As String) As System.Threading.Tasks.Task(Of String)
    End Interface
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface IwsLoginChannel
        Inherits srvLogin.IwsLogin, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class IwsLoginClient
        Inherits System.ServiceModel.ClientBase(Of srvLogin.IwsLogin)
        Implements srvLogin.IwsLogin
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        Public Function GetData(ByVal value As String) As String Implements srvLogin.IwsLogin.GetData
            Return MyBase.Channel.GetData(value)
        End Function
        
        Public Function GetDataAsync(ByVal value As String) As System.Threading.Tasks.Task(Of String) Implements srvLogin.IwsLogin.GetDataAsync
            Return MyBase.Channel.GetDataAsync(value)
        End Function
        
        Public Function Login(ByVal Username As String, ByVal Password As String) As String Implements srvLogin.IwsLogin.Login
            Return MyBase.Channel.Login(Username, Password)
        End Function
        
        Public Function LoginAsync(ByVal Username As String, ByVal Password As String) As System.Threading.Tasks.Task(Of String) Implements srvLogin.IwsLogin.LoginAsync
            Return MyBase.Channel.LoginAsync(Username, Password)
        End Function
        
        Public Function Session(ByVal Username As String, ByVal key As String) As String Implements srvLogin.IwsLogin.Session
            Return MyBase.Channel.Session(Username, key)
        End Function
        
        Public Function SessionAsync(ByVal Username As String, ByVal key As String) As System.Threading.Tasks.Task(Of String) Implements srvLogin.IwsLogin.SessionAsync
            Return MyBase.Channel.SessionAsync(Username, key)
        End Function
        
        Public Function SetAuthKey(ByVal Username As String) As String Implements srvLogin.IwsLogin.SetAuthKey
            Return MyBase.Channel.SetAuthKey(Username)
        End Function
        
        Public Function SetAuthKeyAsync(ByVal Username As String) As System.Threading.Tasks.Task(Of String) Implements srvLogin.IwsLogin.SetAuthKeyAsync
            Return MyBase.Channel.SetAuthKeyAsync(Username)
        End Function
        
        Public Function GetAuthKey(ByVal AuthKey As String) As String Implements srvLogin.IwsLogin.GetAuthKey
            Return MyBase.Channel.GetAuthKey(AuthKey)
        End Function
        
        Public Function GetAuthKeyAsync(ByVal AuthKey As String) As System.Threading.Tasks.Task(Of String) Implements srvLogin.IwsLogin.GetAuthKeyAsync
            Return MyBase.Channel.GetAuthKeyAsync(AuthKey)
        End Function
    End Class
End Namespace
