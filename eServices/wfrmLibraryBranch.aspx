﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmLibraryBranch.aspx.vb" Inherits="eServices.wfrmLibraryBranch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/Content/skel.css" />
    <link rel="stylesheet" href="~/Content/style.css" />
    <link rel="stylesheet" href="~/Content/style-xlarge.css" />
    <link rel="stylesheet" href="~/Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="~/Content/bootstrap.min.css" />
    <link rel="stylesheet" href="~/Content/Site.css" />
    <link rel="stylesheet" href="~/Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="~/Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='Scripts/CodeMirror/codemirror.min.css' />
    <script src='Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div>
                        <ej:Grid ID="BranchLib" runat="server" DetailsTemplate="#tabGridContents">
                            <ClientSideEvents DetailsDataBound="detailGridData" />
                            <Columns>
                                <ej:Column Field="LibararyBranchID" IsPrimaryKey="True" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ATitle" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ETitle" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="ADescription" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="EDescription" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="AAddress" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="EAddress" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="Telephone" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="URL" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="Email" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="Latitude" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="Longitude" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="WorkingHours" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="Likes" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="Memebers" HeaderText="" Width="100" Visible="false" />
                                <ej:Column Field="NumberOfWatchings" HeaderText="" Width="100" Visible="false" />
                            </Columns>
                        </ej:Grid>
                        <script src="Scripts/jsondata.min.js"></script>
                        <script id="tabGridContents" type="text/x-jsrender">
                            <div class="tabcontrol">
                                <div id="detailChart{{:LibararyBranchID}}">
                                    <table>
                                        <tr>
                                            <td style="width: 70%;">
                                                <asp:Label ID="lblTitleT" runat="server" Text="Title"></asp:Label>
                                            </td>
                                            <td style="width: 30%;">
                                                <asp:Label ID="lblTitleV" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDescriptionT" runat="server" Text="Description"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDescriptionV" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddressT" runat="server" Text="Address"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAddressV" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblTelephoneT" runat="server" Text="Telephone"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTelephoneV" runat="server" Text="{{:Telephone}}"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblURLT" runat="server" Text="URL"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblURLV" runat="server" Text="{{:URL}}"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblEmailT" runat="server" Text="Email"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmailV" runat="server" Text="{{:Email}}"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input id="Button1" type="button" value="Show Map" onclick="initMap1(1,'{{:LibararyBranchID}}', {{:Latitude}},{{:Longitude}});" /><br />
                                                <br />
                                                <div id="map{{:LibararyBranchID}}"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblWorking_HoursT" runat="server" Text="Working Hours"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblWorking_HoursV" runat="server" Text="{{:WorkingHours}}"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblLikesT" runat="server" Text="Likes"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLikesV" runat="server" Text="{{:Likes}}"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMemberT" runat="server" Text="Member"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMemberV" runat="server" Text="{{:Member}}"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNumberOfWatchingsT" runat="server" Text="Number Of Watchings"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNumberOfWatchingsV" runat="server" Text="{{:NumberOfWatchings}}"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                    </div>
                    </script>
                        <script type="text/javascript">
                            function detailGridData(e) {
                                var filteredData = e.data["LibararyBranchID"];
                                var data = ej.DataManager(window.ordersView).executeLocal(ej.Query().where("LibararyBranchID", "equal", parseInt(filteredData), true));
                                e.detailsElement.css("display", "");
                                e.detailsElement.css("height", "900px");
                                e.detailsElement.find(".tabcontrol").ejTab({ selectedItemIndex: 1 });
                                window.parent.document.getElementById("iframe1").height = document.getElementById("PageBody").scrollHeight + 180;
                            }
                            function detailGridCol() {
                                window.parent.document.getElementById("iframe1").height = document.getElementById("PageBody").scrollHeight + 180;
                            }
                        </script>
                    <script>
                        var x;
                        function initMap1(x,LibID,lt,ln)
                        {
                            var divMap = 'map' + LibID
                            if (x==1)
                            {
                                document.getElementById(divMap).setAttribute("style","height:400px; width=100%");
                            }
                            var uluru = { lat: lt , lng: ln };
                            var map = new google.maps.Map(document.getElementById(divMap), { zoom: 16, center: uluru });
                            var marker = new google.maps.Marker({ position: uluru, map: map });
                        }
                    </script>
                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApDgAh4Yutrp5gE3NP4bDYuS6c6BU48lE&callback=initMap">    </script>
                    </script>
                    </div>
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
             <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        
    </form>
</body>
</html>
