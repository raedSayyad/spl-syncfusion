﻿Public Class wfrmResulte
    Inherits System.Web.UI.Page

    Public gotoURL As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            gotoURL = Request.QueryString("URL")
            lblTitle.Text = Request.QueryString("Title")
            lblText.Text = Request.QueryString("Text")
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect("~/" + gotoURL)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class