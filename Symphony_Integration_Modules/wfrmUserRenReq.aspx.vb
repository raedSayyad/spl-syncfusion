﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmUserRenReq
    Inherits System.Web.UI.Page

    Public Detail As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objAllUserRenRequest As New List(Of Users.UserRenRequest)
            Dim objUserRenRequest As New Users.UserRenRequest
            lblError.Text += CStr(objEwsUserRegClient.SelectUserRenewal().Count)
            For i As Integer = 0 To objEwsUserRegClient.SelectUserRenewal().Count - 1
                objUserRenRequest = New Users.UserRenRequest
                objUserRenRequest = objEwsUserRegClient.SelectUserRenewal()(i)
                objAllUserRenRequest.Add(objUserRenRequest)
            Next
            UserRenReqGrid.DataSource = objAllUserRenRequest
            UserRenReqGrid.DataBind()
            lblError.Text += " Page 1 "
            Detail = "Detail"
        Catch ex As Exception
            lblError.Text += ex.Message + " Page Error"
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objUserRenRequest As New Users.UserRenRequest
            For i As Integer = 0 To objEwsUserRegClient.SelectUserRenewal().Count - 1
                If i = CInt(selectedIndex) Then
                    objUserRenRequest = New Users.UserRenRequest
                    objUserRenRequest = objEwsUserRegClient.SelectUserRenewal()(i)
                End If
            Next
            Session("UserRenRequest") = objUserRenRequest
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmUserRenReqDetail.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmUserRenReqDetail.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class