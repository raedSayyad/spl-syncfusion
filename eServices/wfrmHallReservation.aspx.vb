﻿Imports System.Web.UI.WebControls
Imports Syncfusion.JavaScript.Web
Imports System.Net
Imports System.IO
Imports ClassLibraries

Public Class wfrmHallReservation
    Inherits System.Web.UI.Page
    Dim Mode As String = ""
    Dim objHallReservation As New Others.ReserveHall
    Dim UserID As String '= "11523"
    Dim gotoURL As String = ""
    Public ddlLibraryV As String
    Public tpFromV As String
    Public tpToV As String
    Public Lang As String
    Public Culture_Used As System.Globalization.CultureInfo = Nothing
    Public Passport_Number As String
    Public Instructor_Name As String
    Public Passport_Image As String

    Private Instructor As New List(Of Instructors)()
    Public objUserInfo As New Users.UserInfo

    Public Row As String
    Public u As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Session("UserInfo") Is Nothing Then
                objUserInfo = Session("UserInfo")
                Dim User_ID As String = objUserInfo.User_ID
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                objUserInfo = objEwsUserClient.UserInfo(User_ID)
            Else
                Response.Redirect("~/wfrmLogin.aspx")
            End If
            UserID = objUserInfo.User_ID
            Mode = Request.QueryString("Mode")

            Lang = Request.QueryString("Lang")
            Dim objOthersClient As New ewsOthers.IewsOthersClient
            'Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            Dim objSymAdminClient As New wsSymAdmin.AdminEndpointClient
            Dim objSymAdminSDH As New wsSymAdmin.SdHeader
            Dim objlookupPolicyListReq As New wsSymAdmin.LookupPolicyListRequest
            Dim objlookupPolicyInfoRes() As wsSymAdmin.PolicyInfo
            objSymAdminSDH.clientID = "DS_CLIENT"
            If Lang = "Ara" Then
                objSymAdminSDH.locale = "ar"
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                ddlLibrary.EnableRTL = True
                dpFrom.EnableRTL = True
                InstructorsGrid.EnableRTL = True
                dpTo.EnableRTL = True
                tpFrom.EnableRTL = True
                tpTo.EnableRTL = True
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
                objSymAdminSDH.locale = "en"
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))

            lblLibrary.Text = rc.GetString("Library", Culture_Used)
            lblDFrom.Text = rc.GetString("DateFrom", Culture_Used)
            lblDTo.Text = rc.GetString("DateTo", Culture_Used)
            lblPurpose.Text = rc.GetString("Purpose", Culture_Used)
            lblTFrom.Text = rc.GetString("TimeFrom", Culture_Used)
            lblTTo.Text = rc.GetString("TimeTo", Culture_Used)
            btnBack.Text = rc.GetString("Back", Culture_Used)
            btnDelete.Text = rc.GetString("Delete", Culture_Used)
            btnReserve.Text = rc.GetString("Reserve", Culture_Used)
            dpFrom.WatermarkText = rc.GetString("DatePickerWaterMark", Culture_Used)
            dpTo.WatermarkText = rc.GetString("DatePickerWaterMark", Culture_Used)
            lblLecrType.Text = rc.GetString("LectureType", Culture_Used)
            lblAttnce.Text = rc.GetString("Attendence", Culture_Used)
            lblDevice.Text = rc.GetString("Devices", Culture_Used)
            lblInstructor.Text = rc.GetString("Instructors", Culture_Used)
            InstructorsGrid.Columns(0).HeaderText = rc.GetString("Instructors_PassportN", Culture_Used)
            InstructorsGrid.Columns(1).HeaderText = rc.GetString("Instructors_PassportI", Culture_Used)
            InstructorsGrid.Columns(2).HeaderText = rc.GetString("Instructors_Name", Culture_Used)

            Passport_Number = rc.GetString("Instructors_PassportN", Culture_Used)
            Instructor_Name = rc.GetString("Instructors_Name", Culture_Used)
            Passport_Image = rc.GetString("Instructors_PassportI", Culture_Used)

            objlookupPolicyListReq.policyType = "LIBR"
            objlookupPolicyInfoRes = objSymAdminClient.lookupPolicyList(objSymAdminSDH, objlookupPolicyListReq)
            Dim objDDLIST As New Others.DDLList
            Dim objAllDDlist As New List(Of Others.DDLList)

            For i As Integer = 0 To objlookupPolicyInfoRes.Length - 1
                objDDLIST = New Others.DDLList
                objDDLIST.Text = objlookupPolicyInfoRes(i).policyDescription
                objDDLIST.Value = objlookupPolicyInfoRes(i).policyID
                objAllDDlist.Add(objDDLIST)
            Next
            ddlLibrary.DataSource = objAllDDlist ' objOthersClient.LookUp("LIBR", Lang)
            ddlLibrary.DataValueField = "Value"
            ddlLibrary.DataTextField = "Text"
            ddlLibrary.DataBind()

            If Not IsPostBack Then
                cblDevices.DataSource = objOthersClient.GetAllHallDevices
                cblDevices.DataValueField = "ID"
                If Lang = "Ara" Then
                    cblDevices.DataTextField = "AraDesc"
                Else
                    cblDevices.DataTextField = "EngDesc"
                End If
                cblDevices.DataBind()
            End If
            '  lblError.Text += " |" + Mode
            If Mode = "Add" Then
                BindDataSource()
                Session("ExternalFormDataSource") = Instructor
                Dim stDate As DateTime = Now
                stDate = stDate.AddDays(1)
                Dim fnDate As DateTime = Now
                fnDate = fnDate.AddMonths(6)
                dpFrom.MinDate = stDate
                dpFrom.MaxDate = fnDate
                dpFrom.StartDay = 0
                ddlLibraryV = "1"
                fnDate = fnDate.AddDays(14)
                dpTo.MinDate = stDate
                dpTo.MaxDate = fnDate
                dpTo.StartDay = 0
            End If

            Dim asc As New AsyncPostBackTrigger
            asc.ControlID = dpFrom.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = dpTo.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = tpFrom.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = tpTo.UniqueID
            asc.EventName = "Select"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = ddlLibrary.UniqueID
            asc.EventName = "ValueSelect"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = txtPurpose.UniqueID
            asc.EventName = "TextChanged"
            UpdatePanel1.Triggers.Add(asc)
            asc = New AsyncPostBackTrigger
            asc.ControlID = cblDevices.UniqueID
            asc.EventName = "SelectedIndexChanged"
            UpdatePanel1.Triggers.Add(asc)

            If Lang = "Ara" Then
                gotoURL = "~/wfrmResulte.aspx?Source=" & rc.GetString("ReserveHall", Culture_Used) & "&Msg=&" & rc.GetString("ReserveHallSucess", Culture_Used) & "&TermsID=2&Lang=Ara"
            Else
                gotoURL = "~/wfrmResulte.aspx?Source=" & rc.GetString("ReserveHall", Culture_Used) & "&Msg=&" & rc.GetString("ReserveHallSucess", Culture_Used) & "&TermsID=2"
            End If
            If Not IsPostBack Then
                Dim LibraryList() As Others.DDLList
                LibraryList = objOthersClient.LookUp("LIBR", Lang)
                If Mode = "Edit" Then
                    objHallReservation = Session("UserHallReservation")
                    For c As Integer = 0 To LibraryList.Length - 1
                        If LibraryList(c).Value = objHallReservation.Library Then
                            ddlLibraryV = CStr(c)
                        End If
                    Next
                    hfHallID.Value = CStr(objHallReservation.ID)
                    dpFrom.Value = objHallReservation.DateFrom
                    dpTo.Value = objHallReservation.DateTo
                    tpFromV = objHallReservation.DateFrom.Date.ToString("hh:mm tt")
                    tpToV = objHallReservation.DateTo.Date.ToString("hh:mm tt")
                    txtPurpose.Text = objHallReservation.ReservPurpose
                    hdDateCreate.Value = CStr(objHallReservation.DateCreate)
                    txtLecrType.Text = objHallReservation.LectureType
                    txtAttnce.Text = objHallReservation.AttendanceNo
                    Dim UptInstructor As New Instructors
                    Dim getUptInstrutctor() As String = objHallReservation.InstructorName.Split(",")
                    Dim getUptInstrutctorPP() As String = objHallReservation.InstructorPassport.Split(",")
                    Dim getUptInstrutctorPN() As String = objHallReservation.InstructorPpNo.Split(",")
                    For i As Integer = 0 To getUptInstrutctor.Length - 2
                        'Instructor = New List(Of Instructors)()
                        UptInstructor = New Instructors
                        UptInstructor.InstructorN = getUptInstrutctor(i)
                        UptInstructor.PassportImg = "data:image/jpeg;base64," + objOthersClient.imageUrlToBase64(getUptInstrutctorPP(i))
                        UptInstructor.PassportNo = getUptInstrutctorPN(i)
                        Instructor.Add(UptInstructor)
                        Session("ExternalFormDataSource") = Instructor
                    Next
                    BindDataSource()
                    Dim getUptDevices() As String = objHallReservation.Devices.Split(",")
                    For x As Integer = 0 To getUptDevices.Length - 2
                        For z As Integer = 0 To cblDevices.Items.Count - 1
                            If cblDevices.Items(z).Value = getUptDevices(x) Then
                                cblDevices.Items(z).Selected = True
                            End If
                        Next
                    Next
                    btnReserve.Text = rc.GetString("Update", Culture_Used)
                    btnDelete.Visible = True
                    objHallReservation = Session("UserHallReservation")
                    If objHallReservation.Status = "2" Or objHallReservation.Status = "3" Or objHallReservation.Status = "4" Or DateTime.Compare(Now, objHallReservation.DateFrom) > 0 Then
                        btnDelete.Visible = False
                        btnReserve.Visible = False
                        dpFrom.Enabled = False
                        dpTo.Enabled = False
                        tpFrom.Enabled = False
                        tpTo.Enabled = False
                        ddlLibrary.Enabled = False
                        txtPurpose.ReadOnly = True
                        txtLecrType.ReadOnly = True
                        txtAttnce.ReadOnly = True
                        InstructorsGrid.Enabled = False
                        InstructorsGrid.EditSettings.AllowAdding = False
                        InstructorsGrid.EditSettings.AllowEditing = False
                        InstructorsGrid.EditSettings.AllowDeleting = False
                        InstructorsGrid.EditSettings.AllowEditOnDblClick = False
                        InstructorsGrid.ToolbarSettings.ShowToolbar = False
                        cblDevices.Enabled = False
                    End If
                Else
                    btnBack.Text = rc.GetString("Cancel", Culture_Used)
                    tpFromV = "08:00 AM"
                    tpToV = "05:00 PM"
                End If
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub dpFrom_Select(sender As Object, e As Syncfusion.JavaScript.Web.DatePickerSelectEventArgs) Handles dpFrom.Select
        Try
            lblValdDpF.Text = ""
            Dim dt As DateTime = dpFrom.Value
            dpTo.MinDate = New DateTime(dt.Year, dt.Month, dt.Day)
            dpTo.Value = New DateTime(dt.Year, dt.Month, dt.Day)
            If Not ddlLibrary.Value Is Nothing Then
                If Not dpFrom.Value Is Nothing Then
                    If Not dpTo.Value Is Nothing Then
                        If Not tpFrom.Value Is Nothing Then
                            If Not tpTo.Value Is Nothing Then
                                Dim Ava As Boolean = CheckAva()
                                Dim Lang As String = Request.QueryString("Lang")
                                Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                                If Lang = "Ara" Then
                                    Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                                End If
                                Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                                If Ava = True Then
                                    lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                    lblAva.ForeColor = Drawing.Color.Red
                                Else
                                    lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                    lblAva.ForeColor = Drawing.Color.Green
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub tpFrom_Select(sender As Object, e As Syncfusion.JavaScript.Web.TimePickerEventArgs) Handles tpFrom.Select
        Try
            lblValdTpF.Text = ""
            Dim dstr As New List(Of Syncfusion.JavaScript.Models.DisableTimeRange)
            Dim item As New Syncfusion.JavaScript.Models.DisableTimeRange
            item.StartTime = "12:00 AM"
            item.EndTime = tpFrom.Value
            dstr.Add(item)
            item = New Syncfusion.JavaScript.Models.DisableTimeRange
            item.StartTime = "06:00 PM"
            item.EndTime = "11:00 PM"
            dstr.Add(item)
            tpTo.DisableTimeRanges = dstr

            If Not ddlLibrary.Value Is Nothing Then
                If Not dpFrom.Value Is Nothing Then
                    If Not dpTo.Value Is Nothing Then
                        If Not tpFrom.Value Is Nothing Then
                            If Not tpTo.Value Is Nothing Then
                                Dim Ava As Boolean = CheckAva()
                                Dim Lang As String = Request.QueryString("Lang")
                                Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                                If Lang = "Ara" Then
                                    Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                                End If
                                Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                                If Ava = True Then
                                    lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                    lblAva.ForeColor = Drawing.Color.Red
                                Else
                                    lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                    lblAva.ForeColor = Drawing.Color.Green
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub dpTo_Select(sender As Object, e As Syncfusion.JavaScript.Web.DatePickerSelectEventArgs) Handles dpTo.Select
        If Not ddlLibrary.Value Is Nothing Then
            If Not dpFrom.Value Is Nothing Then
                If Not dpTo.Value Is Nothing Then
                    lblValdDpT.Text = ""
                    If Not tpFrom.Value Is Nothing Then
                        If Not tpTo.Value Is Nothing Then
                            Dim Ava As Boolean = CheckAva()
                            Dim Lang As String = Request.QueryString("Lang")
                            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                            If Lang = "Ara" Then
                                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                            End If
                            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                            If Ava = True Then
                                lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                lblAva.ForeColor = Drawing.Color.Red
                            Else
                                lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                lblAva.ForeColor = Drawing.Color.Green
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub tpTo_Select(sender As Object, e As Syncfusion.JavaScript.Web.TimePickerEventArgs) Handles tpTo.Select
        If Not ddlLibrary.Value Is Nothing Then
            If Not dpFrom.Value Is Nothing Then
                If Not dpTo.Value Is Nothing Then
                    If Not tpFrom.Value Is Nothing Then
                        If Not tpTo.Value Is Nothing Then
                            lblValdTpT.Text = ""
                            Dim Ava As Boolean = CheckAva()
                            Dim Lang As String = Request.QueryString("Lang")
                            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                            If Lang = "Ara" Then
                                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                            End If
                            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                            If Ava = True Then
                                lblAva.Text = rc.GetString("NotAvailable", Culture_Used) '"The selected Date or Time Not Available"
                                lblAva.ForeColor = Drawing.Color.Red
                            Else
                                lblAva.Text = rc.GetString("Available", Culture_Used) '"Available"
                                lblAva.ForeColor = Drawing.Color.Green
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnReserve_Click(sender As Object, e As EventArgs) Handles btnReserve.Click
        Try
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient

            Dim ValdBool As Boolean = False
            If Not ddlLibrary.Value Is Nothing Then
                lblValdDdl.Text = ""
            Else
                lblValdDdl.Text = "*"
                ValdBool = True
            End If
            If Not dpFrom.Value Is Nothing Then
                lblValdDpF.Text = ""
            Else
                lblValdDpF.Text = "*"
                ValdBool = True
            End If
            If Not dpTo.Value Is Nothing Then
                lblValdDpT.Text = ""
            Else
                lblValdDpT.Text = "*"
                ValdBool = True
            End If
            If Not tpFrom.Value Is Nothing Then
                lblValdTpF.Text = ""
            Else
                lblValdTpF.Text = "*"
                ValdBool = True
            End If
            If Not tpTo.Value Is Nothing Then
                lblValdTpT.Text = ""
            Else
                lblValdTpT.Text = "*"
                ValdBool = True
            End If

            If txtPurpose.Text <> "" Then
                lblValdTxtP.Text = ""
            Else
                lblValdTxtP.Text = "*"
                ValdBool = True
            End If

            If txtLecrType.Text <> "" Then
                lblValdTxtL.Text = ""
            Else
                lblValdTxtL.Text = "*"
                ValdBool = True
            End If

            If txtAttnce.Text <> "" Then
                lblValdAttnce.Text = ""
            Else
                lblValdAttnce.Text = "*"
                ValdBool = True
            End If

            Dim FillInstructor As New List(Of Instructors)()
            FillInstructor = Session("ExternalFormDataSource")
            Dim gridRows As Integer = FillInstructor.Count
            If gridRows > 0 Then
                lblValdInstr.Text = ""
            Else
                lblValdInstr.Text = "*"
                ValdBool = True
            End If

            If ValdBool = True Then
                Exit Sub
            End If
            Dim InstName As String = ""
            Dim InstPass As String = ""
            Dim InstPassNo As String = ""
            For Each objInstructor As Instructors In FillInstructor
                InstName += objInstructor.InstructorN + ","
                InstPassNo += CStr(objInstructor.PassportNo) + ","
                lblError.Text += objInstructor.PassportImg + " | "
                Dim path As String = objInstructor.PassportImg
                Dim bytes() As Byte = Convert.FromBase64String(path.Replace("data:image/jpeg;base64,", ""))
                Dim resMsg As New ClassLibraries.Responce_Msg
                resMsg = objEwsOtherClient.ImageUpload("Hall" + CStr(objInstructor.PassportNo), "4", "jpeg", bytes)
                InstPass += resMsg.Code + ","
            Next

            Dim strDevices As String = ""
            For i As Integer = 0 To cblDevices.Items.Count - 1
                If cblDevices.Items(i).Selected = True Then
                    strDevices += cblDevices.Items(i).Value + ","
                End If
            Next

            Dim tsFrom As String
            Dim tsFromArr1() As String = dpFrom.Value.ToString.Split(" ")
            Dim tsFromArr2() As String = tsFromArr1(0).Split("/")
            If tsFromArr2(1).Length = 1 Then
                tsFromArr2(1) = "0" + tsFromArr2(1)
            End If
            If tsFromArr2(0).Length = 1 Then
                tsFromArr2(0) = "0" + tsFromArr2(0)
            End If
            Dim tsFrom3Arr() As String = tpFrom.Value.ToString.Split(":")
            If tsFrom3Arr(0).Length = 1 Then
                tsFrom3Arr(0) = "0" + tsFrom3Arr(0)
            End If

            tsFrom = tsFromArr2(0) + "/" + tsFromArr2(1) + "/" + tsFromArr2(2) + " " + tsFrom3Arr(0) + ":" + tsFrom3Arr(1) '+ ":" + tsFrom3Arr(2)
            lblError.Text += tsFrom + "|"
            Dim dtFrom As DateTime = DateTime.ParseExact(tsFrom, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InstalledUICulture) 'New DateTime(dpFrom.Value.Value.Year, dpFrom.Value.Value.Month, dpFrom.Value.Value.Day, CInt(tsFrom3Arr(0)), CInt(tsFrom3Arr(1)), CInt(tsFrom3Arr(2)), 0)

            Dim tsTo As String
            Dim tsToArr1() As String = dpTo.Value.ToString.Split(" ")
            Dim tsToArr2() As String = tsToArr1(0).Split("/")
            If tsToArr2(1).Length = 1 Then
                tsToArr2(1) = "0" + tsToArr2(1)
            End If
            If tsToArr2(0).Length = 1 Then
                tsToArr2(0) = "0" + tsToArr2(0)
            End If
            Dim tsToArr3() As String = tpTo.Value.ToString.Split(":")
            If tsToArr3(0).Length = 1 Then
                tsToArr3(0) = "0" + tsToArr3(0)
            End If

            tsTo = tsToArr2(0) + "/" + tsToArr2(1) + "/" + tsToArr2(2) + " " + tsToArr3(0) + ":" + tsToArr3(1) '+ ":" + tsToArr3(2)
            lblError.Text += tsTo + "|"
            Dim dtTo As DateTime = DateTime.ParseExact(tsTo, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InstalledUICulture) ' New DateTime(dpTo.Value.Value.Year, dpTo.Value.Value.Month, dpTo.Value.Value.Day, CInt(tsToArr3(0)), CInt(tsToArr3(1)), CInt(tsToArr3(2)), 0)

            If Not ddlLibrary.Value Is Nothing Then
                If Not dpFrom.Value Is Nothing Then
                    If Not dpTo.Value Is Nothing Then
                        If Not tpFrom.Value Is Nothing Then
                            If Not tpTo.Value Is Nothing Then
                                Dim Ava As Boolean = CheckAva()
                                If Ava = True Then
                                    Dim Lang As String = Request.QueryString("Lang")
                                    Dim Culture_Used As System.Globalization.CultureInfo = Nothing
                                    If Lang = "Ara" Then
                                        Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                                    End If
                                    Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                                    lblError.Text = rc.GetString("NotAvailableMsg", Culture_Used)
                                Else
                                    Dim result As String
                                    If Mode = "Add" Then
                                        result = objEwsOtherClient.InsReserveHall(UserID, ddlLibrary.Value, txtPurpose.Text, dtFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), dtTo.ToString("dd/MM/yyyy:hh:mm:sstt"), txtLecrType.Text, txtAttnce.Text, strDevices, InstName, InstPass, InstPassNo, "0", "", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                    Else
                                        result = objEwsOtherClient.UptReserveHall(hfHallID.Value, UserID, ddlLibrary.Value, txtPurpose.Text, dtFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), dtTo.ToString("dd/MM/yyyy:hh:mm:sstt"), txtLecrType.Text, txtAttnce.Text, strDevices, InstName, InstPass, InstPassNo, "0", "", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                                    End If
                                    If result = "1" Then
                                        Response.Redirect(gotoURL)
                                        Session("ExternalFormDataSource") = Nothing
                                    Else
                                        lblError.Text += result
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            lblError.Text += ex.Message + " - Error Line : " & ex.StackTrace()
        End Try

    End Sub

    Private Sub ddlLibrary_ValueSelect(sender As Object, e As Syncfusion.JavaScript.Web.DropdownListEventArgs) Handles ddlLibrary.ValueSelect
        If Not ddlLibrary.Value Is Nothing Then
            lblValdDdl.Text = ""
            If Not dpFrom.Value Is Nothing Then
                If Not dpTo.Value Is Nothing Then
                    If Not tpFrom.Value Is Nothing Then
                        If Not tpTo.Value Is Nothing Then
                            Dim Ava As Boolean = CheckAva()
                            If Ava = True Then
                                lblAva.Text = "The selected Date or Time Not Available"
                                lblAva.ForeColor = Drawing.Color.Red
                            Else
                                lblAva.Text = "Available"
                                lblAva.ForeColor = Drawing.Color.Green
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub txtPurpose_TextChanged(sender As Object, e As EventArgs) Handles txtPurpose.TextChanged
        lblValdTxtP.Text = ""
    End Sub

    Function CheckAva() As Boolean

        Dim tsFrom As String
        Dim tsFromArr1() As String = dpFrom.Value.ToString.Split(" ")
        Dim tsFromArr2() As String = tsFromArr1(0).Split("/")
        If tsFromArr2(1).Length = 1 Then
            tsFromArr2(1) = "0" + tsFromArr2(1)
        End If
        If tsFromArr2(0).Length = 1 Then
            tsFromArr2(0) = "0" + tsFromArr2(0)
        End If
        Dim tsFrom3Arr() As String = tpFrom.Value.ToString.Replace(" ", ":00").Split(":")
        If tsFrom3Arr(0).Length = 1 Then
            tsFrom3Arr(0) = "0" + tsFrom3Arr(0)
        End If
        tsFrom = tsFromArr2(1) + "/" + tsFromArr2(0) + "/" + tsFromArr2(2) + ":" + tsFrom3Arr(0) + ":" + tsFrom3Arr(1) + ":" + tsFrom3Arr(2)
        Dim tsTo As String
        Dim tsToArr1() As String = dpTo.Value.ToString.Split(" ")
        Dim tsToArr2() As String = tsToArr1(0).Split("/")
        If tsToArr2(1).Length = 1 Then
            tsToArr2(1) = "0" + tsFromArr2(1)
        End If
        If tsToArr2(0).Length = 1 Then
            tsToArr2(0) = "0" + tsToArr2(0)
        End If
        Dim tsToArr3() As String = tpTo.Value.ToString.Replace(" ", ":00").Split(":")
        If tsToArr3(0).Length = 1 Then
            tsToArr3(0) = "0" + tsToArr3(0)
        End If
        tsTo = tsToArr2(1) + "/" + tsToArr2(0) + "/" + tsToArr2(2) + ":" + tsToArr3(0) + ":" + tsToArr3(1) + ":" + tsToArr3(2)
        Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
        Dim Ava As Boolean
        If Mode = "Add" Then
            Ava = objEwsOtherClient.ReserveHallAva(tsFrom, tsTo, ddlLibrary.Value)
        ElseIf Mode = "Edit" Then
            Ava = objEwsOtherClient.UpdateHallAva(tsFrom, tsTo, ddlLibrary.Value, hfHallID.Value)
        End If
        Return Ava
    End Function

    Private Sub BindDataSource()
        'lblError.Text += " |" + CStr(Instructor.Count)
        If DirectCast(Session("ExternalFormDataSource"), List(Of Instructors)) Is Nothing Then
            Dim passportNo As Integer = 0
        Else
            Instructor = DirectCast(Session("ExternalFormDataSource"), List(Of Instructors))
        End If
        Me.InstructorsGrid.DataSource = Instructor
        Me.InstructorsGrid.DataBind()
    End Sub

    Protected Sub EditEvents_ServerEditRow(sender As Object, e As GridEventArgs)
        EditAction(e.EventType, e.Arguments("data"))
    End Sub

    Protected Sub EditEvents_ServerAddRow(sender As Object, e As GridEventArgs)
        EditAction(e.EventType, e.Arguments("data"))
    End Sub

    Protected Sub EditEvents_ServerDeleteRow(sender As Object, e As GridEventArgs)
        EditAction(e.EventType, e.Arguments("data"))
    End Sub

    Protected Sub EditAction(eventType As String, record As Object)
        Dim data As List(Of Instructors) = TryCast(Session("ExternalFormDataSource"), List(Of Instructors))
        Dim KeyVal__1 As Dictionary(Of String, Object) = TryCast(record, Dictionary(Of String, Object))
        If eventType = "endEdit" Then
            Dim value As New Instructors()
            For Each keyval__2 As KeyValuePair(Of String, Object) In KeyVal__1

                If keyval__2.Key = "PassportNo" Then
                    value = data.Where(Function(d) d.PassportNo = CStr(keyval__2.Value)).FirstOrDefault()
                    value.PassportNo = Convert.ToString(keyval__2.Value)
                ElseIf keyval__2.Key = "InstructorN" Then
                    value.InstructorN = Convert.ToString(keyval__2.Value)
                ElseIf keyval__2.Key = "PassportImg" Then
                    value.PassportImg = Convert.ToString(keyval__2.Value)
                End If
            Next

        ElseIf eventType = "endAdd" Then
            Dim newRecord As New Instructors()
            For Each keyval__2 As KeyValuePair(Of String, Object) In KeyVal__1

                If keyval__2.Key = "PassportNo" Then
                    newRecord.PassportNo = Convert.ToString(keyval__2.Value)
                ElseIf keyval__2.Key = "InstructorN" Then
                    newRecord.InstructorN = Convert.ToString(keyval__2.Value)
                ElseIf keyval__2.Key = "PassportImg" Then
                    newRecord.PassportImg = Convert.ToString(keyval__2.Value)
                End If
            Next
            data.Insert(0, newRecord)

        ElseIf eventType = "endDelete" Then
            For Each keyval__2 As KeyValuePair(Of String, Object) In KeyVal__1
                If keyval__2.Key = "PassportNo" Then
                    Dim value As Instructors = data.Where(Function(d) d.PassportNo = CStr(keyval__2.Value)).FirstOrDefault()
                    data.Remove(value)
                End If
            Next
        End If
        Session("ExternalFormDataSource") = data
        Me.InstructorsGrid.DataSource = data
        Me.InstructorsGrid.DataBind()
    End Sub

    <Serializable> _
    Public Class Instructors

        Public Sub New()
        End Sub
        Public Sub New(passportNo As String, instructorN As String, passportImg As String)
            Me.PassportNo = passportNo
            Me.InstructorN = instructorN
            Me.PassportImg = passportImg
        End Sub
        Public Property PassportNo() As String
            Get
                Return m_PassportNo
            End Get
            Set(value As String)
                m_PassportNo = value
            End Set
        End Property
        Private m_PassportNo As String
        Public Property InstructorN() As String
            Get
                Return m_InstructorN
            End Get
            Set(value As String)
                m_InstructorN = value
            End Set
        End Property
        Private m_InstructorN As String
        Public Property PassportImg() As String
            Get
                Return m_PassportImg
            End Get
            Set(value As String)
                m_PassportImg = value
            End Set
        End Property
        Private m_PassportImg As String
    End Class

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))

            Session("ExternalFormDataSource") = Nothing
            If btnBack.Text = rc.GetString("Back", Culture_Used) Then
                If Lang = "Ara" Then
                    Response.Redirect("~/wfrmUserHallReservation.aspx?Lang=Ara")
                Else
                    Response.Redirect("~/wfrmUserHallReservation.aspx")
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim result As String = ""
            result = objEwsOtherClient.UptReserveHall(objHallReservation.ID, objHallReservation.User_ID, objHallReservation.Library, objHallReservation.ReservPurpose, objHallReservation.DateFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), objHallReservation.DateTo.ToString("dd/MM/yyyy:hh:mm:sstt"), objHallReservation.LectureType, objHallReservation.AttendanceNo, objHallReservation.Devices, objHallReservation.InstructorName, objHallReservation.InstructorPassport, objHallReservation.InstructorPpNo, "4", objHallReservation.User_ID, Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
            If result = "1" Then
                Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
                If Lang = "Ara" Then
                    gotoURL = "~/wfrmResulte.aspx?Source=" & rc.GetString("ReserveHall", Culture_Used) & "&Msg=" & rc.GetString("ReserveHallCenceled", Culture_Used) & "&TermsID=2&Lang=Ara"
                Else
                    gotoURL = "~/wfrmResulte.aspx?Source=" & rc.GetString("ReserveHall", Culture_Used) & "&Msg=" & rc.GetString("ReserveHallCenceled", Culture_Used) & "&TermsID=2"
                End If
                Response.Redirect(gotoURL)
            Else
                lblError.Text = result
            End If
        Catch ex As Exception
            lblError.Text += ex.Message + " - Error Line : " & ex.StackTrace()
        End Try
    End Sub
End Class