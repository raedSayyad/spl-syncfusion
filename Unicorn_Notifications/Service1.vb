﻿Imports ClassLibraries
Imports System.IO

Public Class Service1

#Region "DataMember"
    Public Shared LogFile As String = ""
    Public Shared Log_Mobile_Msg As String = ""
    Shared ComFile As String = ""
    Public Shared DB As String = ""
    Public Shared ODBC_Con_Str As String = ""
    Public Shared Notification_WS_URL As String = ""
    Public Shared Push_Notification_WS_URL As String = ""
    Public Shared Encoding As String = ""
    Public Shared Query_Log As String = ""
    Public Shared ConStr As String = ""

    Public Shared HostIP As String = ""
    Public Shared Port As String = ""
    Public Shared Username As String = ""
    Public Shared Password As String = ""
    Public Shared Email As String = ""

    Public Request_Type As String = ""
    Public Request_Type_Tag As String = ""
    Public Email_Tag As String = ""
    Public Mobile_Tag As String = ""
    Public Users_New_Email_Notifications As String = ""
    Public Users_New_SMS_Notifications As String = ""
    Public Users_New_Mobile_Notifications As String = ""
    Public Charges_New_Email_Notifications As String = ""
    Public Charges_New_SMS_Notifications As String = ""
    Public Charges_New_Mobile_Notifications As String = ""
    Public Charges_Overdue_Email_Notifications As String = ""
    Public Charges_Overdue_SMS_Notifications As String = ""
    Public Charges_Overdue_Mobile_Notifications As String = ""
    Public Charges_Overdue_First_Notifications_Days As Long = 0
    Public Charges_Overdue_Second_Notifications_Days As Long = 0
    Public Charges_Overdue_Final_Notifications_Days As Long = 0
    Public Charges_Overdue_First_Notifications As String = ""
    Public Charges_Overdue_Second_Notifications As String = ""
    Public Charges_Overdue_Final_Notifications As String = ""
    Public Holds_Email_Notifications As String = ""
    Public Holds_SMS_Notifications As String = ""
    Public Holds_Mobile_Notifications As String = ""
    Public Charges_Discharge_Email_Notifications As String = ""
    Public Charges_Discharge_SMS_Notifications As String = ""
    Public Charges_Discharge_Mobile_Notifications As String = ""
    Public Charges_Renew_Email_Notifications As String = ""
    Public Charges_Renew_SMS_Notifications As String = ""
    Public Charges_Renew_Mobile_Notifications As String = ""
    Public Requests_Email_Notifications As String = ""
    Public Requests_SMS_Notifications As String = ""
    Public Requests_Mobile_Notifications As String = ""
    Public Bills_New_Email_Notifications As String = ""
    Public Bills_New_SMS_Notifications As String = ""
    Public Bills_New_Mobile_Notifications As String = ""
    Public Bills_Pay_Email_Notifications As String = ""
    Public Bills_Pay_SMS_Notifications As String = ""
    Public Bills_Pay_Mobile_Notifications As String = ""
    Public Approval_Email_Notifications As String = ""
    Public Approval_SMS_Notifications As String = ""
    Public Approval_Mobile_Notifications As String = ""
    Public Shared EmailCC As String = ""

    Dim InProcess As Boolean = False
    Dim Service_Path_Array() As String = Nothing
    Dim Service_Path As String = ""

    Public Shared Array_Tags() As String = Nothing
    Public Shared Array_Requests() As String = Nothing
    Public Shared Array_Approvals_ID() As String = Nothing
    Public Shared Array_Approvals_Txt() As String = Nothing
    Public Shared Array_Services() As String = Nothing
    Public Shared Obj_NotificationOperation As New NotificationOperation
    Public Shared Obj_SQL As New SQLFunction
#End Region

    Dim _sqlObj As New SQLFunction
    Dim _LogObj As New Logs

    <Conditional("DEBUG")>
    Shared Sub DebugMode()
        If Not Debugger.IsAttached Then
            Debugger.Launch()
        End If

        Debugger.Break()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        'DebugMode()
        Get_Config()
        'Users_Approvals()
        Timer1.Enabled = True
        _LogObj.ProcessLog("Service_Operation", "Service Started Successfuly", DB, ODBC_Con_Str)
    End Sub

    Protected Overrides Sub OnStop()
        _LogObj.ProcessLog("Service_Operation", "Service Stoped", DB, ODBC_Con_Str)
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        Try
            If InProcess = False Then
                '_LogObj.ProcessLog("Timer", "Previous Process Done", DB, ODBC_Con_Str)
                InProcess = True
                '_LogObj.ProcessLog("Timer", "New User Function Start", DB, ODBC_Con_Str)
                Users_New()
                '_LogObj.ProcessLog("Timer", "New User Function Finished", DB, ODBC_Con_Str)
                '_LogObj.ProcessLog("Timer", "User Request Function Start", DB, ODBC_Con_Str)
                Requests()
                '_LogObj.ProcessLog("Timer", "User Request Function Finished", DB, ODBC_Con_Str)
                '_LogObj.ProcessLog("Timer", "User Holds Function Start", DB, ODBC_Con_Str)
                Holds()
                '_LogObj.ProcessLog("Timer", "User Holds Function Finished", DB, ODBC_Con_Str)
                '_LogObj.ProcessLog("Timer", "User Charges Function Start", DB, ODBC_Con_Str)
                Charges_New()
                '_LogObj.ProcessLog("Timer", "User Charges Function Finished", DB, ODBC_Con_Str)
                '_LogObj.ProcessLog("Timer", "User Discharges Function Start", DB, ODBC_Con_Str)
                Charges_Discharge()
                '_LogObj.ProcessLog("Timer", "User Discharges Function Finished", DB, ODBC_Con_Str)
                '_LogObj.ProcessLog("Timer", "User Bills Function Start", DB, ODBC_Con_Str)
                bills()
                '_LogObj.ProcessLog("Timer", "User Bills Function Finished", DB, ODBC_Con_Str)
                '_LogObj.ProcessLog("Timer", "User Payment Function Start", DB, ODBC_Con_Str)
                bills_Payment()
                '_LogObj.ProcessLog("Timer", "User Payment Function Finished", DB, ODBC_Con_Str)
                '_LogObj.ProcessLog("Timer", "User Approvals Function Start", DB, ODBC_Con_Str)
                Users_Approvals()
                '_LogObj.ProcessLog("Timer", "User Approvals Function Finished", DB, ODBC_Con_Str)
                InProcess = False
            Else
                '_LogObj.ProcessLog("Timer", "Previous Process still running", DB, ODBC_Con_Str)
            End If
        Catch ex As Exception
            _LogObj.ProcessLog("Timer", "Timer Error : " & ex.Message, DB, ODBC_Con_Str)
        End Try
    End Sub

    Public Function Get_Config() As Boolean
        Dim Array_Setting() As String = Nothing
        Try
            Array_Setting = Nothing
            fillValue()
            fillSetting()
            Array_Requests = Nothing
            fillArrReq()
            Array_Approvals_ID = Nothing
            Array_Approvals_Txt = Nothing
            fillArrAppr()
            Array_Tags = Nothing
            fillArrTags()
            Return True
        Catch ex As Exception
            SaveTextToFile("D:\Notifications\Notifications_logs.txt", Now & " " & ex.Message)
            _LogObj.ErrorLog("Config", " : Get Config Error: " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True
    End Function

    Public Function fillSetting()

        Dim dt As New DataTable
        dt = New DataTable

        Dim sqlScript As String = ""
        If DB = "SQL" Then
            sqlScript = "SELECT *  FROM tbl_Notification_Enable_Config"
        ElseIf DB = "ORA" Then
            sqlScript = "SELECT *  FROM tbl_Notification_Enable_Config"
        End If
        If sqlScript <> "" Then
            dt = _sqlObj.fillDT(sqlScript, CommandType.Text, ODBC_Con_Str)
            If dt.Rows.Count > 0 Then
                Request_Type = dt.Rows(1).Item(1)
                Users_New_Email_Notifications = dt.Rows(2).Item(1)
                Users_New_SMS_Notifications = dt.Rows(3).Item(1)
                Users_New_Mobile_Notifications = dt.Rows(4).Item(1)
                Charges_New_Email_Notifications = dt.Rows(5).Item(1)
                Charges_New_SMS_Notifications = dt.Rows(6).Item(1)
                Charges_New_Mobile_Notifications = dt.Rows(7).Item(1)
                Charges_Overdue_Email_Notifications = dt.Rows(8).Item(1)
                Charges_Overdue_SMS_Notifications = dt.Rows(9).Item(1)
                Charges_Overdue_Mobile_Notifications = dt.Rows(10).Item(1)
                Charges_Overdue_First_Notifications = dt.Rows(11).Item(1)
                Charges_Overdue_Second_Notifications = dt.Rows(12).Item(1)
                Charges_Overdue_Final_Notifications = dt.Rows(13).Item(1)
                Holds_Email_Notifications = dt.Rows(14).Item(1)
                Holds_SMS_Notifications = dt.Rows(15).Item(1)
                Holds_Mobile_Notifications = dt.Rows(16).Item(1)
                Charges_Discharge_Email_Notifications = dt.Rows(17).Item(1)
                Charges_Discharge_SMS_Notifications = dt.Rows(18).Item(1)
                Charges_Discharge_Mobile_Notifications = dt.Rows(19).Item(1)
                Charges_Renew_Email_Notifications = dt.Rows(20).Item(1)
                Charges_Renew_SMS_Notifications = dt.Rows(21).Item(1)
                Charges_Renew_Mobile_Notifications = dt.Rows(22).Item(1)
                Requests_Email_Notifications = dt.Rows(23).Item(1)
                Requests_SMS_Notifications = dt.Rows(24).Item(1)
                Requests_Mobile_Notifications = dt.Rows(25).Item(1)
                Bills_New_Email_Notifications = dt.Rows(26).Item(1)
                Bills_New_SMS_Notifications = dt.Rows(27).Item(1)
                Bills_New_Mobile_Notifications = dt.Rows(28).Item(1)
                Bills_Pay_Email_Notifications = dt.Rows(29).Item(1)
                Bills_Pay_SMS_Notifications = dt.Rows(30).Item(1)
                Bills_Pay_Mobile_Notifications = dt.Rows(31).Item(1)
                Approval_Email_Notifications = dt.Rows(32).Item(1)
                Approval_SMS_Notifications = dt.Rows(33).Item(1)
                Approval_Mobile_Notifications = dt.Rows(34).Item(1)
            End If
        End If
    End Function

    Public Function fillValue()
        Dim dbType As String = "ORA"
        Dim sqlScript As String = ""
        Dim dt As New DataTable
        If dbType = "SQL" Then
            sqlScript = "SELECT *  FROM tbl_Notification_Value_Config"
        ElseIf dbType = "ORA" Then
            sqlScript = "SELECT *  FROM tbl_Notification_Value_Config"
        End If
        If sqlScript <> "" Then
            dt = _sqlObj.fillDT(sqlScript, CommandType.Text, "Data Source=unic;User Id=ESERVICES;Password=SPL1234;")
            SaveTextToFile("D:\Notifications\Notifications_logs.txt", Now & " " & dt.Rows.Count)
            If dt.Rows.Count > 0 Then
                'Service_logs_FullPath = dt.Rows(0).Item(1)
                ODBC_Con_Str = dt.Rows(1).Item(1)
                Notification_WS_URL = dt.Rows(2).Item(1)
                DB = dt.Rows(3).Item(1)
                Encoding = dt.Rows(4).Item(1)
                Request_Type_Tag = dt.Rows(5).Item(1)
                Email_Tag = dt.Rows(6).Item(1)
                Mobile_Tag = dt.Rows(7).Item(1)
                Charges_Overdue_First_Notifications_Days = dt.Rows(8).Item(1)
                Charges_Overdue_Second_Notifications_Days = dt.Rows(9).Item(1)
                Charges_Overdue_Final_Notifications_Days = dt.Rows(10).Item(1)
                Push_Notification_WS_URL = dt.Rows(11).Item(1)
                'Mobile_MSG_logs_FullPath = dt.Rows(12).Item(1)
                EmailCC = dt.Rows(13).Item(1)
                HostIP = dt.Rows(14).Item(1)
                Port = dt.Rows(15).Item(1)
                Username = dt.Rows(16).Item(1)
                Password = dt.Rows(17).Item(1)
                Email = dt.Rows(18).Item(1)
            End If
        End If
    End Function

    Public Function fillArrReq()

        Dim dt As New DataTable
        dt = New DataTable

        Dim sqlScript As String = ""
        If DB = "SQL" Then
            sqlScript = "SELECT *  FROM [Symphony_Integration].[dbo].[tbl_Notification_Request]"
        ElseIf DB = "ORA" Then
            sqlScript = "SELECT *  FROM tbl_Notification_Request"
        End If
        If sqlScript <> "" Then
            dt = _sqlObj.fillDT(sqlScript, CommandType.Text, ODBC_Con_Str)
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    ReDim Preserve Array_Requests(i)
                    Array_Requests(i) = dt.Rows(i).Item(1)
                Next
            End If
        End If
    End Function

    Public Function fillArrAppr()

        Dim dt As New DataTable
        dt = New DataTable

        Dim sqlScript As String = ""
        If DB = "SQL" Then
            sqlScript = "SELECT *  FROM [Symphony_Integration].[dbo].[tbl_Notification_Approvals]"
        ElseIf DB = "ORA" Then
            sqlScript = "SELECT *  FROM tbl_Notification_Approvals"
        End If
        If sqlScript <> "" Then
            dt = _sqlObj.fillDT(sqlScript, CommandType.Text, ODBC_Con_Str)
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    ReDim Preserve Array_Approvals_ID(i)
                    Array_Approvals_ID(i) = dt.Rows(i).Item(1)
                    ReDim Preserve Array_Approvals_Txt(i)
                    Array_Approvals_Txt(i) = dt.Rows(i).Item(0)
                Next
            End If
        End If
    End Function

    Public Function fillArrTags()
        Array_Tags = {
            "||id||", "||name||",
"||date_time_discharged||",
"||Email||",
"||Mobile||",
            "||Library||",
"||Language||",
"||title||",
"||itemid||",
"||alternative_id||",
"||date_privilege_granted||",
"||date_privilege_expires||",
"||Profile||",
"||Status||",
"||date_time_due||",
"||date_time_charged||",
"||date_placed||",
"||date_expires||",
"||date_responded||",
"||REQ_Type||",
"||REQ_Status||",
"||request_id||",
"||bill_reason||",
"||amount_billed||",
"||date_billed||",
"||payment_amount||",
"||payment_date||",
"||payment_type||",
"||approval||",
"||approval_r||"}

        'Dim dt As New DataTable
        'dt = New DataTable

        'Dim sqlScript As String = ""
        'If DB = "SQL" Then
        '    sqlScript = "SELECT *  FROM [Symphony_Integration].[dbo].[tbl_Notification_Enable_Config]"
        'ElseIf dbType = "ORA" Then
        '    sqlScript = ""
        'End If
        'If sqlScript <> "" Then
        '    dt = _sqlObj.fillDT(sqlScript, CommandType.Text, "DSN=NotificationSQL;UID=sa;PWD=AAS@4770477")
        '    If dt.Rows.Count > 0 Then
        '        For i As Integer = 0 To dt.Rows.Count - 1
        '            ReDim Preserve Array_Tags(i)
        '            Array_Tags(i) = dt.Rows(i).Item(1)
        '        Next
        '    End If
        'End If
    End Function

    Public Function Holds() As Boolean
        Try
            Dim Query As String = ""
            Dim Hold_Key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0
            Dim Canceled As Integer = 0
            Dim Expired As Integer = 0
            Dim Date_expires_changed As Integer = 0
            '============================Get Normal Holds List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[holds] where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from holds where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If
            Dim DS_New_Holds_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_Holds_List IsNot Nothing Then
                If DS_New_Holds_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_Holds_List.Tables(0).Rows.Count - 1
                        Hold_Key = DS_New_Holds_List.Tables(0).Rows(i)("hold_key")
                        id = DS_New_Holds_List.Tables(0).Rows(i)("id")
                        flag_email = DS_New_Holds_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_New_Holds_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_New_Holds_List.Tables(0).Rows(i)("flag_mobile")
                        Canceled = DS_New_Holds_List.Tables(0).Rows(i)("Canceled")
                        Expired = DS_New_Holds_List.Tables(0).Rows(i)("Expired")
                        Date_expires_changed = DS_New_Holds_List.Tables(0).Rows(i)("date_expires_changed")
                        '============================Get Charges New Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id, users.name,hold.date_placed ,hold.date_expires, " & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and hold.library = policy.policy_number ) as Library, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language, " & _
                                    " (select top 1 tag from marc0 where tag_number=245 and marc0.marc in (select marc from catalog where " & _
                                    " catalog.catalog_key=hold.catalog_key)) as title, " & _
                                    " (select top 1 id from items where items.catalog_key=hold.catalog_key and hold.call_sequence=items.call_sequence and " & _
                                    "  hold.copy_number=items.copy_number) as itemid " & _
                                    " from hold,users where users.user_key=hold.user_key  and hold_key=" & Hold_Key
                        Else
                            Query = ""
                        End If
                        If (Holds_Email_Notifications = "1" And flag_email = "0") Or (Holds_Mobile_Notifications = "1" And flag_mobile = "0") Or (Holds_SMS_Notifications = "1" And flag_sms = "0") Then
                            If Canceled = "0" And Expired = "0" And Date_expires_changed = "0" Then
                                Obj_NotificationOperation.Items_Processing("NEW_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "0" And Expired = "0" And Date_expires_changed = "1" Then
                                Obj_NotificationOperation.Items_Processing("NEW_DATE_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "0" And Expired = "1" And Date_expires_changed = "0" Then
                                Obj_NotificationOperation.Items_Processing("EXPIRED_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "0" And Expired = "1" And Date_expires_changed = "1" Then
                                Obj_NotificationOperation.Items_Processing("EXPIRED_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "1" And Expired = "0" And Date_expires_changed = "0" Then
                                Obj_NotificationOperation.Items_Processing("CANCELED_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "1" And Expired = "0" And Date_expires_changed = "1" Then
                                Obj_NotificationOperation.Items_Processing("CANCELED_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "1" And Expired = "1" And Date_expires_changed = "0" Then
                                Obj_NotificationOperation.Items_Processing("CANCELED_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "1" And Expired = "1" And Date_expires_changed = "1" Then
                                Obj_NotificationOperation.Items_Processing("CANCELED_HOLDS", "holds", Query, Holds_Mobile_Notifications, flag_mobile, Holds_Email_Notifications, flag_email, Holds_SMS_Notifications, flag_sms, Hold_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            End If
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Holds", "Holds Error : " + ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True
    End Function

    Public Function Requests() As Boolean
        Try
            Dim Query As String = ""
            Dim Request_Key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0
            Dim Canceled As Integer = 0
            Dim Responded As Integer = 0

            '============================Get Normal Requests List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[requests] where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from requests where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If

            Dim DS_New_Holds_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_Holds_List IsNot Nothing Then
                If DS_New_Holds_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_Holds_List.Tables(0).Rows.Count - 1
                        Request_Key = DS_New_Holds_List.Tables(0).Rows(i)("request_key")
                        id = DS_New_Holds_List.Tables(0).Rows(i)("id")
                        flag_email = DS_New_Holds_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_New_Holds_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_New_Holds_List.Tables(0).Rows(i)("flag_mobile")
                        Canceled = DS_New_Holds_List.Tables(0).Rows(i)("Canceled")
                        Responded = DS_New_Holds_List.Tables(0).Rows(i)("Responded")

                        '============================Get Requests New Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id, users.name,request.date_placed ,request.date_responded,request_id, " & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and request.service_library = policy.policy_number ) as Library, " & _
                                    " (select policy_name from policy where policy_type='RTYP' and request.request_type = policy.policy_number ) as REQ_Type, " & _
                            " (select policy_name from policy where policy_type='RQST' and request.status = policy.policy_number ) as REQ_Status, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language, " & _
                                    " (select top 1 tag from marc0 where tag_number=245 and marc0.marc in (select marc from catalog where " & _
                                    " catalog.catalog_key=request.catalog_key)) as title, " & _
                                    " (select top 1 id from items where items.catalog_key=request.catalog_key and request.call_sequence=items.call_sequence and " & _
                                    "  request.copy_number=items.copy_number) as itemid " & _
                                    " from request,users where users.user_key=request.user_key  and request_key=" & Request_Key
                        Else
                            Query = ""
                        End If
                        If (Requests_Email_Notifications = "1" And flag_email = "0") Or (Requests_Mobile_Notifications = "1" And flag_mobile = "0") Or (Requests_SMS_Notifications = "1" And flag_sms = "0") Then
                            If Canceled = "0" And Responded = "0" Then
                                Obj_NotificationOperation.Items_Processing("NEW_REQ", "requests", Query, Requests_Mobile_Notifications, flag_mobile, Requests_Email_Notifications, flag_email, Requests_SMS_Notifications, flag_sms, Request_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "0" And Responded = "1" Then
                                Obj_NotificationOperation.Items_Processing("RESPONDED_REQ", "requests", Query, Requests_Mobile_Notifications, flag_mobile, Requests_Email_Notifications, flag_email, Requests_SMS_Notifications, flag_sms, Request_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "1" And Responded = "0" Then
                                Query = "select users.id, users.name,[Notification].[dbo].[requests].date_canceled ,[Notification].[dbo].[requests].request_id, " & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language " & _
                                    " from [Notification].[dbo].[requests],users where users.user_key=[Notification].[dbo].[requests].user_key  and [Notification].[dbo].[requests].request_key=" & Request_Key
                                Obj_NotificationOperation.Items_Processing("CANCELED_REQ", "requests", Query, Requests_Mobile_Notifications, flag_mobile, Requests_Email_Notifications, flag_email, Requests_SMS_Notifications, flag_sms, Request_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            ElseIf Canceled = "1" And Responded = "1" Then
                                Query = "select users.id, users.name,[Notification].[dbo].[requests].date_canceled ,[Notification].[dbo].[requests].request_id, " & _
                                   " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                   " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                   " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language " & _
                                   " from [Notification].[dbo].[requests],users where users.user_key=[Notification].[dbo].[requests].user_key  and [Notification].[dbo].[requests].request_key=" & Request_Key
                                Obj_NotificationOperation.Items_Processing("CANCELED_REQ", "requests", Query, Requests_Mobile_Notifications, flag_mobile, Requests_Email_Notifications, flag_email, Requests_SMS_Notifications, flag_sms, Request_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            End If
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Request", "Request Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True

    End Function

    Public Function bills() As Boolean
        Try
            Dim Query As String = ""
            Dim user_Key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0
            Dim bill_number As Integer = 0


            '============================Get Normal Bills List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[bills] where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from bills where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If

            Dim DS_New_bills_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_bills_List IsNot Nothing Then
                If DS_New_bills_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_bills_List.Tables(0).Rows.Count - 1
                        user_Key = DS_New_bills_List.Tables(0).Rows(i)("user_Key")
                        id = DS_New_bills_List.Tables(0).Rows(i)("id")
                        flag_email = DS_New_bills_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_New_bills_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_New_bills_List.Tables(0).Rows(i)("flag_mobile")
                        bill_number = DS_New_bills_List.Tables(0).Rows(i)("bill_number")


                        '============================Get bills New Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id, users.name,bill.date_billed ,bill.bill_number,cast((bill.amount_billed/100) as numeric(20,2)) as amount_billed , " & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and bill.library = policy.policy_number ) as Library, " & _
                                    " (select policy_name from policy where policy_type='BRSN' and bill.reason = policy.policy_number ) as bill_reason, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language, " & _
                                    " (select top 1 tag from marc0 where tag_number=245 and marc0.marc in (select marc from catalog where " & _
                                    " catalog.catalog_key=bill.catalog_key)) as title, " & _
                                    " (select top 1 id from items where items.catalog_key=bill.catalog_key and bill.call_sequence=items.call_sequence and " & _
                                    "  bill.copy_number=items.copy_number) as itemid " & _
                                    " from bill,users where users.user_key=bill.user_key  and bill_number=" & bill_number & " and users.user_key=" & user_Key
                        Else
                            Query = "select " & _
                            "  SIRSI.users.id, SIRSI.users.name, " & _
                            "  SIRSI.bill.date_billed, SIRSI.bill.bill_number, " & _
                            " SIRSI.bill.amount_billed as amount_billed ,  " & _
                            " (select entry from SIRSI.userxinfo where offset=users.address_offset_1 and entry_number=" & Email_Tag & ") as Email, " & _
                            " (select entry from SIRSI.userxinfo where offset=users.address_offset_1 and entry_number=" & Mobile_Tag & ") as Mobile, " & _
                            " (select policy_name from SIRSI.policy where policy_type='LIBR' and bill.library = policy.policy_number ) as Library,  " & _
                            " (select policy_name from SIRSI.policy where policy_type='BRSN' and bill.reason = policy.policy_number ) as bill_reason,  " & _
                            " (select policy_name from SIRSI.policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language,  " & _
                            " (select tag from SIRSI.marc0 where tag_number=245 and marc0.marc in (select marc from SIRSI.catalog where SIRSI.catalog.catalog_key=SIRSI.bill.catalog_key)) as title,  " & _
                            " (select id from SIRSI.item where SIRSI.item.catalog_key=SIRSI.bill.catalog_key and SIRSI.bill.call_sequence=SIRSI.item.call_sequence and SIRSI.bill.copy_number=SIRSI.item.copy_number) as itemid  " & _
                            " from " & _
                            " SIRSI.bill, SIRSI.users where SIRSI.users.user_key=SIRSI.bill.user_key  and bill_number=" & bill_number & " and users.user_key=" & user_Key
                        End If
                        If (Bills_New_Email_Notifications = "1" And flag_email = "0") Or (Bills_New_Mobile_Notifications = "1" And flag_mobile = "0") Or (Bills_New_SMS_Notifications = "1" And flag_sms = "0") Then

                            Obj_NotificationOperation.Items_Processing("NEW_BILL", "bills", Query, Bills_New_Mobile_Notifications, flag_mobile, Bills_New_Email_Notifications, flag_email, Bills_New_SMS_Notifications, flag_sms, user_Key + bill_number, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Bills", "Bills Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True

    End Function

    Public Function bills_Payment() As Boolean
        Try
            Dim Query As String = ""
            Dim bill_payment_key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0



            '============================Get  Bills Payment List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[bills_payment] where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from bills_payment where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If

            Dim DS_New_bills_Pay_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_bills_Pay_List IsNot Nothing Then
                If DS_New_bills_Pay_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_bills_Pay_List.Tables(0).Rows.Count - 1
                        bill_payment_key = DS_New_bills_Pay_List.Tables(0).Rows(i)("bill_payment_key")
                        id = DS_New_bills_Pay_List.Tables(0).Rows(i)("id")
                        flag_email = DS_New_bills_Pay_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_New_bills_Pay_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_New_bills_Pay_List.Tables(0).Rows(i)("flag_mobile")

                        '============================Get bills Payment New Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id, users.name,billpayment.payment_date ,billpayment.bill_number,cast((billpayment.payment_amount/100) as numeric(20,2)) as payment_amount , " & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and billpayment.library = policy.policy_number ) as Library, " & _
                                    " (select policy_name from policy where policy_type='PTYP' and billpayment.payment_type = policy.policy_number ) as payment_type, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language " & _
                                    " from billpayment,users where users.user_key=billpayment.user_key  and billpayment.bill_payment_key=" & bill_payment_key
                        Else
                            Query = ""
                        End If
                        If (Bills_Pay_Email_Notifications = "1" And flag_email = "0") Or (Bills_Pay_Mobile_Notifications = "1" And flag_mobile = "0") Or (Bills_Pay_SMS_Notifications = "1" And flag_sms = "0") Then

                            Obj_NotificationOperation.Items_Processing("BILL_PAY", "bills_payment", Query, Bills_Pay_Mobile_Notifications, flag_mobile, Bills_Pay_Email_Notifications, flag_email, Bills_Pay_SMS_Notifications, flag_sms, bill_payment_key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Bills_Payment", "Bills Payment Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True

    End Function

    Public Function Users_New()
        Try
            Dim Query As String = ""
            Dim User_Key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0
            '============================Get New Users List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[users_new] where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from users where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If

            Dim DS_New_Users_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_Users_List IsNot Nothing Then
                If DS_New_Users_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_Users_List.Tables(0).Rows.Count - 1
                        User_Key = DS_New_Users_List.Tables(0).Rows(i)("user_key")
                        id = DS_New_Users_List.Tables(0).Rows(i)("id")
                        flag_email = DS_New_Users_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_New_Users_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_New_Users_List.Tables(0).Rows(i)("flag_mobile")
                        '============================Get New Users Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id,users.alternative_id,users.date_privilege_granted, " & _
                                    " users.date_privilege_expires , users.name,users.pin," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Request_Type_Tag & " ),'') as RequestType," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and users.library = policy.policy_number ) as Library," & _
                                    " (select policy_name from policy where policy_type='UPRF' and users.profile = policy.policy_number ) as Profile," & _
                                    " (select top 1 delinquent from userstatus where userstatus.user_key=users.user_key ) as Status, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language " & _
                                    " from users where user_key=" & User_Key
                        Else
                            Query = ""
                        End If
                        Obj_NotificationOperation.Items_Processing("NEW_USERS", "users_new", Query, Users_New_Mobile_Notifications, flag_mobile, Users_New_Email_Notifications, flag_email, Users_New_SMS_Notifications, flag_sms, User_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("User_New", "User New Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True
    End Function

    Public Function Charges_New() As Boolean
        Try
            Dim Query As String = ""
            Dim Charge_Key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0
            Dim renewed As Integer = 0
            '============================Get Charges List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[charges] where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from charges where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If

            Dim DS_New_Charges_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_Charges_List IsNot Nothing Then
                If DS_New_Charges_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_Charges_List.Tables(0).Rows.Count - 1
                        Charge_Key = DS_New_Charges_List.Tables(0).Rows(i)("charge_key")
                        id = DS_New_Charges_List.Tables(0).Rows(i)("id")
                        flag_email = DS_New_Charges_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_New_Charges_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_New_Charges_List.Tables(0).Rows(i)("flag_mobile")
                        renewed = DS_New_Charges_List.Tables(0).Rows(i)("renewed")
                        '============================Get Charges New Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id, users.name,charge.date_time_charged ,charge.date_time_due, " & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and charge.library = policy.policy_number ) as Library, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language, " & _
                                    " (select top 1 tag from marc0 where tag_number=245 and marc0.marc in (select marc from catalog where " & _
                                    " catalog.catalog_key=charge.catalog_key)) as title, " & _
                                    " (select top 1 id from items where items.catalog_key=charge.catalog_key and charge.call_sequence=items.call_sequence and " & _
                                    "  charge.copy_number=items.copy_number) as itemid " & _
                                    " from charge,users where users.user_key=charge.user_key  and charge_key=" & Charge_Key
                        Else
                            Query = ""
                        End If
                        If (Charges_New_Email_Notifications = "1" And flag_email = "0") Or (Charges_New_Mobile_Notifications = "1" And flag_mobile = "0") Or (Charges_New_SMS_Notifications = "1" And flag_sms = "0") Then
                            If renewed = "0" Then
                                Obj_NotificationOperation.Items_Processing("NEW_CHARGES", "charges", Query, Charges_New_Mobile_Notifications, flag_mobile, Charges_New_Email_Notifications, flag_email, Charges_New_SMS_Notifications, flag_sms, Charge_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            End If
                        End If
                        If (Charges_Renew_Email_Notifications = "1" And flag_email = "0") Or (Charges_Renew_Mobile_Notifications = "1" And flag_mobile = "0") Or (Charges_Renew_SMS_Notifications = "1" And flag_sms = "0") Then
                            If renewed = "1" Then
                                Obj_NotificationOperation.Items_Processing("RENEWED_CHARGES", "charges", Query, Charges_Renew_Mobile_Notifications, flag_mobile, Charges_Renew_Email_Notifications, flag_email, Charges_Renew_SMS_Notifications, flag_sms, Charge_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                            End If
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Charges_New", "Charges New Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True

    End Function

    Public Function Charges_Discharge() As Boolean
        Try
            Dim Query As String = ""
            Dim Chargehist_Key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0
            '============================Get Charges List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[charges_discharge] where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from charges_discharge where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If

            Dim DS_New_ChargesHist_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_ChargesHist_List IsNot Nothing Then
                If DS_New_ChargesHist_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_ChargesHist_List.Tables(0).Rows.Count - 1
                        Chargehist_Key = DS_New_ChargesHist_List.Tables(0).Rows(i)("chargehist_key")
                        id = DS_New_ChargesHist_List.Tables(0).Rows(i)("id")
                        flag_email = DS_New_ChargesHist_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_New_ChargesHist_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_New_ChargesHist_List.Tables(0).Rows(i)("flag_mobile")

                        '============================Get Charges New Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id, users.name,chargehist.date_time_discharged , " & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and chargehist.library = policy.policy_number ) as Library, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language, " & _
                                    " (select top 1 tag from marc0 where tag_number=245 and marc0.marc in (select marc from catalog where " & _
                                    " catalog.catalog_key=chargehist.catalog_key)) as title, " & _
                                    " (select top 1 id from items where items.catalog_key=chargehist.catalog_key and chargehist.call_sequence=items.call_sequence and " & _
                                    "  chargehist.copy_number=items.copy_number) as itemid " & _
                                    " from chargehist,users where users.user_key=chargehist.user_key  and chargehist_key=" & Chargehist_Key
                        Else
                            Query = ""
                        End If
                        If (Charges_Discharge_Email_Notifications = "1" And flag_email = "0") Or (Charges_Discharge_Mobile_Notifications = "1" And flag_mobile = "0") Or (Charges_Discharge_SMS_Notifications = "1" And flag_sms = "0") Then
                            Obj_NotificationOperation.Items_Processing("NEW_DISCHARGES", "charges_discharge", Query, Charges_New_Mobile_Notifications, flag_mobile, Charges_New_Email_Notifications, flag_email, Charges_New_SMS_Notifications, flag_sms, Chargehist_Key, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Charges_Discharge", "Charges_Discharge Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True

    End Function

    Public Function Charges_Overdue() As Boolean
        Try
            Dim Query As String = ""
            Dim Charge_Key As Integer = 0
            Dim id As Integer = 0
            Dim flag_email_fnotice As Integer = 0
            Dim flag_email_snotice As Integer = 0
            Dim flag_email_lnotice As Integer = 0
            Dim flag_sms_fnotice As Integer = 0
            Dim flag_sms_snotice As Integer = 0
            Dim flag_sms_lnotice As Integer = 0
            Dim flag_mobile_fnotice As Integer = 0
            Dim flag_mobile_snotice As Integer = 0
            Dim flag_mobile_lnotice As Integer = 0
            Dim due_date As New DateTime
            '============================Get Charges Overdue Notice List=====================================
            If DB = "SQL" Then
                Query = "select * from [Notification].[dbo].[charges] where flag_email_fnotice=0 or flag_email_snotice=0 or flag_email_lnotice=0 or flag_sms_fnotice=0 or flag_sms_snotice=0 or flag_sms_lnotice=0 or flag_mobile_fnotice=0 or flag_mobile_snotice=0"
            Else
                Query = "select * from charges where flag_email_fnotice=0 or flag_email_snotice=0 or flag_email_lnotice=0 or flag_sms_fnotice=0 or flag_sms_snotice=0 or flag_sms_lnotice=0 or flag_mobile_fnotice=0 or flag_mobile_snotice=0"
            End If

            Dim DS_New_Charges_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_New_Charges_List IsNot Nothing Then
                If DS_New_Charges_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_New_Charges_List.Tables(0).Rows.Count - 1
                        Charge_Key = DS_New_Charges_List.Tables(0).Rows(i)("charge_key")
                        id = DS_New_Charges_List.Tables(0).Rows(i)("id")
                        flag_email_fnotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_email_fnotice")
                        flag_email_snotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_email_snotice")
                        flag_email_lnotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_email_lnotice")
                        flag_sms_fnotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_sms_fnotice")
                        flag_sms_snotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_sms_snotice")
                        flag_sms_lnotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_sms_lnotice")
                        flag_mobile_fnotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_mobile_fnotice")
                        flag_mobile_snotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_mobile_snotice")
                        flag_mobile_lnotice = DS_New_Charges_List.Tables(0).Rows(i)("flag_mobile_lnotice")
                        due_date = DS_New_Charges_List.Tables(0).Rows(i)("date_time_due")


                        '============================Get Overdue Days =============================
                        Dim NumOfHourssDue As Long = DateDiff(DateInterval.Hour, Now, due_date)
                        Dim NumOfDaysDue As Long = NumOfHourssDue / 24
                        If flag_email_fnotice = "0" Or flag_email_snotice = "0" Or flag_email_lnotice = "0" Or flag_sms_fnotice = "0" Or flag_sms_snotice = "0" Or flag_sms_lnotice = "0" Or flag_mobile_fnotice = "0" Or flag_mobile_snotice = "0" Or flag_mobile_lnotice = "0" Then
                            Obj_NotificationOperation.Overdue_Processing(Charges_Overdue_Email_Notifications, flag_email_fnotice, flag_email_snotice, flag_email_lnotice, NumOfDaysDue, Charges_Overdue_First_Notifications, Charges_Overdue_First_Notifications_Days, Charges_Overdue_Second_Notifications, Charges_Overdue_Second_Notifications_Days, Charges_Overdue_Final_Notifications, Charges_Overdue_Final_Notifications_Days)
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Charges_Overdue", "Charges_Overdue Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True
    End Function

    Public Function Users_Approvals()
        Try
            Dim Query As String = ""
            Dim User_id As String = "0"
            Dim id As Integer = 0
            Dim flag_email As Integer = 0
            Dim flag_sms As Integer = 0
            Dim flag_mobile As Integer = 0
            Dim Status As Integer = 0
            Dim Reason As String = 0
            Dim opt As Integer = 0
            '============================Get New Users List=====================================
            If DB = "SQL" Then
                Query = "select * from users_opt where flag_email=0 or flag_sms=0 or flag_mobile=0"
            Else
                Query = "select * from users_opt where flag_email=0 or flag_sms=0 or flag_mobile=0"
            End If
            Dim DS_Approval_List As DataSet = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_Approval_List IsNot Nothing Then
                If DS_Approval_List.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To DS_Approval_List.Tables(0).Rows.Count - 1
                        id = DS_Approval_List.Tables(0).Rows(i)("id")
                        flag_email = DS_Approval_List.Tables(0).Rows(i)("flag_email")
                        flag_sms = DS_Approval_List.Tables(0).Rows(i)("flag_sms")
                        flag_mobile = DS_Approval_List.Tables(0).Rows(i)("flag_mobile")
                        Status = DS_Approval_List.Tables(0).Rows(i)("status")
                        Reason = DS_Approval_List.Tables(0).Rows(i)("reason")
                        opt = DS_Approval_List.Tables(0).Rows(i)("opt")
                        User_id = DS_Approval_List.Tables(0).Rows(i)("user_id")
                        '============================Get New Users Info =============================
                        Query = ""
                        If DB = "SQL" Then
                            Query = "select users.id,users.alternative_id,users.date_privilege_granted, " & _
                                    " users.date_privilege_expires , users.name,users.pin," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Email_Tag & " ),'') as Email," & _
                                    " isnull((select top 1 isnull(entry,'') from userxinfo where offset_key=users.address_offset_1 and tag_number=" & Mobile_Tag & " ),'') as Mobile," & _
                                    " (select policy_name from policy where policy_type='LIBR' and users.library = policy.policy_number ) as Library," & _
                                    " (select policy_name from policy where policy_type='UPRF' and users.profile = policy.policy_number ) as Profile," & _
                                    " (select top 1 delinquent from userstatus where userstatus.user_key=users.user_key ) as Status, " & _
                                    " (select policy_name from policy where policy_type='LANG' and users.preferred_language = policy.policy_number ) as Language " & _
                                    " , reason from users ,Notification.dbo.users_opt where " & _
                                    " users.id collate SQL_Latin1_General_CP1_CI_AS =Notification.dbo.users_opt.user_id " & _
                                    " and Notification.dbo.users_opt.id=" & id & " and  users.id='" & User_id & "'"
                        Else
                            Query = "select SIRSI.users.id,SIRSI.users.alternative_id,SIRSI.users.date_privilege_granted,SIRSI.users.date_privilege_expires,SIRSI.users.name,SIRSI.users.pin," & _
                                    " (select  entry from SIRSI.userxinfo where offset=SIRSI.users.address_offset_1 and entry_number=" & Email_Tag & " ) as Email," & _
                                    " (select entry from SIRSI.userxinfo where offset=SIRSI.users.address_offset_1 and entry_number=" & Mobile_Tag & ") as Mobile," & _
                                    " (select policy_name from SIRSI.policy where policy_type='LIBR' and SIRSI.users.library = policy.policy_number ) as Library," & _
                                    " (select policy_name from SIRSI.policy where policy_type='UPRF' and SIRSI.users.profile = policy.policy_number ) as Profile," & _
                                    " (select delinquent from SIRSI.userstatus where userstatus.user_key=SIRSI.users.user_key ) as Status, " & _
                                    " (select policy_name from SIRSI.policy where policy_type='LANG' and SIRSI.users.preferred_language = SIRSI.policy.policy_number ) as Language," & _
                                    " ESERVICES.USERS_OPT.reason from SIRSI.users, ESERVICES.USERS_OPT" & _
                                    " where users.id =ESERVICES.USERS_OPT.user_id and ESERVICES.USERS_OPT.id='" & id & "' and  users.id = '" & User_id & "'"
                        End If
                        Dim Process_Type As String = ""
                        For u As Integer = 0 To Array_Approvals_ID.Length - 1
                            If Array_Approvals_ID(u) = opt Then
                                Process_Type = Array_Approvals_Txt(u)
                            End If
                        Next
                        If Status = 2 Then
                            Obj_NotificationOperation.Items_Processing(Process_Type, "users_opt", Query, Approval_Mobile_Notifications, flag_mobile, Approval_Email_Notifications, flag_email, Approval_SMS_Notifications, flag_sms, User_id, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                        Else
                            Obj_NotificationOperation.Items_Processing(Process_Type & "_R", "users_opt", Query, Approval_Mobile_Notifications, flag_mobile, Approval_Email_Notifications, flag_email, Approval_SMS_Notifications, flag_sms, User_id, id, Array_Tags, Array_Requests, Array_Approvals_Txt, ODBC_Con_Str, DB, HostIP, Port, Username, Password, Email, EmailCC)
                        End If
                    Next
                End If
            End If
            Return True
        Catch ex As Exception
            _LogObj.ErrorLog("Approvals", "Approvals Error : " & ex.Message, DB, ODBC_Con_Str)
            Return False
        End Try
        Return True
    End Function


    Public Function SaveTextToFile(ByVal FullPath As String, ByVal new_line As String) As Boolean


        Dim bAns As Boolean = False
        Dim objReader As StreamWriter
        Try


            objReader = New StreamWriter(FullPath.Trim, True, System.Text.Encoding.UTF8)
            objReader.Write(new_line & ControlChars.NewLine)
            objReader.Close()
            bAns = True
        Catch Ex As Exception

        End Try
        Return bAns
    End Function

End Class
