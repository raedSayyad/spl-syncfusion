﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmPayment.aspx.vb" Inherits="eServices.wfrmPayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%=PageTitle%></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form id="form1" runat="server">
        <div class="branding">
            <img src="http://<%=ServNam %>/WebCat_Images/Arabic/Other/Miscib/LIBLOGO.gif" />
        </div>
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div style="width: 100%; height: 100%!important">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <h3><%=PageTitle%></h3>
                        <hr />
                        <asp:GridView ID="grvBills" runat="server">
                        </asp:GridView>
                        <asp:HiddenField ID="UserID" runat="server" />
                        <asp:HiddenField ID="billV" runat="server" />
                        <asp:HiddenField ID="billN" runat="server" />
                        <asp:HiddenField ID="billR" runat="server" />
                        <asp:HiddenField ID="billVFinal" runat="server" />
                        <asp:HiddenField ID="billNFinal" runat="server" />
                        <asp:HiddenField ID="billRFinal" runat="server" />
                        <asp:Label ID="lblAmmount" runat="server" Text="Amount to Pay: "></asp:Label>
                        <asp:TextBox ID="txtAmount" runat="server" ReadOnly="true"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnPay" runat="server" Text="Pay" />
                        <br />
                        <asp:Label ID="LBLMsg" runat="server" Text=""></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                    <ProgressTemplate>
                        <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                            <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
        </div>
    </form>
</body>
</html>