﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmNotification.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmNotification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHead" runat="server">
    <title>Notification</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" type="text/css" />
    <link type="text/css" href="../Content/ui.all.css" rel="stylesheet" />
    <link href="../Content/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/ui.progressbar.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.signalR-2.2.1.min.js"></script>
    <script src="/signalr/hubs"></script>
    <script type="text/javascript">
        function StartBar() {
            // initialize progress bar
            $("#progressbar").progressbar({ value: 0 });
            // initialize the connection to the server
            var progressNotifier = $.connection.progressHub;
            // client-side sendMessage function that will be called from the server-side
            progressNotifier.client.sendMessage = function (message) {
                // update progress
                UpdateProgress(message);
            };
            // establish the connection to the server and start server-side operation
            $.connection.hub.start().done(function () {
                // call the method CallLongOperation defined in the Hub
                progressNotifier.server.callLongOperation();
            });
        }
        function UpdateProgress(message) {
            // get result div
            var result = $("#result");
            // set message
            result.html(message);
            // get progress bar
            var value = $("#progressbar").progressbar("option", "value");
            // update progress bar
            $("#progressbar").progressbar("value", value + 1);
        }
    </script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-switch.css" rel="stylesheet" />
    <%--<script src="Scripts/jquery-1.11.0.min.js"></script>--%>
    <script src="Scripts/bootstrap-switch.js"></script>
    <script>
        function pageLoad() {
            $("#<%=CheckBox1.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox2.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox3.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox4.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox5.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox6.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox7.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox8.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox9.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox10.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox11.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox12.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox13.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox14.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox15.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox16.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox17.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox18.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox19.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox20.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox21.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox22.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox23.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox24.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox25.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox26.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox27.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox28.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox29.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox30.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox31.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox32.ClientID%>").bootstrapSwitch();
            $("#<%=CheckBox33.ClientID%>").bootstrapSwitch();
        }
    </script>
    <style type="text/css">
        #LayoutSection_ControlsSection_DefaultAccordion img {
            float: left;
            height: 40px;
            padding-right: 6px;
        }

        .emp-list {
            border-bottom: 1px solid #BBBCBB;
            margin-bottom: 9px;
            padding-bottom: 9px;
        }

            .emp-list:last-child {
                border-bottom: none;
                padding-bottom: 0;
            }

        .time-panel {
            float: right;
            color: #2382C3;
        }

        .headername {
            font-size: 16px;
            font-weight: 600;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <header class="major">
        <h3>Unicorn Notification Windows Service</h3>
        <p></p>
    </header>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="PanelInstall" runat="server" Visible="False">
                    <h4>Installing Unicorn Notification Windows Service</h4>
                    <asp:Panel ID="PanelDB" runat="server">
                        <p>
                            Fill Database Server Details:
                        </p>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="DataBase Type"></asp:Label>
                                </td>
                                <td>
                                    <ej:DropDownList ID="ddlDBType" runat="server">
                                        <Items>
                                            <ej:DropDownListItem Text="SQL Server" Value="SQL"></ej:DropDownListItem>
                                            <ej:DropDownListItem Text="Oracle" Value="ORA"></ej:DropDownListItem>
                                        </Items>
                                    </ej:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="DSN"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDSN" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text="User ID"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="Password"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right;">
                                    <asp:Label ID="lblDBCheck" runat="server" Text=""></asp:Label>
                                    <asp:Button ID="btnDBCheck" runat="server" Text="Next" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelProcess" runat="server" Visible="False">
                        <table>
                            <tr>
                                <td style="text-align: center;">
                                    <asp:Button ID="btnInstall" runat="server" Text="Install" OnClientClick="StartBar();document.getElementById('navButton').className += ' hide';" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV1" align="center" valign="middle" class="ModalPopupBG" runat="server">
                    <div style="width: 80%; margin-top: 30%;">
                        <div id="result" style="font-family: Tahoma; font-size: 0.9em; color: Black; margin-top: 30px; padding-bottom: 5px">
                        </div>
                        <div id="progressbar" style="width: 100%; height: 20px"></div>
                        <br />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:Panel ID="PanelManage" runat="server">
                    <hr />
                    <div>
                        <h5>Status Mangement</h5>
                        <asp:Label ID="lblWinSrvStatustxt" runat="server" Text="Current Status: "></asp:Label>
                        <asp:Label ID="lblWinSrvStatus" runat="server" Text="Status"></asp:Label>
                        <br />
                        <br />
                        <ej:ToggleButton ID="tgbService" runat="server" Size="small" DefaultText="Run" ActiveText="Stop" ContentType="TextAndImage" DefaultPrefixIcon="e-icon e-mediaplay" ActivePrefixIcon="e-icon e-stop" ToggleState="false" Type="Submit" ClientSideOnClick="document.getElementById('navButton').className += ' hide';"></ej:ToggleButton>
                        <hr />
                    </div>
                    <h5>Configuration and Setting</h5>
                    <ej:Accordion ID="Accordion1" runat="server" SelectedItemIndex="-1" Collapsible="true">
                        <Items>
                            <ej:AccordionItem Text="Configuration">
                                <ContentSection>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>Notification Web Service URL</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Push Notification Web Service URL</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Request Type Tag Number</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email Tag Number</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Mobile Tag Number</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Charges Overdue First Notifications Days</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox><asp:CheckBox ID="CheckBox28" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Charges Overdue Second Notifications Days</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox><asp:CheckBox ID="CheckBox29" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Charges Overdue Final Notifications Days</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox><asp:CheckBox ID="CheckBox30" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email in BCC</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email Host</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Port</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox11" runat="server"  ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Username</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Password</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox13" runat="server" TextMode="Password"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center;">
                                                    <asp:Button ID="Button3" runat="server" Text="Submit" OnClientClick="document.getElementById('navButton').className += ' hide';" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                                        <hr>
                                    </div>
                                </ContentSection>
                            </ej:AccordionItem>
                            <ej:AccordionItem Text="Email Notification Setting">
                                <ContentSection>
                                    <table>
                                        <tr>
                                            <td>New Users
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox31" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>New Charges
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Holds
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox2" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Discharge
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox3" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Renew
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox4" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Requests
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox5" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>NEW Bills
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox6" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bills PAY
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox7" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Approval
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox8" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Overdue
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox9" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <div style="text-align: center">
                                        <asp:Button ID="btnEmail" runat="server" Text="Submit" OnClientClick="document.getElementById('navButton').className += ' hide';" />
                                        <asp:Label ID="lblEmailEr" runat="server" Text=""></asp:Label>
                                    </div>
                                </ContentSection>
                            </ej:AccordionItem>
                            <ej:AccordionItem Text="Mobile Notification Setting">
                                <ContentSection>
                                    <table>
                                        <tr>
                                            <td>New Users
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox32" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>New Charges
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox10" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Holds
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox11" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Discharge
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox12" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Renew
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox13" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Requests
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox14" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>NEW Bills
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox15" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bills PAY
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox16" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Approval
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox17" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Overdue
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox18" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="text-align: center">
                                        <asp:Button ID="btnMobile" runat="server" Text="Submit" OnClientClick="document.getElementById('navButton').className += ' hide';" />
                                        <asp:Label ID="lblMobEr" runat="server" Text=""></asp:Label>
                                    </div>
                                </ContentSection>
                            </ej:AccordionItem>
                            <ej:AccordionItem Text="SMS Notification Setting">
                                <ContentSection>
                                    <table>
                                        <tr>
                                            <td>New Users
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox33" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>New Charges
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox19" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Holds
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox20" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Discharge
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox21" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Renew
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox22" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Requests
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox23" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>NEW Bills
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox24" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bills PAY
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox25" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Approval
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox26" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Charges Overdue
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="CheckBox27" runat="server" CssClass="make-switch" Checked="true" data-on-color="default" data-off-color="danger" data-on-text="No" data-off-text="Yes" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="text-align: center">
                                        <asp:Button ID="btnSms" runat="server" Text="Submit" OnClientClick="document.getElementById('navButton').className += ' hide';" />
                                        <asp:Label ID="lblSmsEr" runat="server" Text=""></asp:Label>
                                    </div>
                                </ContentSection>
                            </ej:AccordionItem>
                        </Items>
                    </ej:Accordion>
                    <hr>
                    <h5>Templates</h5>
                    <ej:Accordion ID="Accordion2" runat="server" SelectedItemIndex="-1" Collapsible="true">
                        <Items>
                            <ej:AccordionItem Text="Email Tempaltes">
                                <ContentSection>
                                    <iframe id="EmailTemp" src="wfrmEmailTemp.aspx" width="100%" height="730px"></iframe>
                                </ContentSection>
                            </ej:AccordionItem>
                            <ej:AccordionItem Text="Mobile Tempaltes">
                                <ContentSection>
                                    <iframe id="MobileTemp" src="wfrmMobileTemp.aspx" width="100%" height="700px"></iframe>
                                </ContentSection>
                            </ej:AccordionItem>
                            <ej:AccordionItem Text="SMS Tempaltes">
                                <ContentSection>
                                    <iframe id="SMSTemp" src="wfrmSMSTemp.aspx" width="100%" height="700px"></iframe>
                                </ContentSection>
                            </ej:AccordionItem>
                        </Items>
                    </ej:Accordion>
                    <hr>
                    <h5>Service Log</h5>
                    <ej:Accordion ID="Accordion3" runat="server" SelectedItemIndex="-1" Collapsible="true">
                        <Items>
                            <ej:AccordionItem Text="Process Log">
                                <ContentSection>
                                    <iframe id="ProcessLog" src="wfrmNotiProcessLog.aspx" width="100%" height="1100px"></iframe>
                                </ContentSection>
                            </ej:AccordionItem>
                            <ej:AccordionItem Text="Error Log">
                                <ContentSection>
                                    <iframe id="ErrorLog" src="wfrmNotiErrorLog.aspx" width="100%" height="1100px"></iframe>
                                </ContentSection>
                            </ej:AccordionItem>
                        </Items>
                    </ej:Accordion>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="uprgWorkRequest" AssociatedUpdatePanelID="UpdatePanel2" runat="server">
            <ProgressTemplate>
                <div id="IMGDIV2" align="center" valign="middle" class="ModalPopupBG" runat="server">
                    <asp:Image ID="Image1" runat="server" ImageUrl="../Images/loading.gif" />
                    Processing 
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>

