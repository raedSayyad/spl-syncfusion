﻿Public Class NotificationOperation
    Dim Obj_SQL As New SQLFunction
    Dim Obj_InternetOperation As New InternetOperation
    Dim Obj_MobileOperation As New MobileOperation
    Dim Obj_Template As New Template
    Dim Obj_Logs As New Logs

    Public Function UUID_Notification(ByVal Mobile_txt() As String, ByVal Users_DeviceID() As String, ByVal ID As String, ByVal Process_Type As String, ByVal table As String, ByVal ODBC_Con_Str As String)
        Dim Send_UUID_Notification As Boolean = True
        Dim Add_UUID_Notification_Ara As Boolean = True
        Dim Add_UUID_Notification_Eng As Boolean = True
        If Mobile_txt IsNot Nothing Then
            If Mobile_txt(0) <> "" Then
                'Add Notification for Eng and Ara
                If Mobile_txt(1) <> "" And Mobile_txt(2) <> "" Then
                    Add_UUID_Notification_Ara = Obj_MobileOperation.Add_UsertDeviceID_Notifications_Ar_En(Users_DeviceID, Mobile_txt(1), "ara", ID, Process_Type)
                    Add_UUID_Notification_Eng = Obj_MobileOperation.Add_UsertDeviceID_Notifications_Ar_En(Users_DeviceID, Mobile_txt(2), "eng", ID, Process_Type)
                    If Add_UUID_Notification_Ara = False Then 'Notification Add Error
                        Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=5 where id=" & ID, ODBC_Con_Str)
                    End If
                    If Add_UUID_Notification_Eng = False Then 'Notification Add Error
                        Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=5 where id=" & ID, ODBC_Con_Str)
                    End If
                    If Add_UUID_Notification_Eng = True And Add_UUID_Notification_Ara = True Then
                        Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=1 where id=" & ID, ODBC_Con_Str)
                    End If
                Else
                    Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=10 where id=" & ID, ODBC_Con_Str)
                End If

                Send_UUID_Notification = Obj_MobileOperation.Set_UsertDeviceID_Notifications(Users_DeviceID, Mobile_txt(0))
                If Send_UUID_Notification = True Then ' UUID Notification Sent Success
                    Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=1 where id=" & ID, ODBC_Con_Str)
                ElseIf Send_UUID_Notification = False Then ' UUID Notification Sent Success
                    Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=4 where id=" & ID, ODBC_Con_Str)
                End If
            ElseIf Mobile_txt(0) = "" Then
                Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=10 where id=" & ID, ODBC_Con_Str)
            End If
        ElseIf Mobile_txt Is Nothing Then
            Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=10 where id=" & ID, ODBC_Con_Str)
        End If
    End Function

    Public Function Holds_Mobile_Items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Mobile_Txt() As String = Nothing
        If Process_Type = "NEW_HOLDS" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Holds_New_Mobile", "txt", "Holds_New_Template_AR", "Holds_New_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "EXPIRED_HOLDS" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Holds_Expired_Mobile", "txt", "Holds_Expired_Template_AR", "Holds_Expired_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "CANCELED_HOLDS" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Holds_Canceled_Mobile", "txt", "Holds_Canceled_Template_AR", "Holds_Canceled_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "NEW_DATE_HOLDS" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Holds_New_Date_Mobile", "txt", "Holds_New_Date_Template_AR", "Holds_New_Date_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Mobile_Txt
    End Function

    Public Function Request_Mobile_Items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Mobile_Txt() As String = Nothing
        If Process_Type = "NEW_REQ" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Requests_New_Mobile", "txt", "Requests_New_Template_AR", "Requests_New_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), "", DS_Info.Tables(0).Rows(0)("date_responded"), DS_Info.Tables(0).Rows(0)("REQ_Status"), DS_Info.Tables(0).Rows(0)("REQ_Type"), DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "RESPONDED_REQ" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Requests_Responded_Mobile", "txt", "Requests_Responded_Template_AR", "Requests_Responded_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), "", DS_Info.Tables(0).Rows(0)("date_responded"), DS_Info.Tables(0).Rows(0)("REQ_Status"), DS_Info.Tables(0).Rows(0)("REQ_Type"), DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "CANCELED_REQ" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Requests_Canceled_Mobile", "txt", "Requests_Canceled_Template_AR", "Requests_Canceled_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Mobile_Txt
    End Function

    Public Function Charges_Mobile_Items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Mobile_Txt() As String = Nothing
        If Process_Type = "NEW_CHARGES" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Charges_New_Mobile", "txt", "Charges_New_Template_AR", "Charges_New_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", DS_Info.Tables(0).Rows(0)("date_time_charged"), "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "RENEWED_CHARGES" Then
            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Charges_Renew_Mobile", "txt", "Charges_Renew_Template_AR", "Charges_Renew_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", DS_Info.Tables(0).Rows(0)("date_time_charged"), "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Mobile_Txt
    End Function

    Public Function Approval_Mobile_items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Email_FullPath() As String = Nothing
        If Process_Type = "APPROVALS_U" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval_U", "Approval_U", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_U_R" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval_U_R", "Approval_U_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_R" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval_R", "Approval_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_R_R" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval_R_R", "Approval_R_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_C" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval_C", "Approval_C", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_C_R" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval_C_R", "Approval_C_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_H" Or Process_Type = "APPROVALS_CL" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval", "Approval", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_H_R" Or Process_Type = "APPROVALS_CL_R" Then
            Email_FullPath = Obj_Template.Create_Template("Mobile", Process_Type, "txt", "Approval_R", "Approval_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Email_FullPath
    End Function

    Public Function Holds_Email_Items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Email_FullPath() As String = Nothing
        If Process_Type = "NEW_HOLDS" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Holds_New_Mobile", "html", "Holds_New", "Holds_New", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "EXPIRED_HOLDS" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Holds_Expired_Mobile", "html", "Holds_Expired", "Holds_Expired", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "CANCELED_HOLDS" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Holds_Canceled_Mobile", "html", "Holds_Canceled", "Holds_Canceled", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "NEW_DATE_HOLDS" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Holds_New_Date_Mobile", "html", "Holds_New_Date", "Holds_New_Date", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), DS_Info.Tables(0).Rows(0)("date_expires"), "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Email_FullPath
    End Function

    Public Function Request_Email_Items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Email_FullPath() As String = Nothing
        If Process_Type = "NEW_REQ" Then
            'Email_FullPath = Obj_Template.Create_Template("Email", "Requests_New_Mobile", "html", "Requests_New_Template_AR", "Requests_New_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), "", DS_Info.Tables(0).Rows(0)("date_responded"), DS_Info.Tables(0).Rows(0)("REQ_Status"), DS_Info.Tables(0).Rows(0)("REQ_Type"), DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "")
        End If
        If Process_Type = "RESPONDED_REQ" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Requests_Responded_Mobile", "html", "Requests_Responded", "Requests_Responded", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), "", DS_Info.Tables(0).Rows(0)("date_responded"), DS_Info.Tables(0).Rows(0)("REQ_Status"), DS_Info.Tables(0).Rows(0)("REQ_Type"), DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "CANCELED_REQ" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Requests_Canceled_Mobile", "html", "Requests_Responded", "Requests_Responded", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Email_FullPath
    End Function

    Public Function Charges_Email_Items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Email_FullPath() As String = Nothing
        If Process_Type = "NEW_CHARGES" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Charges_New_Mobile", "html", "Charges_New", "Charges_New", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", DS_Info.Tables(0).Rows(0)("date_time_charged"), "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "RENEWED_CHARGES" Then
            Email_FullPath = Obj_Template.Create_Template("Email", "Charges_Renew_Mobile", "txt", "Charges_Renew", "Charges_Renew", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", DS_Info.Tables(0).Rows(0)("date_time_charged"), "", "", DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Email_FullPath
    End Function

    Public Function Approval_Email_items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, ByVal Array_Approvals() As String) As String()
        Dim Email_FullPath() As String = Nothing
        If Process_Type = "APPROVALS_U" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_U", "Approval_U", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_U_R" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_U_R", "Approval_U_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_R" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_R", "Approval_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_R_R" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_R_R", "Approval_R_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_C" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_C", "Approval_C", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_C_R" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_C_R", "Approval_C_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_H" Or Process_Type = "APPROVALS_CL" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval", "Approval", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "APPROVALS_H_R" Or Process_Type = "APPROVALS_CL_R" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_R", "Approval_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If

        If Process_Type = "Approval_Hall" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_Hall", "Approval_Hall", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "Approval_Hall_R" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_Hall_R", "Approval_Hall_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "Approval_Visit" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_Visit", "Approval_Visit", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        If Process_Type = "Approval_Visit_R" Then
            Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Approval_Visit_R", "Approval_Visit_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("reason"), "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
        End If
        Return Email_FullPath
    End Function

    Public Function Admin_Email_items(ByVal Process_Type As String, ByVal DS_Info As DataSet, ByVal key As String, ByVal id As Integer, ByVal Array_Tags() As String, ByVal Array_Requests() As String, _
                                      ByVal Array_Approvals() As String, ByVal ODBC_Con_Str As String, ByVal HostIP As String, ByVal Port As String, ByVal Username As String, _
                                      ByVal Password As String, ByVal Email As String, ByVal EmailCC As String) As String
        Dim Email_FullPath() As String = Nothing
        Dim adminEmalis As String = ""
        '  Dim uLIBR As DataSet = Obj_SQL.Get_Info_DataSet("", ODBC_Con_Str)
        Dim AdminEmail As DataSet
        'If Process_Type = "REQUEST_U" Then
        '    AdminEmail = Obj_SQL.Get_Info_DataSet("SELECT [Email] FROM [DubaiLibrary].[dbo].[SymStaff] where Library like '%" & uLIBR.Tables(0).Rows(0)("LIBR") & "%' and [Email] <> ''", ODBC_Con_Str)
        'Else
        AdminEmail = Obj_SQL.Get_Info_DataSet("SELECT Email FROM SymStaff where Library like '%" & DS_Info.Tables(0).Rows(0)("Library") & "%' and Email is not null", ODBC_Con_Str)
        ' End If
        If AdminEmail.Tables.Count > 0 Then
            If AdminEmail.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To AdminEmail.Tables(0).Rows.Count - 1
                    If i = AdminEmail.Tables(0).Rows.Count - 1 Then
                        adminEmalis = adminEmalis + AdminEmail.Tables(0).Rows(i).Item(0)
                    Else
                        adminEmalis = adminEmalis + AdminEmail.Tables(0).Rows(i).Item(0) + ";"
                    End If
                Next
                If Process_Type = "NEW_HOLDS" Then
                    Email_FullPath = Obj_Template.Create_Template("Email", "Requests_New_Mobile", "html", "NEW_HOLDS_Template_AR", "NEW_HOLDS_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), "", DS_Info.Tables(0).Rows(0)("date_responded"), DS_Info.Tables(0).Rows(0)("REQ_Status"), DS_Info.Tables(0).Rows(0)("REQ_Type"), DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                End If
                If Process_Type = "NEW_REQ" Then
                    Email_FullPath = Obj_Template.Create_Template("Email", "Requests_New_Mobile", "html", "NEW_REQ_Template_AR", "NEW_REQ_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("date_placed"), "", DS_Info.Tables(0).Rows(0)("date_responded"), DS_Info.Tables(0).Rows(0)("REQ_Status"), DS_Info.Tables(0).Rows(0)("REQ_Type"), DS_Info.Tables(0).Rows(0)("request_id"), "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                End If
                If Process_Type = "REQUEST_U" Then
                    Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Request_U", "Request_U", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                End If
                If Process_Type = "REQUEST_R" Then
                    Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Request_C", "Request_C", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                End If
                If Process_Type = "REQUEST_C" Then
                    Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "Request_R", "Request_R", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                End If
                If Process_Type = "REQ_HALL" Then
                    Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "REQ_HALL", "REQ_HALL", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                End If
                If Process_Type = "REQ_VISIT" Then
                    Email_FullPath = Obj_Template.Create_Template("Email", Process_Type, "html", "REQ_VISIT", "REQ_VISIT", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                End If

            End If
            Dim Send_email_Bol As String = Obj_InternetOperation.SendEmail(Username, Password, Email, HostIP, Port, Email_FullPath(0), adminEmalis, "Library Request Notifiication", EmailCC)
            Return Send_email_Bol
        End If
    End Function

    Public Function Items_Processing(Process_Type As String, table As String, Query As String, Mobile_Notifications As String, flag_mobile As String, Email_Notifications As String, _
                                     flag_email As String, SMS_Notifications As String, flag_sms As String, key As String, id As Integer, ByVal Array_Tags() As String, _
                                     ByVal Array_Requests() As String, ByVal Array_Approvals() As String, ByVal ODBC_Con_Str As String, ByVal DB As String, ByVal HostIP As String, _
                                     ByVal Port As String, ByVal Username As String, ByVal Password As String, ByVal Email As String, ByVal EmailCC As String) As Boolean
        Try
            'flag = 0 , New flag
            'flag = 1 , Notification sent
            'flag = 2 , No Email/UUID/SMS with user data
            'flag = 3 , Notification disabled
            'flag = 4 , Push Notification Error
            'flag = 5 , Notification Add Error
            'flag = 6 , Error in Email Field
            'flag = 10 , Notification Create Error
            Dim DS_Info As New DataSet
            DS_Info = Obj_SQL.Get_Info_DataSet(Query, ODBC_Con_Str)
            If DS_Info IsNot Nothing Then
                If Not IsDBNull(DS_Info) Then
                    If DS_Info.Tables.Count > 0 Then
                        If DS_Info.Tables(0).Rows.Count > 0 Then
                            If Mobile_Notifications = "1" Then 'Mobile
                                If flag_mobile = 0 Then
                                    Dim Users_DeviceID As String() = Obj_MobileOperation.Get_UsertDeviceID(DS_Info.Tables(0).Rows(0)("id")) ' Get All User Device UUID's
                                    If Users_DeviceID IsNot Nothing Then 'Check if user a valid UUID
                                        Dim Mobile_Txt() As String = Nothing
                                        If table = "holds" Then 'Group
                                            Mobile_Txt = Holds_Mobile_Items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If table = "requests" Then 'Group
                                            Mobile_Txt = Request_Mobile_Items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "NEW_BILL" Then 'Single
                                            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Bills_New_Mobile", "txt", "Bills_New_Template_AR", "Bills_New_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("bill_reason"), DS_Info.Tables(0).Rows(0)("amount_billed"), DS_Info.Tables(0).Rows(0)("date_billed"), "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "BILL_PAY" Then 'Single
                                            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Bills_Pay_Mobile", "txt", "Bills_Pay_Template_AR", "Bills_Pay_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("payment_amount"), DS_Info.Tables(0).Rows(0)("payment_date"), DS_Info.Tables(0).Rows(0)("payment_type"), Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "NEW_USERS" Then 'Single
                                            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Users_New_Email", "txt", "Users_New_Template_AR", "Users_New_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), DS_Info.Tables(0).Rows(0)("name"), DS_Info.Tables(0).Rows(0)("id"), "", "", "", "", DS_Info.Tables(0).Rows(0)("Email"), DS_Info.Tables(0).Rows(0)("Mobile"), DS_Info.Tables(0).Rows(0)("Library"), "", DS_Info.Tables(0).Rows(0)("alternative_id"), DS_Info.Tables(0).Rows(0)("date_privilege_granted"), DS_Info.Tables(0).Rows(0)("date_privilege_expires"), DS_Info.Tables(0).Rows(0)("Profile"), DS_Info.Tables(0).Rows(0)("Status"), "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If table = "charges" Then 'Group
                                            Mobile_Txt = Charges_Mobile_Items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "NEW_DISCHARGES" Then 'Single
                                            Mobile_Txt = Obj_Template.Create_Template("Mobile", "Charges_Discharge_Mobile", "txt", "Charges_Discharge_Template_AR", "Charges_Discharge_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", DS_Info.Tables(0).Rows(0)("date_time_discharged"), DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If table = "users_opt" Then 'Group
                                            Mobile_Txt = Approval_Mobile_items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        UUID_Notification(Mobile_Txt, Users_DeviceID, DS_Info.Tables(0).Rows(0)("id"), Process_Type, table, ODBC_Con_Str)
                                    ElseIf Users_DeviceID Is Nothing Then 'User has no UUID , update the flag Automatically , flag=2.
                                        If DB = "SQL" Then
                                            Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=2 where id=" & id, ODBC_Con_Str)
                                        ElseIf DB = "ORA" Then
                                            Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_mobile=2 where id=" & id, ODBC_Con_Str)
                                        End If
                                    End If
                                End If
                            ElseIf Mobile_Notifications = "0" Then 'Mobile
                                If DB = "SQL" Then
                                    Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=3 where id=" & id, ODBC_Con_Str)
                                ElseIf DB = "ORA" Then
                                    Dim Update_Mobile_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_mobile=3 where id=" & id, ODBC_Con_Str)
                                End If
                            End If

                            If Email_Notifications = "1" Then ' Email
                                If flag_email = 0 Then
                                    Dim Email_FullPath() As String = Nothing
                                    If DS_Info.Tables(0).Rows(0)("Email") <> "" Then 'Check if user has valid email

                                        If table = "holds" Then 'Group
                                            Email_FullPath = Holds_Email_Items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If table = "requests" Then 'Group
                                            Email_FullPath = Request_Email_Items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "NEW_BILL" Then 'Single
                                            Email_FullPath = Obj_Template.Create_Template("Email", "Bills_New_Mobile", "html", "Bills_New", "Bills_New", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("bill_reason"), DS_Info.Tables(0).Rows(0)("amount_billed"), DS_Info.Tables(0).Rows(0)("date_billed"), "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "BILL_PAY" Then 'Single
                                            Email_FullPath = Obj_Template.Create_Template("Email", "Bills_Pay_Mobile", "html", "Bills_Pay", "Bills_Pay", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", DS_Info.Tables(0).Rows(0)("payment_amount"), DS_Info.Tables(0).Rows(0)("payment_date"), DS_Info.Tables(0).Rows(0)("payment_type"), Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "NEW_USERS" Then 'Single
                                            Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_email=3 where id=" & id, ODBC_Con_Str)
                                        End If
                                        If table = "charges" Then 'Group
                                            Email_FullPath = Charges_Email_Items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If Process_Type = "NEW_DISCHARGES" Then 'Single
                                            Email_FullPath = Obj_Template.Create_Template("Email", "Charges_Discharge_Mobile", "html", "Charges_Discharge_Template_AR", "Charges_Discharge_Template_EN", key, DS_Info.Tables(0).Rows(0)("Language"), "", "", "", "", DS_Info.Tables(0).Rows(0)("date_time_discharged"), DS_Info.Tables(0).Rows(0)("title"), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", Array_Tags, Array_Requests, Array_Approvals)
                                        End If
                                        If table = "users_opt" Then 'Group
                                            Email_FullPath = Approval_Email_items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals)
                                        End If

                                        If Email_FullPath IsNot Nothing Then
                                            Dim Send_email_Bol As String = Obj_InternetOperation.SendEmail(Username, Password, Email, HostIP, Port, Email_FullPath(0), DS_Info.Tables(0).Rows(0)("Email"), "Library Notification", EmailCC)
                                            If Send_email_Bol.Contains("sucessfully") Then ' Email Sent Success
                                                Obj_Logs.ProcessLog("Service_Operation", Send_email_Bol, DB, ODBC_Con_Str)
                                                If DB = "SQL" Then
                                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_email=1 where id=" & id, ODBC_Con_Str)
                                                ElseIf DB = "ORA" Then
                                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_email=1 where id=" & id, ODBC_Con_Str)
                                                End If
                                            Else
                                                Obj_Logs.ErrorLog("Service_Operation", Send_email_Bol, DB, ODBC_Con_Str)
                                                If DB = "SQL" Then
                                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_email=10 where id=" & id, ODBC_Con_Str)
                                                ElseIf DB = "ORA" Then
                                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_email=10 where id=" & id, ODBC_Con_Str)
                                                End If
                                            End If
                                        End If
                                    Else
                                        If DB = "SQL" Then
                                            Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_email=6 where id=" & id, ODBC_Con_Str)
                                        ElseIf DB = "ORA" Then
                                            Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_email=6 where id=" & id, ODBC_Con_Str)
                                        End If
                                    End If
                                    'Admin Email Process
                                    'REQ_HALL
                                    'REQ_VISIT
                                    If Email_FullPath Is Nothing And (Process_Type = "NEW_REQ" Or Process_Type = "REQUEST_U" Or Process_Type = "REQUEST_R" Or Process_Type = "REQUEST_C" Or Process_Type = "REQ_HALL" Or Process_Type = "REQ_VISIT") Then
                                        Dim Send_email_Bol As String = Admin_Email_items(Process_Type, DS_Info, key, id, Array_Tags, Array_Requests, Array_Approvals, ODBC_Con_Str, HostIP, Port, Username, Password, Email, EmailCC)
                                        If Send_email_Bol.Contains("sucessfully") Then
                                            Obj_Logs.ProcessLog("Service_Operation", Send_email_Bol, DB, ODBC_Con_Str)
                                            If DB = "SQL" Then
                                                Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_email=1 where id=" & id, ODBC_Con_Str)
                                            ElseIf DB = "ORA" Then
                                                Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_email=1 where id=" & id, ODBC_Con_Str)
                                            End If
                                        Else
                                            Obj_Logs.ErrorLog("Service_Operation", Send_email_Bol, DB, ODBC_Con_Str)
                                            If DB = "SQL" Then
                                                Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_email=10 where id=" & id, ODBC_Con_Str)
                                            ElseIf DB = "ORA" Then
                                                Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_email=10 where id=" & id, ODBC_Con_Str)
                                            End If
                                        End If
                                    End If
                                End If
                            ElseIf Email_Notifications = "0" Then ' Email
                                If DB = "SQL" Then
                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_email=3 where id=" & id, ODBC_Con_Str)
                                ElseIf DB = "ORA" Then
                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_email=3 where id=" & id, ODBC_Con_Str)
                                End If
                            End If
                            If SMS_Notifications = "1" Then 'SMS
                                If flag_sms = 0 Then
                                    If DS_Info.Tables(0).Rows(0)("Sms") <> "" Then 'Check if user a valid Mobile Number

                                    End If
                                End If
                            ElseIf SMS_Notifications = "0" Then 'SMS
                                If DB = "SQL" Then
                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_sms=3 where id=" & id, ODBC_Con_Str)
                                ElseIf DB = "ORA" Then
                                    Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_sms=3 where id=" & id, ODBC_Con_Str)
                                End If
                            End If
                            Return True
                        Else
                            If DB = "SQL" Then
                                Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=4,flag_email=4,flag_sms=4 where id=" & id, ODBC_Con_Str)
                            ElseIf DB = "ORA" Then
                                Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_mobile=4,flag_email=4,flag_sms=4 where id=" & id, ODBC_Con_Str)
                            End If
                        End If
                        Return False
                    Else
                        If DB = "SQL" Then
                            Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=4,flag_email=4,flag_sms=4 where id=" & id, ODBC_Con_Str)
                        ElseIf DB = "ORA" Then
                            Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_mobile=4,flag_email=4,flag_sms=4 where id=" & id, ODBC_Con_Str)
                        End If
                    End If
                    'End If
                    'End If
                Else
                    If DB = "SQL" Then
                        Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update [Notification].[dbo].[" & table & "] set flag_mobile=4,flag_email=4,flag_sms=4 where id=" & id, ODBC_Con_Str)
                    ElseIf DB = "ORA" Then
                        Dim Update_Email_Flag As Boolean = Obj_SQL.Update_Info("update " & table & " set flag_mobile=4,flag_email=4,flag_sms=4 where id=" & id, ODBC_Con_Str)
                    End If
                End If
            End If
        Catch ex As Exception
            Obj_Logs.ErrorLog("Items_Processing", "Items_Processing Error : " + ex.Message, DB, ODBC_Con_Str)
        End Try
        Return False
    End Function

    Public Function Overdue_Processing(Charges_Overdue_Notifications As String, flag_fnotice As Integer, flag_Snotice As Integer, flag_lnotice As Integer, NumOfDaysDue As Long, Charges_Overdue_First_Notifications As String, Charges_Overdue_First_Notifications_Days As Integer _
                                      , Charges_Overdue_Second_Notifications As String, Charges_Overdue_Second_Notifications_Days As Integer, Charges_Overdue_Final_Notifications As String, Charges_Overdue_Final_Notifications_Days As Integer) As Boolean
        Try
            If Charges_Overdue_Notifications = "1" Then
                If flag_fnotice = 0 Then
                    '===================First Notice======================================
                    If Charges_Overdue_First_Notifications = "1" Then
                        If Charges_Overdue_First_Notifications_Days < 0 Then
                            If NumOfDaysDue > 0 Then
                                If Math.Abs(Charges_Overdue_First_Notifications_Days) >= NumOfDaysDue Then
                                    'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : first notice before ")
                                End If
                            End If
                        ElseIf Charges_Overdue_First_Notifications_Days > 0 Then
                            If NumOfDaysDue < 0 Then
                                If Charges_Overdue_First_Notifications_Days <= Math.Abs(NumOfDaysDue) Then
                                    'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : first notice after ")
                                End If
                            End If
                        ElseIf Charges_Overdue_First_Notifications_Days = 0 Then
                            If NumOfDaysDue = 0 Then
                                'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : first notice same day ")
                            End If
                        End If
                    End If
                End If

                '===================Second Notice======================================
                If flag_Snotice = 0 Then
                    If Charges_Overdue_Second_Notifications = "1" Then
                        If Charges_Overdue_Second_Notifications_Days < 0 Then
                            If NumOfDaysDue > 0 Then
                                If Math.Abs(Charges_Overdue_Second_Notifications_Days) >= NumOfDaysDue Then
                                    'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : Second notice before ")
                                End If
                            End If
                        ElseIf Charges_Overdue_Second_Notifications_Days > 0 Then
                            If NumOfDaysDue < 0 Then
                                If Charges_Overdue_Second_Notifications_Days <= Math.Abs(NumOfDaysDue) Then
                                    'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : Second notice after ")
                                End If
                            End If
                        ElseIf Charges_Overdue_Second_Notifications_Days = 0 Then
                            If NumOfDaysDue = 0 Then
                                'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : Second notice same day ")
                            End If
                        End If
                    End If
                End If
                '===================Final Notice======================================
                If flag_lnotice = 0 Then
                    If Charges_Overdue_Final_Notifications = "1" Then
                        If Charges_Overdue_Final_Notifications_Days < 0 Then
                            If NumOfDaysDue > 0 Then
                                If Math.Abs(Charges_Overdue_Final_Notifications_Days) >= NumOfDaysDue Then
                                    'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : Final notice before ")
                                End If
                            End If
                        ElseIf Charges_Overdue_Final_Notifications_Days > 0 Then
                            If NumOfDaysDue < 0 Then
                                If Charges_Overdue_Final_Notifications_Days <= Math.Abs(NumOfDaysDue) Then
                                    'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : Final notice after " & Charges_Overdue_Final_Notifications_Days & NumOfDaysDue)
                                End If
                            End If
                        ElseIf Charges_Overdue_Final_Notifications_Days = 0 Then
                            If NumOfDaysDue = 0 Then
                                'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : Final notice same day ")
                            End If
                        End If
                    End If
                End If

            End If
            Return True
        Catch Ex As Exception
            'SaveTextToFile(Unicorn_Notification.LogFile, Now & " : ERROR : Overdue_Processing : " & Ex.Message)
            Return False
        End Try
        Return True
    End Function

End Class
