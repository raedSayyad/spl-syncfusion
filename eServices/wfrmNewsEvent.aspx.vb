﻿Imports ClassLibraries

Public Class wfrmNewsEvent
    Inherits System.Web.UI.Page
    Dim Lang As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Lang = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                lblDescription.Text = "{{:ADESCRIPTION}}"
                grdNews.Columns(1).Visible = True
                grdNews.EnableRTL = True
                lblDescriptionE.Text = "{{:ADESCRIPTION}}"
                grdEvent.Columns(1).Visible = True
                grdEvent.EnableRTL = True
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
            Else
                lblDescription.Text = "{{:EDESCRIPTION}}"
                grdNews.Columns(2).Visible = True
                lblDescriptionE.Text = "{{:EDESCRIPTION}}"
                grdEvent.Columns(2).Visible = True
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            grdNews.Columns(1).HeaderText = rc.GetString("News", Culture_Used)
            grdNews.Columns(2).HeaderText = rc.GetString("News", Culture_Used)
            grdEvent.Columns(1).HeaderText = rc.GetString("Events", Culture_Used)
            grdEvent.Columns(2).HeaderText = rc.GetString("Events", Culture_Used)

            grdNews.DataSource = fillNews()
            grdNews.DataBind()

            grdEvent.DataSource = fillEvents()
            grdEvent.DataBind()
           
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Function fillNews()
        Dim News As New Others.News
        Dim AllNews As New List(Of Others.News)

        Dim objRes As New ewsOthers.IewsOthersClient
        For i As Integer = 0 To objRes.GetAllNews.Count - 1
            News = New Others.News
            News = objRes.GetAllNews(i)
            AllNews.Add(News)
        Next
        Return AllNews
    End Function

    Function fillEvents()
        Dim Events As New Others.Events
        Dim AllEvents As New List(Of Others.Events)

        Dim objRes As New ewsOthers.IewsOthersClient
        For i As Integer = 0 To objRes.GetAllNews.Count - 1
            Events = New Others.Events
            Events = objRes.GetAllEvents(i)
            AllEvents.Add(Events)
        Next
        Return AllEvents
    End Function

End Class