﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmUserRenReq.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmUserRenReq" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <ej:Grid ID="UserRenReqGrid" runat="server" ClientIDMode="Static" AllowPaging="True" OnServerRecordClick="onClick">
                        <ClientSideEvents RecordClick="RecordClick" />
                        <Columns>
                            <ej:Column Field="ID" HeaderText="ID" IsPrimaryKey="True" Visible="false" TextAlign="Right" Width="20"></ej:Column>
                            <ej:Column Field="User_ID" HeaderText="User ID" Width="30" Visible="true"></ej:Column>
                            <ej:Column Field="FullName" HeaderText="Full Name" Width="70" Visible="true"></ej:Column>
                            <ej:Column Field="DOB" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Library" HeaderText="Library" Width="30" Visible="true"></ej:Column>
                            <ej:Column Field="MemberType" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Nationality" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="JobTitle" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="NationalID" HeaderText="National ID" Width="50" Visible="true"></ej:Column>
                            <ej:Column Field="SNeeds" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Employer" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Gender" HeaderText="Gender" Width="30" Visible="true"></ej:Column>
                            <ej:Column Field="NationalIDPhoto1" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="NationalIDPhoto2" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="PersonalPhoto" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="HomePhone" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="WorkPhone" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Ext" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Mobile" HeaderText="Mobile" Width="50" Visible="true"></ej:Column>
                            <ej:Column Field="POBox" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Fax" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="WebSite" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="Status" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="RejectMsg" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="AdminName" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column Field="DateCreate" HeaderText="Date Create" Width="70" Visible="true"></ej:Column>
                            <ej:Column Field="DateUpdate" HeaderText="User_ID" Width="50" Visible="false"></ej:Column>
                            <ej:Column HeaderText="Details" Template="true" TemplateID="#buttonTemplate" TextAlign="Center" Width="75"></ej:Column>
                        </Columns>
                    </ej:Grid>
                    <script type="text/x-jsrender" id="buttonTemplate">
                        <button class="Details" name="Details"><%:Detail%></button>
                    </script>
                    <script type="text/javascript">
                        $(function () {
                            $(".Details").ejButton();
                        });
                    </script>
                    <script type="text/javascript">
                        $(function () {
                            $(".Details").click(function (e) {
                                triggerEvent(e);
                            });
                        });

                        function triggerEvent(e) {
                            var obj = $("#UserRenReqGrid").data("ejGrid");
                            var args = { currentTarget: e.currentTarget.name, selectedRecord: obj.getSelectedRecords(), selectedIndex: obj.model.selectedRowIndex };
                            obj._trigger("recordClick", args);
                        }

                        function RecordClick(e) {
                            if (e.currentTarget != "Details")
                                return false
                            else {
                                triggerEvent(e);
                            }
                        }
                    </script>
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
