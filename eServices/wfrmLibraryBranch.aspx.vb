﻿Imports ClassLibraries
Imports System.Web.UI.HtmlControls


Public Class wfrmLibraryBranch
    Inherits System.Web.UI.Page

    Dim Lang As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Lang = Request.QueryString("Lang")
            If Lang = "Ara" Then
                lblTitleV.Text = "{{:ATitle}}"
                lblDescriptionV.Text = "{{:ADescription}}"
                lblAddressV.Text = "{{:AAddress}}"
                BranchLib.Columns(1).HeaderText = "الفرع"
                BranchLib.Columns(5).HeaderText = "العنوان"
                BranchLib.Columns(7).HeaderText = "الهاتف"
                BranchLib.Columns(1).Visible = True
                BranchLib.Columns(5).Visible = True
                BranchLib.Columns(7).Visible = True
                BranchLib.Columns(2).Visible = False
                BranchLib.Columns(6).Visible = False
            Else
                lblTitleV.Text = "{{:ETitle}}"
                lblDescriptionV.Text = "{{:EDescription}}"
                lblAddressV.Text = "{{:EAddress}}"
                BranchLib.Columns(2).HeaderText = "Branch"
                BranchLib.Columns(6).HeaderText = "Address"
                BranchLib.Columns(7).HeaderText = "Telephone"
                BranchLib.Columns(2).Visible = True
                BranchLib.Columns(6).Visible = True
                BranchLib.Columns(7).Visible = True
                BranchLib.Columns(1).Visible = False
                BranchLib.Columns(5).Visible = False
            End If
            BranchLib.DataSource = fillListBox()
            BranchLib.DataBind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Function fillListBox()
        Dim LibBranch As New Others.LibraryBranch
        Dim AllLibBranch As New List(Of Others.LibraryBranch)
        Dim objRes As New ewsOthers.IewsOthersClient
        For i As Integer = 0 To objRes.GetAllLibraryBranch.Count - 1
            LibBranch = New Others.LibraryBranch
            LibBranch = objRes.GetAllLibraryBranch(i)
            AllLibBranch.Add(LibBranch)
        Next
        Return AllLibBranch
    End Function

End Class