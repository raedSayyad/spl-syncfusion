﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Threading
Imports System.Web
Imports Microsoft.AspNet.SignalR
Public Class ProgressHub
    Inherits Hub
    Public msg As String = "Initializing and Preparing..."
    Public count As Integer = 100

    Public Sub CallLongOperation()
        For x As Integer = 0 To count
            If x = 20 Then
                msg = "Copying Service Files..."
            ElseIf x = 40 Then
                msg = "Installing Service..."
            ElseIf x = 60 Then
                msg = "Loading Service Settings..."
            ElseIf x = 80 Then
                msg = "Applying Service Settings..."
            ElseIf x = 100 Then
                msg = "Process Completed."
            End If
            ' call client-side SendMethod method
            Clients.Caller.sendMessage(String.Format(msg & " {0}% of {1}%", x, count))
            ' delay the process to see things clearly
            Thread.Sleep(600)
        Next
    End Sub
End Class
