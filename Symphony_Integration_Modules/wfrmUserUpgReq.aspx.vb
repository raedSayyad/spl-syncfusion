﻿Imports ClassLibraries
Imports Syncfusion.JavaScript.Web

Public Class wfrmUserUpgReq
    Inherits System.Web.UI.Page

    Public Detail As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim Lang As String = Request.QueryString("Lang")
            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objAllUserUpgRequest As New List(Of Users.UserUpgRequest)
            Dim objUserUpgRequest As New Users.UserUpgRequest
            lblError.Text += CStr(objEwsUserRegClient.SelectUserUpgrade().Count)
            For i As Integer = 0 To objEwsUserRegClient.SelectUserUpgrade().Count - 1
                objUserUpgRequest = New Users.UserUpgRequest
                objUserUpgRequest = objEwsUserRegClient.SelectUserUpgrade()(i)
                objAllUserUpgRequest.Add(objUserUpgRequest)
            Next
            UserUpgReqGrid.DataSource = objAllUserUpgRequest
            UserUpgReqGrid.DataBind()
            lblError.Text += " Page 1 "
            Detail = "Detail"
        Catch ex As Exception
            lblError.Text += ex.Message + " Page Error"
        End Try
    End Sub

    Protected Sub onClick(Sender As Object, e As GridEventArgs)
        Try
            lblError.Text = ""
            Dim selectedIndex As String = e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            Dim objEwsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objUserUpgRequest As New Users.UserUpgRequest
            For i As Integer = 0 To objEwsUserRegClient.SelectUserUpgrade().Count - 1
                If i = CInt(selectedIndex) Then
                    objUserUpgRequest = New Users.UserUpgRequest
                    objUserUpgRequest = objEwsUserRegClient.SelectUserUpgrade()(i)
                End If
            Next
            Session("UserUpgRequest") = objUserUpgRequest
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmUserUpgReqDetail.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmUserUpgReqDetail.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub Update(Sender As Object, e As GridEventArgs)
        Try
            'lblError.Text = ""
            ''Dim currentTarget As String = e.Arguments("currentTarget").ToString  'returns current target details
            ''Dim selectedRecord As Object = e.Arguments("selectedRecord")  'yields selected record details
            'Dim selectedID As String '= e.Arguments.ToString   'e.Arguments("selectedIndex").ToString  'yields selectedIndex value
            'lblError.Text += "Arg: " + e.Arguments.ToString
            'Dim KeyVal As Dictionary(Of String, Object) = e.Arguments("Data")
            'For Each KeyV As KeyValuePair(Of String, Object) In KeyVal
            '    If KeyV.Key = "ID" Then
            '        selectedID = CStr(KeyV.Value)
            '    End If
            'Next
            'lblError.Text += selectedID
            'Dim objEwsOthersClient As New ewsOthers.IewsOthersClient
            'Dim objUserVisitReservation As New Others.ReserveVisit
            'For i As Integer = 0 To objEwsOthersClient.GetAllReserveVisitByUserID(UserID).Count - 1
            '    If objEwsOthersClient.GetAllReserveVisitByUserID(UserID)(i).ID = CInt(selectedID) Then
            '        objUserVisitReservation = New Others.ReserveVisit
            '        objUserVisitReservation = objEwsOthersClient.GetAllReserveVisitByUserID(UserID)(i)
            '    End If
            'Next
            'If objUserVisitReservation.Status = "0" Or objUserVisitReservation.Status = "0" Then
            '    Session("UserVisitReservation") = objUserVisitReservation
            '    Dim Lang As String = Request.QueryString("Lang")
            '    If Lang = "Ara" Then
            '        Response.Redirect("~/wfrmVisitReservation.aspx?Mode=Edit&Lang=Ara")
            '    Else
            '        Response.Redirect("~/wfrmVisitReservation.aspx?Mode=Edit")
            '    End If
            'Else
            '    lblError.Text = "Selected Reservation can't be updated." + selectedID
            '    lblError.ForeColor = Drawing.Color.Red
            'End If
        Catch ex As Exception
            lblError.Text += ex.Message
        End Try
    End Sub

End Class