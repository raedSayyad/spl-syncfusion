﻿' NOTE: You can use the "Rename" command on the context menu to change the class name "wsUsers" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select wsUsers.svc or wsUsers.svc.vb at the Solution Explorer and start debugging.

Imports ClassLibraries

Public Class wsUsers
    Implements IwsUsers

    Dim Obj_EncDec As New EncDec
    Dim Obj_SQL As New SQLFunction

    Public Function AddUser(ByVal Username As String, ByVal Password As String, ByVal Email As String) As Boolean Implements IwsUsers.AddUser
        Try
            Dim key As String = "D653FF45281B76EEBBF8EA5C513D4387D7743R61FE35E20C962DC412A8657837D7F3408543CDBDA37158ACBDCAACE9C38618C6M92F3439D6EE133585H3693B6ED"
            Dim encPassword As String = Obj_EncDec.Encrypt(Password, key)
            Dim dbType As String = System.Configuration.ConfigurationManager.AppSettings("DB_Type")
            Dim sqlScript As String = ""
            If dbType = "SQL" Then
                sqlScript = "INSERT INTO [Integration_Admin].[dbo].[Users] ([Username],[Password],[Email],[Notification_Admin],[Symphony_Admin],[Users_Admin],[System_Admin]) VALUES ('" & Username & "','" & encPassword & "','" & Email & "',0,0,0,0)"
            ElseIf dbType = "ORA" Then
                sqlScript = ""
            End If
            If sqlScript <> "" Then
                Obj_SQL.exeNonQuery(sqlScript, System.Configuration.ConfigurationManager.AppSettings("SymConnStr"))
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
