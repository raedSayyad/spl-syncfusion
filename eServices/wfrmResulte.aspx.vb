﻿Imports ClassLibraries

Public Class wfrmResulte
    Inherits System.Web.UI.Page

    Public gotoURL As String = ""
    Public objUserInfo As New Users.UserInfo
    Public BTNCONTXT As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'If Not Session("UserInfo") Is Nothing Then
            '    objUserInfo = Session("UserInfo")
            '    Dim User_ID As String = objUserInfo.User_ID
            '    Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
            '    objUserInfo = objEwsUserClient.UserInfo(User_ID)
            'Else
            '    Response.Redirect("~/wfrmLogin.aspx")
            'End If
            Dim Source As String = Request.QueryString("Source")
            Dim MsgTxt As String = Request.QueryString("Msg")
            Dim Lang As String = Request.QueryString("Lang")
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            lblTitle.Text = Source
            lblText.Text = MsgTxt
            Dim Terms As String = Request.QueryString("TermsID")
            If Terms = "1" Or Terms = "2" Then
                If Lang = "Ara" Then
                    gotoURL = "https://" + ConfigurationManager.AppSettings("ServNam") + "/uhtbin/cgisirsi.exe/x/0/0/49/X"
                Else
                    gotoURL = "https://" + ConfigurationManager.AppSettings("ServNam") + "/uhtbin/cgisirsi.exe/x/0/0/49/X"
                End If
            ElseIf Terms = "Usrugd" Then
                lblTitle.Text = Source
                lblText.Text = MsgTxt
                gotoURL = "https://" + ConfigurationManager.AppSettings("ServNam") + "/uhtbin/cgisirsi.exe/x/0/0/49/X"
            ElseIf Terms = "UserReg" Then
                lblTitle.Text = Source
                lblText.Text = MsgTxt
                gotoURL = "~/wfrmLogin.aspx"
            End If
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))

            btnContinue.Text = rc.GetString("_Continue", Culture_Used)
            BTNCONTXT = rc.GetString("_Continue", Culture_Used)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Try
            ' Response.Write("ASDASD" + gotoURL)
            Response.Redirect(gotoURL)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub
End Class