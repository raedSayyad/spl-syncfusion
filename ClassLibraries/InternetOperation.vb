﻿Imports System.IO
Imports System.Net

Public Class InternetOperation

    Public Function SendEmail(ByVal NetCreden As String, ByVal Pass As String, ByVal mailFrom As String, ByVal HostIP As String, ByVal port As String, _
                              ByVal FileP As String, ByVal email As String, ByVal mailSubject As String, ByVal CCEmail As String) As String
        Try
            Dim SmtpServer As New Net.Mail.SmtpClient()
            Dim mail As New Net.Mail.MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential(NetCreden, Pass)
            SmtpServer.Port = CInt(port)
            SmtpServer.EnableSsl = True
            SmtpServer.Host = HostIP
            mail = New Net.Mail.MailMessage()
            mail.From = New Net.Mail.MailAddress(mailFrom)
            Dim emailList() As String = Nothing
            If email.Contains(";") Then
                emailList = email.Split(";")
                For i As Integer = 0 To emailList.Length - 1
                    mail.To.Add(emailList(i))
                Next
            Else
                mail.To.Add(email)
            End If
            If CCEmail <> "" Then
                Dim emailCC As String = CCEmail
                Dim emailListCC() As String = Nothing
                If emailCC.Contains(";") Then
                    emailListCC = emailCC.Split(";")
                    For i As Integer = 0 To emailListCC.Length - 1
                        mail.Bcc.Add(emailListCC(i))
                    Next
                Else
                    mail.Bcc.Add(emailCC)
                End If
            End If
            mail.Subject = mailSubject
            Dim htmlSource As String = File.ReadAllText(FileP)
            mail.Body = htmlSource
            mail.IsBodyHtml = True
            SmtpServer.Send(mail)
            Return "Email sent sucessfully to: " + email
        Catch ex As Exception
            Return "ERROREMAIL : " & ex.ToString()
        End Try
    End Function

    Public Function SendStringEmail(ByVal NetCreden As String, ByVal Pass As String, ByVal mailFrom As String, ByVal HostIP As String, ByVal port As String, _
                              ByVal FileP As String, ByVal email As String, ByVal mailSubject As String, ByVal CCEmail As String) As String
        Try
            Dim SmtpServer As New Net.Mail.SmtpClient()
            Dim mail As New Net.Mail.MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential(NetCreden, Pass)
            SmtpServer.Port = CInt(port)
            SmtpServer.EnableSsl = True
            SmtpServer.Host = HostIP
            mail = New Net.Mail.MailMessage()
            mail.From = New Net.Mail.MailAddress(mailFrom)
            Dim emailList() As String = Nothing
            If email.Contains(";") Then
                emailList = email.Split(";")
                For i As Integer = 0 To emailList.Length - 1
                    mail.To.Add(emailList(i))
                Next
            Else
                mail.To.Add(email)
            End If
            If CCEmail <> "" Then
                Dim emailCC As String = CCEmail
                Dim emailListCC() As String = Nothing
                If emailCC.Contains(";") Then
                    emailListCC = emailCC.Split(";")
                    For i As Integer = 0 To emailListCC.Length - 1
                        mail.Bcc.Add(emailListCC(i))
                    Next
                Else
                    mail.Bcc.Add(emailCC)
                End If
            End If
            mail.Subject = mailSubject
            mail.Body = FileP
            mail.IsBodyHtml = True
            SmtpServer.Send(mail)
            Return "Email sent sucessfully to: " + email
        Catch ex As Exception
            Return "ERROREMAIL : " & ex.ToString()
        End Try
    End Function
End Class
