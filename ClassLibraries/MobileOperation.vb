﻿Public Class MobileOperation

    Public Notification_WS_URL As String = ""
    Public Push_Notification_WS_URL As String = ""
    Public Function Get_UsertDeviceID(ByVal user_id As String) As String()
        Try
            '?UserID=testu&isSent=false&isSentSpecified=false&lang=ara
            Dim WS_URL As String = Notification_WS_URL
            If WS_URL IsNot Nothing Then
                WS_URL = WS_URL & "/GetDeviceIDForUserID?UserID=" & user_id
            End If
            'If Unicorn_Notification.Query_Log = 1 Then
            '    Unicorn_Notification.Obj_Functions.SaveTextToFile(Unicorn_Notification.LogFile, Now & " : Web Service Request : " & WS_URL)
            'End If
            Dim jsonResponse_List() As String = Nothing
            Dim req2 As Net.WebRequest = Net.WebRequest.Create(WS_URL)

            req2.Method = "GET"
            req2.ContentType = "application/json; charset=utf-8"
            Dim response As Net.HttpWebResponse = DirectCast(req2.GetResponse(), Net.HttpWebResponse)
            Dim jsonResponse As String = String.Empty
            Dim counter As Integer = 0
            Using sr As New IO.StreamReader(response.GetResponseStream())
                jsonResponse = sr.ReadToEnd()
                ReDim Preserve jsonResponse_List(counter)
                jsonResponse_List(counter) = jsonResponse
                counter += 1
            End Using
            Return jsonResponse_List
        Catch ex As Exception
            'If Unicorn_Notification.Query_Log = 1 Then
            '    SaveTextToFile(Unicorn_Notification.LogFile, Now & " : ERROR : " & ex.Message)
            'End If
            Return Nothing
        End Try
        Return Nothing
    End Function

    Public Function Set_UsertDeviceID_Notifications(ByVal Users_DeviceID() As String, Mobile_Txt As String) As Boolean
        Try
            For i As Integer = 0 To Users_DeviceID.Length - 1
                Dim DeviceID_Array() As String = Nothing
                Dim DeviceID As String = ""
                DeviceID_Array = Users_DeviceID(i).Split(",")
                If DeviceID_Array IsNot Nothing Then
                    For y As Integer = 0 To DeviceID_Array.Length - 1
                        DeviceID = DeviceID_Array(y)
                        DeviceID = DeviceID.Replace("[", "")
                        DeviceID = DeviceID.Replace("]", "")
                        DeviceID = DeviceID.Replace("""", "")
                        DeviceID = DeviceID.Replace(",", "")
                        If DeviceID <> "" Then
                            Dim sWatch As System.Diagnostics.Stopwatch = New System.Diagnostics.Stopwatch()
                            sWatch.Start()
                            Dim WS_URL As String = Push_Notification_WS_URL
                            If WS_URL IsNot Nothing Then
                                WS_URL = WS_URL & "/PushNotificationM?deviceid=" & DeviceID & "&messagetext=" & Mobile_Txt
                            End If
                            Dim jsonResponse_List() As String = Nothing
                            Dim req2 As Net.HttpWebRequest = Net.HttpWebRequest.Create(WS_URL)
                            req2.ProtocolVersion = Net.HttpVersion.Version11
                            req2.KeepAlive = False
                            req2.Method = "GET"
                            req2.ContentType = "application/json; charset=utf-8"
                            Dim response As Net.HttpWebResponse = DirectCast(req2.GetResponse(), Net.HttpWebResponse)
                            Dim jsonResponse As String = String.Empty
                            Dim counter As Integer = 0
                            Using sr As New IO.StreamReader(response.GetResponseStream())
                                jsonResponse = sr.ReadToEnd()
                                ReDim Preserve jsonResponse_List(counter)
                                jsonResponse_List(counter) = jsonResponse
                                counter += 1
                            End Using
                            sWatch.Stop()
                        End If
                    Next
                End If
            Next

            Return True
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Public Function Add_UsertDeviceID_Notifications_Ar_En(ByVal Users_DeviceID() As String, Mobile_Txt As String, lang As String, Userid As String, ServiceName As String) As Boolean
        Try
            'For u As Integer = 0 To Unicorn_Notification.Array_Services.Length - 1
            '    If Unicorn_Notification.Array_Services(u).ToString.Split("|")(0) = ServiceName Then
            '        If lang.ToUpper = "ARA" Then
            '            ServiceName = Unicorn_Notification.Array_Services(u).ToString.Split("|")(2)
            '        ElseIf lang.ToUpper = "ENG" Then
            '            ServiceName = Unicorn_Notification.Array_Services(u).ToString.Split("|")(1)
            '        End If
            '    End If
            'Next
            Dim sWatch As System.Diagnostics.Stopwatch = New System.Diagnostics.Stopwatch()
            sWatch.Start()
            Dim WS_URL As String = Notification_WS_URL
            If WS_URL IsNot Nothing Then
                WS_URL = WS_URL & "/AddNewUsersNotificationsDetails?UserID=" & Userid & "&ServiceName=" & ServiceName & "&Message=" & Mobile_Txt & "&Lang=" & lang
            End If
            Dim jsonResponse_List() As String = Nothing
            Dim req2 As Net.HttpWebRequest = Net.HttpWebRequest.Create(WS_URL)
            req2.ProtocolVersion = Net.HttpVersion.Version11
            req2.KeepAlive = False
            req2.Method = "GET"
            req2.ContentType = "application/json; charset=utf-8"
            Dim response As Net.HttpWebResponse = DirectCast(req2.GetResponse(), Net.HttpWebResponse)
            Dim jsonResponse As String = String.Empty
            Dim counter As Integer = 0
            Using sr As New IO.StreamReader(response.GetResponseStream())
                jsonResponse = sr.ReadToEnd()
                ReDim Preserve jsonResponse_List(counter)
                jsonResponse_List(counter) = jsonResponse
                counter += 1
            End Using
            sWatch.Stop()
            Return True
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

End Class
