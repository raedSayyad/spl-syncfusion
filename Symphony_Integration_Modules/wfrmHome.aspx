﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="wfrmHome.aspx.vb" Inherits="Symphony_Integration_Modules.wfrmHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section id="one" class="wrapper style1 special">
        <div class="container">
            <header class="major">
                <h2>Symphony Integration Modules</h2>
                <p></p>
            </header>
            <div class="row 150%">
                <div class="4u 12u$(medium)">
                    <section class="box">
                        <i class="icon big rounded color1 fa fa-bell-o"></i>
                        <h3>Notification Service</h3>
                        <p></p>
                        <p>
                            <a class="btn btn-default" href="#">Review more &raquo;</a>
                        </p>
                    </section>
                </div>
                <div class="4u 12u$(medium)">
                    <section class="box">
                        <i class="icon big rounded color9 fa fa-users"></i>
                        <h3>Users</h3>
                        <p></p>
                        <p>
                            <a class="btn btn-default" href="#">Review more &raquo;</a>
                        </p>
                    </section>
                </div>
                <div class="4u$ 12u$(medium)">
                    <section class="box">
                        <i class="icon big rounded color6 fa fa-thumbs-o-up"></i>
                        <h3>Approval</h3>
                        <p></p>
                        <p>
                            <a class="btn btn-default" href="#">Review more &raquo;</a>
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
