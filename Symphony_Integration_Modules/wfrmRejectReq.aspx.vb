﻿Imports ClassLibraries
Imports ClassLibraries.Users

Public Class wfrmRejectReq
    Inherits System.Web.UI.Page

    Public objUserUpgRequest As New Users.UserUpgRequest
    Public objVisitRequest As New Others.ReserveVisit
    Public objHallRequest As New Others.ReserveHall
    Public objUserInfo As New Users.UserInfo
    Public ReqType As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ReqType = Request.QueryString("ReqType")
            If ReqType = "Upg" Then
                objUserUpgRequest = Session("UserUpgRequest")
            ElseIf ReqType = "Visit" Then
                objUserInfo = Session("VisitUserInfo")
                objVisitRequest = Session("VisitRequest")
            ElseIf ReqType = "Hall" Then
                objUserInfo = Session("HallUserInfo")
                objHallRequest = Session("HallRequest")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If ReqType = "Upg" Then
                If Lang = "Ara" Then
                    Response.Redirect("~/wfrmUserUpgReqDetail.aspx?Lang=Ara")
                Else
                    Response.Redirect("~/wfrmUserUpgReqDetail.aspx")
                End If
            ElseIf ReqType = "Visit" Then
                If Lang = "Ara" Then
                    Response.Redirect("~/wfrmUserVisitReqDetail.aspx?Lang=Ara")
                Else
                    Response.Redirect("~/wfrmUserVisitReqDetail.aspx")
                End If
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            If txtReason.Text <> "" And Not txtReason.Text Is Nothing Then
                Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
                If ReqType = "Upg" Then
                    Dim objUserRegType As New UserRegType
                    objUserRegType = objewsUserRegClient.UserRegType(objUserUpgRequest.User_ID, objUserUpgRequest.Library, "OK", "4")
                    If objUserRegType.Execute = True Then
                        Dim result As String = objewsUserRegClient.UpdateUserUpgrade(objUserUpgRequest.ID, "3", txtReason.Text, "ADMIN", Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                        If result = "1" Then
                            'lblError.Text = "Rejected Successfuly"
                            objewsUserRegClient.InsUsersOPT(objUserUpgRequest.User_ID, "0", "0", "0", "1", txtReason.Text, "1", "Admin", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                            Response.Redirect("~/wfrmResulte.aspx?Title=User Upgrade&Text=Rejected Successfuly &Url=wfrmUserUpgReq.aspx")
                        Else
                            lblError.Text += result + " || "
                        End If
                    Else
                        lblError.Text = objUserRegType.objException.msg
                    End If
                ElseIf ReqType = "Visit" Then
                    Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
                    Dim result As String = ""
                    result = objEwsOtherClient.UptReserveVisit(objVisitRequest.ID, objVisitRequest.User_ID, objVisitRequest.Library, objVisitRequest.ReservPurpose, objVisitRequest.DateFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), objVisitRequest.DateTo.ToString("dd/MM/yyyy:hh:mm:sstt"), "3", "Admin", objVisitRequest.DateCreate.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                    If result = "1" Then
                        objewsUserRegClient.InsUsersOPT(objVisitRequest.User_ID, "0", "0", "0", "1", txtReason.Text, "12", "Admin", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                        Response.Redirect("~/wfrmResulte.aspx?Title=Visit Reservation&Text=Rejected Successfuly&Url=wfrmVisitReq.aspx")
                    Else
                        lblError.Text = result
                    End If
                ElseIf ReqType = "Hall" Then
                    Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
                    Dim result As String = ""
                    result = objEwsOtherClient.UptReserveHall(objHallRequest.ID, objHallRequest.User_ID, objHallRequest.Library, objHallRequest.ReservPurpose, objHallRequest.DateFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), objHallRequest.DateTo.ToString("dd/MM/yyyy:hh:mm:sstt"), objHallRequest.LectureType, objHallRequest.AttendanceNo, objHallRequest.Devices, objHallRequest.InstructorName, objHallRequest.InstructorPassport, objHallRequest.InstructorPpNo, "3", "Admin", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                    If result = "1" Then
                        objewsUserRegClient.InsUsersOPT(objHallRequest.User_ID, "0", "0", "0", "1", txtReason.Text, "11", "Admin", Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                        Response.Redirect("~/wfrmResulte.aspx?Title=Hall Reservation&Text=Rejected Successfuly&Url=wfrmHallReq.aspx")
                    Else
                        lblError.Text = result
                    End If
                End If
            Else
                lblError.Text = "Please fill rejection reason"
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class