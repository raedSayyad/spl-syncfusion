﻿Imports System.ServiceProcess

Public Class wfrmSymphony
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getStatus()
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
                                            "var str2 = str.replace('hide','');" & _
                                            "document.getElementById('navButton').className=str2;", True)
        Dim checkList() As Integer = {0, 0, 0, 0, 0, 0, 0, 0}
        Dim checkName() As String = {"Ipcm", "Loadpay", "Netstarter", "Reportcron", "Sockserver", "Sockwrapper", "Starter", "Webstarter"}
        If CheckBox0.Checked = True Then
            checkList(0) = 1
        End If
        If CheckBox1.Checked = True Then
            checkList(1) = 1
        End If
        If CheckBox2.Checked = True Then
            checkList(2) = 1
        End If
        If CheckBox3.Checked = True Then
            checkList(3) = 1
        End If
        If CheckBox4.Checked = True Then
            checkList(4) = 1
        End If
        If CheckBox5.Checked = True Then
            checkList(5) = 1
        End If
        If CheckBox6.Checked = True Then
            checkList(6) = 1
        End If
        If CheckBox7.Checked = True Then
            checkList(7) = 1
        End If

        For i As Integer = 0 To 7
            Dim servic As ServiceController = New ServiceController(checkName(i))
            If checkList(i) = 1 Then
                If servic.Status = ServiceControllerStatus.Stopped Then
                    servic.Start()
                    servic.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(5))
                    System.Threading.Thread.Sleep(5000)
                    'ckbWinService.Checked = True
                    'lblWinSrvStatus.Text = servic.Status.ToString
                End If
            Else
                If servic.Status = ServiceControllerStatus.Running Then
                    servic.Stop()
                    servic.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(5))
                    System.Threading.Thread.Sleep(5000)
                    'ckbWinService.Checked = False
                    'lblWinSrvStatus.Text = servic.Status.ToString
                End If
            End If
        Next
        getStatus()
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "var str = document.getElementById('navButton').className;" & _
                                            "var str2 = str.replace('hide','');" & _
                                            "document.getElementById('navButton').className=str2;", True)
        Dim checkName() As String = {"Ipcm", "Loadpay", "Netstarter", "Reportcron", "Sockserver", "Sockwrapper", "Starter", "Webstarter"}
        For i As Integer = 0 To 7
            Dim servic As ServiceController = New ServiceController(checkName(i))
            If servic.Status = ServiceControllerStatus.Running Then
                servic.Stop()
                servic.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(5))
                System.Threading.Thread.Sleep(5000)
                CheckBox0.Checked = False
                lblWinSrvStatus.Text = servic.Status.ToString
            End If
        Next
        For i As Integer = 0 To 7
            Dim servic As ServiceController = New ServiceController(checkName(i))
            If servic.Status = ServiceControllerStatus.Stopped Then
                servic.Start()
                servic.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(5))
                System.Threading.Thread.Sleep(5000)
                CheckBox0.Checked = True
                lblWinSrvStatus.Text = servic.Status.ToString
            End If
        Next
        getStatus()
    End Sub

    Function getStatus()
        Dim checkList() As Integer = {0, 0, 0, 0, 0, 0, 0, 0}
        Dim checkName() As String = {"Ipcm", "Loadpay", "Netstarter", "Reportcron", "Sockserver", "Sockwrapper", "Starter", "Webstarter"}
        Dim checkStatus() As String = {"", "", "", "", "", "", "", ""}


        For i As Integer = 0 To 7
            Dim servic As ServiceController = New ServiceController(checkName(i))
            If Not servic Is Nothing Then
                If servic.Status = ServiceControllerStatus.Running Then
                    checkList(i) = 1
                ElseIf servic.Status = ServiceControllerStatus.Stopped Then
                    checkList(i) = 0
                End If
                checkStatus(i) = servic.Status.ToString
            End If
        Next
        If checkList(0) = 1 Then
            CheckBox0.Checked = True
        End If
        If checkList(1) = 1 Then
            CheckBox1.Checked = True
        End If
        If checkList(2) = 1 Then
            CheckBox2.Checked = True
        End If
        If checkList(3) = 1 Then
            CheckBox3.Checked = True
        End If
        If checkList(4) = 1 Then
            CheckBox4.Checked = True
        End If
        If checkList(5) = 1 Then
            CheckBox5.Checked = True
        End If
        If checkList(6) = 1 Then
            CheckBox6.Checked = True
        End If
        If checkList(7) = 1 Then
            CheckBox7.Checked = True
        End If

        lblWinSrvStatus.Text = checkStatus(0)
        Label2.Text = checkStatus(1)
        Label4.Text = checkStatus(2)
        Label6.Text = checkStatus(3)
        Label8.Text = checkStatus(4)
        Label10.Text = checkStatus(5)
        Label12.Text = checkStatus(6)
        Label14.Text = checkStatus(7)
    End Function
End Class