﻿Imports ClassLibraries
Imports ClassLibraries.Users

Public Class wfrmVisitReqDetail
    Inherits System.Web.UI.Page

    Public objUserInfo As New Users.UserInfo
    Public objVisitRequest As New Others.ReserveVisit
    Public Row As String
    Public u As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Session("VisitUserInfo") Is Nothing Then
                objUserInfo = Session("VisitUserInfo")
                Dim User_ID As String = objUserInfo.User_ID
                Dim objEwsUserClient As New ewsUserReg.IewsUserRegClient
                objUserInfo = objEwsUserClient.UserInfo(User_ID)
            End If
            objVisitRequest = Session("VisitRequest")
            Dim Lang As String = Request.QueryString("Lang")
            Dim objOthersClient As New ewsOthers.IewsOthersClient
            Dim Culture_Used As System.Globalization.CultureInfo = Nothing
            If Lang = "Ara" Then
                Culture_Used = New System.Globalization.CultureInfo("ar-AE")
                PageBody.Attributes.Remove("dir")
                PageBody.Attributes.Add("dir", "rtl")
                Row = "rowAra"
                u = "uAr"
            Else
                Row = "row"
                u = "u"
            End If
            Dim rc As System.Resources.ResourceManager = New System.Resources.ResourceManager("Resources.Strings", System.Reflection.Assembly.Load("App_GlobalResources"))
            lblFulName.Text = rc.GetString("FullName", Culture_Used)
            lblTitle.Text = rc.GetString("Title", Culture_Used)
            lblCompany.Text = rc.GetString("Company", Culture_Used)
            lblNat.Text = rc.GetString("Nationallity", Culture_Used)
            lblMobile.Text = rc.GetString("Mobile", Culture_Used)
            lblTele.Text = rc.GetString("Tele", Culture_Used)
            lblFax.Text = rc.GetString("Fax", Culture_Used)
            lblPostBox.Text = rc.GetString("POBox", Culture_Used)
            lblCity.Text = rc.GetString("City", Culture_Used)

            lblLibrary.Text = rc.GetString("Library", Culture_Used)
            lblDFrom.Text = rc.GetString("DateFrom", Culture_Used)
            lblDTo.Text = rc.GetString("DateTo", Culture_Used)
            lblPurpose.Text = rc.GetString("Purpose", Culture_Used)
            lblTFrom.Text = rc.GetString("TimeFrom", Culture_Used)
            lblTTo.Text = rc.GetString("TimeTo", Culture_Used)

            txtFullName.Text = objUserInfo.FullName
            txtTitle.Text = objUserInfo.JobTitle
            txtCompany.Text = objUserInfo.Employer
            txtNat.Text = objUserInfo.NationalID
            txtMobile.Text = objUserInfo.Mobile
            txtTele.Text = objUserInfo.HomePhone
            txtFax.Text = objUserInfo.Fax
            txtPostBox.Text = objUserInfo.POBox
            txtCity.Text = objUserInfo.City

            txtLibrary.Text = objVisitRequest.Library
            txtDFrom.Text = objVisitRequest.DateFrom
            txtDTo.Text = objVisitRequest.DateTo
            txtPurpose.Text = objVisitRequest.ReservPurpose
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Private Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            Dim objewsUserRegClient As New ewsUserReg.IewsUserRegClient
            Dim objEwsOtherClient As New ewsOthers.IewsOthersClient
            Dim result As String = ""
            result = objEwsOtherClient.UptReserveVisit(objVisitRequest.ID, objVisitRequest.User_ID, objVisitRequest.Library, objVisitRequest.ReservPurpose, objVisitRequest.DateFrom.ToString("dd/MM/yyyy:hh:mm:sstt"), objVisitRequest.DateTo.ToString("dd/MM/yyyy:hh:mm:sstt"), "2", "Admin", objVisitRequest.DateCreate.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
            If result = "1" Then
                objewsUserRegClient.InsUsersOPT(objUserInfo.User_ID, "0", "0", "0", "2", " ", "12", objUserInfo.User_ID, Now.ToString("dd/MM/yyyy:hh:mm:sstt"), Now.ToString("dd/MM/yyyy:hh:mm:sstt"))
                Response.Redirect("~/wfrmResulte.aspx?Title=Visit Reservation&Text=Approved Successfuly&Url=wfrmVisitReq.aspx")
            Else
                lblError.Text = result
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmVisitReq.aspx?Lang=Ara")
            Else
                Response.Redirect("~/wfrmVisitReq.aspx")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            Dim Lang As String = Request.QueryString("Lang")
            If Lang = "Ara" Then
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Visit&Lang=Ara")
            Else
                Response.Redirect("~/wfrmRejectReq.aspx?ReqType=Visit")
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class