﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="wfrmLogin.aspx.vb" Inherits="eServices.wfrmLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../Content/skel.css" />
    <link rel="stylesheet" href="../Content/style.css" />
    <link rel="stylesheet" href="../Content/style-xlarge.css" />
    <link rel="stylesheet" href="../Content/jquery.mobile-1.4.5.min.css" />
    <link rel="stylesheet" href="../Content/bootstrap.min.css" />
    <link rel="stylesheet" href="../Content/Site.css" />
    <link rel="stylesheet" href="../Content/ej/web/ej.widgets.core.min.css" />
    <link rel="stylesheet" href="../Content/ej/web/default-theme/ej.theme.min.css" />
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jsrender.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.web.all.min.js" type="text/javascript"></script>
    <script src="../Scripts/ej/ej.webform.min.js" type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/codemirror.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/javascript.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/htmlmixed.js' type="text/javascript"></script>
    <script src='../Scripts/CodeMirror/xml.js' type="text/javascript"></script>
    <link rel="stylesheet" href='../Scripts/CodeMirror/codemirror.min.css' />
    <script src='../Scripts/CodeMirror/css.js' type="text/javascript"></script>
    <style>
        .ModalPopupBG img {
            width: 50%;
        }
    </style>
</head>
<body id="PageBody" runat="server">
    <form runat="server" id="form1">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:HiddenField ID="HiddenField_UserID" runat="server" />
        <div style="width: 100%; height: 100%!important">
            <section>
                <div class="<%=Row%> uniform 50%">
                    <div class="4<%=u%> 12<%=u%>$(3)">
                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    </div>
                    <div class="4<%=u%>$ 12<%=u%>$(3)">
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        <asp:Label ID="lblValdEmail" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </div>
                    <div class="4<%=u%> 12<%=u%>$(3)">
                        <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                    </div>
                    <div class="4<%=u%>$ 12<%=u%>$(3)">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <a id="forgotpass" runat="server" href="http://10.1.1.28:8083/uhtbin/cgisirsi.exe/X/0/0/1/44/X" target="_parent" ><%=ChangePassword%></a>
                        <asp:Label ID="lblValdCPass" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </div>
                    <div class="12<%=u%>$">
                        <asp:Button ID="btnSubmit" runat="server" Text="Login" />
                    </div>
                     <div class="4<%=u%> 12<%=u%>$(3)">
                        <a id="newuserreg" runat="server" href="http://10.1.1.28:8083/uhtbin/cgisirsi.exe/X/0/0/1/45/X" target="_parent" ><%=NewUser%></a>
                    </div>
                    <div class="12<%=u%>$">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </section>
        </div>
    </form>
    <form action="http://<%= ServNam%><%= goto_url%>" method="post" name="loginform" target="_parent">
        <input type="hidden" name="user_id" id="user_id" value="<%=session("user_web")%>" />
        <input type="hidden" name="password" id="password" value="<%=session("Pass")%>" />
        <script>
            <% Session("user_web") = ""%>
            <% Session("Pass") = ""%>
            if (document.getElementById("HiddenField_UserID").value != "") {
                loginform.submit();
            }
        </script>
    </form>
</body>
</html>
